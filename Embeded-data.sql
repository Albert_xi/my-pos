/* menambahkan menu report report sale */
insert into kmj_routing (id,name,path,sequence,parent,icon,routing_group) values ('5d1bc0cb-6376-4757-be75-a10ab8f264ba','report_sale','report_sale_index', 6,null,null,'report');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('acb99f43-da8f-4f2d-a5b1-2547d64c5a23','5d1bc0cb-6376-4757-be75-a10ab8f264ba','ROLE_SUPER_USER','report_sale_index','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('cff135f6-7da1-4751-92ae-6b824f578829','5d1bc0cb-6376-4757-be75-a10ab8f264ba','ROLE_SUPER_USER','report_sale_print','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('33e6faf5-929f-454b-9ff0-df76874da738','5d1bc0cb-6376-4757-be75-a10ab8f264ba','ROLE_ADMINISTRATOR','report_sale_index','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('10c7ac74-988c-4cb7-80d1-3238ad9612a8','5d1bc0cb-6376-4757-be75-a10ab8f264ba','ROLE_ADMINISTRATOR','report_sale_print','t','2020-09-11 09:50:21');

insert into kmj_routing (id,name,path,sequence,parent,icon,routing_group) values ('5d1bc0cb-6376-4757-be75-a10ab8f264bb','report_purchase','report_purchase_index', 5,null,null,'report');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('acb99f43-da8f-4f2d-a5b1-2547d64c5a24','5d1bc0cb-6376-4757-be75-a10ab8f264bb','ROLE_SUPER_USER','report_sale_index','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('cff135f6-7da1-4751-92ae-6b824f578830','5d1bc0cb-6376-4757-be75-a10ab8f264bb','ROLE_SUPER_USER','report_sale_print','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('33e6faf5-929f-454b-9ff0-df76874da739','5d1bc0cb-6376-4757-be75-a10ab8f264bb','ROLE_ADMINISTRATOR','report_sale_index','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('10c7ac74-988c-4cb7-80d1-3238ad9612a9','5d1bc0cb-6376-4757-be75-a10ab8f264bb','ROLE_ADMINISTRATOR','report_sale_print','t','2020-09-11 09:50:21');

insert into kmj_routing (id,name,path,sequence,parent,icon,routing_group) values ('5d1bc0cb-6376-4757-be75-a10ab8f264bc','report_transaction','report_transaction_index', 7,null,null,'report');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('acb99f43-da8f-4f2d-a5b1-2547d64c5a25','5d1bc0cb-6376-4757-be75-a10ab8f264bc','ROLE_SUPER_USER','report_transaction_index','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('cff135f6-7da1-4751-92ae-6b824f578831','5d1bc0cb-6376-4757-be75-a10ab8f264bc','ROLE_SUPER_USER','report_transaction_print','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('33e6faf5-929f-454b-9ff0-df76874da740','5d1bc0cb-6376-4757-be75-a10ab8f264bc','ROLE_ADMINISTRATOR','report_transaction_index','t','2020-09-11 09:50:21');
insert into kmj_routing_role (id,routing_id,role,path,is_allowed,updated_at) values ('10c7ac74-988c-4cb7-80d1-3238ad9612a4','5d1bc0cb-6376-4757-be75-a10ab8f264bc','ROLE_ADMINISTRATOR','report_transaction_print','t','2020-09-11 09:50:21');

update kmj_routing set sequence = 1 where path ='store_index';
update kmj_routing set sequence = 1 where path ='packaging_index';
update kmj_routing set sequence = 1 where path ='report_profit_index';
update kmj_routing set sequence = 1 where path ='purchase_index';
update kmj_routing set sequence = 2 where path ='kmj_routing_index';
update kmj_routing set sequence = 3 where path ='kmj_user_index';
update kmj_routing set sequence = 4 where path ='kmj_printer_index';
update kmj_routing set sequence = 2 where path ='item_category_index';
update kmj_routing set sequence = 3 where path ='item_index';
update kmj_routing set sequence = 4 where path ='supplier_index';
update kmj_routing set sequence = 5 where path ='customer_index';
update kmj_routing set sequence = 2 where path ='sale_index';
update kmj_routing set sequence = 3 where path ='eod_index';
update kmj_routing set sequence = 2 where path ='report_profit_store_index';
update kmj_routing set sequence = 3 where path ='report_item_stock_index';
update kmj_routing set sequence = 4 where path ='report_unpaid_transaction_index';