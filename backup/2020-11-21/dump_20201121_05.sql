--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: backup_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.backup_log (
    id uuid NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    filename character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.backup_log OWNER TO postgres;

--
-- Name: COLUMN backup_log.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.backup_log.id IS '(DC2Type:uuid)';


--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer (
    id uuid NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    address text NOT NULL,
    phone character varying(255) DEFAULT NULL::character varying,
    mail character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- Name: COLUMN customer.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.customer.id IS '(DC2Type:uuid)';


--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO postgres;

--
-- Name: documentation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documentation (
    id uuid NOT NULL,
    topic character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    content text NOT NULL
);


ALTER TABLE public.documentation OWNER TO postgres;

--
-- Name: COLUMN documentation.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.documentation.id IS '(DC2Type:uuid)';


--
-- Name: item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item (
    id uuid NOT NULL,
    category_id uuid NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    use_barcode boolean NOT NULL,
    last_stock double precision NOT NULL,
    last_price double precision NOT NULL,
    harga_pokok double precision NOT NULL
);


ALTER TABLE public.item OWNER TO postgres;

--
-- Name: COLUMN item.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN item.category_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item.category_id IS '(DC2Type:uuid)';


--
-- Name: item_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item_category (
    id uuid NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.item_category OWNER TO postgres;

--
-- Name: COLUMN item_category.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item_category.id IS '(DC2Type:uuid)';


--
-- Name: item_package; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item_package (
    id uuid NOT NULL,
    item_id uuid NOT NULL,
    packaging_id uuid NOT NULL,
    is_smallest_unit boolean NOT NULL,
    quantity double precision NOT NULL,
    principal_price double precision NOT NULL,
    sale_price double precision NOT NULL
);


ALTER TABLE public.item_package OWNER TO postgres;

--
-- Name: COLUMN item_package.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item_package.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN item_package.item_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item_package.item_id IS '(DC2Type:uuid)';


--
-- Name: COLUMN item_package.packaging_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item_package.packaging_id IS '(DC2Type:uuid)';


--
-- Name: item_price_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item_price_log (
    id uuid NOT NULL,
    item_id uuid NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    principal_price_old double precision,
    principal_price double precision NOT NULL,
    sale_price_old double precision,
    sale_price double precision NOT NULL,
    status integer NOT NULL,
    inserted_by character varying(255) NOT NULL,
    approved_by character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.item_price_log OWNER TO postgres;

--
-- Name: COLUMN item_price_log.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item_price_log.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN item_price_log.item_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.item_price_log.item_id IS '(DC2Type:uuid)';


--
-- Name: kmj_printer_client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kmj_printer_client (
    id uuid NOT NULL,
    kmj_user_id uuid NOT NULL,
    computer_name character varying(255) NOT NULL,
    printer_name character varying(255) NOT NULL,
    is_default boolean
);


ALTER TABLE public.kmj_printer_client OWNER TO postgres;

--
-- Name: COLUMN kmj_printer_client.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_printer_client.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN kmj_printer_client.kmj_user_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_printer_client.kmj_user_id IS '(DC2Type:uuid)';


--
-- Name: kmj_routing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kmj_routing (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    sequence integer NOT NULL,
    parent uuid,
    icon character varying(255) DEFAULT NULL::character varying,
    routing_group character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.kmj_routing OWNER TO postgres;

--
-- Name: COLUMN kmj_routing.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_routing.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN kmj_routing.parent; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_routing.parent IS '(DC2Type:uuid)';


--
-- Name: kmj_routing_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kmj_routing_role (
    id uuid NOT NULL,
    routing_id uuid NOT NULL,
    role character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    is_allowed boolean NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.kmj_routing_role OWNER TO postgres;

--
-- Name: COLUMN kmj_routing_role.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_routing_role.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN kmj_routing_role.routing_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_routing_role.routing_id IS '(DC2Type:uuid)';


--
-- Name: kmj_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kmj_user (
    id uuid NOT NULL,
    store_id uuid,
    username character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    is_active boolean NOT NULL
);


ALTER TABLE public.kmj_user OWNER TO postgres;

--
-- Name: COLUMN kmj_user.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_user.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN kmj_user.store_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.kmj_user.store_id IS '(DC2Type:uuid)';


--
-- Name: packaging; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.packaging (
    id uuid NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.packaging OWNER TO postgres;

--
-- Name: COLUMN packaging.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.packaging.id IS '(DC2Type:uuid)';


--
-- Name: purchase; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.purchase (
    id uuid NOT NULL,
    supplier_id uuid,
    code character varying(255) NOT NULL,
    purchase_at date NOT NULL,
    total double precision NOT NULL,
    description text,
    is_locked boolean,
    checked_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.purchase OWNER TO postgres;

--
-- Name: COLUMN purchase.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.purchase.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN purchase.supplier_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.purchase.supplier_id IS '(DC2Type:uuid)';


--
-- Name: purchase_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.purchase_detail (
    id uuid NOT NULL,
    item_id uuid,
    purchase_id uuid NOT NULL,
    packaging_id uuid,
    quantity double precision NOT NULL,
    price double precision NOT NULL,
    total double precision NOT NULL,
    tax double precision NOT NULL
);


ALTER TABLE public.purchase_detail OWNER TO postgres;

--
-- Name: COLUMN purchase_detail.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.purchase_detail.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN purchase_detail.item_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.purchase_detail.item_id IS '(DC2Type:uuid)';


--
-- Name: COLUMN purchase_detail.purchase_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.purchase_detail.purchase_id IS '(DC2Type:uuid)';


--
-- Name: COLUMN purchase_detail.packaging_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.purchase_detail.packaging_id IS '(DC2Type:uuid)';


--
-- Name: sale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sale (
    id uuid NOT NULL,
    customer_id uuid,
    store_id uuid NOT NULL,
    code character varying(255) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    created_by character varying(255) NOT NULL,
    sub_total double precision NOT NULL,
    tax double precision NOT NULL,
    total double precision NOT NULL,
    is_locked boolean,
    discount double precision,
    payment double precision,
    payment_change double precision,
    checked_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.sale OWNER TO postgres;

--
-- Name: COLUMN sale.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.sale.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN sale.customer_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.sale.customer_id IS '(DC2Type:uuid)';


--
-- Name: COLUMN sale.store_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.sale.store_id IS '(DC2Type:uuid)';


--
-- Name: sale_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sale_item (
    id uuid NOT NULL,
    item_id uuid NOT NULL,
    sale_id uuid NOT NULL,
    quantity double precision NOT NULL,
    principal_price double precision NOT NULL,
    sale_price double precision NOT NULL,
    tax double precision NOT NULL,
    sub_total double precision NOT NULL,
    total double precision NOT NULL,
    discount double precision
);


ALTER TABLE public.sale_item OWNER TO postgres;

--
-- Name: COLUMN sale_item.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.sale_item.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN sale_item.item_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.sale_item.item_id IS '(DC2Type:uuid)';


--
-- Name: COLUMN sale_item.sale_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.sale_item.sale_id IS '(DC2Type:uuid)';


--
-- Name: serah_terima; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.serah_terima (
    id integer NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    total_debt double precision,
    total_credit double precision,
    description text,
    delegated_by character varying(255) NOT NULL,
    approved_by character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.serah_terima OWNER TO postgres;

--
-- Name: serah_terima_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.serah_terima_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.serah_terima_id_seq OWNER TO postgres;

--
-- Name: stock_card; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stock_card (
    id uuid NOT NULL,
    item_id uuid NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    class_name character varying(255) NOT NULL,
    class_id uuid NOT NULL,
    quantity double precision NOT NULL,
    type character varying(255) NOT NULL,
    total double precision NOT NULL
);


ALTER TABLE public.stock_card OWNER TO postgres;

--
-- Name: COLUMN stock_card.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.stock_card.id IS '(DC2Type:uuid)';


--
-- Name: COLUMN stock_card.item_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.stock_card.item_id IS '(DC2Type:uuid)';


--
-- Name: COLUMN stock_card.class_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.stock_card.class_id IS '(DC2Type:uuid)';


--
-- Name: store; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.store (
    id uuid NOT NULL,
    code character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    address text NOT NULL,
    is_default boolean NOT NULL
);


ALTER TABLE public.store OWNER TO postgres;

--
-- Name: COLUMN store.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.store.id IS '(DC2Type:uuid)';


--
-- Name: supplier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.supplier (
    id uuid NOT NULL,
    name character varying(255) NOT NULL,
    address text NOT NULL,
    phone character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.supplier OWNER TO postgres;

--
-- Name: COLUMN supplier.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.supplier.id IS '(DC2Type:uuid)';


--
-- Data for Name: backup_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.backup_log (id, created_at, filename, type) FROM stdin;
46c483a1-7b46-4ac5-8575-b7205d6bd5fb	2020-11-20 04:17:40	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-20\\dump_20201120_04.sql	backup
45150d13-a983-4e7b-b17d-32957858a0ad	2020-11-20 04:28:46	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-20\\dump_20201120_04.sql	backup
ee3b6ccb-6529-4600-a0cb-cf54b834a393	2020-11-20 04:33:58	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-20\\dump_20201120_04.sql	backup
20b49cb0-7047-4f41-b18d-0865c831aae3	2020-11-20 04:34:03	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-20\\dump_20201120_04.sql	backup
a5c97d49-b1d4-44b8-8dfc-6336a74e07b9	2020-11-21 03:53:33	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-21\\dump_20201121_03.sql	backup
f330ccc4-92a6-4ade-bed7-14918bc2aaae	2020-11-21 03:55:44	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-21\\dump_20201121_03.sql	backup
26e0fc05-8938-43f9-8569-07f6d9d2b02b	2020-11-21 04:09:39	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-21\\dump_20201121_04.sql	backup
8fe1a347-86f3-47ca-8dd0-a5b8db47f9e9	2020-11-21 04:11:19	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-21\\dump_20201121_04.sql	backup
b2343c2e-6271-4f46-9caf-2b9918306f97	2020-11-21 04:16:18	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-21\\dump_20201121_04.sql	backup
64fdcbf3-1b0b-406f-a5a6-265be0365034	2020-11-21 04:19:31	C:\\xampp\\htdocs\\my-pos\\backup\\2020-11-21\\dump_20201121_04.sql	backup
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer (id, code, name, address, phone, mail) FROM stdin;
9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	6	umum	-	-	tes@gmail.com
a5bf898e-ade5-4b48-b6eb-e1dc142eb36a	3	MI	-	-	tes@gmail.com
87f2ba43-0e55-4c31-bed7-9872dd4f9474	1	YAYASAN	-	-	tes@gmail.com
eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	4	SMP	-	-	tes@gmail.com
e82d3076-0885-4f2c-b584-b07ed1bf9a56	2	RA	-	-	tes@gmail.com
6ce67b8c-e974-49ce-800d-334068591f48	5	SMK	-	-	tes@gmail.com
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
App\\Migrations\\Version20200718224956	2020-07-28 04:28:18	462
\.


--
-- Data for Name: documentation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documentation (id, topic, title, created_at, content) FROM stdin;
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.item (id, category_id, code, name, use_barcode, last_stock, last_price, harga_pokok) FROM stdin;
6dd6f6f6-edd1-414d-ac78-50a8acea0b58	a52cacc0-bcae-4f10-91d5-6bf047663766	2	Faster C6	f	35	3000	2100
57e79c6d-774b-4c52-818b-d25c1047596b	a52cacc0-bcae-4f10-91d5-6bf047663766	10	Pilot Biru	f	6	2500	1750
725b6572-b2bf-4f8e-b7b9-d9fea4688769	a52cacc0-bcae-4f10-91d5-6bf047663766	13	Standart Biru	f	61	2000	1400
4c303161-3363-4793-b4a8-fd55c576b0db	a52cacc0-bcae-4f10-91d5-6bf047663766	15	Isi Pensil Besar	f	81	3000	2100
61a83d25-fcae-4be9-af02-430b5fef4fd3	a52cacc0-bcae-4f10-91d5-6bf047663766	16	Isi Pensil Kecil	f	22	2500	1750
32ec6310-c7ce-4b9e-9dc6-31e3f0375e83	a52cacc0-bcae-4f10-91d5-6bf047663766	17	Penghapus Stadler	f	40	3000	2100
e24420db-e080-476a-9c82-415bb96c6424	a52cacc0-bcae-4f10-91d5-6bf047663766	18	Penghapus Kecil	f	141	1000	700
3a68b52e-cdf6-48b4-bcdc-deacfcee19a4	a52cacc0-bcae-4f10-91d5-6bf047663766	19	Penghapus Besar	f	76	3000	2100
f235a2c6-6d5a-4145-ab3d-eec6b3362f4a	a52cacc0-bcae-4f10-91d5-6bf047663766	20	Penghapus Panjang	f	79	2000	1400
403ab72a-ea6a-46bb-bf98-e0ae68171056	a52cacc0-bcae-4f10-91d5-6bf047663766	22	Pensil Faber Castell	f	67	4000	2800
76d866fa-cb74-4222-a3b0-841d81e751d6	a52cacc0-bcae-4f10-91d5-6bf047663766	23	Pensil Stadler	f	73	4000	2800
89d701e7-8192-40f7-90a5-38a53e80f011	a52cacc0-bcae-4f10-91d5-6bf047663766	25	Spidol Biru Kecil	f	103	1500	1050
88fbaecf-b5a0-4b77-8a2b-b5ac5bc0ee00	a52cacc0-bcae-4f10-91d5-6bf047663766	30	Spidol Hitam Permanent 	f	14	12000	8400
fcf6bb53-4f1f-4cb9-a236-ce551f0ca2fd	a52cacc0-bcae-4f10-91d5-6bf047663766	31	Spidol Biru Permanent 	f	12	7000	4900
c95a9a40-14a0-4a12-98c6-0fab2934dfcd	a52cacc0-bcae-4f10-91d5-6bf047663766	32	Buku Gambar A3 Sidu	f	36	8000	5600
c8ce88f6-2775-4df1-b252-c19a6a0668fc	a52cacc0-bcae-4f10-91d5-6bf047663766	34	Buku Gambar A4 Dodo	f	46	3000	2100
b6deec2a-3487-4a1c-b2f7-b81601aee746	a52cacc0-bcae-4f10-91d5-6bf047663766	35	Mika Putih	f	76	500	350
c2a97a85-4960-4364-9d96-81d420213907	a52cacc0-bcae-4f10-91d5-6bf047663766	36	Mika Orange	f	30	500	350
c3612489-8939-478a-b0d7-f25c933690a2	a52cacc0-bcae-4f10-91d5-6bf047663766	37	Mika Ungu	f	41	500	350
ea609d01-1d90-4e3b-b6ef-cc6527a39dd7	a52cacc0-bcae-4f10-91d5-6bf047663766	38	Mika Merah	f	59	500	350
3ed61c2b-0b1a-4457-bdb1-bc640a852049	a52cacc0-bcae-4f10-91d5-6bf047663766	39	HVS Kuning	f	39	1000	700
b9a709f5-98c7-4b4b-a5a7-6d97c92f790c	a52cacc0-bcae-4f10-91d5-6bf047663766	40	HVS Putih	f	59	1000	700
0e22f6c4-2b6d-41c9-9a8d-4ec26da4dfae	a52cacc0-bcae-4f10-91d5-6bf047663766	41	Bufallo Pink	f	101	500	350
91d6df82-3afc-4883-9ad2-308a6c3d7417	a52cacc0-bcae-4f10-91d5-6bf047663766	42	Bufallo Biru Tua	f	172	500	350
1c3393ab-dad0-48cd-a0bc-9e768663c241	a52cacc0-bcae-4f10-91d5-6bf047663766	43	Bufallo Biru Muda	f	100	500	350
1849484a-75fe-4472-b1a7-0b4749f620da	a52cacc0-bcae-4f10-91d5-6bf047663766	44	Bufallo Merah	f	39	500	350
e234053f-313a-4c98-a955-8c8751a13e0c	a52cacc0-bcae-4f10-91d5-6bf047663766	45	Bufallo Hijau Tua 	f	60	500	350
ee6d6a70-b231-4b28-80a7-b80478cc551f	a52cacc0-bcae-4f10-91d5-6bf047663766	46	Bufallo Pink 	f	1	34000	23800
8d7481db-1046-4c54-a9a7-08a385b66de5	a52cacc0-bcae-4f10-91d5-6bf047663766	47	Bufallo Abu-abu	f	2	34000	23800
d6b08104-18be-41fe-afa6-23f2a9857baf	a52cacc0-bcae-4f10-91d5-6bf047663766	48	Bufallo Peach	f	1	34000	23800
60334281-b0f8-4957-b0fe-d0aa4c8cc627	a52cacc0-bcae-4f10-91d5-6bf047663766	49	Bufallo Biru Tua	f	1	34000	23800
2ef7a649-59fa-44a7-9c08-12d6daf07e67	a52cacc0-bcae-4f10-91d5-6bf047663766	50	Bufallo Ungu Tua	f	1	34000	23800
93dd574b-a214-4929-967b-32bec100b410	a52cacc0-bcae-4f10-91d5-6bf047663766	51	Bufallo Coklat 	f	2	34000	23800
4712b156-b046-4da1-b746-96cd9aea237f	a52cacc0-bcae-4f10-91d5-6bf047663766	52	Sampul Tabungan 	f	748	2000	1400
10b3916f-daa6-4fe2-b7b7-fce8a7f5210a	a52cacc0-bcae-4f10-91d5-6bf047663766	53	Sampul SPP	f	764	3000	2100
357accc3-4a7d-401f-b8c6-0137f83e1d0f	a52cacc0-bcae-4f10-91d5-6bf047663766	54	Sampul LKS Tipis	f	237	500	350
d47953eb-2ff7-4b7b-911c-329a474b50c8	a52cacc0-bcae-4f10-91d5-6bf047663766	55	Sampul LKS Tebal	f	83	1000	700
6ca549d4-66c0-4ddf-a9dc-9b59cdcafb82	a52cacc0-bcae-4f10-91d5-6bf047663766	56	Sampul Plastik Buku Tulis	f	493	500	350
a407173b-2320-4e56-ac23-46b42059b2a6	a52cacc0-bcae-4f10-91d5-6bf047663766	57	Sampul Coklat Buku Tulis	f	412	500	350
21758598-fafd-4cd4-b115-4ae8e87a8238	a52cacc0-bcae-4f10-91d5-6bf047663766	60	Amplop Coklat E308	f	21	1500	1050
764b47b7-a652-4dd3-9a9e-39ba3098cac5	a52cacc0-bcae-4f10-91d5-6bf047663766	61	Amplop Coklat E304	f	15	2000	1400
a81c8f88-9d55-4f50-866c-602e5bc81bcd	a52cacc0-bcae-4f10-91d5-6bf047663766	63	Gunting Kecil	f	19	5000	3500
f803d84f-019f-4b6e-ab21-c3a7c739453c	a52cacc0-bcae-4f10-91d5-6bf047663766	64	Gunting Besar	f	2	7000	4900
e9e1805f-ce4d-47fb-a69c-131b763c923e	a52cacc0-bcae-4f10-91d5-6bf047663766	65	Solasi Bening 12 MM	f	11	3000	2100
06975a24-684a-4992-a691-0207ea762f97	a52cacc0-bcae-4f10-91d5-6bf047663766	66	Solasi Bening 24 MM	f	8	5000	3500
42508efe-236e-49a0-b9ba-dc6eea1d09ed	a52cacc0-bcae-4f10-91d5-6bf047663766	69	Lakban Coklat	f	6	15000	10500
f3ff2c87-7d65-41a8-9d3a-dd08c9d1eec1	a52cacc0-bcae-4f10-91d5-6bf047663766	74	Cutter Besar	f	12	5000	3500
6069d5c1-b82f-4bc9-8a10-b15d8b4cacc8	a52cacc0-bcae-4f10-91d5-6bf047663766	75	Cutter Kecil 	f	8	2500	1750
40805e7d-9a61-4d08-aee6-8b865c526880	a52cacc0-bcae-4f10-91d5-6bf047663766	76	Isi Staples Kecil 	f	39	2500	1750
e662ecc3-8c27-44e7-92a1-ff1e7b277ac4	a52cacc0-bcae-4f10-91d5-6bf047663766	73	Double Tipe 24 MM	f	8	7000	4900
8a554019-4e83-458a-83c0-20fb0cd842d3	a52cacc0-bcae-4f10-91d5-6bf047663766	28	Board Marker Biru	f	1	7000	4900
8a758252-d9fd-4d16-8b56-a94382216abd	a52cacc0-bcae-4f10-91d5-6bf047663766	33	Buku Gambar A4 Sidu	f	18	3500	2450
0710bf62-a460-477c-a8f9-b9386d9b457f	a52cacc0-bcae-4f10-91d5-6bf047663766	58	Tissue Kecil	f	55	3000	2100
0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	a52cacc0-bcae-4f10-91d5-6bf047663766	12	Standart Merah	f	22	2000	1400
171fabf2-d96a-4423-8ee2-9626efbbd16e	a52cacc0-bcae-4f10-91d5-6bf047663766	70	Lakban Hitam Kecil	f	9	13000	9100
d31957df-bf1f-45bf-a931-39635432e566	a52cacc0-bcae-4f10-91d5-6bf047663766	8	Snowman V1	f	71	2500	1750
b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	a52cacc0-bcae-4f10-91d5-6bf047663766	11	Bulpoint Jel	f	248	3000	2100
10327621-aaad-460d-b944-daa8c8a9a927	a52cacc0-bcae-4f10-91d5-6bf047663766	24	Spidol Hitam Kecil	f	218	1500	1050
aa8051af-fcf7-43ea-9449-c61ab4597017	a52cacc0-bcae-4f10-91d5-6bf047663766	7	Snowman V5	f	41	3000	2100
b67980a9-de04-4973-b7dc-bc61242c8f38	a52cacc0-bcae-4f10-91d5-6bf047663766	9	Pilot Hitam	f	166	2500	1750
06a0cade-0e05-456a-b408-ba1842c95f18	a52cacc0-bcae-4f10-91d5-6bf047663766	6	Queens	f	122	2000	1400
d234256e-925c-4d1b-8426-c843d9f395aa	a52cacc0-bcae-4f10-91d5-6bf047663766	68	Solasi Kecil 	f	53	1000	700
3a9a6286-20b7-42e8-903f-0a64cc5ec639	a52cacc0-bcae-4f10-91d5-6bf047663766	72	Double Tipe 12 MM	f	3	3000	2100
2aeb8827-643d-4b9d-885a-577ad3de919c	a52cacc0-bcae-4f10-91d5-6bf047663766	62	Buku Kwarto Kecil 100	f	3	10000	7000
3178a471-6e02-4db9-a506-602818e6447f	a52cacc0-bcae-4f10-91d5-6bf047663766	27	Board Marker Hitam	f	22	7000	4900
0c5cc742-e4c9-4e54-8c49-a9fcda2cf45a	a52cacc0-bcae-4f10-91d5-6bf047663766	67	Solasi Bening Besar 	f	18	13000	9100
5c9e60e8-2825-4e46-945b-74e8b1a73fc9	a52cacc0-bcae-4f10-91d5-6bf047663766	108	Buku Tabungan	f	725	3000	2100
17a6b989-7718-4487-94a3-651d019a5a4a	a52cacc0-bcae-4f10-91d5-6bf047663766	116	Stipo Kertas	f	44	5500	3850
e20071bd-1488-4e16-8b65-f9ba74f5c0e5	a52cacc0-bcae-4f10-91d5-6bf047663766	84	Jangka	f	14	3000	2100
eb82db1e-8ca7-4145-a22c-256eca2a6241	a52cacc0-bcae-4f10-91d5-6bf047663766	82	Baterai ABC AA	f	45	2500	1750
3bbdb71a-0bf1-490b-98df-78e140c73f3c	a52cacc0-bcae-4f10-91d5-6bf047663766	77	Isi Staples Besar	f	4	5000	3500
61814f08-830c-4af5-b767-77ede47326e7	a52cacc0-bcae-4f10-91d5-6bf047663766	78	Permes	f	11	1000	700
fe638b99-f4d3-4603-b98f-f3127d365f4c	a52cacc0-bcae-4f10-91d5-6bf047663766	79	Pembatas Buku	f	0	8000	5600
64821565-0d87-4bba-b303-1af38bd69d41	a52cacc0-bcae-4f10-91d5-6bf047663766	81	Orotan Karakter	f	17	2500	1750
4e1b850b-1c2b-4b0b-9ce6-9017befed503	a52cacc0-bcae-4f10-91d5-6bf047663766	83	Baterai Mix	f	3	15000	10500
5fd4844e-43b5-46ec-b171-63ac0ccfc4cf	a52cacc0-bcae-4f10-91d5-6bf047663766	87	Lem Povinal Kecil 	f	49	2500	1750
9e001f35-cb8d-4e99-9a05-6517a877c7fe	a52cacc0-bcae-4f10-91d5-6bf047663766	88	Lem Povinal Besar	f	3	6000	4200
63dfb9c8-5415-49cd-b409-26518474dc4a	a52cacc0-bcae-4f10-91d5-6bf047663766	89	Lem Stik 	f	36	3500	2450
faffad1d-fecf-4ed2-b1fa-4d14f2a532a6	a52cacc0-bcae-4f10-91d5-6bf047663766	92	Clip 105	f	9	4000	2800
ebc6fcd6-373a-4a52-9f80-d2d720cccd63	a52cacc0-bcae-4f10-91d5-6bf047663766	94	Clip 111	f	9	6500	4550
5960fad5-5469-407a-85ce-79923e6b4f9d	a52cacc0-bcae-4f10-91d5-6bf047663766	97	Tinta Spidol Biru	f	3	16000	11200
661f2a06-8bb1-4538-b77e-6cc18912362d	a52cacc0-bcae-4f10-91d5-6bf047663766	98	Tinta Spidol Merah	f	2	16000	11200
e871d1a2-7132-408d-ba6e-3538d4f7cc88	a52cacc0-bcae-4f10-91d5-6bf047663766	99	Tinta Spidol Hitam	f	0	16000	11200
df7b1006-92c2-4877-babb-bec571055415	a52cacc0-bcae-4f10-91d5-6bf047663766	100	Tinta Stempel Hitam 	f	4	12500	8750
c22ea675-f597-487e-b813-efa3a5d1aadd	a52cacc0-bcae-4f10-91d5-6bf047663766	101	Tinta Stempel Ungu	f	0	12500	8750
d0cb5263-65eb-4888-b248-597e7b621536	a52cacc0-bcae-4f10-91d5-6bf047663766	102	Tinta Stempel Biru	f	0	12500	8750
4aa57910-561e-4517-b844-5f521aacfda1	a52cacc0-bcae-4f10-91d5-6bf047663766	103	Penghapus Papan Tulis	f	5	4000	2800
6a750749-ec0c-410a-873a-010d8e6b59a5	a52cacc0-bcae-4f10-91d5-6bf047663766	104	Bak Stempel 300	f	2	11000	7700
43ea7a2f-d4c3-478d-b213-f26915e26b18	a52cacc0-bcae-4f10-91d5-6bf047663766	105	Bak Stempel 400	f	1	15000	10500
0971c46a-011f-47a4-9860-98b38a53296c	a52cacc0-bcae-4f10-91d5-6bf047663766	106	Staples Kecil	f	2	16000	11200
25bef35a-3a0f-4643-abae-7fd3e9304647	a52cacc0-bcae-4f10-91d5-6bf047663766	107	Staples Besar	f	5	20000	14000
8d7bf82b-f55a-4cb7-8f5a-571b29983059	a52cacc0-bcae-4f10-91d5-6bf047663766	111	Buku Tulis Bigbos	f	56	5000	3500
9405b93e-e94e-47c8-9585-a11fcf382014	a52cacc0-bcae-4f10-91d5-6bf047663766	112	Buku Tulis Kotak	f	17	3000	2100
caeb9e19-b9f1-42af-acf1-a219144da6ef	a52cacc0-bcae-4f10-91d5-6bf047663766	113	Buku Tulis Tegak Bersambung	f	7	3000	2100
256e269a-a0c1-4a1d-a144-b4fe1b0f8451	a52cacc0-bcae-4f10-91d5-6bf047663766	117	Pensil Isi Kecil	f	77	2500	1750
d51d966d-a7c7-4db4-9613-0ffcd5e3061a	a52cacc0-bcae-4f10-91d5-6bf047663766	118	Pensil Isi  Besar	f	142	3000	2100
c24d23ce-5bff-460a-859c-4c04bd1143a5	a52cacc0-bcae-4f10-91d5-6bf047663766	119	Isi Bolpoint 	f	3	1000	700
106c07fd-b074-4a79-b2df-9e1b6cf04e44	a52cacc0-bcae-4f10-91d5-6bf047663766	120	Bola Pimpong 	f	47	3000	2100
09cfaf4d-011f-4a61-acc3-f768c131aabe	a52cacc0-bcae-4f10-91d5-6bf047663766	121	Buku SKU Siaga	f	27	2000	1400
9ae4e8cd-164e-4d00-b263-61f264c95279	a52cacc0-bcae-4f10-91d5-6bf047663766	122	Buku SKU Penggalang	f	21	2000	1400
37e21335-7f61-4d38-bd49-e89233dfcae1	a52cacc0-bcae-4f10-91d5-6bf047663766	86	Stabilo 	f	20	3500	2450
805f19d0-7aff-4694-818a-3e5165d146ef	a52cacc0-bcae-4f10-91d5-6bf047663766	109	Buku Tulis 38	f	54	3000	2100
1fc50d3b-148a-4492-b499-d675c202d459	a52cacc0-bcae-4f10-91d5-6bf047663766	95	Clip 155	f	3	9000	6300
c280399a-dd0f-487a-93c2-93d631d82782	a52cacc0-bcae-4f10-91d5-6bf047663766	85	Materai 6000	f	84	7000	6000
230e0a48-0047-45e7-a253-601ba845011e	a52cacc0-bcae-4f10-91d5-6bf047663766	96	Clip Kertas	f	0	3000	2100
689391ef-c406-4d68-accf-2cdf8e3f2c82	a52cacc0-bcae-4f10-91d5-6bf047663766	80	Orotan 	f	87	1000	700
14397549-e408-4470-979a-8e794351086e	a52cacc0-bcae-4f10-91d5-6bf047663766	91	Penggaris Butterfly	f	146	2500	1750
60f0ecfe-0c71-4b43-a002-de6e2994e076	a52cacc0-bcae-4f10-91d5-6bf047663766	90	Penggaris Lipat	f	24	3000	2100
a710f31b-9840-46a3-a903-0bed2ad13807	a52cacc0-bcae-4f10-91d5-6bf047663766	115	Stipo Cair	f	55	5500	3850
5770e029-af52-4630-840c-0703653586ce	a52cacc0-bcae-4f10-91d5-6bf047663766	93	Clip 107	f	3	4500	3150
2a3db456-1c32-4bbe-bcaa-e6f303c40401	a52cacc0-bcae-4f10-91d5-6bf047663766	114	Folio Bergaris	f	195	500	350
9f9c516a-b6f1-449c-8663-9cbedc6ca0a5	a52cacc0-bcae-4f10-91d5-6bf047663766	110	Buku Tulis 58	f	35	5000	3500
25c1a510-67bd-484a-acff-8b2a4dc984bb	a52cacc0-bcae-4f10-91d5-6bf047663766	123	Kertas Foto	f	15	2000	1400
35230a29-9129-4631-a55e-a8b82b9ad37f	a52cacc0-bcae-4f10-91d5-6bf047663766	125	Map Snail Putih	f	24	2500	1750
8c95b730-638f-40d4-aaed-f7396c709c41	a52cacc0-bcae-4f10-91d5-6bf047663766	126	Map Snail Hijau	f	67	2500	1750
d3d6686d-e21f-423c-9901-ba7ebbb6f63d	a52cacc0-bcae-4f10-91d5-6bf047663766	129	Map Snail Orange	f	1	2500	1750
9711c5e4-adf4-48c4-a338-7d0a6fb9778e	a52cacc0-bcae-4f10-91d5-6bf047663766	130	Map Snail Hitam	f	31	2500	1750
3b3dcf22-79ee-4494-890b-004d1bf59b1a	a52cacc0-bcae-4f10-91d5-6bf047663766	131	Map Snail Hijau Tosca	f	24	2500	1750
3f5ee67a-2484-4147-a3fb-40c8136973d9	a52cacc0-bcae-4f10-91d5-6bf047663766	132	Map Snail ungu	f	8	2500	1750
030f7848-6562-40dd-8d40-4b303c724960	a52cacc0-bcae-4f10-91d5-6bf047663766	134	Map Kertas Kuning	f	44	1000	700
187944fa-73b8-41ae-a9a8-6245a9724e3a	a52cacc0-bcae-4f10-91d5-6bf047663766	136	Map Kertas Biru 	f	0	1000	700
1d3cbc5f-cd61-44ac-826e-0f9b1c3ce796	a52cacc0-bcae-4f10-91d5-6bf047663766	138	Map Kancing Hijau	f	8	7500	5250
c6b4ee0e-8c02-4c11-a7e0-bf335e341a9d	a52cacc0-bcae-4f10-91d5-6bf047663766	139	Map Kancing Kuning	f	8	7500	5250
611e30cb-aa51-4278-b358-c5488d01911f	a52cacc0-bcae-4f10-91d5-6bf047663766	140	Map Kancing Merah	f	24	7500	5250
673997df-fe73-4fe9-aa0a-d2cc10f4fa28	a52cacc0-bcae-4f10-91d5-6bf047663766	141	Map Kancing Putih	f	12	7500	5250
599ffecd-03b5-4c54-ba14-d9f6f67f77ca	a52cacc0-bcae-4f10-91d5-6bf047663766	142	Map Kancing Biru	f	12	7500	5250
950099cb-14a5-4d2f-a6c7-5733b905d18e	a52cacc0-bcae-4f10-91d5-6bf047663766	143	Map L Merah	f	8	2500	1750
b2e3a8a0-21a9-4cf5-a798-f00a058d421d	a52cacc0-bcae-4f10-91d5-6bf047663766	144	Map L Biru	f	12	2500	1750
483637cd-fe09-4ca1-b310-2a5ad8ae2658	a52cacc0-bcae-4f10-91d5-6bf047663766	145	Map L Hijau	f	12	2500	1750
3e01c670-c774-496f-9cbc-1816f8627726	a52cacc0-bcae-4f10-91d5-6bf047663766	147	Map L Kuning	f	12	2500	1750
98fac7bf-4029-44c2-a9b4-17f85c9b9d45	a52cacc0-bcae-4f10-91d5-6bf047663766	149	Map Plong	f	16	7000	4900
d98e765b-7e7d-4f49-895d-960b4f71263e	a52cacc0-bcae-4f10-91d5-6bf047663766	150	Map Soal Sedang 	f	7	47000	32900
76308db9-8ec1-4801-b886-8286571e881c	a52cacc0-bcae-4f10-91d5-6bf047663766	151	Map Soal Kecil 	f	1	25000	17500
5f6a0c31-1042-4f2d-a14b-002722fd9187	a52cacc0-bcae-4f10-91d5-6bf047663766	153	Amplop Besar	f	17	23000	16100
0e770aef-8872-488e-b6e7-9c3b37cc8525	a52cacc0-bcae-4f10-91d5-6bf047663766	154	Amplop Coklat Polos	f	2	16000	11200
a3d035d2-1dd2-41f5-afa3-d9e87decc09e	a52cacc0-bcae-4f10-91d5-6bf047663766	158	Mika Putih 	f	1	36000	25200
79887006-0bd0-4f88-a523-8258dba47134	a52cacc0-bcae-4f10-91d5-6bf047663766	160	Kertas Karton 	f	1	3000	2100
3c1847cd-b942-42c8-a272-bb248571c1de	a52cacc0-bcae-4f10-91d5-6bf047663766	161	Kok	f	14	3000	2100
9c097a47-cb6a-4e4c-911e-505e8de59227	a52cacc0-bcae-4f10-91d5-6bf047663766	164	Kertas F4 Hijau	f	4	75500	52850
2273a601-eff0-4ed2-8e4a-44dbfe6b8c11	a52cacc0-bcae-4f10-91d5-6bf047663766	165	Kertas F4 Kuning	f	4	75500	52850
04636f4c-c6a8-4817-8bc5-88e5b0b3621d	a52cacc0-bcae-4f10-91d5-6bf047663766	166	Kertas F4 Biru	f	5	75500	52850
00ebaa3a-ba04-45c3-8ae2-901f5b37a51d	a52cacc0-bcae-4f10-91d5-6bf047663766	167	Kertas F4 Pink 	f	3	75500	52850
ae6cdc50-feae-4dd1-b238-89b6f10cf980	a52cacc0-bcae-4f10-91d5-6bf047663766	168	Penggaris Besi	f	12	3500	2500
581f08a6-453c-4993-8d74-638ebccc1cf3	a52cacc0-bcae-4f10-91d5-6bf047663766	169	Buku Kwarto Mini 100	f	5	5000	3500
356d1f9b-943a-401b-8eeb-ec147264f25a	a52cacc0-bcae-4f10-91d5-6bf047663766	170	Bufallo Putih 	f	5	34000	23800
80a25700-e910-4409-83db-6021009723d2	a52cacc0-bcae-4f10-91d5-6bf047663766	171	Bufallo Orange	f	3	34000	23800
6c999ab5-1af9-41f3-b2f7-cd1d6bc186f7	a52cacc0-bcae-4f10-91d5-6bf047663766	157	Box File	f	8	19000	13300
c2f5f050-3dd7-4862-9b67-e72cb4aa9c57	a52cacc0-bcae-4f10-91d5-6bf047663766	174	Outner Besar	f	9	25500	17850
77ea7188-2457-4523-999f-a1812416c570	a52cacc0-bcae-4f10-91d5-6bf047663766	26	Spidol Merah Kecil	f	194	1500	1050
43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	a52cacc0-bcae-4f10-91d5-6bf047663766	128	Map Snail Biru	f	23	2500	1750
f54c7cdd-d5d5-4237-acbf-b0d87fecd5e9	a52cacc0-bcae-4f10-91d5-6bf047663766	5	Youmei	f	125	2000	1400
f9d4e754-8efe-43ff-bb7a-cad985bed182	a52cacc0-bcae-4f10-91d5-6bf047663766	163	Kertas F4 Putih	f	13	58500	40950
4a478acc-84aa-4340-9839-32ba0505a2d8	a52cacc0-bcae-4f10-91d5-6bf047663766	162	Kertas A4	f	-2	54500	38150
4ca13666-5ad0-4357-9355-6be3323214fa	a52cacc0-bcae-4f10-91d5-6bf047663766	127	Map Snail Kuning	f	44	2500	1750
dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	a52cacc0-bcae-4f10-91d5-6bf047663766	156	Outner Besar	f	-4	25500	17850
b58e0a50-2e90-4527-9aab-812dbdcc7f02	a52cacc0-bcae-4f10-91d5-6bf047663766	59	Tissue Besar	f	18	10000	7000
b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	a52cacc0-bcae-4f10-91d5-6bf047663766	14	Standart Hitam	f	140	2000	1400
3d031565-23f8-431c-a867-bbf3a17a69da	a52cacc0-bcae-4f10-91d5-6bf047663766	135	Map Kertas Hijau	f	1	1000	700
2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	a52cacc0-bcae-4f10-91d5-6bf047663766	21	Pensil Joyko	f	197	2000	1400
7df4ed86-c958-41e0-96ef-c2059d9ebae4	a52cacc0-bcae-4f10-91d5-6bf047663766	71	Lakban Hitam Besar	f	10	15000	10500
788a1deb-c1d9-449d-b07e-d800e4feaab3	a52cacc0-bcae-4f10-91d5-6bf047663766	1	Faster F3	f	41	2500	1750
914edc82-ac6f-47e2-8a37-4693f562e586	a52cacc0-bcae-4f10-91d5-6bf047663766	124	Map Snail Merah	f	16	2500	1750
ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	a52cacc0-bcae-4f10-91d5-6bf047663766	4	Iphen	f	118	2000	1400
2dbfef19-f0b5-4fa6-903c-67b818864e6a	a52cacc0-bcae-4f10-91d5-6bf047663766	152	Amplop Kecil	f	14	13500	9450
9737c64e-cda0-4e10-9c0d-de9e94bc4a51	a52cacc0-bcae-4f10-91d5-6bf047663766	159	Kertas Kado	f	7	1500	1050
d566b1d8-efbd-4a86-9943-9514d141175d	a52cacc0-bcae-4f10-91d5-6bf047663766	29	Board Marker Merah 	f	6	7000	4900
61a18df1-774f-49b6-8f49-7cb2682b3fd6	a52cacc0-bcae-4f10-91d5-6bf047663766	137	Map Tas	f	7	10000	7000
9e9e5f57-a956-470f-8b8c-a3d6f29a9894	a52cacc0-bcae-4f10-91d5-6bf047663766	148	Map Jepret	f	5	7000	4900
78561982-b3fa-45d6-94c6-3b99b397c756	a52cacc0-bcae-4f10-91d5-6bf047663766	133	Map Kertas Pink	f	19	1000	700
5556e2f3-a8c0-441d-b696-07ea76048743	a52cacc0-bcae-4f10-91d5-6bf047663766	146	Map L Putih	f	6	2500	1750
ce2cad02-0a94-4138-bd11-fae5f15376b1	a52cacc0-bcae-4f10-91d5-6bf047663766	155	Outner Kecil	f	5	24500	17150
6e780ad3-48b9-4cae-9faf-e39cfb3c4176	a52cacc0-bcae-4f10-91d5-6bf047663766	3	Iphen XS	f	96	2000	1400
9668ff45-83d7-466d-a266-9c4d54eb2993	a52cacc0-bcae-4f10-91d5-6bf047663766	173	Kertas A4 	f	7	54500	38150
c4ece0d9-bb42-4240-bff0-7adf92f4b93c	a52cacc0-bcae-4f10-91d5-6bf047663766	172	Bufallo Kuning 	f	1	34000	23800
\.


--
-- Data for Name: item_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.item_category (id, code, name, is_active) FROM stdin;
a52cacc0-bcae-4f10-91d5-6bf047663766	001	ATK	t
ce3c6b1e-5577-4a0f-8bb7-bc9348a9bbe2	002	ATRIBUT	t
\.


--
-- Data for Name: item_package; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.item_package (id, item_id, packaging_id, is_smallest_unit, quantity, principal_price, sale_price) FROM stdin;
8e5a4b7f-f2e0-4053-84b0-d33fcabef562	788a1deb-c1d9-449d-b07e-d800e4feaab3	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
326cfe29-2176-41ca-a5fb-f67936e10eb2	6dd6f6f6-edd1-414d-ac78-50a8acea0b58	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
c025ce35-37be-4ffc-b9f6-74e866256dd1	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
5e29114b-0fcc-446d-ab0f-4e4ddfe16b5a	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
5e1bd01f-6ea5-4ea3-9856-5e1c7121e1fa	f54c7cdd-d5d5-4237-acbf-b0d87fecd5e9	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
adeb6427-4d3f-43cd-8890-09cac141691a	06a0cade-0e05-456a-b408-ba1842c95f18	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
dd5bc38a-0409-4e7b-b6f5-1d0e918afac5	aa8051af-fcf7-43ea-9449-c61ab4597017	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
17c3f1ae-c3f4-41a5-808b-abd575e52dfe	d31957df-bf1f-45bf-a931-39635432e566	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
bbe78318-c24c-4ced-bf99-7763ae522c95	b67980a9-de04-4973-b7dc-bc61242c8f38	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
21474f87-288f-46c2-821a-3af6ee313fc1	57e79c6d-774b-4c52-818b-d25c1047596b	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
90d3ace9-04f3-498c-9f4f-a21983db4b06	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
05f274fe-ea82-46a2-885f-29a5348c7961	0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
3a576abc-1759-4b1d-ba08-b63e1191f60d	725b6572-b2bf-4f8e-b7b9-d9fea4688769	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
647c488d-d12f-4053-a289-b64ab2574a68	b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
ae63cfde-eefb-4561-8f19-bbabf7b1c9bb	4c303161-3363-4793-b4a8-fd55c576b0db	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
0fb96875-c042-4f7c-9855-89961e225f42	61a83d25-fcae-4be9-af02-430b5fef4fd3	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
aad88d0b-0048-4188-8062-7eae7e171391	32ec6310-c7ce-4b9e-9dc6-31e3f0375e83	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
1d02721c-fd02-49f1-a967-77799f1842f7	e24420db-e080-476a-9c82-415bb96c6424	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
ae5f9db1-9618-4ea2-84d4-8f0d25d1988c	3a68b52e-cdf6-48b4-bcdc-deacfcee19a4	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
fe6dd422-1dc3-4ec5-9745-b315656f3725	f235a2c6-6d5a-4145-ab3d-eec6b3362f4a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
54be3ec5-b2ea-4867-88df-59ac476cc44c	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
5566813b-7628-4e33-911a-c7fff55e0209	403ab72a-ea6a-46bb-bf98-e0ae68171056	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2800	4000
8eb273a2-30d7-4fec-805e-3d39f026b9ec	76d866fa-cb74-4222-a3b0-841d81e751d6	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2800	4000
3dea290e-2b84-420b-b950-2f311434f561	10327621-aaad-460d-b944-daa8c8a9a927	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1050	1500
4d9589bf-91a8-408e-9109-42c0a3f89b91	89d701e7-8192-40f7-90a5-38a53e80f011	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1050	1500
8cd1142a-da3f-41cc-8053-e7d6604e62a7	77ea7188-2457-4523-999f-a1812416c570	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1050	1500
4d49696a-3ff4-4f64-ad34-b20e45d1adaa	3178a471-6e02-4db9-a506-602818e6447f	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
ccfee4d2-6985-4ad3-8021-ca2e1162f277	8a554019-4e83-458a-83c0-20fb0cd842d3	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
aa21a6e3-7b58-4b4b-870c-4e0be1b930f5	d566b1d8-efbd-4a86-9943-9514d141175d	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
8260d80a-116d-4a75-9408-a641617d324e	88fbaecf-b5a0-4b77-8a2b-b5ac5bc0ee00	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	8400	12000
e464a8ac-0834-44b2-91a5-7e441513a29c	fcf6bb53-4f1f-4cb9-a236-ce551f0ca2fd	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
9748826a-b9cd-4dd7-a35a-51bbc4158bf7	c95a9a40-14a0-4a12-98c6-0fab2934dfcd	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	5600	8000
97b9590b-c232-484e-ab37-723a55968903	8a758252-d9fd-4d16-8b56-a94382216abd	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2450	3500
c90cad4f-50f4-480e-9299-8946482a014b	c8ce88f6-2775-4df1-b252-c19a6a0668fc	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
626fe494-d5ba-4a42-83e6-97e31f5951b1	b6deec2a-3487-4a1c-b2f7-b81601aee746	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
590c9ed5-2068-4c5d-a208-365b2fc3e93e	c2a97a85-4960-4364-9d96-81d420213907	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
e4ab57c5-234c-4faa-b5f1-993ceadea597	c3612489-8939-478a-b0d7-f25c933690a2	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
b90d6550-27e4-47dd-82d8-541d69af0afe	ea609d01-1d90-4e3b-b6ef-cc6527a39dd7	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
4d312001-0d5e-4df0-be05-68ae14b15ad9	3ed61c2b-0b1a-4457-bdb1-bc640a852049	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
9e09891c-9660-4490-9523-e0e566e23ccb	b9a709f5-98c7-4b4b-a5a7-6d97c92f790c	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
1863bdab-9e96-4829-9f6a-e5c993b8e649	0e22f6c4-2b6d-41c9-9a8d-4ec26da4dfae	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
ee5c8a73-1c33-4f4e-9a77-63931f04f785	91d6df82-3afc-4883-9ad2-308a6c3d7417	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
ac856cec-76a2-414e-a51a-062f073b7fbc	1c3393ab-dad0-48cd-a0bc-9e768663c241	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
7215ba4d-36aa-4d35-b7be-085d94d76062	1849484a-75fe-4472-b1a7-0b4749f620da	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
bf1d579a-838c-4ff1-8792-769bd67c0933	e234053f-313a-4c98-a955-8c8751a13e0c	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
79ae131b-fd01-4d7d-8ddd-a3016eaac357	ee6d6a70-b231-4b28-80a7-b80478cc551f	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
08b825ba-78ac-4774-97f9-32edec1a83c0	8d7481db-1046-4c54-a9a7-08a385b66de5	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
7829f595-c8d7-4a24-8eed-e7fac98e8519	d6b08104-18be-41fe-afa6-23f2a9857baf	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
4be3903d-4989-4180-a821-2fdee04c9065	60334281-b0f8-4957-b0fe-d0aa4c8cc627	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
a2467615-f98c-4dec-bae3-9f89f7a05562	2ef7a649-59fa-44a7-9c08-12d6daf07e67	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
8b5d7188-7819-4533-994e-b4948071c9e5	93dd574b-a214-4929-967b-32bec100b410	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
c3bd394a-9143-4d4a-a8e9-d8c84ca010b3	4712b156-b046-4da1-b746-96cd9aea237f	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
418e2e5e-7c43-462a-a905-7f20883b5737	10b3916f-daa6-4fe2-b7b7-fce8a7f5210a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
fdd0e12c-4235-4ad8-a2ba-a0a4e0fc2631	357accc3-4a7d-401f-b8c6-0137f83e1d0f	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
8318a751-7544-4c0b-b606-3d55cf36fe14	d47953eb-2ff7-4b7b-911c-329a474b50c8	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
ec958aac-b6ee-46b9-9977-9df8393edcc0	6ca549d4-66c0-4ddf-a9dc-9b59cdcafb82	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
5eeb6566-9d2d-4abc-b8c5-07de32c7ebc1	a407173b-2320-4e56-ac23-46b42059b2a6	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
269bfa68-fd72-42ca-a8b6-72a86f28f4a1	0710bf62-a460-477c-a8f9-b9386d9b457f	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
bb848226-5cb9-4276-b97b-67624a9cda72	b58e0a50-2e90-4527-9aab-812dbdcc7f02	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	7000	10000
67e89af7-b706-4ad6-82f5-f82abff48747	21758598-fafd-4cd4-b115-4ae8e87a8238	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1050	1500
2c6a886f-92d8-479c-9336-c977d337338c	764b47b7-a652-4dd3-9a9e-39ba3098cac5	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
7dbbf734-1ff5-4db4-9708-035d20c21020	2aeb8827-643d-4b9d-885a-577ad3de919c	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	7000	10000
950d3974-ccb5-46a6-9204-f70b29fec086	a81c8f88-9d55-4f50-866c-602e5bc81bcd	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3500	5000
59aba7fe-0e89-488b-bf6d-a534c414735d	f803d84f-019f-4b6e-ab21-c3a7c739453c	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
60423795-7ea6-4b13-ade2-9c084bae40c1	e9e1805f-ce4d-47fb-a69c-131b763c923e	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
97a0f3ee-dd87-42c1-9d3f-c1f8bbbe5fd2	06975a24-684a-4992-a691-0207ea762f97	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3500	5000
81bf4fa0-3353-4af9-b116-d2c701133650	0c5cc742-e4c9-4e54-8c49-a9fcda2cf45a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	9100	13000
453e096d-2088-4001-ad4f-2bad5e9f40d2	d234256e-925c-4d1b-8426-c843d9f395aa	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
85b7e57c-5bd6-4f06-b872-26ebc49e42f5	42508efe-236e-49a0-b9ba-dc6eea1d09ed	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	10500	15000
a8f8de19-4f70-46ec-b30a-d2909007953a	171fabf2-d96a-4423-8ee2-9626efbbd16e	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	9100	13000
da54bab4-458a-48a0-ba2a-a2c0ba35d41c	7df4ed86-c958-41e0-96ef-c2059d9ebae4	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	10500	15000
6dd1f461-63f5-4612-863a-db4081533708	3a9a6286-20b7-42e8-903f-0a64cc5ec639	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
3b267a06-e534-4667-a498-22c2aebc3592	e662ecc3-8c27-44e7-92a1-ff1e7b277ac4	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
0e58a01c-1939-464c-8ce2-35d5ae439275	f3ff2c87-7d65-41a8-9d3a-dd08c9d1eec1	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3500	5000
b48f5f1b-a0c2-4d42-941e-847bc0be43ca	6069d5c1-b82f-4bc9-8a10-b15d8b4cacc8	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
c11978d1-3f53-4ce8-b815-bccdd249f287	40805e7d-9a61-4d08-aee6-8b865c526880	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
e65b8253-cd52-4dcb-ae7e-48895310e7c0	3bbdb71a-0bf1-490b-98df-78e140c73f3c	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3500	5000
6fe244ee-cae8-4b1d-b8cc-4a0e30ff3a40	61814f08-830c-4af5-b767-77ede47326e7	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
01693682-5e47-40aa-b7a4-273cdca36044	fe638b99-f4d3-4603-b98f-f3127d365f4c	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	5600	8000
3363e9fd-e1f1-4131-a0b6-6e0e2865b5a2	689391ef-c406-4d68-accf-2cdf8e3f2c82	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
211e584e-25fd-478f-b59a-d492590a8b88	64821565-0d87-4bba-b303-1af38bd69d41	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
4392e64d-70ba-4e14-a205-2b33dc1b6a58	eb82db1e-8ca7-4145-a22c-256eca2a6241	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
c5677703-13d5-4450-a964-b9be37ec447b	4e1b850b-1c2b-4b0b-9ce6-9017befed503	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	10500	15000
b96f1c86-726c-438a-a3dd-2e8076cefc17	e20071bd-1488-4e16-8b65-f9ba74f5c0e5	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
b6bc5032-3b13-48d6-88a6-c6dc7af17edd	c280399a-dd0f-487a-93c2-93d631d82782	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
c8a5d041-eea1-4ce3-a3c0-421f14649dce	37e21335-7f61-4d38-bd49-e89233dfcae1	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2450	3500
f26a289c-0f14-40a0-bc46-a75173d7463b	5fd4844e-43b5-46ec-b171-63ac0ccfc4cf	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
c9b090e6-8f76-4c9c-944c-2a6afdfdbd60	9e001f35-cb8d-4e99-9a05-6517a877c7fe	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4200	6000
e8006b1f-1f0e-4525-8fb6-985dd2c3fc5c	63dfb9c8-5415-49cd-b409-26518474dc4a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2450	3500
205e75da-4d6b-4218-8beb-6217530bd0db	60f0ecfe-0c71-4b43-a002-de6e2994e076	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
798e54a1-632e-48af-a4cc-c5e811eb3731	14397549-e408-4470-979a-8e794351086e	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
1abe4b01-9d29-4419-bdf9-a2cea3d04f5b	faffad1d-fecf-4ed2-b1fa-4d14f2a532a6	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	2800	4000
f760561d-b8d7-4a4a-a359-bcc8e1a1bf48	5770e029-af52-4630-840c-0703653586ce	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	3150	4500
c8c4c2ef-3ddf-4c95-9613-cb8e49e4d776	ebc6fcd6-373a-4a52-9f80-d2d720cccd63	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	4550	6500
d509d69b-a2db-4e9e-8912-bc0d1921efb3	1fc50d3b-148a-4492-b499-d675c202d459	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	6300	9000
7b72a1e3-08d6-48bb-8b0d-b4db1edfa1ee	230e0a48-0047-45e7-a253-601ba845011e	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	2100	3000
46999004-3360-4955-a551-365ae03d9ab6	5960fad5-5469-407a-85ce-79923e6b4f9d	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	11200	16000
95c1163a-9b27-454b-892c-97986346d337	661f2a06-8bb1-4538-b77e-6cc18912362d	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	11200	16000
3ccd25d9-e82f-4821-addb-7347c9a4538c	e871d1a2-7132-408d-ba6e-3538d4f7cc88	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	11200	16000
9a6b825c-fd00-4b67-a14c-6eb0a287c44c	df7b1006-92c2-4877-babb-bec571055415	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	8750	12500
dda05f97-ee65-4231-91c0-d7001c9e3747	c22ea675-f597-487e-b813-efa3a5d1aadd	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	8750	12500
2bce0324-77e8-4fe2-b260-615af5f12b6a	d0cb5263-65eb-4888-b248-597e7b621536	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	8750	12500
af050928-9e37-4fd5-9381-57eebfb6ff97	4aa57910-561e-4517-b844-5f521aacfda1	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2800	4000
bade5e41-d251-4c36-9cad-aa257039faac	6a750749-ec0c-410a-873a-010d8e6b59a5	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	7700	11000
58cc7d43-2f7f-4e37-9077-42157be6fbf5	43ea7a2f-d4c3-478d-b213-f26915e26b18	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	10500	15000
618ca335-cfa8-40d2-ba0f-f3bdc21d4bb3	0971c46a-011f-47a4-9860-98b38a53296c	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	11200	16000
3af7beac-c6ac-4e85-8daf-b25c8094dde4	25bef35a-3a0f-4643-abae-7fd3e9304647	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	14000	20000
380ea0ee-e4a7-44ea-bb0f-673429241647	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
61a701c3-b0e0-4504-a91b-9422e0a6db0f	805f19d0-7aff-4694-818a-3e5165d146ef	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
8b466b6f-2053-46bd-aea9-5ae056daa500	9f9c516a-b6f1-449c-8663-9cbedc6ca0a5	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3500	5000
b217b4c7-356f-4891-beea-de6819169959	8d7bf82b-f55a-4cb7-8f5a-571b29983059	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3500	5000
e17fce0c-0a1f-4dbc-956b-3dd5230eaa2e	9405b93e-e94e-47c8-9585-a11fcf382014	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
c4d52eca-8627-4902-a93b-685179e7dfe0	caeb9e19-b9f1-42af-acf1-a219144da6ef	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
1300573c-ea19-4e04-bdf9-40ddf0a7c6f2	2a3db456-1c32-4bbe-bcaa-e6f303c40401	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	350	500
c58f5438-34af-46ea-b361-14c86860b19f	a710f31b-9840-46a3-a903-0bed2ad13807	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3850	5500
7fd2d51c-88bc-4a44-8494-edecc0bbe5fe	17a6b989-7718-4487-94a3-651d019a5a4a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3850	5500
29628097-ddcf-46f0-9eea-4f746ccc5730	256e269a-a0c1-4a1d-a144-b4fe1b0f8451	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
cb2dc0f0-3f9e-48b1-ae1a-0d3e407d6ac7	d51d966d-a7c7-4db4-9613-0ffcd5e3061a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
ef5b05a1-e990-49f7-84e7-24060d74865f	c24d23ce-5bff-460a-859c-4c04bd1143a5	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
0a1d3b37-9080-4c57-9d86-2f76c9d251d1	106c07fd-b074-4a79-b2df-9e1b6cf04e44	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
a3ea1dde-0803-4192-8793-9d3ea6a72d2c	09cfaf4d-011f-4a61-acc3-f768c131aabe	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
023a575d-16cc-4cb5-931b-a1c88728851f	9ae4e8cd-164e-4d00-b263-61f264c95279	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
93710041-d357-4317-8836-0f97120a1bdd	25c1a510-67bd-484a-acff-8b2a4dc984bb	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1400	2000
e6a9874c-f230-42d5-93eb-fe1d26a2d947	914edc82-ac6f-47e2-8a37-4693f562e586	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
a9126e63-383c-434f-a365-2c19421e18ec	35230a29-9129-4631-a55e-a8b82b9ad37f	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
b1c56632-87fb-4b84-9532-6ae93c0e5cfe	8c95b730-638f-40d4-aaed-f7396c709c41	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
6e62e215-9a26-4881-bec9-b3aa59b6ac56	4ca13666-5ad0-4357-9355-6be3323214fa	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
4336df9e-e329-4be4-876b-a8800d2f6f35	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
ebae6abc-4239-4a69-ab2b-abff4ed47dc5	d3d6686d-e21f-423c-9901-ba7ebbb6f63d	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
c54be874-7909-4bd6-8179-bb31a5b73fb9	9711c5e4-adf4-48c4-a338-7d0a6fb9778e	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
af2729c5-acc0-4877-9415-5a3794cb085b	3b3dcf22-79ee-4494-890b-004d1bf59b1a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
fc5f97ca-14d0-4132-9eea-069fe565d9e8	3f5ee67a-2484-4147-a3fb-40c8136973d9	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
b597b037-2f51-41ba-b305-5f441bc40989	78561982-b3fa-45d6-94c6-3b99b397c756	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
49d44b7c-f757-480f-be66-437c346cca0c	030f7848-6562-40dd-8d40-4b303c724960	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
6a2200cc-0463-49a2-a019-a8c634547d9e	3d031565-23f8-431c-a867-bbf3a17a69da	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
f9a5f5ef-f750-4e87-a39f-dfd3f7410bae	187944fa-73b8-41ae-a9a8-6245a9724e3a	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	700	1000
abd81ec2-506e-4b42-a709-0e4dff34d5e5	61a18df1-774f-49b6-8f49-7cb2682b3fd6	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	7000	10000
4c35cb31-8ebb-443d-89d8-930ba7a05215	1d3cbc5f-cd61-44ac-826e-0f9b1c3ce796	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	5250	7500
ae2c7533-cf73-43e0-8f71-25e07b5822db	c6b4ee0e-8c02-4c11-a7e0-bf335e341a9d	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	5250	7500
0d04b880-41ee-43fd-a3a0-c71dda7963b2	611e30cb-aa51-4278-b358-c5488d01911f	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	5250	7500
dc6157c0-2c1b-461d-bfd5-a51c37b5d9d9	673997df-fe73-4fe9-aa0a-d2cc10f4fa28	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	5250	7500
15db26d1-5098-4e21-9679-d4e60720ea02	599ffecd-03b5-4c54-ba14-d9f6f67f77ca	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	5250	7500
13153c50-119a-4fa5-ac47-0e16173d5f1e	950099cb-14a5-4d2f-a6c7-5733b905d18e	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
cae1856f-f6f3-494f-a105-d32c4e11df6a	b2e3a8a0-21a9-4cf5-a798-f00a058d421d	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
6fd8afc4-5a71-44dd-8f91-3cabb8efd5d4	483637cd-fe09-4ca1-b310-2a5ad8ae2658	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
11c2fa72-67e3-4175-9014-5b1f4d9b9d41	5556e2f3-a8c0-441d-b696-07ea76048743	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
2585b380-d3af-4331-b19d-b5883781236d	3e01c670-c774-496f-9cbc-1816f8627726	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1750	2500
bf49954c-6d03-4426-acc9-755862e7c84f	9e9e5f57-a956-470f-8b8c-a3d6f29a9894	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
55e1ba68-951f-4c6c-9ef1-17bb2c044d11	98fac7bf-4029-44c2-a9b4-17f85c9b9d45	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	4900	7000
e2c52f6f-b9a6-431c-9ba4-f652c7acf6e9	d98e765b-7e7d-4f49-895d-960b4f71263e	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	32900	47000
f957d9e6-be99-488a-9ed8-7c588170ff78	76308db9-8ec1-4801-b886-8286571e881c	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	17500	25000
3b2ef8a3-bb68-4e14-bca0-482edd643df0	2dbfef19-f0b5-4fa6-903c-67b818864e6a	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	9450	13500
6c17048c-1281-426d-8027-e20a9e77ade1	5f6a0c31-1042-4f2d-a14b-002722fd9187	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	16100	23000
f8889836-3f6c-4afe-aabf-c75eb7fc738a	0e770aef-8872-488e-b6e7-9c3b37cc8525	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	11200	16000
203d1835-0621-46cb-a0bf-80d0266035ca	ce2cad02-0a94-4138-bd11-fae5f15376b1	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	17150	24500
fe65033b-55ef-47e5-9da2-f67507713735	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	17850	25500
1fa8052a-3f10-44f2-94b6-c46379104f78	6c999ab5-1af9-41f3-b2f7-cd1d6bc186f7	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	13300	19000
9faca4c3-bc12-421d-bfec-d2648da9b851	a3d035d2-1dd2-41f5-afa3-d9e87decc09e	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	25200	36000
f2c7ffdc-ad5c-42c0-b71e-1942fa6e01d3	9737c64e-cda0-4e10-9c0d-de9e94bc4a51	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	1050	1500
7dd6055b-f0bf-4aa5-8a1f-91bfba99a374	79887006-0bd0-4f88-a523-8258dba47134	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
3dfaf5b7-1629-43bf-b282-cfc2cdb31b6e	3c1847cd-b942-42c8-a272-bb248571c1de	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2100	3000
62546c51-0dbf-4a69-8f4b-1ecab0411903	4a478acc-84aa-4340-9839-32ba0505a2d8	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	38150	54500
3a23e6f0-9fc0-4e8b-bafa-6c3d2864cdad	f9d4e754-8efe-43ff-bb7a-cad985bed182	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	40950	58500
835c1146-484a-4eb4-943c-539e05da8b97	9c097a47-cb6a-4e4c-911e-505e8de59227	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	52850	75500
d1b5ee93-e21a-4e71-b496-86ecabfa8f70	2273a601-eff0-4ed2-8e4a-44dbfe6b8c11	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	52850	75500
486201cf-e250-419b-bb75-9525f5fbec5a	04636f4c-c6a8-4817-8bc5-88e5b0b3621d	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	52850	75500
7b8b9a33-1d02-4ace-a2a5-e8d70bd03bfe	00ebaa3a-ba04-45c3-8ae2-901f5b37a51d	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	52850	75500
12ddfdbc-d512-41f7-9211-032ee29a582c	ae6cdc50-feae-4dd1-b238-89b6f10cf980	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	2500	3500
05b9426f-3260-465d-a52a-9b9c76a2eea1	581f08a6-453c-4993-8d74-638ebccc1cf3	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	3500	5000
6ae22bd6-6e6c-492c-950d-2ef820331e78	356d1f9b-943a-401b-8eeb-ec147264f25a	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
f94f6cc8-601f-4644-bc39-d3e188f49c57	80a25700-e910-4409-83db-6021009723d2	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
cc20e65c-9d50-4c6f-b9f2-94fc25a9c74f	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	23800	34000
f44c9528-d89b-4ebc-9694-f9a84fc96a65	9668ff45-83d7-466d-a266-9c4d54eb2993	bdc0e6e1-5788-4b33-8adc-380e9e21dd94	t	1	38150	54500
9e05bd7f-6490-45e4-ab81-3bdb126748b7	c2f5f050-3dd7-4862-9b67-e72cb4aa9c57	7114b595-ae1e-4b0c-81f2-9128d228e434	t	1	17850	25500
\.


--
-- Data for Name: item_price_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.item_price_log (id, item_id, created_at, principal_price_old, principal_price, sale_price_old, sale_price, status, inserted_by, approved_by) FROM stdin;
5a84f7d0-4f8e-4a6b-b2bc-556fe727f67b	c280399a-dd0f-487a-93c2-93d631d82782	2020-11-07 02:28:08	4900	6000	7000	7000	2	operator_01	admin
\.


--
-- Data for Name: kmj_printer_client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kmj_printer_client (id, kmj_user_id, computer_name, printer_name, is_default) FROM stdin;
3537b978-21ec-49e8-b0ac-323394f83f33	845865c8-b0a8-4197-8428-51f4e831a824	HP-PC	POS-58	\N
f0f4fd87-f577-48d0-86d7-e5225c66e6b5	a129f5c0-77f5-4ebf-8ccc-debc1e6f4e26	HP-PC	POS-58	\N
\.


--
-- Data for Name: kmj_routing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kmj_routing (id, name, path, sequence, parent, icon, routing_group) FROM stdin;
e9298f7f-5db4-40d0-956e-c238001573a5	store	store_index	1	\N	\N	administrator
09e8bedf-f394-4677-9f61-1735a31cf1ba	packaging	packaging_index	1	\N	\N	master
05cf6180-5d2e-4539-b1e8-0b8dc5393e5c	report_profit	report_profit_index	1	\N	\N	report
8e055f35-5321-4775-924a-1fa43637baaa	purchase	purchase_index	1	\N	\N	transaction
27d8f1c7-a23d-4e3f-9482-b5c55c258afc	kmj_routing	kmj_routing_index	2	\N	\N	administrator
a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	kmj_user	kmj_user_index	3	\N	\N	administrator
fab7b41a-5584-487e-bb7f-ba06572ab2a8	kmj_printer	kmj_printer_index	4	\N	\N	administrator
9f8a642c-5372-4d29-9fad-7f29e0efe519	item_category	item_category_index	2	\N	\N	master
dbf3766f-5f23-48ff-ad25-81b681be09fc	item	item_index	3	\N	\N	master
edcd3e57-9599-4a16-8bcc-5ea514a957eb	supplier	supplier_index	4	\N	\N	master
d32e2472-8588-4e4f-b52f-212840e7836e	customer	customer_index	5	\N	\N	master
22f27cf2-4737-4cd6-adcb-537320c3606e	sale	sale_index	2	\N	\N	transaction
f50ce8d8-25e0-4686-b4ae-b96d0462af54	eod	eod_index	3	\N	\N	transaction
a3fdeff1-d0ae-4d43-9727-716c4d7b435c	report_profit_store	report_profit_store_index	2	\N	\N	report
40d50aac-535e-4ef8-86b3-e1b45969fe86	report_item_stock	report_item_stock_index	3	\N	\N	report
e7c6e8bb-9b8b-4962-818a-3d7fee4245ad	report_unpaid_transaction	report_unpaid_transaction_index	4	\N	\N	report
2aa43b9b-8863-4188-a234-260b3c224e43	report_sale	report_sale_index	1	\N	\N	report
442224fd-eaec-4838-9d44-a3d1706e1396	report_purchase	report_purchase_index	1	\N	\N	report
f6497f8e-c47d-4089-90d5-26f00fbb4ee2	report_transaction	report_transaction_index	1	\N	\N	report
\.


--
-- Data for Name: kmj_routing_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kmj_routing_role (id, routing_id, role, path, is_allowed, updated_at) FROM stdin;
bcdc63f8-ce16-46e3-86bc-65ea741bae12	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_SUPER_USER	kmj_printer_index	t	2020-09-22 05:53:54
e5da4c00-4667-45ea-85d1-e099a1e09c72	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_SUPER_USER	kmj_printer_create	t	2020-09-22 05:53:54
8e719ed4-7879-480d-abfe-f4eb94da823e	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_SUPER_USER	kmj_printer_edit	t	2020-09-22 05:53:54
61302b4b-050b-439f-b853-274672f684d4	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_SUPER_USER	kmj_printer_detail	t	2020-09-22 05:53:54
73b67771-573e-4f73-87c9-fff438e91513	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_SUPER_USER	kmj_printer_delete	t	2020-09-22 05:53:54
4fba58ce-122f-45cf-94f3-6311304485f8	27d8f1c7-a23d-4e3f-9482-b5c55c258afc	ROLE_SUPER_USER	kmj_routing_index	t	2020-09-22 05:53:54
405a6aa3-83a3-4733-92c4-c8fc8e31fdc7	27d8f1c7-a23d-4e3f-9482-b5c55c258afc	ROLE_SUPER_USER	kmj_routing_roles	t	2020-09-22 05:53:54
ac2bb340-3883-4ef6-a1ba-2096ff8ae112	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_login	t	2020-09-22 05:53:54
6210050c-18e6-4a4e-af61-1b217496fd5d	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_logout	t	2020-09-22 05:53:54
983d04aa-22dc-416d-9bc4-dabee9e4a810	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_profile	t	2020-09-22 05:53:54
c66b3ec3-a04d-48c2-9c5b-941138efd27f	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_change_password	t	2020-09-22 05:53:54
45ae6a44-3474-4d3a-b4ae-b2700ad156ae	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_index	t	2020-09-22 05:53:54
1cc1e191-b760-4a95-b475-dd416e7621f5	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_create	t	2020-09-22 05:53:54
e4c0ef8e-f16b-45c1-b542-18ebdddf9a0b	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_edit	t	2020-09-22 05:53:54
03fab1cd-cd59-4b32-b46b-058a9abc6f48	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_detail	t	2020-09-22 05:53:54
0c29fab4-2b0d-4f51-aab6-01e0f6d43ee0	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_SUPER_USER	kmj_user_delete	t	2020-09-22 05:53:54
ec8070d4-ba95-4d7a-93bc-98ac54be9eb6	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_SUPER_USER	store_index	t	2020-09-22 05:53:54
9d4c96a8-d730-42d3-9a52-59a177b03262	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_SUPER_USER	store_create	t	2020-09-22 05:53:54
c8248f4e-0008-4afc-b43c-4da2fce8c3c3	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_SUPER_USER	store_edit	t	2020-09-22 05:53:54
b2f790fc-70ac-4165-98c7-34f660ba9eea	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_SUPER_USER	store_detail	t	2020-09-22 05:53:54
23d60cab-b54c-48c2-8bb5-3d1727425373	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_SUPER_USER	store_delete	t	2020-09-22 05:53:54
d9b2baa3-a24b-42ec-8e1d-b43c7296626f	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_SUPER_USER	customer_index	t	2020-09-22 05:53:54
66bc4abd-4d8f-4f3e-9f3c-eaeaa63dd26c	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_SUPER_USER	customer_create	t	2020-09-22 05:53:54
a929cf75-995a-4480-9e2a-eaf955ffa8e7	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_SUPER_USER	customer_edit	t	2020-09-22 05:53:54
9bd83c89-14e9-40dc-b425-a6ea7ad5ab0f	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_SUPER_USER	customer_detail	t	2020-09-22 05:53:54
4becf7d2-16a8-4bec-8258-07573015099d	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_SUPER_USER	customer_delete	t	2020-09-22 05:53:54
c4171978-d6e6-4065-af54-342a122cf0f6	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_SUPER_USER	item_category_index	t	2020-09-22 05:53:54
8c952748-115c-44e8-a8c4-1ce54e4657f8	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_SUPER_USER	item_category_create	t	2020-09-22 05:53:54
428ef8ad-43dc-4623-a494-60148e7e9238	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_SUPER_USER	item_category_edit	t	2020-09-22 05:53:54
badd1fdc-075d-4abe-afc7-4c3455d5a89e	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_SUPER_USER	item_category_detail	t	2020-09-22 05:53:54
7c50ec50-9a7d-42fd-aea4-f5b5d0bb1f39	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_SUPER_USER	item_category_delete	t	2020-09-22 05:53:54
6fd485c5-61e9-4a7e-9332-0b72040d8318	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_index	t	2020-09-22 05:53:54
3bbb9ae9-4d4a-46d6-be38-cb471faa2110	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_create	t	2020-09-22 05:53:54
7b98d396-81de-4db3-b7c7-84b4f015114e	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_data	t	2020-09-22 05:53:54
2e1ec0fd-94a4-4667-b56d-8b0bf466bbdb	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_edit	t	2020-09-22 05:53:54
1cd480f7-d416-408f-a676-601387998311	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_detail	t	2020-09-22 05:53:54
5c8373ce-dabb-4549-be97-26b8f70750b7	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_delete	t	2020-09-22 05:53:54
9a59cef3-da17-4fb4-8400-a81f13ce93c6	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_export	t	2020-09-22 05:53:54
48a92189-36f9-48b5-882d-c522f9643dbc	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_import	t	2020-09-22 05:53:54
92f8d2c4-48a2-46a5-b515-267cfa6b361f	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_template_import	t	2020-09-22 05:53:54
75861c77-217b-4ea6-a3d9-19733316075a	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_price_approval_list	t	2020-09-22 05:53:54
bc9d968b-4769-4f8e-8680-4504742a2ccf	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_package_create	t	2020-09-22 05:53:54
51a476be-6a12-4278-a435-b51814befc79	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_package_edit	t	2020-09-22 05:53:54
e3ce2a50-1f98-408a-b45d-d14d5de45efa	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_package_delete	t	2020-09-22 05:53:54
09daa0cf-55a6-4a24-9795-dd1470098b1d	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_SUPER_USER	item_price_log_approval	t	2020-09-22 05:53:54
a29b80e8-4482-4b6e-acb9-846caa34a838	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_SUPER_USER	packaging_index	t	2020-09-22 05:53:54
1449bb53-488e-41bd-8db9-4913e373f6ce	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_SUPER_USER	packaging_create	t	2020-09-22 05:53:54
d4638020-6036-4804-9a83-de82792e8bef	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_SUPER_USER	packaging_edit	t	2020-09-22 05:53:54
a47495d6-a57e-40af-b343-44a67c273625	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_SUPER_USER	packaging_detail	t	2020-09-22 05:53:54
4d4f9c02-c969-41b2-8112-24466d019344	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_SUPER_USER	packaging_delete	t	2020-09-22 05:53:54
ce98c7ed-6b81-4968-aaa7-c568de98b217	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_SUPER_USER	supplier_index	t	2020-09-22 05:53:54
f27c266d-7454-4f3e-a3e4-5c4aef160e15	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_SUPER_USER	supplier_create	t	2020-09-22 05:53:54
d52fa549-6fcb-40dc-aba5-1a25cde28896	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_SUPER_USER	supplier_edit	t	2020-09-22 05:53:54
24e3e371-be3b-4eb9-9b88-9e9f7a503321	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_SUPER_USER	supplier_detail	t	2020-09-22 05:53:54
6ec5f766-c229-458d-a83e-65b1a4758442	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_SUPER_USER	supplier_delete	t	2020-09-22 05:53:54
39932e5f-81f0-412c-bf8c-24700deaac6f	05cf6180-5d2e-4539-b1e8-0b8dc5393e5c	ROLE_SUPER_USER	report_profit_index	t	2020-09-22 05:53:54
599d1727-c2e5-4c78-a9cd-eda882b71824	05cf6180-5d2e-4539-b1e8-0b8dc5393e5c	ROLE_SUPER_USER	report_profit_print	t	2020-09-22 05:53:54
a9f19d14-ec5f-412a-a971-3089374e60d3	a3fdeff1-d0ae-4d43-9727-716c4d7b435c	ROLE_SUPER_USER	report_profit_store_index	t	2020-09-22 05:53:54
3fbb5b70-bf79-4e46-8287-57ef54669998	a3fdeff1-d0ae-4d43-9727-716c4d7b435c	ROLE_SUPER_USER	report_profit_store_print	t	2020-09-22 05:53:54
0e7f7080-be9b-4b96-a669-447f1737a617	40d50aac-535e-4ef8-86b3-e1b45969fe86	ROLE_SUPER_USER	report_item_stock_index	t	2020-09-22 05:53:54
918bd851-8111-4e20-a2f8-a4a6047e5df3	e7c6e8bb-9b8b-4962-818a-3d7fee4245ad	ROLE_SUPER_USER	report_unpaid_transaction_index	t	2020-09-22 05:53:54
a0d887fc-50b6-4b54-a53a-5618774976ad	e7c6e8bb-9b8b-4962-818a-3d7fee4245ad	ROLE_SUPER_USER	report_unpaid_transaction_print	t	2020-09-22 05:53:54
e3ca4c2d-6e13-47d0-acee-bc8b340c0acd	2aa43b9b-8863-4188-a234-260b3c224e43	ROLE_SUPER_USER	report_sale_index	t	2020-09-22 05:53:54
59135cba-d5a1-4c17-a224-e5a263fb74ed	2aa43b9b-8863-4188-a234-260b3c224e43	ROLE_SUPER_USER	report_sale_print	t	2020-09-22 05:53:54
81efa836-84b6-4fc4-8f4e-165a70ed45ef	442224fd-eaec-4838-9d44-a3d1706e1396	ROLE_SUPER_USER	report_purchase_index	t	2020-09-22 05:53:54
6b72cec8-a61d-4fab-9cb1-198a679c496d	442224fd-eaec-4838-9d44-a3d1706e1396	ROLE_SUPER_USER	report_purchase_print	t	2020-09-22 05:53:54
2994258e-3244-4bb5-a519-3a01c929431c	f6497f8e-c47d-4089-90d5-26f00fbb4ee2	ROLE_SUPER_USER	report_transaction_index	t	2020-09-22 05:53:54
0a8599fc-8282-4995-ab01-8c0a03360897	f6497f8e-c47d-4089-90d5-26f00fbb4ee2	ROLE_SUPER_USER	report_transaction_print	t	2020-09-22 05:53:54
40706f5d-cd6e-480e-be6a-a875dd249875	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_SUPER_USER	purchase_index	t	2020-09-22 05:53:54
f58a49ce-2fbd-4189-918d-2f1ee9c38712	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_SUPER_USER	purchase_create	t	2020-09-22 05:53:54
f2692b4e-70b6-4b01-a082-fac79e5a5995	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_SUPER_USER	purchase_detail	t	2020-09-22 05:53:54
5538b1aa-250f-46a8-934c-2603d18f158c	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_SUPER_USER	purchase_delete	t	2020-09-22 05:53:54
67fc681f-51c9-4417-b398-5b65be860988	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_SUPER_USER	purchase_detail_create	t	2020-09-22 05:53:54
6237ccca-968a-466b-a2ae-cff4a85f6052	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_SUPER_USER	purchase_detail_edit	t	2020-09-22 05:53:54
84fbf322-766d-4982-993b-20cd42672303	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_SUPER_USER	purchase_detail_delete	t	2020-09-22 05:53:54
a764c4bc-ac98-4b1d-94f6-333db19901bf	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_index	t	2020-09-22 05:53:54
3bdfda96-2b49-4e8b-9272-3e1c655a230d	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_create	t	2020-09-22 05:53:54
8b684569-db83-4225-b4e8-d0cf8cd65642	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_printout_test	t	2020-09-22 05:53:54
37ee6388-faa5-43cf-8699-9121b53029e3	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_payment	t	2020-09-22 05:53:54
8e7704fc-7f84-4cdc-811f-6e6556fd85a9	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_printout	t	2020-09-22 05:53:54
576261c2-cd89-4b5b-9657-7ef9920198a3	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_detail	t	2020-09-22 05:53:54
567ec66b-3d79-4c7f-831b-905fbbe21d66	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_delete	t	2020-09-22 05:53:54
e137bbfb-a938-4d73-9f81-1002621ae601	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_item_create	t	2020-09-22 05:53:54
d9dd0857-49a1-4d0d-98f8-bc57cea02b18	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_item_edit	t	2020-09-22 05:53:54
e480c7ac-0615-4e53-8388-b9585ae094a9	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_SUPER_USER	sale_item_delete	t	2020-09-22 05:53:54
914a219f-9b3e-4723-b717-cb5bf2dbb7ee	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_SUPER_USER	eod_index	t	2020-09-22 05:53:54
de32bac7-fcb3-4e6b-9546-a2eabb9e8ea8	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_ADMINISTRATOR	kmj_printer_index	t	2020-09-22 05:54:04
968f4d93-1cfd-48fa-a829-b8ae5e6a583a	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_ADMINISTRATOR	kmj_printer_create	t	2020-09-22 05:54:04
cdb2bdc6-aefd-42de-a2f0-7b2d1ffb335f	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_ADMINISTRATOR	kmj_printer_edit	t	2020-09-22 05:54:04
f3bb7c8d-fd8e-43ec-825e-db1fa9ba472f	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_ADMINISTRATOR	kmj_printer_detail	t	2020-09-22 05:54:04
f0df2bea-95e2-4acb-97d4-cd05cd464d09	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_ADMINISTRATOR	kmj_printer_delete	t	2020-09-22 05:54:04
a07f2f32-6ad6-4f4f-8431-6aeece47de69	27d8f1c7-a23d-4e3f-9482-b5c55c258afc	ROLE_ADMINISTRATOR	kmj_routing_index	t	2020-09-22 05:54:04
2f0c0da9-302d-412f-bfeb-7f4b6288d324	27d8f1c7-a23d-4e3f-9482-b5c55c258afc	ROLE_ADMINISTRATOR	kmj_routing_roles	t	2020-09-22 05:54:04
83886ff6-9ec0-429d-9588-761ef94d1f12	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_login	t	2020-09-22 05:54:04
59b37321-9856-4bf5-afdb-d1fd70b7ca37	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_logout	t	2020-09-22 05:54:04
d35cd54d-ae14-43e8-a952-fa86a1d2b9ed	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_profile	t	2020-09-22 05:54:04
d27a390c-e112-4216-bc33-fcb7397ed166	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_change_password	t	2020-09-22 05:54:04
b8d3deb4-586f-44fd-811a-d7cbe993cedc	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_index	t	2020-09-22 05:54:04
50d9587e-f81d-48a9-ac0a-5f445d98e69d	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_create	t	2020-09-22 05:54:04
3fbf0035-91f0-4062-81b3-f4e146c158c8	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_edit	t	2020-09-22 05:54:04
a6b09ba2-d8ea-4126-92f0-073973a81f7a	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_detail	t	2020-09-22 05:54:04
c0b8b820-ce71-4c95-914b-9cc718dbf5dd	a49a5412-bc8c-4ae4-8eca-9b12ded7ba4d	ROLE_ADMINISTRATOR	kmj_user_delete	t	2020-09-22 05:54:04
858ad2f3-c059-4cf6-9382-15f30847cd57	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_ADMINISTRATOR	store_index	t	2020-09-22 05:54:04
913c934f-6cf1-4ef2-850b-09068fbf439b	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_ADMINISTRATOR	store_create	t	2020-09-22 05:54:04
c4b65f33-1ea7-4429-80bd-c3de6451709a	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_ADMINISTRATOR	store_edit	t	2020-09-22 05:54:04
e2e561fe-106b-41dc-b6ce-d5d2b6735427	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_ADMINISTRATOR	store_detail	t	2020-09-22 05:54:04
254f7cbb-72dd-4fc0-a668-edfe466f5d0a	e9298f7f-5db4-40d0-956e-c238001573a5	ROLE_ADMINISTRATOR	store_delete	t	2020-09-22 05:54:04
0100ea57-2717-4d98-af49-8ed1b0059022	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_ADMINISTRATOR	customer_index	t	2020-09-22 05:54:04
666b91a6-fe28-468e-9b21-e4d3a296f54a	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_ADMINISTRATOR	customer_create	t	2020-09-22 05:54:04
cd021f9f-a215-4a89-a054-53ad262b5077	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_ADMINISTRATOR	customer_edit	t	2020-09-22 05:54:04
16862e9a-e77b-4822-8f92-30ebfbdec4fc	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_ADMINISTRATOR	customer_detail	t	2020-09-22 05:54:04
2fd832ef-447b-4a56-b399-97acf65767f5	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_ADMINISTRATOR	customer_delete	t	2020-09-22 05:54:04
510c36a5-2501-408a-9dc3-efe16569454c	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_ADMINISTRATOR	item_category_index	t	2020-09-22 05:54:04
09b53203-c508-4744-ae72-c0725af6cf62	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_ADMINISTRATOR	item_category_create	t	2020-09-22 05:54:04
727d5dd8-7bc7-44a5-a303-58cb07ccc3e1	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_ADMINISTRATOR	supplier_delete	t	2020-09-22 05:54:04
e1eddf98-52dd-402c-bfe5-367b3b1cc635	05cf6180-5d2e-4539-b1e8-0b8dc5393e5c	ROLE_ADMINISTRATOR	report_profit_index	t	2020-09-22 05:54:04
9fde58ad-540b-44a3-be5f-54fd89d768e6	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_SUPER_USER	eod_create	t	2020-09-22 05:53:54
e0e89c03-a400-4504-afda-6fd42d24a1d9	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_SUPER_USER	eod_detail	t	2020-09-22 05:53:54
44fd30ce-03c8-4e57-adc0-64347c58da83	05cf6180-5d2e-4539-b1e8-0b8dc5393e5c	ROLE_ADMINISTRATOR	report_profit_print	t	2020-09-22 05:54:04
518b912f-8ebd-42c2-9913-876b10b67c4d	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_ADMINISTRATOR	item_category_edit	t	2020-09-22 05:54:04
f1fde39a-19e0-4856-9229-d344d77760f5	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_ADMINISTRATOR	item_category_detail	t	2020-09-22 05:54:04
fd129e36-6ac1-43c8-bfe7-46ee9d7916e9	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_ADMINISTRATOR	item_category_delete	t	2020-09-22 05:54:04
0691e828-ba71-4921-961a-4bfd0dc8a002	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_index	t	2020-09-22 05:54:04
e7202e09-b2e9-466d-bb3c-c1432ef60eed	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_create	t	2020-09-22 05:54:04
5218867c-5f47-4c8f-9d58-56c5a11702cd	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_data	t	2020-09-22 05:54:04
c2669e8e-d89b-43da-9d4d-78ba75041592	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_edit	t	2020-09-22 05:54:04
b2896801-0cb9-4378-acff-ed6091164a66	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_detail	t	2020-09-22 05:54:04
89c63dda-2b37-4c74-89f4-19f98489c8fb	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_delete	t	2020-09-22 05:54:04
51838f79-5352-44cc-99e8-3ed5a7ba9b4c	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_export	t	2020-09-22 05:54:04
278151c2-88bb-41f6-8241-06966bbf23d8	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_import	t	2020-09-22 05:54:04
c4b98587-60e3-45fe-95e2-764902327689	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_template_import	t	2020-09-22 05:54:04
bf22fe0c-82b2-423d-8351-42205273b39b	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_price_approval_list	t	2020-09-22 05:54:04
e5d5e1c3-1d78-4265-a8ed-54d7eb309cc1	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_package_create	t	2020-09-22 05:54:04
10a6de74-3925-47a3-9ca5-184a1f28c765	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_package_edit	t	2020-09-22 05:54:04
aded44e3-5e10-4329-9f74-2efef8831b2b	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_package_delete	t	2020-09-22 05:54:04
231d6bf8-0c80-4067-9e0c-679fa15b23b4	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_ADMINISTRATOR	item_price_log_approval	t	2020-09-22 05:54:04
343c9c67-564b-48e0-be9c-71a0e1800065	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_ADMINISTRATOR	packaging_index	t	2020-09-22 05:54:04
365c5e41-6407-42b9-a1c7-11a95e6f00c2	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_ADMINISTRATOR	packaging_create	t	2020-09-22 05:54:04
e21a8b5a-5403-4c99-bdb9-d81fecfe3240	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_ADMINISTRATOR	packaging_edit	t	2020-09-22 05:54:04
b87f1f06-c719-48ce-a33a-4de71d0c624f	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_ADMINISTRATOR	packaging_detail	t	2020-09-22 05:54:04
7e4956fe-f585-4c2a-9358-0a78be6d8632	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_ADMINISTRATOR	packaging_delete	t	2020-09-22 05:54:04
cdf36b8f-2f67-4f92-a043-f24edabb2a4b	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_ADMINISTRATOR	supplier_index	t	2020-09-22 05:54:04
3b09c2a4-d29a-4a0b-aaab-2cf3f5de3533	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_ADMINISTRATOR	supplier_create	t	2020-09-22 05:54:04
7bb8c880-0513-43f2-8fc1-a10783bac0f7	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_ADMINISTRATOR	supplier_edit	t	2020-09-22 05:54:04
e9a01751-5d30-4010-988e-5985c19a1584	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_ADMINISTRATOR	supplier_detail	t	2020-09-22 05:54:04
792b1a28-68f4-4e2b-9082-bad897d05473	a3fdeff1-d0ae-4d43-9727-716c4d7b435c	ROLE_ADMINISTRATOR	report_profit_store_index	t	2020-09-22 05:54:04
7639c8d5-87bb-4a4e-9503-ce1c7fb5536d	a3fdeff1-d0ae-4d43-9727-716c4d7b435c	ROLE_ADMINISTRATOR	report_profit_store_print	t	2020-09-22 05:54:04
4e0c29b1-8d5b-402a-b5ee-cd17ab9314e7	40d50aac-535e-4ef8-86b3-e1b45969fe86	ROLE_ADMINISTRATOR	report_item_stock_index	t	2020-09-22 05:54:04
b9f38582-b9fc-494d-b098-44b39d561cbe	e7c6e8bb-9b8b-4962-818a-3d7fee4245ad	ROLE_ADMINISTRATOR	report_unpaid_transaction_index	t	2020-09-22 05:54:04
2a2fb3d3-6357-47b7-b5b5-54a9f8337f6e	e7c6e8bb-9b8b-4962-818a-3d7fee4245ad	ROLE_ADMINISTRATOR	report_unpaid_transaction_print	t	2020-09-22 05:54:04
de60bf2e-31dd-49d2-9881-6e6354035f46	2aa43b9b-8863-4188-a234-260b3c224e43	ROLE_ADMINISTRATOR	report_sale_index	t	2020-09-22 05:54:04
7b80c029-0597-4761-9336-25532fd3faf1	2aa43b9b-8863-4188-a234-260b3c224e43	ROLE_ADMINISTRATOR	report_sale_print	t	2020-09-22 05:54:04
f29f2d2b-abdb-4e87-aa3c-007425e7a598	442224fd-eaec-4838-9d44-a3d1706e1396	ROLE_ADMINISTRATOR	report_purchase_index	t	2020-09-22 05:54:04
41d74f5c-e115-4dca-aa50-5f1255dfce88	442224fd-eaec-4838-9d44-a3d1706e1396	ROLE_ADMINISTRATOR	report_purchase_print	t	2020-09-22 05:54:04
a59e69e9-0905-4b9f-9e0c-f5b7eb6ddec1	f6497f8e-c47d-4089-90d5-26f00fbb4ee2	ROLE_ADMINISTRATOR	report_transaction_index	t	2020-09-22 05:54:04
36ab83d4-5cdd-462b-8fc3-a1d67498d398	f6497f8e-c47d-4089-90d5-26f00fbb4ee2	ROLE_ADMINISTRATOR	report_transaction_print	t	2020-09-22 05:54:04
61b80ec4-abd4-4fed-a631-43d09e63cf8b	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_ADMINISTRATOR	purchase_index	t	2020-09-22 05:54:04
94be2d1e-6fa8-47d1-ac26-854b7fd59c02	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_ADMINISTRATOR	purchase_create	t	2020-09-22 05:54:04
72ad312e-0933-461c-abc8-892cf8ad5237	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_ADMINISTRATOR	purchase_detail	t	2020-09-22 05:54:04
450b1982-7e0b-4e25-b1dc-c2e01907272c	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_ADMINISTRATOR	purchase_delete	t	2020-09-22 05:54:04
8ecf0510-0b5c-4056-9d2f-c2a35f3431ab	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_ADMINISTRATOR	purchase_detail_create	t	2020-09-22 05:54:04
c0a2ed74-3236-4a36-bfd4-25f8374454f3	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_ADMINISTRATOR	purchase_detail_edit	t	2020-09-22 05:54:04
558966b0-5285-4c7b-81e6-d0fb3bcbee9e	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_ADMINISTRATOR	purchase_detail_delete	t	2020-09-22 05:54:04
93dd0d43-ff18-43d6-96f5-c5fded962e14	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_index	t	2020-09-22 05:54:04
c234e8a5-323f-4006-a88c-95ee42ee0d6b	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_create	t	2020-09-22 05:54:04
414d70c6-2805-42bd-921d-e8acfcd3447c	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_printout_test	t	2020-09-22 05:54:04
be60993c-8a9a-4d66-ae20-d5cee01a2c69	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_payment	t	2020-09-22 05:54:04
23c395bb-7c57-488b-864d-ca789bc2b5fd	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_printout	t	2020-09-22 05:54:04
b66b118e-3cec-4d9c-a83a-13af24fd4072	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_detail	t	2020-09-22 05:54:04
42b5920f-3488-4773-8856-5651858bea8d	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_delete	t	2020-09-22 05:54:04
b7b1dfc9-3732-4040-b737-c8e8dafb4ba1	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_item_create	t	2020-09-22 05:54:04
f00dcd9e-456d-4d3d-be02-3fc1fa36863a	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_item_edit	t	2020-09-22 05:54:04
364ee90a-d1fc-4cc2-a79e-4ef1c193a988	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_ADMINISTRATOR	sale_item_delete	t	2020-09-22 05:54:04
dfa6703b-25bd-47c0-a15d-38e730825d4c	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_ADMINISTRATOR	eod_index	t	2020-09-22 05:54:04
0618568e-4fa4-4b4a-bd0e-55e8a6ad8306	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_ADMINISTRATOR	eod_create	t	2020-09-22 05:54:04
3e6a1197-0e6d-42c1-bf3a-8575e4cfde8f	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_ADMINISTRATOR	eod_detail	t	2020-09-22 05:54:04
9a290cc7-bef5-49e1-ba43-af76358608cc	05cf6180-5d2e-4539-b1e8-0b8dc5393e5c	ROLE_OPERATOR	report_profit_index	t	2020-09-29 06:29:21
235df3df-64ac-4763-8f5e-6306c9f07d90	a3fdeff1-d0ae-4d43-9727-716c4d7b435c	ROLE_OPERATOR	report_profit_store_index	t	2020-09-29 06:29:21
5c60e1e1-1b26-4c4c-ac48-503cc0a2d188	2aa43b9b-8863-4188-a234-260b3c224e43	ROLE_OPERATOR	report_sale_index	t	2020-09-29 06:29:21
4228345f-34ff-471d-8453-2ce5d22fead6	442224fd-eaec-4838-9d44-a3d1706e1396	ROLE_OPERATOR	report_purchase_index	t	2020-09-29 06:29:21
e40d75e2-a532-44cb-b490-2959dd26c755	f6497f8e-c47d-4089-90d5-26f00fbb4ee2	ROLE_OPERATOR	report_transaction_index	t	2020-09-29 06:29:21
b47298f0-cbdc-418e-b212-a9c3d020011f	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_OPERATOR	kmj_printer_index	t	2020-09-29 06:29:21
dd385c58-b44a-4855-99e7-5b198ccba36d	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_OPERATOR	kmj_printer_create	t	2020-09-29 06:29:21
3ff41a95-e48e-431b-991a-56b26af72781	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_OPERATOR	kmj_printer_edit	t	2020-09-29 06:29:21
e99c4eb9-b769-4cb6-9a77-e66532e17cb8	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_OPERATOR	kmj_printer_detail	t	2020-09-29 06:29:21
873e32e2-a5d4-4c76-83ff-06216231b718	fab7b41a-5584-487e-bb7f-ba06572ab2a8	ROLE_OPERATOR	kmj_printer_delete	t	2020-09-29 06:29:21
2397ac4a-35af-40a9-865e-e5673c103713	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_OPERATOR	customer_index	t	2020-09-29 06:29:21
1da1bed2-cbc8-4433-bba1-5ea61cb4afdb	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_OPERATOR	customer_create	t	2020-09-29 06:29:21
ffd5bc14-4b04-4e7c-b901-f83da9d6ca34	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_OPERATOR	customer_edit	t	2020-09-29 06:29:21
f3df2f75-1561-467b-b7a3-2077e963213d	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_OPERATOR	customer_detail	t	2020-09-29 06:29:21
fdb34e6d-a7a2-4117-bc82-d1afa20c9a08	d32e2472-8588-4e4f-b52f-212840e7836e	ROLE_OPERATOR	customer_delete	t	2020-09-29 06:29:21
acb05921-5356-44b0-a230-9471bd15097f	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_OPERATOR	item_category_index	t	2020-09-29 06:29:21
248a40ba-efec-439d-849f-4131d1ebb061	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_OPERATOR	item_category_create	t	2020-09-29 06:29:21
91b97773-6291-4b55-b97f-ed1d66a8b2b4	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_OPERATOR	item_category_edit	t	2020-09-29 06:29:21
7b2e1f45-4776-49db-a913-5e839387a762	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_OPERATOR	item_category_detail	t	2020-09-29 06:29:21
5129b6a4-9b88-4e67-80fa-e683de175e27	9f8a642c-5372-4d29-9fad-7f29e0efe519	ROLE_OPERATOR	item_category_delete	t	2020-09-29 06:29:21
611150e6-0475-4624-ab49-29d53f3318ed	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_index	t	2020-09-29 06:29:21
4d863aa2-cbfd-42dd-bf91-3b7861e24928	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_create	t	2020-09-29 06:29:21
e710beef-2e5d-41e9-98ea-625d26902768	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_data	t	2020-09-29 06:29:21
b9ace4a9-57d4-460d-a814-96352e1f9274	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_edit	t	2020-09-29 06:29:21
f40a1e71-fa27-4e40-bf0e-6af14fc81a1b	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_detail	t	2020-09-29 06:29:21
677c39ed-c117-4b91-9d5e-8b68da48a5be	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_delete	t	2020-09-29 06:29:21
bbf4cc56-e7da-46b8-8360-7ebdd317c929	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_export	t	2020-09-29 06:29:21
f4325e93-655f-4571-83da-f66b98225114	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_import	t	2020-09-29 06:29:21
e74cf131-e8a6-4cc5-9412-0415272978f9	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_template_import	t	2020-09-29 06:29:21
16226da6-f092-4e4a-b97e-7c3cafebf33c	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_price_approval_list	t	2020-09-29 06:29:21
b18323e4-878e-4e38-834a-29b5fa3b40f9	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_package_create	t	2020-09-29 06:29:21
b5a68813-dabf-4a0d-8fd8-fb94c4192a16	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_package_edit	t	2020-09-29 06:29:21
3d1ab234-1a03-4764-83b1-9abb6a8bc71e	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_package_delete	t	2020-09-29 06:29:21
cb8739d5-94b2-4285-85f6-27cb024576f2	dbf3766f-5f23-48ff-ad25-81b681be09fc	ROLE_OPERATOR	item_price_log_approval	t	2020-09-29 06:29:21
a83d8264-0489-4f83-ac74-3a5ab9b23a81	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_OPERATOR	packaging_index	t	2020-09-29 06:29:21
d7a8e324-ddc5-410a-831c-4a45f71396ca	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_OPERATOR	packaging_create	t	2020-09-29 06:29:21
1c685840-d429-416e-99a3-83374626f4cb	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_OPERATOR	packaging_edit	t	2020-09-29 06:29:21
a1e96033-4bcd-4cb4-8441-66205f92017b	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_OPERATOR	packaging_detail	t	2020-09-29 06:29:21
db4d42a0-9529-47d5-8d23-6b3383b53e1b	09e8bedf-f394-4677-9f61-1735a31cf1ba	ROLE_OPERATOR	packaging_delete	t	2020-09-29 06:29:21
e58964ba-7564-4888-9b6e-e6837ece7a5a	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_OPERATOR	supplier_index	t	2020-09-29 06:29:21
5419ec09-e147-4f31-ab42-0bca9d0fde29	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_OPERATOR	supplier_create	t	2020-09-29 06:29:21
0939c738-c786-41d5-8a6c-3f5994c645d7	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_OPERATOR	supplier_edit	t	2020-09-29 06:29:21
59888c0d-da46-4426-9fd6-2ce579f0cdf2	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_OPERATOR	supplier_detail	t	2020-09-29 06:29:21
91eb9b49-b025-44d4-810c-4431408084e1	edcd3e57-9599-4a16-8bcc-5ea514a957eb	ROLE_OPERATOR	supplier_delete	t	2020-09-29 06:29:21
10156dc6-7a05-495f-ab70-a92eec8dd2e0	40d50aac-535e-4ef8-86b3-e1b45969fe86	ROLE_OPERATOR	report_item_stock_index	t	2020-09-29 06:29:21
4273e482-13b1-4e47-a906-ea282e3cfa98	e7c6e8bb-9b8b-4962-818a-3d7fee4245ad	ROLE_OPERATOR	report_unpaid_transaction_index	t	2020-09-29 06:29:21
214d093b-12dc-48f7-98aa-e09bb85359a6	e7c6e8bb-9b8b-4962-818a-3d7fee4245ad	ROLE_OPERATOR	report_unpaid_transaction_print	t	2020-09-29 06:29:21
de62ce1d-00d2-40f5-8906-1cd5736c1bad	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_OPERATOR	purchase_index	t	2020-09-29 06:29:21
31c69161-757b-4180-b4af-801dc6759c12	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_OPERATOR	purchase_create	t	2020-09-29 06:29:21
7cc98ce4-b65a-4c54-b640-c0d7cc9b0a4e	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_OPERATOR	purchase_detail	t	2020-09-29 06:29:21
f5389f37-b50b-4311-b03d-0cd75ce3eeea	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_OPERATOR	purchase_delete	t	2020-09-29 06:29:21
deac3e9d-0fbb-4b24-a0f3-b2fa6e816719	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_OPERATOR	purchase_detail_create	t	2020-09-29 06:29:21
a4ee4b12-5df9-4be5-97a9-1e9d7436f7e9	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_OPERATOR	purchase_detail_edit	t	2020-09-29 06:29:21
bc2deca4-2bc7-485b-b778-65c637b501ec	8e055f35-5321-4775-924a-1fa43637baaa	ROLE_OPERATOR	purchase_detail_delete	t	2020-09-29 06:29:21
c51cc42a-6426-4297-89d6-1fae05960a7c	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_index	t	2020-09-29 06:29:21
49b20caf-1807-4011-9281-ecab86783a6f	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_create	t	2020-09-29 06:29:21
40dbf8ec-0c45-4363-af42-224130023929	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_printout_test	t	2020-09-29 06:29:21
6a11ee67-15c4-4bce-b2c3-cf06addf766d	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_payment	t	2020-09-29 06:29:21
b93a6ee1-12eb-45a2-b321-12a824e8016f	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_printout	t	2020-09-29 06:29:21
f9b7d54e-39fe-43ed-b1ed-1a050118287f	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_detail	t	2020-09-29 06:29:21
587f7cf4-4233-447d-8540-0e5a09e208c1	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_delete	t	2020-09-29 06:29:21
67274724-72c6-4001-b5a6-4c03177acf5b	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_item_create	t	2020-09-29 06:29:21
f2e9a05c-ab0f-4ee1-a9ec-5d84a7a0a9fb	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_item_edit	t	2020-09-29 06:29:21
1e36d609-1e65-4789-b8e5-06a837892045	22f27cf2-4737-4cd6-adcb-537320c3606e	ROLE_OPERATOR	sale_item_delete	t	2020-09-29 06:29:21
57b707c5-9dca-4ce8-b072-db07ab7cc22e	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_OPERATOR	eod_index	t	2020-09-29 06:29:21
1344e9e3-8bf0-443b-88ed-ef726e809bf2	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_OPERATOR	eod_create	t	2020-09-29 06:29:21
df433342-b220-4af8-88b8-8e98b0d63f46	f50ce8d8-25e0-4686-b4ae-b96d0462af54	ROLE_OPERATOR	eod_detail	t	2020-09-29 06:29:21
\.


--
-- Data for Name: kmj_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kmj_user (id, store_id, username, roles, password, name, is_active) FROM stdin;
845865c8-b0a8-4197-8428-51f4e831a824	\N	root	["ROLE_SUPER_USER"]	$argon2id$v=19$m=65536,t=4,p=1$NzZwZkdrWkpqNmpYNklXMw$e+Y6+IvTecsNPO/YesHCT1gWvftGoC1Z/mSC5XbST7Q	ROOT	t
525d8c8e-e754-4640-92b0-d7a736c0f14c	\N	admin	["ROLE_ADMINISTRATOR"]	$argon2id$v=19$m=65536,t=4,p=1$Wm9TZWhMV214ME9TUmEwYQ$2wcDnCiLqY2AsYLrm47b6m4pNB4Uy5bjwPisvcEEQrI	ADMIN	t
391ce93c-10ce-4aa0-b081-f7c28f204e65	567be6e8-f932-4cd7-b70f-3f5689781c64	kepala_01	["ROLE_KEPALA"]	$argon2id$v=19$m=65536,t=4,p=1$LnY4Y0xTd1o3R3dJeGdMaA$DlVhXvlHGiNa8NxGBeamUI8o6FwW/X+Ue8ivobyjXJk	Kepala 01	t
a129f5c0-77f5-4ebf-8ccc-debc1e6f4e26	567be6e8-f932-4cd7-b70f-3f5689781c64	operator_01	["ROLE_OPERATOR"]	$argon2id$v=19$m=65536,t=4,p=1$QXNMS1J4MWxxYTYzdFhYTw$47689MwipibYbGravOGWSU7cLV34jB7BdNAPvn+p8UA	Operator 01	t
4cca120d-3938-4f10-881c-30566d33488b	bf17e7ff-a64f-4b0b-9cdb-90afb4033cea	operator_02	["ROLE_OPERATOR"]	$argon2id$v=19$m=65536,t=4,p=1$c3gzYkRCUmNON0J3V0QuVg$Wf6pjoboM385aYi4CbZKEBjvgzDtfVgiQ2SOacUnw2Q	Operator 02	t
\.


--
-- Data for Name: packaging; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.packaging (id, code, name) FROM stdin;
a4c8a130-cf6a-4a0e-9c44-9cf424bcc0b2	Kg	Kilogram
a1dd44fe-d0b6-47fe-b0ab-814c8adf6b52	gram	Gram
55ca7641-6767-4c32-806e-e72210e17068	Lt	liter
c21daabc-8ea2-430f-986a-60f4c87e769c	Dus	Dus
bdc0e6e1-5788-4b33-8adc-380e9e21dd94	Pack	Pack
7114b595-ae1e-4b0c-81f2-9128d228e434	Pcs	Pcs
\.


--
-- Data for Name: purchase; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.purchase (id, supplier_id, code, purchase_at, total, description, is_locked, checked_at) FROM stdin;
92c4ea11-5574-4bef-af3d-947854cf6f38	6838c151-22bd-4c2b-8e87-8ce4d084ae96	01	2020-11-07	600000	Pembelian Materai	t	\N
\.


--
-- Data for Name: purchase_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.purchase_detail (id, item_id, purchase_id, packaging_id, quantity, price, total, tax) FROM stdin;
3f133c07-5b48-428c-82ff-c39f74bc4eb6	c280399a-dd0f-487a-93c2-93d631d82782	92c4ea11-5574-4bef-af3d-947854cf6f38	7114b595-ae1e-4b0c-81f2-9128d228e434	100	6000	600000	0
\.


--
-- Data for Name: sale; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sale (id, customer_id, store_id, code, created_at, created_by, sub_total, tax, total, is_locked, discount, payment, payment_change, checked_at) FROM stdin;
f36288b7-7e8c-492a-80d5-2e128da5429c	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0008/SL/10/2020	2020-10-01 03:00:47	operator_01	7000	0	7000	t	0	10000	3000	2020-10-01 07:01:33
b8fbe099-7966-495b-9898-97095c92344c	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0032/SL/10/2020	2020-10-08 04:15:43	operator_01	2500	0	2500	t	0	2500	0	2020-10-08 07:06:10
d0056917-e1f2-40f3-b00b-7ec8dc60ab99	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0011/SL/10/2020	2020-10-01 04:03:57	operator_01	5000	0	5000	t	0	10000	5000	2020-10-01 07:01:33
8cb99df5-c9d6-42bc-a8b9-f0ef1cf746bf	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0013/SL/10/2020	2020-10-02 03:40:03	operator_01	76500	0	76500	t	0	76500	0	\N
5a810123-5f72-47c5-a8be-c2243c6299ec	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0023/SL/10/2020	2020-10-06 03:20:57	operator_01	2500	0	2500	t	0	2500	0	2020-10-06 07:10:51
531a34cf-0bca-4048-afbd-fe076434d07f	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0024/SL/10/2020	2020-10-06 03:48:07	operator_01	15000	0	15000	t	0	15000	0	2020-10-06 07:10:51
a9dc9c7d-45f5-41c2-828a-8ca8e2c6d81f	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0019/SL/10/2020	2020-10-05 03:40:25	operator_01	2500	0	2500	t	0	5000	2500	2020-10-05 07:32:13
0a1c725b-9ff4-4f0c-9ebb-fb845d4fd0bf	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0020/SL/10/2020	2020-10-05 03:48:10	operator_01	2500	0	2500	t	0	5000	2500	2020-10-05 07:32:13
1ab69d34-f224-4db4-9168-acfa610f41ec	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0016/SL/10/2020	2020-10-03 03:54:17	operator_01	67500	0	67500	t	0	67500	0	2020-10-03 06:38:07
327984d5-f8e9-4129-a8b4-4c4c98d58d94	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0018/SL/10/2020	2020-10-05 03:32:14	operator_01	58500	0	58500	t	0	58500	0	2020-10-05 07:32:13
8b6e1310-38f7-428c-9083-0f8b2788f242	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0030/SL/10/2020	2020-10-07 06:08:12	operator_01	9500	0	9500	t	0	10000	500	2020-10-07 07:12:20
bfbae5d4-a28c-4299-abf5-0114345335dc	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0009/SL/10/2020	2020-10-01 03:29:01	operator_01	54500	0	54500	t	0	54500	0	2020-10-01 07:01:33
ca1e9e43-b861-4754-b87a-c857e49886b9	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0014/SL/10/2020	2020-10-03 02:43:57	operator_01	58500	0	58500	t	0	58500	0	2020-10-03 06:38:07
48f03452-a5fd-4f3c-bed0-8be47ee0abb1	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0015/SL/10/2020	2020-10-03 02:45:09	operator_01	9000	0	9000	t	0	9000	0	2020-10-03 06:38:07
0026e96e-412c-46d6-b41e-e2fcecf7bf1a	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0034/SL/10/2020	2020-10-08 04:54:18	operator_01	4500	0	4500	t	0	4500	0	2020-10-08 07:06:10
1c7749d1-1999-4c5c-a2b8-045a2743071b	eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	567be6e8-f932-4cd7-b70f-3f5689781c64	0010/SL/10/2020	2020-10-01 03:41:34	operator_01	25500	0	25500	t	0	25500	0	2020-10-01 07:01:33
4835109e-3c56-4c7b-a6d3-c35e4d504a3a	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0007/SL/09/2020	2020-09-30 03:22:06	operator_01	1000	0	1000	t	0	1000	0	2020-09-30 07:32:27
31dc67cd-7690-4994-b73f-021a74fead7f	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0001/SL/09/2020	2020-09-29 04:43:47	operator_01	10000	0	10000	t	0	10000	0	2020-09-29 07:23:21
c6824723-fc32-4429-acc9-9f3caf7ca12c	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0003/SL/09/2020	2020-09-29 06:04:42	operator_01	19000	0	19000	t	0	19000	0	2020-09-29 07:23:21
ea18205c-bc24-4823-a9df-bbd61fda5466	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0004/SL/09/2020	2020-09-29 06:32:30	operator_01	10000	0	10000	t	0	10000	0	2020-09-29 07:23:21
7c7466d3-fef0-428f-b557-bbcdb55a685c	eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	567be6e8-f932-4cd7-b70f-3f5689781c64	0005/SL/09/2020	2020-09-29 06:43:21	operator_01	117000	0	117000	t	0	117000	0	2020-09-29 07:23:21
4c8623c3-d17f-42c2-9e1f-8a870e0feb93	a5bf898e-ade5-4b48-b6eb-e1dc142eb36a	567be6e8-f932-4cd7-b70f-3f5689781c64	0002/SL/09/2020	2020-09-29 05:45:34	operator_01	4000	0	4000	t	0	4000	0	2020-09-29 07:23:21
93266af6-09a5-484c-a034-0f0d72c2b6c3	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0006/SL/09/2020	2020-09-29 06:44:09	operator_01	13500	0	13500	t	0	13500	0	2020-09-29 07:23:21
3fbb267e-1b02-4462-a79b-32bb4533e15f	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0017/SL/10/2020	2020-10-03 04:45:05	operator_01	1500	0	1500	t	0	1500	0	2020-10-03 06:38:07
3fbd0298-406c-424c-91b0-487250b39b89	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0028/SL/10/2020	2020-10-06 05:56:34	operator_01	9000	0	9000	t	0	9000	0	2020-10-06 07:10:51
09f1c481-d337-4884-8c85-7e6a01087f30	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0035/SL/10/2020	2020-10-09 03:05:22	operator_01	8000	0	8000	t	0	8000	0	2020-10-09 05:17:45
27e6f569-3cb5-4e11-a70d-930ab5735972	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0025/SL/10/2020	2020-10-06 04:43:04	operator_01	5000	0	5000	t	0	5000	0	2020-10-06 07:10:51
eea1c289-034b-445c-baa2-7778fb21bb5a	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0021/SL/10/2020	2020-10-06 03:11:49	operator_01	2500	0	2500	t	0	2500	0	2020-10-06 07:10:51
d755ede0-fb1d-4d20-a77d-453db7888a48	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0026/SL/10/2020	2020-10-06 04:58:47	operator_01	3000	0	3000	t	0	5000	2000	2020-10-06 07:10:51
d83db3f9-36fe-49b0-b677-4b6207ef4485	eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	567be6e8-f932-4cd7-b70f-3f5689781c64	0022/SL/10/2020	2020-10-06 03:17:46	operator_01	148500	0	148500	t	0	148500	0	2020-10-06 07:10:51
7cd95bb8-f94b-4a68-a2be-68ede467cd1f	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0027/SL/10/2020	2020-10-06 05:07:05	operator_01	3000	0	3000	t	0	5000	2000	2020-10-06 07:10:51
c3b0d9c1-7b36-4adc-8dc7-735e9b3ff7fe	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0036/SL/10/2020	2020-10-09 04:12:07	operator_01	3000	0	3000	t	0	3000	0	2020-10-09 05:17:45
ed317685-ed8b-49f4-bbb1-6d6115c16058	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0033/SL/10/2020	2020-10-08 04:32:19	operator_01	2500	0	2500	t	0	2500	0	2020-10-08 07:06:10
a4d84319-9659-458a-929c-132d979a1c7f	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0012/SL/10/2020	2020-10-01 05:00:09	operator_01	54500	0	54500	t	0	54500	0	2020-10-01 07:01:33
b2e69c44-6364-4cc1-849b-b7a93232062f	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0031/SL/10/2020	2020-10-08 03:05:00	operator_01	54500	0	54500	t	0	54500	0	2020-10-08 07:06:10
3bb502d5-a167-454f-a965-88eeb0492f44	a5bf898e-ade5-4b48-b6eb-e1dc142eb36a	567be6e8-f932-4cd7-b70f-3f5689781c64	0038/SL/10/2020	2020-10-12 07:06:33	operator_01	10000	0	10000	t	0	10000	0	2020-10-12 07:10:46
ff78a0fb-fd0e-4379-a8a4-42b1889c4192	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0029/SL/10/2020	2020-10-07 05:23:53	operator_01	13000	0	13000	t	0	13000	0	2020-10-07 07:12:20
bd0af098-bf6b-460d-95a7-c7b444d90bce	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0037/SL/10/2020	2020-10-12 07:05:30	operator_01	5000	0	5000	t	0	5000	0	2020-10-12 07:10:46
9cf6d54c-9c3c-4ad8-a811-bfb821bba144	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0039/SL/10/2020	2020-10-12 07:07:46	operator_01	2000	0	2000	t	0	2000	0	2020-10-12 07:10:46
c6d2411f-9ae9-4d63-a467-1e3de28aa4a8	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0040/SL/10/2020	2020-10-20 03:14:56	operator_01	3000	0	3000	t	0	3000	0	2020-10-20 07:33:00
d7372ae6-5c8c-4c3d-bbb9-0955436e33a6	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0050/SL/11/2020	2020-11-06 01:38:36	operator_01	20000	0	20000	t	0	20000	0	2020-11-06 04:14:06
fb375427-3962-4442-ab2d-3f37fd7e7853	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0051/SL/11/2020	2020-11-06 04:10:58	operator_01	2000	0	2000	t	0	2000	0	2020-11-06 04:14:06
b4d9308c-8997-4e15-9f41-91b8fd144d10	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0052/SL/11/2020	2020-11-06 04:11:35	operator_01	54500	0	54500	t	0	0	-54500	2020-11-06 04:14:06
a77396a5-a1f6-4903-8162-c52e67d7cc18	eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	567be6e8-f932-4cd7-b70f-3f5689781c64	0042/SL/10/2020	2020-10-20 03:16:18	operator_01	7000	0	7000	t	0	7000	0	2020-10-20 07:33:00
9771e5d9-5f22-4b22-9064-f76b1272382d	87f2ba43-0e55-4c31-bed7-9872dd4f9474	567be6e8-f932-4cd7-b70f-3f5689781c64	0058/SL/11/2020	2020-11-12 03:42:05	operator_01	49000	0	49000	t	0	49000	0	2020-11-12 06:26:06
4081e16f-e52e-41b8-ac4b-974cbf9872b9	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0060/SL/11/2020	2020-11-17 02:28:40	operator_01	13000	0	13000	t	0	0	-13000	2020-11-17 05:28:11
442b192d-e091-4693-9825-fed8ea3db474	eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	567be6e8-f932-4cd7-b70f-3f5689781c64	0045/SL/10/2020	2020-10-22 03:03:30	operator_01	175500	0	175500	t	0	175500	0	2020-10-22 07:13:10
a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0063/SL/11/2020	2020-11-21 02:51:43	operator_01	88500	0	88500	t	0	88500	0	2020-11-21 04:43:40
5922311e-4218-4e84-babe-e28bf52ff4a8	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0046/SL/10/2020	2020-10-22 04:46:02	operator_01	58500	0	58500	t	0	58500	0	2020-10-22 07:13:10
123fa238-3eaf-4129-b7e9-4a982b919f66	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0043/SL/10/2020	2020-10-20 03:16:55	operator_01	151500	0	151500	t	0	151500	0	2020-10-20 07:33:00
b769a347-2475-4589-ada3-835e6effc3b5	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0047/SL/10/2020	2020-10-23 05:42:32	operator_01	6000	0	6000	t	0	6000	0	2020-10-23 05:51:00
ac11f8f8-1b02-49dc-a3e5-a83c4f709721	a5bf898e-ade5-4b48-b6eb-e1dc142eb36a	567be6e8-f932-4cd7-b70f-3f5689781c64	0044/SL/10/2020	2020-10-20 05:17:32	operator_01	10000	0	10000	t	0	10000	0	2020-10-20 07:33:00
6ca68ab0-cf99-48fb-b618-f099caca314e	e82d3076-0885-4f2c-b584-b07ed1bf9a56	567be6e8-f932-4cd7-b70f-3f5689781c64	0041/SL/10/2020	2020-10-20 03:15:49	operator_01	117000	0	117000	t	0	117000	0	2020-10-20 07:33:00
1d74c315-bee1-4738-9870-08a68234f287	a5bf898e-ade5-4b48-b6eb-e1dc142eb36a	567be6e8-f932-4cd7-b70f-3f5689781c64	0062/SL/11/2020	2020-11-17 02:30:01	operator_01	64000	0	64000	t	0	0	-64000	2020-11-17 05:28:11
8f335ba1-6fcb-4bd1-abf8-0c2103da63a0	\N	567be6e8-f932-4cd7-b70f-3f5689781c64	0064/SL/11/2020	2020-11-21 03:00:05	operator_01	70000	0	70000	t	0	70000	0	2020-11-21 04:43:40
f88bc49b-a811-4746-9090-b7094e33a6b8	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0065/SL/11/2020	2020-11-21 03:10:38	operator_01	57500	0	57500	t	0	0	-57500	2020-11-21 04:43:40
e15c65c2-65b8-44bd-9ffa-8d939394c119	eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	567be6e8-f932-4cd7-b70f-3f5689781c64	0061/SL/11/2020	2020-11-17 02:29:14	operator_01	34000	0	34000	t	0	34000	0	2020-11-17 05:28:11
a5a6a172-3756-4a55-9ae2-3d2d31df578f	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0048/SL/11/2020	2020-11-06 01:31:29	operator_01	28000	0	28000	t	0	28000	0	2020-11-06 04:14:06
87e72dbd-447d-4a99-8130-9be5bc473876	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0049/SL/11/2020	2020-11-06 01:34:19	operator_01	133000	0	133000	t	0	0	-133000	2020-11-06 04:14:06
8e1f8145-3447-4977-9661-b375fd4144bb	eb7ad1a8-69ef-40c7-a90b-f2d3c6185e93	567be6e8-f932-4cd7-b70f-3f5689781c64	0056/SL/11/2020	2020-11-12 03:34:09	operator_01	58000	0	58000	t	0	58000	0	2020-11-12 06:26:06
084d45a7-c16c-4d53-9275-db690cce5398	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0053/SL/11/2020	2020-11-12 03:24:48	operator_01	58500	0	58500	t	0	58500	0	2020-11-12 06:26:06
389ed395-cd13-4985-9a5d-6429c6538ff3	a5bf898e-ade5-4b48-b6eb-e1dc142eb36a	567be6e8-f932-4cd7-b70f-3f5689781c64	0054/SL/11/2020	2020-11-12 03:30:17	operator_01	176000	0	176000	t	0	0	-176000	2020-11-12 06:26:06
fc69fd89-2966-4b9e-97ce-0541693aed6a	e82d3076-0885-4f2c-b584-b07ed1bf9a56	567be6e8-f932-4cd7-b70f-3f5689781c64	0055/SL/11/2020	2020-11-12 03:33:37	operator_01	54500	0	54500	t	0	0	-54500	2020-11-12 06:26:06
8706bf81-bd8d-437d-b67a-72c98cd8dae4	6ce67b8c-e974-49ce-800d-334068591f48	567be6e8-f932-4cd7-b70f-3f5689781c64	0057/SL/11/2020	2020-11-12 03:36:28	operator_01	236500	0	236500	t	0	0	-236500	2020-11-12 06:26:06
c5e62460-88f5-4013-9eb6-b5b2e9c995e9	9a2bc806-dd1c-4fb3-8815-1f3d5ca95733	567be6e8-f932-4cd7-b70f-3f5689781c64	0059/SL/11/2020	2020-11-17 02:25:15	operator_01	69500	0	69500	t	0	69500	0	2020-11-17 05:28:11
\.


--
-- Data for Name: sale_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sale_item (id, item_id, sale_id, quantity, principal_price, sale_price, tax, sub_total, total, discount) FROM stdin;
2b85630d-cad2-4092-8026-1fb1f855db90	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	31dc67cd-7690-4994-b73f-021a74fead7f	5	1400	2000	0	10000	10000	\N
5ab298b9-517c-4d5d-9334-577af35fc06c	0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	4c8623c3-d17f-42c2-9e1f-8a870e0feb93	1	1400	2000	0	2000	2000	\N
d6a3446f-38b6-4a27-b856-cef9d389b7b2	f54c7cdd-d5d5-4237-acbf-b0d87fecd5e9	4c8623c3-d17f-42c2-9e1f-8a870e0feb93	1	1400	2000	0	2000	2000	\N
086ef92b-4360-4980-add7-02e2576b0760	6c999ab5-1af9-41f3-b2f7-cd1d6bc186f7	c6824723-fc32-4429-acc9-9f3caf7ca12c	1	13300	19000	0	19000	19000	\N
756aa72f-1772-4f94-8962-bdfa9d82e279	b58e0a50-2e90-4527-9aab-812dbdcc7f02	ea18205c-bc24-4823-a9df-bbd61fda5466	1	7000	10000	0	10000	10000	\N
3e5b7287-0a00-4ac7-b38d-c32cabc41207	f9d4e754-8efe-43ff-bb7a-cad985bed182	7c7466d3-fef0-428f-b557-bbcdb55a685c	2	40950	58500	0	117000	117000	\N
28f1dcc0-0a07-444f-b4f4-f512db1132c2	2dbfef19-f0b5-4fa6-903c-67b818864e6a	93266af6-09a5-484c-a034-0f0d72c2b6c3	1	9450	13500	0	13500	13500	\N
29e52b36-e8ab-4e26-ab1d-af9b29811d26	3d031565-23f8-431c-a867-bbf3a17a69da	4835109e-3c56-4c7b-a6d3-c35e4d504a3a	1	700	1000	0	1000	1000	\N
27e3c627-8d10-4b61-a5dd-5668e2ce57d6	788a1deb-c1d9-449d-b07e-d800e4feaab3	f36288b7-7e8c-492a-80d5-2e128da5429c	2	1750	2500	0	5000	5000	\N
146b64ff-63b9-41b5-8cdd-d67554af15ee	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	f36288b7-7e8c-492a-80d5-2e128da5429c	1	1400	2000	0	2000	2000	\N
4690cb23-6131-4360-a302-5512742c84ad	4a478acc-84aa-4340-9839-32ba0505a2d8	bfbae5d4-a28c-4299-abf5-0114345335dc	1	38150	54500	0	54500	54500	\N
18db9580-fb26-45e0-96ea-256073c8c03c	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	1c7749d1-1999-4c5c-a2b8-045a2743071b	1	17850	25500	0	25500	25500	\N
0e9ecf30-3120-4f02-bc5a-ec39a2acfad9	2a3db456-1c32-4bbe-bcaa-e6f303c40401	d0056917-e1f2-40f3-b00b-7ec8dc60ab99	10	350	500	0	5000	5000	\N
5468c9e1-5544-4abf-9f8c-09b241957b26	4a478acc-84aa-4340-9839-32ba0505a2d8	a4d84319-9659-458a-929c-132d979a1c7f	1	38150	54500	0	54500	54500	\N
bb14127f-0376-474f-aa4a-f741cd7ef469	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	8cb99df5-c9d6-42bc-a8b9-f0ef1cf746bf	3	17850	25500	0	76500	76500	\N
24858c42-5e27-4840-a925-673efde0173c	f9d4e754-8efe-43ff-bb7a-cad985bed182	ca1e9e43-b861-4754-b87a-c857e49886b9	1	40950	58500	0	58500	58500	\N
b9fc8fe8-c517-4fdc-bf3b-faf9eaf16eb0	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	48f03452-a5fd-4f3c-bed0-8be47ee0abb1	1	2100	3000	0	3000	3000	\N
801d6efa-120d-4f45-8ae5-d3049d1a1257	14397549-e408-4470-979a-8e794351086e	48f03452-a5fd-4f3c-bed0-8be47ee0abb1	1	1750	2500	0	2500	2500	\N
e72b07e2-56b3-4feb-8884-34e0224795a2	8a758252-d9fd-4d16-8b56-a94382216abd	48f03452-a5fd-4f3c-bed0-8be47ee0abb1	1	2450	3500	0	3500	3500	\N
b2b14fda-0dcf-458c-9e6c-ab9e34ac717f	2dbfef19-f0b5-4fa6-903c-67b818864e6a	1ab69d34-f224-4db4-9168-acfa610f41ec	5	9450	13500	0	67500	67500	\N
7a73e18d-8db7-48d5-9047-a2c578b5e305	77ea7188-2457-4523-999f-a1812416c570	3fbb267e-1b02-4462-a79b-32bb4533e15f	1	1050	1500	0	1500	1500	\N
9d31028d-be19-4ab7-9de3-e041ffa56b41	f9d4e754-8efe-43ff-bb7a-cad985bed182	327984d5-f8e9-4129-a8b4-4c4c98d58d94	1	40950	58500	0	58500	58500	\N
f80612c3-03bc-4a39-b190-f01ce709dc11	4ca13666-5ad0-4357-9355-6be3323214fa	a9dc9c7d-45f5-41c2-828a-8ca8e2c6d81f	1	1750	2500	0	2500	2500	\N
e1100a5c-df8e-4f01-9feb-89e9475e900a	4ca13666-5ad0-4357-9355-6be3323214fa	0a1c725b-9ff4-4f0c-9ebb-fb845d4fd0bf	1	1750	2500	0	2500	2500	\N
25aae80c-be3e-4972-a1c0-71680f18eb91	eb82db1e-8ca7-4145-a22c-256eca2a6241	eea1c289-034b-445c-baa2-7778fb21bb5a	1	1750	2500	0	2500	2500	\N
31e425e3-4c63-4123-9812-2239e4001841	f9d4e754-8efe-43ff-bb7a-cad985bed182	d83db3f9-36fe-49b0-b677-4b6207ef4485	2	40950	58500	0	117000	117000	\N
a2558d64-d6d2-4594-9027-de1d7c90cc82	37e21335-7f61-4d38-bd49-e89233dfcae1	d83db3f9-36fe-49b0-b677-4b6207ef4485	2	2450	3500	0	7000	7000	\N
18c0612d-aa91-4201-9fc3-51fde9c44716	ce2cad02-0a94-4138-bd11-fae5f15376b1	d83db3f9-36fe-49b0-b677-4b6207ef4485	1	17150	24500	0	24500	24500	\N
b0d8538b-333c-4c9f-a462-ee0e0f9a4490	b67980a9-de04-4973-b7dc-bc61242c8f38	5a810123-5f72-47c5-a8be-c2243c6299ec	1	1750	2500	0	2500	2500	\N
9e9620f0-b64f-4d31-aff0-25cd6bd6eae9	7df4ed86-c958-41e0-96ef-c2059d9ebae4	531a34cf-0bca-4048-afbd-fe076434d07f	1	10500	15000	0	15000	15000	\N
c2215afe-7b6e-4dc5-9c47-04c88625f35b	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	27e6f569-3cb5-4e11-a70d-930ab5735972	1	1400	2000	0	2000	2000	\N
637412ef-9a54-43b2-a262-ed45ad5c4c3d	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	27e6f569-3cb5-4e11-a70d-930ab5735972	1	2100	3000	0	3000	3000	\N
37ce73f0-aafe-4066-b36f-8c1b57113424	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	d755ede0-fb1d-4d20-a77d-453db7888a48	1	2100	3000	0	3000	3000	\N
965b3290-5c74-4c4d-b8d7-07b3d53d09a5	805f19d0-7aff-4694-818a-3e5165d146ef	7cd95bb8-f94b-4a68-a2be-68ede467cd1f	1	2100	3000	0	3000	3000	\N
efe9a1e5-feec-40e9-9794-68ea0c06e502	1fc50d3b-148a-4492-b499-d675c202d459	3fbd0298-406c-424c-91b0-487250b39b89	1	6300	9000	0	9000	9000	\N
a80d55c4-eef0-4beb-b33f-a5ffa3d5b9c0	171fabf2-d96a-4423-8ee2-9626efbbd16e	ff78a0fb-fd0e-4379-a8a4-42b1889c4192	1	9100	13000	0	13000	13000	\N
a0df57b4-5bce-45cb-b5ce-d8d4cfc23236	805f19d0-7aff-4694-818a-3e5165d146ef	8b6e1310-38f7-428c-9083-0f8b2788f242	1	2100	3000	0	3000	3000	\N
6fc39e8e-3e4c-4098-bb76-cc08ee4ef8ae	689391ef-c406-4d68-accf-2cdf8e3f2c82	8b6e1310-38f7-428c-9083-0f8b2788f242	1	700	1000	0	1000	1000	\N
1cdaafd6-a44d-443c-a53a-c5605704b930	0710bf62-a460-477c-a8f9-b9386d9b457f	8b6e1310-38f7-428c-9083-0f8b2788f242	1	2100	3000	0	3000	3000	\N
0142427a-baef-4f30-b070-6bbf1530162e	14397549-e408-4470-979a-8e794351086e	8b6e1310-38f7-428c-9083-0f8b2788f242	1	1750	2500	0	2500	2500	\N
9b2f86ff-f98d-471e-a077-04fbf6378230	9668ff45-83d7-466d-a266-9c4d54eb2993	b2e69c44-6364-4cc1-849b-b7a93232062f	1	38150	54500	0	54500	54500	\N
6e5cabb7-d19b-4727-981f-407096666138	914edc82-ac6f-47e2-8a37-4693f562e586	b8fbe099-7966-495b-9898-97095c92344c	1	1750	2500	0	2500	2500	\N
a7ef662b-3cc3-4963-ae87-796d5b13add4	5556e2f3-a8c0-441d-b696-07ea76048743	ed317685-ed8b-49f4-bbb1-6d6115c16058	1	1750	2500	0	2500	2500	\N
c08a6568-294e-49fe-975e-21c81f12cfc5	5770e029-af52-4630-840c-0703653586ce	0026e96e-412c-46d6-b41e-e2fcecf7bf1a	1	3150	4500	0	4500	4500	\N
365b5d05-7185-48d0-828e-a745202d0626	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	09f1c481-d337-4884-8c85-7e6a01087f30	1	2100	3000	0	3000	3000	\N
776f6aa5-5be9-43a3-b62e-f214427163fc	aa8051af-fcf7-43ea-9449-c61ab4597017	09f1c481-d337-4884-8c85-7e6a01087f30	1	2100	3000	0	3000	3000	\N
80574af1-ee25-4c6f-b9de-ca24b8a202a9	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	09f1c481-d337-4884-8c85-7e6a01087f30	1	1400	2000	0	2000	2000	\N
9d2a9663-8c62-404a-a636-3e4dd215cb72	aa8051af-fcf7-43ea-9449-c61ab4597017	c3b0d9c1-7b36-4adc-8dc7-735e9b3ff7fe	1	2100	3000	0	3000	3000	\N
6e44398c-459c-4758-bcbf-1aba92e08b54	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	bd0af098-bf6b-460d-95a7-c7b444d90bce	1	2100	3000	0	3000	3000	\N
2d931a0b-ee62-4426-8cd7-8eaf4250f850	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	bd0af098-bf6b-460d-95a7-c7b444d90bce	1	1400	2000	0	2000	2000	\N
d3bcafaf-b9a3-45b6-ba07-26f832fcea8d	b58e0a50-2e90-4527-9aab-812dbdcc7f02	3bb502d5-a167-454f-a965-88eeb0492f44	1	7000	10000	0	10000	10000	\N
5740f06f-3e97-41a2-8c48-8f4e8626ab48	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	9cf6d54c-9c3c-4ad8-a811-bfb821bba144	1	1400	2000	0	2000	2000	\N
5d185d76-deb7-4d39-b733-e9b48874edc0	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	c6d2411f-9ae9-4d63-a467-1e3de28aa4a8	1	2100	3000	0	3000	3000	\N
de5e3949-21fd-410a-a91d-fe3c10bbe572	f9d4e754-8efe-43ff-bb7a-cad985bed182	6ca68ab0-cf99-48fb-b618-f099caca314e	2	40950	58500	0	117000	117000	\N
c86cdfaf-ac98-4b7c-bb14-2a0cb4a8a588	e662ecc3-8c27-44e7-92a1-ff1e7b277ac4	a77396a5-a1f6-4903-8162-c52e67d7cc18	1	4900	7000	0	7000	7000	\N
2967a83e-2e3e-4b51-8160-47306b04c0bc	3d031565-23f8-431c-a867-bbf3a17a69da	123fa238-3eaf-4129-b7e9-4a982b919f66	50	700	1000	0	50000	50000	\N
72a00f43-95da-4905-8875-f5688a75fc28	aa8051af-fcf7-43ea-9449-c61ab4597017	123fa238-3eaf-4129-b7e9-4a982b919f66	12	2100	3000	0	36000	36000	\N
05ec29f2-a0e3-423a-84f3-e16ce617ecd5	d234256e-925c-4d1b-8426-c843d9f395aa	123fa238-3eaf-4129-b7e9-4a982b919f66	1	700	1000	0	1000	1000	\N
9f0a4810-dfd1-4de7-a2c7-90f59de23de5	10327621-aaad-460d-b944-daa8c8a9a927	123fa238-3eaf-4129-b7e9-4a982b919f66	1	1050	1500	0	1500	1500	\N
0a7b96b7-c3d8-4ec1-a509-86522d20f751	d566b1d8-efbd-4a86-9943-9514d141175d	123fa238-3eaf-4129-b7e9-4a982b919f66	1	4900	7000	0	7000	7000	\N
aabb96ad-8721-4f0b-bdab-fb035540729e	10327621-aaad-460d-b944-daa8c8a9a927	123fa238-3eaf-4129-b7e9-4a982b919f66	1	1050	1500	0	1500	1500	\N
63a4cc6f-c8fa-4c05-8ddc-8ba39aa2de14	9668ff45-83d7-466d-a266-9c4d54eb2993	123fa238-3eaf-4129-b7e9-4a982b919f66	1	38150	54500	0	54500	54500	\N
ef9fbdbd-0b38-4d62-a8ab-348d83eeb5d7	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	ac11f8f8-1b02-49dc-a3e5-a83c4f709721	4	1750	2500	0	10000	10000	\N
9034dec3-738b-4e01-a0c1-c9be54589dad	f9d4e754-8efe-43ff-bb7a-cad985bed182	442b192d-e091-4693-9825-fed8ea3db474	3	40950	58500	0	175500	175500	\N
2ef09636-2e6b-448d-985d-d9d23d0ab9e4	f9d4e754-8efe-43ff-bb7a-cad985bed182	5922311e-4218-4e84-babe-e28bf52ff4a8	1	40950	58500	0	58500	58500	\N
85c87643-335a-4be0-9016-863c06593e34	aa8051af-fcf7-43ea-9449-c61ab4597017	b769a347-2475-4589-ada3-835e6effc3b5	2	2100	3000	0	6000	6000	\N
67a6c988-fd6c-448c-94df-7515cbac12f8	788a1deb-c1d9-449d-b07e-d800e4feaab3	a5a6a172-3756-4a55-9ae2-3d2d31df578f	4	1750	2500	0	10000	10000	\N
81028238-9a52-4b68-9527-818d1aca398a	17a6b989-7718-4487-94a3-651d019a5a4a	a5a6a172-3756-4a55-9ae2-3d2d31df578f	1	3850	5500	0	5500	5500	\N
a61fb3dd-5e81-47ec-936c-f6aa30ca1dac	d31957df-bf1f-45bf-a931-39635432e566	a5a6a172-3756-4a55-9ae2-3d2d31df578f	1	1750	2500	0	2500	2500	\N
fc09d49e-7062-48c0-9786-6f79a8480a49	aa8051af-fcf7-43ea-9449-c61ab4597017	a5a6a172-3756-4a55-9ae2-3d2d31df578f	1	2100	3000	0	3000	3000	\N
33419aa8-0b0f-49ae-8f64-b0b250413390	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	a5a6a172-3756-4a55-9ae2-3d2d31df578f	1	1400	2000	0	2000	2000	\N
fb65f1bf-db4c-4e3c-9e4e-30e531794378	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	a5a6a172-3756-4a55-9ae2-3d2d31df578f	1	2100	3000	0	3000	3000	\N
a10d230d-4729-47af-9e10-61d4839b19ab	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	a5a6a172-3756-4a55-9ae2-3d2d31df578f	1	1400	2000	0	2000	2000	\N
1cba2401-ec16-43ce-b168-6c250c2f2330	2aeb8827-643d-4b9d-885a-577ad3de919c	87e72dbd-447d-4a99-8130-9be5bc473876	1	7000	10000	0	10000	10000	\N
85e61d7c-a9e3-40e1-9837-0f0c43438297	9668ff45-83d7-466d-a266-9c4d54eb2993	87e72dbd-447d-4a99-8130-9be5bc473876	1	38150	54500	0	54500	54500	\N
0b7d1cce-43d3-4d86-88d3-323e57f2515b	61a18df1-774f-49b6-8f49-7cb2682b3fd6	87e72dbd-447d-4a99-8130-9be5bc473876	1	7000	10000	0	10000	10000	\N
441c75f3-01fb-4905-9106-333e2c26a4b1	f9d4e754-8efe-43ff-bb7a-cad985bed182	87e72dbd-447d-4a99-8130-9be5bc473876	1	40950	58500	0	58500	58500	\N
aee814a7-55f5-4189-ada3-4346a5e4aaec	b58e0a50-2e90-4527-9aab-812dbdcc7f02	d7372ae6-5c8c-4c3d-bbb9-0955436e33a6	2	7000	10000	0	20000	20000	\N
f7ee1f1a-f91e-4abe-8ee8-6157d2438a5c	b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	fb375427-3962-4442-ab2d-3f37fd7e7853	1	1400	2000	0	2000	2000	\N
97613694-dd88-4066-8430-e750f4b55206	9668ff45-83d7-466d-a266-9c4d54eb2993	b4d9308c-8997-4e15-9f41-91b8fd144d10	1	38150	54500	0	54500	54500	\N
9ce272f5-70c8-472d-8c0b-f55530d1bf8b	78561982-b3fa-45d6-94c6-3b99b397c756	084d45a7-c16c-4d53-9275-db690cce5398	2	700	1000	0	2000	2000	\N
339e3c9e-ae49-44df-8b9a-37b1371d8909	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	084d45a7-c16c-4d53-9275-db690cce5398	3	2100	3000	0	9000	9000	\N
eba60025-f469-441c-84d3-52ec5bb9c6ce	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	084d45a7-c16c-4d53-9275-db690cce5398	1	1400	2000	0	2000	2000	\N
146fd5a1-d582-4b9f-abb1-5a618b7a93a0	788a1deb-c1d9-449d-b07e-d800e4feaab3	084d45a7-c16c-4d53-9275-db690cce5398	6	1750	2500	0	15000	15000	\N
7f97f811-93bb-4100-b78c-5842317654b5	788a1deb-c1d9-449d-b07e-d800e4feaab3	084d45a7-c16c-4d53-9275-db690cce5398	6	1750	2500	0	15000	15000	\N
afd0cbab-693b-4f72-aa74-7bd7eaee3e3c	60f0ecfe-0c71-4b43-a002-de6e2994e076	084d45a7-c16c-4d53-9275-db690cce5398	1	2100	3000	0	3000	3000	\N
c1d57688-9580-438b-8ca7-dc7a38482653	06a0cade-0e05-456a-b408-ba1842c95f18	084d45a7-c16c-4d53-9275-db690cce5398	1	1400	2000	0	2000	2000	\N
d7cb73a1-8e3a-4516-a2bb-af4e94bc4d35	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	084d45a7-c16c-4d53-9275-db690cce5398	1	1400	2000	0	2000	2000	\N
d39a2d22-7aa0-46de-9a24-003a6c50d686	a710f31b-9840-46a3-a903-0bed2ad13807	084d45a7-c16c-4d53-9275-db690cce5398	1	3850	5500	0	5500	5500	\N
1de280ca-41c3-4b1b-8a32-33f9d6b0a652	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	084d45a7-c16c-4d53-9275-db690cce5398	1	2100	3000	0	3000	3000	\N
581f57c2-78bf-4833-9a03-e879fb12cf8b	f9d4e754-8efe-43ff-bb7a-cad985bed182	389ed395-cd13-4985-9a5d-6429c6538ff3	2	40950	58500	0	117000	117000	\N
db20f13e-1680-4930-a274-a4a72e871b9a	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	389ed395-cd13-4985-9a5d-6429c6538ff3	12	1400	2000	0	24000	24000	\N
0d968b2f-666f-45ad-a9d3-1e2d138598de	788a1deb-c1d9-449d-b07e-d800e4feaab3	389ed395-cd13-4985-9a5d-6429c6538ff3	12	1750	2500	0	30000	30000	\N
499505a9-ad05-481f-bfbf-3baeceee0f09	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	389ed395-cd13-4985-9a5d-6429c6538ff3	2	1750	2500	0	5000	5000	\N
3c9c2463-b7a2-4e78-adfc-9a4340d4e7ad	9668ff45-83d7-466d-a266-9c4d54eb2993	fc69fd89-2966-4b9e-97ce-0541693aed6a	1	38150	54500	0	54500	54500	\N
08e8c964-5cae-4e11-8336-feb22c7b2479	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	8e1f8145-3447-4977-9661-b375fd4144bb	1	1750	2500	0	2500	2500	\N
06c0335c-564b-4bd3-a082-c94cf4183be2	c2f5f050-3dd7-4862-9b67-e72cb4aa9c57	8e1f8145-3447-4977-9661-b375fd4144bb	1	17850	25500	0	25500	25500	\N
d1ebcc7d-1cd7-4861-9af4-0eac8c0969ff	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	8e1f8145-3447-4977-9661-b375fd4144bb	12	1750	2500	0	30000	30000	\N
71bd3d2f-4c2f-4bdd-a13a-0d305c67a072	788a1deb-c1d9-449d-b07e-d800e4feaab3	8706bf81-bd8d-437d-b67a-72c98cd8dae4	12	1750	2500	0	30000	30000	\N
985a9eaf-1b5b-4029-aed9-8650a73190ca	9668ff45-83d7-466d-a266-9c4d54eb2993	8706bf81-bd8d-437d-b67a-72c98cd8dae4	2	38150	54500	0	109000	109000	\N
45a4d99e-c022-4b9e-aced-6883796e6f5d	78561982-b3fa-45d6-94c6-3b99b397c756	8706bf81-bd8d-437d-b67a-72c98cd8dae4	50	700	1000	0	50000	50000	\N
e2f7d07a-5e60-4627-98fb-7eb124127673	9e9e5f57-a956-470f-8b8c-a3d6f29a9894	8706bf81-bd8d-437d-b67a-72c98cd8dae4	5	4900	7000	0	35000	35000	\N
51a0972f-4d4d-4f75-9888-1552e4b0ea6e	5770e029-af52-4630-840c-0703653586ce	8706bf81-bd8d-437d-b67a-72c98cd8dae4	1	3150	4500	0	4500	4500	\N
bef5cd26-07ec-4f6b-afb5-f31edb4de4ad	3a9a6286-20b7-42e8-903f-0a64cc5ec639	8706bf81-bd8d-437d-b67a-72c98cd8dae4	1	2100	3000	0	3000	3000	\N
dbb5a4eb-675b-4f09-9f07-6b5265d6cea2	5556e2f3-a8c0-441d-b696-07ea76048743	8706bf81-bd8d-437d-b67a-72c98cd8dae4	2	1750	2500	0	5000	5000	\N
94a7c911-4968-461b-a0e3-a246885f1737	ce2cad02-0a94-4138-bd11-fae5f15376b1	9771e5d9-5f22-4b22-9064-f76b1272382d	2	17150	24500	0	49000	49000	\N
a5da85cc-73b9-4912-b5b7-1acbb84a61df	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	7	2100	3000	0	21000	21000	\N
a1b98fe7-af62-4641-8853-5f3736275db1	2a3db456-1c32-4bbe-bcaa-e6f303c40401	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	2	350	500	0	1000	1000	\N
2920aedb-edbd-4479-a499-1a21af3214c4	788a1deb-c1d9-449d-b07e-d800e4feaab3	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	1	1750	2500	0	2500	2500	\N
51f9302f-2921-47ff-88e0-e9b6de6d337a	3178a471-6e02-4db9-a506-602818e6447f	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	2	4900	7000	0	14000	14000	\N
becf9589-ab3b-474c-845c-c2cddc99c549	8a554019-4e83-458a-83c0-20fb0cd842d3	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	3	4900	7000	0	21000	21000	\N
f6bb0b8a-5afc-4762-8dc5-2e46db84fec1	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	1	2100	3000	0	3000	3000	\N
5fcb1364-490a-40bc-b629-b0c687271074	9f9c516a-b6f1-449c-8663-9cbedc6ca0a5	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	1	3500	5000	0	5000	5000	\N
bb645e19-307f-4dfd-be58-f3346eb7144c	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	c5e62460-88f5-4013-9eb6-b5b2e9c995e9	1	1400	2000	0	2000	2000	\N
8f7d8b64-e83e-4015-8f88-fd941c2efb5e	0c5cc742-e4c9-4e54-8c49-a9fcda2cf45a	4081e16f-e52e-41b8-ac4b-974cbf9872b9	1	9100	13000	0	13000	13000	\N
1d20b57a-2eed-451a-8f1f-3d68f2890a75	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	e15c65c2-65b8-44bd-9ffa-8d939394c119	1	23800	34000	0	34000	34000	\N
5e45a40c-d188-4596-8c61-73e88301d565	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	1d74c315-bee1-4738-9870-08a68234f287	1	23800	34000	0	34000	34000	\N
1ff5c221-c8f2-40c3-abc7-ebd0c1a19ddc	914edc82-ac6f-47e2-8a37-4693f562e586	1d74c315-bee1-4738-9870-08a68234f287	12	1750	2500	0	30000	30000	\N
5377bda0-dd2e-4c2b-b12f-174425115de3	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	3	2100	3000	0	9000	9000	\N
bbd85f7d-2531-462a-97fd-be47ea7b636a	0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	2	1400	2000	0	4000	4000	\N
c4fc47fd-7466-4e75-beef-5e48810c5b38	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	3	2100	3000	0	9000	9000	\N
14aef385-36d7-44f6-b912-d2e4db635e82	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	2	1400	2000	0	4000	4000	\N
f5f1379d-f0e3-47b0-8b2b-297ca616caab	b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	2	1400	2000	0	4000	4000	\N
6eba46fa-e133-45b4-9420-c24caca601e5	aa8051af-fcf7-43ea-9449-c61ab4597017	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	4	2100	3000	0	12000	12000	\N
b9671579-9ade-47d1-b247-6c230c641018	b67980a9-de04-4973-b7dc-bc61242c8f38	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	2	1750	2500	0	5000	5000	\N
e37c9cc0-6b44-44da-8c04-83a5e02257ab	06a0cade-0e05-456a-b408-ba1842c95f18	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	2	1400	2000	0	4000	4000	\N
a5647cbe-1bf3-48ce-bb2b-982960156940	e20071bd-1488-4e16-8b65-f9ba74f5c0e5	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	1	2100	3000	0	3000	3000	\N
d105601f-c595-4724-9173-bc66fe48e193	805f19d0-7aff-4694-818a-3e5165d146ef	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	1	2100	3000	0	3000	3000	\N
f12d2fe7-abb0-4547-89eb-9410067ee43d	d234256e-925c-4d1b-8426-c843d9f395aa	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	1	700	1000	0	1000	1000	\N
b65129ef-b0ca-467b-9ee9-57fa4088b3ec	3a9a6286-20b7-42e8-903f-0a64cc5ec639	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	1	2100	3000	0	3000	3000	\N
bf8ccc31-64f5-4fdb-9acd-050f49229612	788a1deb-c1d9-449d-b07e-d800e4feaab3	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	3	1750	2500	0	7500	7500	\N
f35424fb-e1e6-4de8-b1cd-828dd1c378fa	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	5	1400	2000	0	10000	10000	\N
c65517d0-734a-43d5-802b-6415e0c4e7c2	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	1	1400	2000	0	2000	2000	\N
7343803b-cda9-455c-9682-180fd4c80458	9737c64e-cda0-4e10-9c0d-de9e94bc4a51	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	2	1050	1500	0	3000	3000	\N
e2a3a539-9b5e-49a1-94e2-7fb2de34af26	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	1	1400	2000	0	2000	2000	\N
1cfb58c3-16e2-4b7c-8664-3235de827fbd	aa8051af-fcf7-43ea-9449-c61ab4597017	a383b4d2-d4a1-4445-8ea8-1aed9c6e2e22	1	2100	3000	0	3000	3000	\N
34e8abab-4a9f-4728-add5-1a6051afc306	c280399a-dd0f-487a-93c2-93d631d82782	8f335ba1-6fcb-4bd1-abf8-0c2103da63a0	10	6000	7000	0	70000	70000	\N
9c24e0ed-f4d0-47e6-a593-63ff77f65443	9668ff45-83d7-466d-a266-9c4d54eb2993	f88bc49b-a811-4746-9090-b7094e33a6b8	1	38150	54500	0	54500	54500	\N
2e48ce49-4482-40dd-b613-f2a92adbd2ab	230e0a48-0047-45e7-a253-601ba845011e	f88bc49b-a811-4746-9090-b7094e33a6b8	1	2100	3000	0	3000	3000	\N
\.


--
-- Data for Name: serah_terima; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.serah_terima (id, created_at, total_debt, total_credit, description, delegated_by, approved_by) FROM stdin;
3	2020-09-29 07:23:21	173500	0	terima penjualan	operator_01	operator_01
4	2020-09-30 07:32:27	1000	0	terima penjualan	operator_01	operator_01
7	2020-10-01 07:01:33	146500	0	terima penjualan	operator_01	operator_01
8	2020-10-03 06:38:07	136500	0	terima penjualan	operator_01	operator_01
9	2020-10-05 07:32:13	63500	0	Terima Penjualan	operator_01	operator_01
10	2020-10-06 07:10:51	188500	0	Terima Penjualan	operator_01	operator_01
11	2020-10-07 07:12:20	22500	0	Terima Penjualan	operator_01	operator_01
12	2020-10-08 07:06:10	64000	0	Terima Penjualan	operator_01	operator_01
13	2020-10-09 05:17:45	11000	0	Terima Penjualan	operator_01	operator_01
14	2020-10-12 07:10:46	17000	0	terima penjualan	operator_01	operator_01
15	2020-10-20 07:33:00	288500	0	terima penjualan	operator_01	operator_01
16	2020-10-22 07:13:10	234000	0	Terima Penjualan	operator_01	operator_01
17	2020-10-23 05:51:00	6000	0	Terima Penjualan	operator_01	operator_01
18	2020-11-06 04:14:06	237500	0	Terima Penjualan	operator_01	operator_01
19	2020-11-12 06:26:06	632500	0	Terima Penjualan	operator_01	admin
20	2020-11-17 05:28:11	180500	0	Terima Penjualan	operator_01	operator_01
21	2020-11-21 04:43:40	216000	0	Terima penjualan	operator_01	operator_01
\.


--
-- Data for Name: stock_card; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stock_card (id, item_id, created_at, class_name, class_id, quantity, type, total) FROM stdin;
3afe98fe-0fc4-4967-9c1a-a3ea6bbd8461	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-09-29 04:49:14	App\\Entity\\SaleItem	2b85630d-cad2-4092-8026-1fb1f855db90	5	GET	116
a071c134-d76c-47d9-ad33-52b4a4756c51	0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	2020-09-29 05:46:20	App\\Entity\\SaleItem	5ab298b9-517c-4d5d-9334-577af35fc06c	1	GET	27
f450714f-f282-480d-87e3-fa688f15b22c	f54c7cdd-d5d5-4237-acbf-b0d87fecd5e9	2020-09-29 05:46:20	App\\Entity\\SaleItem	d6a3446f-38b6-4a27-b856-cef9d389b7b2	1	GET	126
10a75839-15c4-4b5f-8c5d-aa0f30fe84a3	6c999ab5-1af9-41f3-b2f7-cd1d6bc186f7	2020-09-29 06:05:31	App\\Entity\\SaleItem	086ef92b-4360-4980-add7-02e2576b0760	1	GET	8
6219ee40-974d-42c8-847d-e6ead7d92487	b58e0a50-2e90-4527-9aab-812dbdcc7f02	2020-09-29 06:33:16	App\\Entity\\SaleItem	756aa72f-1772-4f94-8962-bdfa9d82e279	1	GET	27
264b780f-0b75-44b5-8edb-2bd44e8f4377	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-09-29 06:44:03	App\\Entity\\SaleItem	3e5b7287-0a00-4ac7-b38d-c32cabc41207	2	GET	32
675db203-d243-4118-96be-f876ee6bee85	2dbfef19-f0b5-4fa6-903c-67b818864e6a	2020-09-29 06:44:45	App\\Entity\\SaleItem	28f1dcc0-0a07-444f-b4f4-f512db1132c2	1	GET	30
4cb7f94e-8238-4f00-a0fa-ec6e5f8f7afd	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-09-29 06:58:47	App\\Entity\\SaleItem	3e5b7287-0a00-4ac7-b38d-c32cabc41207	2	GET	30
f3dc73f2-cf0f-4d8a-9935-2bc562e23567	3d031565-23f8-431c-a867-bbf3a17a69da	2020-09-30 03:22:50	App\\Entity\\SaleItem	29e52b36-e8ab-4e26-ab1d-af9b29811d26	1	GET	51
b6c199c4-19dd-4ad5-b5c2-86723d632128	0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	2020-09-30 03:49:09	App\\Entity\\SaleItem	5ab298b9-517c-4d5d-9334-577af35fc06c	1	GET	26
0b77f698-3cdf-4d6f-95e2-19b66f49f322	f54c7cdd-d5d5-4237-acbf-b0d87fecd5e9	2020-09-30 03:49:09	App\\Entity\\SaleItem	d6a3446f-38b6-4a27-b856-cef9d389b7b2	1	GET	125
bbb1c298-eea4-4617-acb6-035c5d121d13	2dbfef19-f0b5-4fa6-903c-67b818864e6a	2020-09-30 04:12:42	App\\Entity\\SaleItem	28f1dcc0-0a07-444f-b4f4-f512db1132c2	1	GET	29
7b5b4f5c-c979-4273-8956-e0343cd85c5d	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-10-01 03:01:55	App\\Entity\\SaleItem	27e3c627-8d10-4b61-a5dd-5668e2ce57d6	2	GET	131
4b52965f-3cbd-4236-97e8-562a24947daa	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-01 03:01:55	App\\Entity\\SaleItem	146b64ff-63b9-41b5-8cdd-d67554af15ee	1	GET	115
5ffb57c7-7ff6-4857-83d1-d251bf5a6999	4a478acc-84aa-4340-9839-32ba0505a2d8	2020-10-01 03:29:39	App\\Entity\\SaleItem	4690cb23-6131-4360-a302-5512742c84ad	1	GET	3
e4c3ef31-8154-416e-8183-ce24d35cea2c	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	2020-10-01 03:42:15	App\\Entity\\SaleItem	18db9580-fb26-45e0-96ea-256073c8c03c	1	GET	4
ad390d85-9f00-475d-ac8a-14173b1aa074	2a3db456-1c32-4bbe-bcaa-e6f303c40401	2020-10-01 04:04:37	App\\Entity\\SaleItem	0e9ecf30-3120-4f02-bc5a-ec39a2acfad9	10	GET	209
ddcd6cef-547c-432c-9017-df0b773cddf9	4a478acc-84aa-4340-9839-32ba0505a2d8	2020-10-01 05:00:40	App\\Entity\\SaleItem	5468c9e1-5544-4abf-9f8c-09b241957b26	1	GET	2
fb528f11-9033-4505-a780-2155393515a3	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-10-01 07:02:05	App\\Entity\\SaleItem	27e3c627-8d10-4b61-a5dd-5668e2ce57d6	2	GET	129
3727a852-88b1-4dec-bc7e-fcf6f9b64d26	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-01 07:02:05	App\\Entity\\SaleItem	146b64ff-63b9-41b5-8cdd-d67554af15ee	1	GET	114
b25ff2e5-876d-4c91-b264-018a9cbe357a	4a478acc-84aa-4340-9839-32ba0505a2d8	2020-10-01 07:02:05	App\\Entity\\SaleItem	4690cb23-6131-4360-a302-5512742c84ad	1	GET	1
5e0112cc-3b2b-4416-9eee-5ba1444c7f85	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	2020-10-01 07:02:05	App\\Entity\\SaleItem	18db9580-fb26-45e0-96ea-256073c8c03c	1	GET	3
b11d543c-b781-442a-b4fd-d6676f7eda39	2a3db456-1c32-4bbe-bcaa-e6f303c40401	2020-10-01 07:02:05	App\\Entity\\SaleItem	0e9ecf30-3120-4f02-bc5a-ec39a2acfad9	10	GET	199
2edc7d97-7d1c-4672-aa37-ffedd9fef1d2	4a478acc-84aa-4340-9839-32ba0505a2d8	2020-10-01 07:02:05	App\\Entity\\SaleItem	5468c9e1-5544-4abf-9f8c-09b241957b26	1	GET	0
4e557a99-ed8d-4a0d-92c1-60dacd665c47	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	2020-10-02 03:40:53	App\\Entity\\SaleItem	bb14127f-0376-474f-aa4a-f741cd7ef469	3	GET	0
4e583b40-dfd9-4180-b0c9-6ba6fde57a62	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-03 02:44:59	App\\Entity\\SaleItem	24858c42-5e27-4840-a925-673efde0173c	1	GET	29
7d0f1921-ef6f-445b-a281-2e30ca2cb640	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-10-03 02:46:56	App\\Entity\\SaleItem	b9fc8fe8-c517-4fdc-bf3b-faf9eaf16eb0	1	GET	758
1fdc2676-8d91-4cda-9ab0-bf9c700575d6	14397549-e408-4470-979a-8e794351086e	2020-10-03 02:46:56	App\\Entity\\SaleItem	801d6efa-120d-4f45-8ae5-d3049d1a1257	1	GET	149
82520c15-7806-4703-b731-b03a59285a76	8a758252-d9fd-4d16-8b56-a94382216abd	2020-10-03 02:46:56	App\\Entity\\SaleItem	e72b07e2-56b3-4feb-8884-34e0224795a2	1	GET	19
59cddc24-71a7-4652-883a-5c036e0de25f	2dbfef19-f0b5-4fa6-903c-67b818864e6a	2020-10-03 03:54:51	App\\Entity\\SaleItem	b2b14fda-0dcf-458c-9e6c-ab9e34ac717f	5	GET	24
22300840-1db6-4937-bae0-25335540dfa7	77ea7188-2457-4523-999f-a1812416c570	2020-10-03 04:45:35	App\\Entity\\SaleItem	7a73e18d-8db7-48d5-9047-a2c578b5e305	1	GET	195
e92c2714-68a8-49c9-a5c3-455143bbf6ae	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-03 06:38:26	App\\Entity\\SaleItem	24858c42-5e27-4840-a925-673efde0173c	1	GET	28
93735acc-479d-49d3-9784-e59098a76037	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-10-03 06:38:26	App\\Entity\\SaleItem	b9fc8fe8-c517-4fdc-bf3b-faf9eaf16eb0	1	GET	757
5b0b872b-b794-4c8c-86cf-e7a8a0d57192	14397549-e408-4470-979a-8e794351086e	2020-10-03 06:38:26	App\\Entity\\SaleItem	801d6efa-120d-4f45-8ae5-d3049d1a1257	1	GET	148
80f80330-03db-4800-bb43-eb949c2eba7b	8a758252-d9fd-4d16-8b56-a94382216abd	2020-10-03 06:38:26	App\\Entity\\SaleItem	e72b07e2-56b3-4feb-8884-34e0224795a2	1	GET	18
346c2d27-13c3-49e4-8257-fa42827d3135	2dbfef19-f0b5-4fa6-903c-67b818864e6a	2020-10-03 06:38:26	App\\Entity\\SaleItem	b2b14fda-0dcf-458c-9e6c-ab9e34ac717f	5	GET	19
c278a9fe-3227-42c8-b971-64fb97b79848	77ea7188-2457-4523-999f-a1812416c570	2020-10-03 06:38:26	App\\Entity\\SaleItem	7a73e18d-8db7-48d5-9047-a2c578b5e305	1	GET	194
c26e57c0-6685-4135-bd34-a0bcb981a4c3	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-05 03:32:59	App\\Entity\\SaleItem	9d31028d-be19-4ab7-9de3-e041ffa56b41	1	GET	27
e257270f-592a-4274-a6f2-42d96884d42c	4ca13666-5ad0-4357-9355-6be3323214fa	2020-10-05 03:41:01	App\\Entity\\SaleItem	f80612c3-03bc-4a39-b190-f01ce709dc11	1	GET	47
eea69179-e9bb-43cb-8409-2bc6859fe1db	4ca13666-5ad0-4357-9355-6be3323214fa	2020-10-05 03:49:06	App\\Entity\\SaleItem	e1100a5c-df8e-4f01-9feb-89e9475e900a	1	GET	46
1f7de0c6-1c6f-4ad7-a69f-7cc197733e73	4ca13666-5ad0-4357-9355-6be3323214fa	2020-10-05 07:32:52	App\\Entity\\SaleItem	f80612c3-03bc-4a39-b190-f01ce709dc11	1	GET	45
d8877573-8696-4d0b-9d9a-72cf7dd8f6c4	4ca13666-5ad0-4357-9355-6be3323214fa	2020-10-05 07:32:52	App\\Entity\\SaleItem	e1100a5c-df8e-4f01-9feb-89e9475e900a	1	GET	44
e59beec9-f568-4d00-9639-ab6b483eca30	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-05 07:32:52	App\\Entity\\SaleItem	9d31028d-be19-4ab7-9de3-e041ffa56b41	1	GET	26
71af8a7b-67cc-45fd-9a4f-559826155423	eb82db1e-8ca7-4145-a22c-256eca2a6241	2020-10-06 03:12:32	App\\Entity\\SaleItem	25aae80c-be3e-4972-a1c0-71680f18eb91	1	GET	46
e04623b1-0c65-4286-a25b-709d9f03ff72	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-06 03:19:10	App\\Entity\\SaleItem	31e425e3-4c63-4123-9812-2239e4001841	2	GET	24
e2281360-c6e3-4eac-8df3-554f0029d154	37e21335-7f61-4d38-bd49-e89233dfcae1	2020-10-06 03:19:10	App\\Entity\\SaleItem	a2558d64-d6d2-4594-9027-de1d7c90cc82	2	GET	22
7d9fe559-35fd-4cdf-a756-b7b7607dbcea	ce2cad02-0a94-4138-bd11-fae5f15376b1	2020-10-06 03:19:10	App\\Entity\\SaleItem	18c0612d-aa91-4201-9fc3-51fde9c44716	1	GET	10
32e6cbd7-0fd7-4880-a49a-fd650488351d	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	2020-10-06 03:20:00	App\\Entity\\SaleItem	18db9580-fb26-45e0-96ea-256073c8c03c	1	GET	-1
bdf3b458-b992-4cea-8e8e-7eefb49d9df7	b67980a9-de04-4973-b7dc-bc61242c8f38	2020-10-06 03:21:31	App\\Entity\\SaleItem	b0d8538b-333c-4c9f-a462-ee0e0f9a4490	1	GET	171
10691164-72fb-4fa0-89e6-34676a67c760	7df4ed86-c958-41e0-96ef-c2059d9ebae4	2020-10-06 03:48:37	App\\Entity\\SaleItem	9e9620f0-b64f-4d31-aff0-25cd6bd6eae9	1	GET	12
234e5c8b-668e-4850-a2b2-7f51a1197f2b	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-06 04:43:59	App\\Entity\\SaleItem	c2215afe-7b6e-4dc5-9c47-04c88625f35b	1	GET	113
f3ef5680-badf-4945-8abe-0c97708ac0e9	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-10-06 04:43:59	App\\Entity\\SaleItem	637412ef-9a54-43b2-a262-ed45ad5c4c3d	1	GET	263
a34662eb-bd40-4d74-8726-f1ec23b139d0	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-10-06 04:59:21	App\\Entity\\SaleItem	37ce73f0-aafe-4066-b36f-8c1b57113424	1	GET	262
4310a9b5-17f4-455c-98e9-16ca12e7cab1	805f19d0-7aff-4694-818a-3e5165d146ef	2020-10-06 05:08:08	App\\Entity\\SaleItem	965b3290-5c74-4c4d-b8d7-07b3d53d09a5	1	GET	59
5af37163-ee44-4c91-9c7b-98061c3aedbf	1fc50d3b-148a-4492-b499-d675c202d459	2020-10-06 05:57:09	App\\Entity\\SaleItem	efe9a1e5-feec-40e9-9794-68ea0c06e502	1	GET	4
5103bf92-ce52-48b9-bf99-c9b32f95abd2	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-06 07:12:36	App\\Entity\\SaleItem	c2215afe-7b6e-4dc5-9c47-04c88625f35b	1	GET	112
d412cfc3-3d37-403c-a5a1-1a7050c63dac	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-10-06 07:12:36	App\\Entity\\SaleItem	637412ef-9a54-43b2-a262-ed45ad5c4c3d	1	GET	261
59d1f637-c123-43eb-ba93-bb9903ce4af2	eb82db1e-8ca7-4145-a22c-256eca2a6241	2020-10-06 07:12:36	App\\Entity\\SaleItem	25aae80c-be3e-4972-a1c0-71680f18eb91	1	GET	45
1519e11c-0a4d-4e21-9743-ba29f9e372bf	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-10-06 07:12:36	App\\Entity\\SaleItem	37ce73f0-aafe-4066-b36f-8c1b57113424	1	GET	260
3a299459-9a32-4af1-b373-2ed8030f663b	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-06 07:12:36	App\\Entity\\SaleItem	31e425e3-4c63-4123-9812-2239e4001841	2	GET	22
835972c0-b438-4f37-8b43-4cb48c8b4fe6	37e21335-7f61-4d38-bd49-e89233dfcae1	2020-10-06 07:12:36	App\\Entity\\SaleItem	a2558d64-d6d2-4594-9027-de1d7c90cc82	2	GET	20
447976fa-44eb-4345-b5b9-20594063e84e	ce2cad02-0a94-4138-bd11-fae5f15376b1	2020-10-06 07:12:36	App\\Entity\\SaleItem	18c0612d-aa91-4201-9fc3-51fde9c44716	1	GET	9
9b7a7b67-d71e-4328-8561-68b998b49220	805f19d0-7aff-4694-818a-3e5165d146ef	2020-10-06 07:12:36	App\\Entity\\SaleItem	965b3290-5c74-4c4d-b8d7-07b3d53d09a5	1	GET	58
ec8bcf81-c392-4a5a-9331-f5da276a9763	b67980a9-de04-4973-b7dc-bc61242c8f38	2020-10-06 07:12:36	App\\Entity\\SaleItem	b0d8538b-333c-4c9f-a462-ee0e0f9a4490	1	GET	170
7558203e-fdd5-4485-a233-7feb258d0c44	7df4ed86-c958-41e0-96ef-c2059d9ebae4	2020-10-06 07:12:36	App\\Entity\\SaleItem	9e9620f0-b64f-4d31-aff0-25cd6bd6eae9	1	GET	11
5addf60c-94c9-4dda-8565-675348ce8185	1fc50d3b-148a-4492-b499-d675c202d459	2020-10-06 07:12:36	App\\Entity\\SaleItem	efe9a1e5-feec-40e9-9794-68ea0c06e502	1	GET	3
cea3d8dc-6a08-4266-8d69-0950c7a9eaf4	171fabf2-d96a-4423-8ee2-9626efbbd16e	2020-10-07 05:24:20	App\\Entity\\SaleItem	a80d55c4-eef0-4beb-b33f-a5ffa3d5b9c0	1	GET	11
515ab0b6-7b3b-48de-9ee2-a5f0b961090d	805f19d0-7aff-4694-818a-3e5165d146ef	2020-10-07 06:09:49	App\\Entity\\SaleItem	a0df57b4-5bce-45cb-b5ce-d8d4cfc23236	1	GET	57
518d8fbe-186a-4d7c-b83f-428035005d2c	689391ef-c406-4d68-accf-2cdf8e3f2c82	2020-10-07 06:09:49	App\\Entity\\SaleItem	6fc39e8e-3e4c-4098-bb76-cc08ee4ef8ae	1	GET	88
64cf7378-3860-4d6a-a5c2-0ffe81cf2302	0710bf62-a460-477c-a8f9-b9386d9b457f	2020-10-07 06:09:49	App\\Entity\\SaleItem	1cdaafd6-a44d-443c-a53a-c5605704b930	1	GET	56
8a9aa051-6578-462d-b6bf-ac7c4581ae81	14397549-e408-4470-979a-8e794351086e	2020-10-07 06:09:49	App\\Entity\\SaleItem	0142427a-baef-4f30-b070-6bbf1530162e	1	GET	147
15f184b6-ae97-48cf-9dce-fb91bdc15416	805f19d0-7aff-4694-818a-3e5165d146ef	2020-10-07 07:12:40	App\\Entity\\SaleItem	a0df57b4-5bce-45cb-b5ce-d8d4cfc23236	1	GET	56
ec377c6f-351f-486c-9580-18799f964881	689391ef-c406-4d68-accf-2cdf8e3f2c82	2020-10-07 07:12:40	App\\Entity\\SaleItem	6fc39e8e-3e4c-4098-bb76-cc08ee4ef8ae	1	GET	87
b34f9d7c-807d-4904-b55f-f58b5733b551	0710bf62-a460-477c-a8f9-b9386d9b457f	2020-10-07 07:12:40	App\\Entity\\SaleItem	1cdaafd6-a44d-443c-a53a-c5605704b930	1	GET	55
861d2fe8-2d63-487b-b1d7-faa8856b9dee	14397549-e408-4470-979a-8e794351086e	2020-10-07 07:12:40	App\\Entity\\SaleItem	0142427a-baef-4f30-b070-6bbf1530162e	1	GET	146
378e21e9-3af7-4c7f-8baa-645847aaf00f	171fabf2-d96a-4423-8ee2-9626efbbd16e	2020-10-07 07:12:40	App\\Entity\\SaleItem	a80d55c4-eef0-4beb-b33f-a5ffa3d5b9c0	1	GET	10
d81d83cf-6daf-4f0c-9386-c7b359b44ce7	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-10-08 03:05:45	App\\Entity\\SaleItem	9b2f86ff-f98d-471e-a077-04fbf6378230	1	GET	24
372e44ad-4456-453c-b8fc-381ee4f5b789	914edc82-ac6f-47e2-8a37-4693f562e586	2020-10-08 04:17:35	App\\Entity\\SaleItem	6e5cabb7-d19b-4727-981f-407096666138	1	GET	42
48c942f8-503d-4ced-8738-21864eccd8ef	5556e2f3-a8c0-441d-b696-07ea76048743	2020-10-08 04:33:39	App\\Entity\\SaleItem	a7ef662b-3cc3-4963-ae87-796d5b13add4	1	GET	11
81f715bc-aa87-44bf-96ee-d720b46ed540	5770e029-af52-4630-840c-0703653586ce	2020-10-08 04:55:11	App\\Entity\\SaleItem	c08a6568-294e-49fe-975e-21c81f12cfc5	1	GET	7
26ce37b0-1048-4f6f-96ed-9bfb16b75a41	5556e2f3-a8c0-441d-b696-07ea76048743	2020-10-08 07:06:26	App\\Entity\\SaleItem	a7ef662b-3cc3-4963-ae87-796d5b13add4	1	GET	10
b70d1fad-0ad6-4e77-9603-8c4fb163ef2c	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-10-08 07:06:26	App\\Entity\\SaleItem	9b2f86ff-f98d-471e-a077-04fbf6378230	1	GET	23
59ab7abb-260c-468d-978b-4d3beef8a1f6	914edc82-ac6f-47e2-8a37-4693f562e586	2020-10-08 07:06:26	App\\Entity\\SaleItem	6e5cabb7-d19b-4727-981f-407096666138	1	GET	41
e3662355-bff5-4477-bd15-854bb14c6f53	5770e029-af52-4630-840c-0703653586ce	2020-10-08 07:06:26	App\\Entity\\SaleItem	c08a6568-294e-49fe-975e-21c81f12cfc5	1	GET	6
bef8ddd3-8f04-4cdc-b6ab-43c98af05cbe	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-10-09 03:08:14	App\\Entity\\SaleItem	365b5d05-7185-48d0-828e-a745202d0626	1	GET	259
e125c9e2-8e74-4343-b4d2-061d5d40a091	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-09 03:08:14	App\\Entity\\SaleItem	776f6aa5-5be9-43a3-b62e-f214427163fc	1	GET	97
ef6ead77-0e19-4cdb-9f99-59696b821f8b	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-09 03:08:14	App\\Entity\\SaleItem	80574af1-ee25-4c6f-b9de-ca24b8a202a9	1	GET	111
8aaee08f-16aa-4538-bb95-d479aed1fccf	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-09 04:12:42	App\\Entity\\SaleItem	9d2a9663-8c62-404a-a636-3e4dd215cb72	1	GET	96
6a0fd639-2602-44a9-b34e-b88b389b8457	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-09 04:13:23	App\\Entity\\SaleItem	9d2a9663-8c62-404a-a636-3e4dd215cb72	1	GET	95
f53b796a-8ca2-4bdf-9ecb-374a1a81355f	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-10-09 05:18:02	App\\Entity\\SaleItem	365b5d05-7185-48d0-828e-a745202d0626	1	GET	258
928d2fd3-4bcb-4bf2-96a9-daa0c8e59a0f	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-09 05:18:02	App\\Entity\\SaleItem	776f6aa5-5be9-43a3-b62e-f214427163fc	1	GET	94
3efe8365-3e75-48dd-92ac-223a212800a6	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-09 05:18:02	App\\Entity\\SaleItem	80574af1-ee25-4c6f-b9de-ca24b8a202a9	1	GET	110
f7a81535-03a6-45c0-b6cf-484960823781	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-09 05:18:02	App\\Entity\\SaleItem	9d2a9663-8c62-404a-a636-3e4dd215cb72	1	GET	93
3926434e-edec-476f-844b-eee07159a62f	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-10-12 07:06:25	App\\Entity\\SaleItem	6e44398c-459c-4758-bcbf-1aba92e08b54	1	GET	756
52908fa7-2e25-435f-b5a1-10d8c859baf9	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-12 07:06:25	App\\Entity\\SaleItem	2d931a0b-ee62-4426-8cd7-8eaf4250f850	1	GET	109
cfa5037c-91cb-462e-a203-a7fb405249e1	b58e0a50-2e90-4527-9aab-812dbdcc7f02	2020-10-12 07:06:59	App\\Entity\\SaleItem	d3bcafaf-b9a3-45b6-ba07-26f832fcea8d	1	GET	26
7487079b-f52c-4146-ae96-55066a92b6a8	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	2020-10-12 07:08:13	App\\Entity\\SaleItem	5740f06f-3e97-41a2-8c48-8f4e8626ab48	1	GET	201
567c2c69-033d-4915-8151-5e29dc43fd19	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	2020-10-12 07:10:17	App\\Entity\\SaleItem	5740f06f-3e97-41a2-8c48-8f4e8626ab48	1	GET	200
f5ccb062-1aca-435a-9cb4-0535e90980f6	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-10-12 07:13:08	App\\Entity\\SaleItem	6e44398c-459c-4758-bcbf-1aba92e08b54	1	GET	755
7a5a2310-c4bc-4003-8ee5-c8589b8d1e1a	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-10-12 07:13:08	App\\Entity\\SaleItem	2d931a0b-ee62-4426-8cd7-8eaf4250f850	1	GET	108
afaf6839-cc9b-4476-a63a-7c45cd0150ad	b58e0a50-2e90-4527-9aab-812dbdcc7f02	2020-10-12 07:13:08	App\\Entity\\SaleItem	d3bcafaf-b9a3-45b6-ba07-26f832fcea8d	1	GET	25
cc4a8c3c-d845-475e-9b7a-fb963bdb298c	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	2020-10-12 07:13:08	App\\Entity\\SaleItem	5740f06f-3e97-41a2-8c48-8f4e8626ab48	1	GET	199
5d42336f-3910-4862-8eb7-aa8a461ea756	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-10-20 03:15:41	App\\Entity\\SaleItem	5d185d76-deb7-4d39-b733-e9b48874edc0	1	GET	754
40c9fbab-ba05-411e-ae3c-e8ddbaa3e2e1	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-20 03:16:15	App\\Entity\\SaleItem	de5e3949-21fd-410a-a91d-fe3c10bbe572	2	GET	20
f8050fdd-ef01-4820-ab93-78e1fc77c314	e662ecc3-8c27-44e7-92a1-ff1e7b277ac4	2020-10-20 03:16:52	App\\Entity\\SaleItem	c86cdfaf-ac98-4b7c-bb14-2a0cb4a8a588	1	GET	10
5394da7c-b7ed-49e2-aac0-1112c65db9fe	3d031565-23f8-431c-a867-bbf3a17a69da	2020-10-20 03:20:40	App\\Entity\\SaleItem	2967a83e-2e3e-4b51-8160-47306b04c0bc	50	GET	1
ee1b566e-7f3e-4c48-86c9-b6bb8e812efc	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-20 03:20:40	App\\Entity\\SaleItem	72a00f43-95da-4905-8875-f5688a75fc28	12	GET	81
2f4396b1-8734-4a2a-9d54-0f83a5e4bb0f	d234256e-925c-4d1b-8426-c843d9f395aa	2020-10-20 03:20:40	App\\Entity\\SaleItem	05ec29f2-a0e3-423a-84f3-e16ce617ecd5	1	GET	57
d52269ec-0a81-4065-a53a-b2bc96145390	10327621-aaad-460d-b944-daa8c8a9a927	2020-10-20 03:20:40	App\\Entity\\SaleItem	9f0a4810-dfd1-4de7-a2c7-90f59de23de5	1	GET	223
fca076b8-232f-42eb-9299-c5eca007fd03	d566b1d8-efbd-4a86-9943-9514d141175d	2020-10-20 03:20:40	App\\Entity\\SaleItem	0a7b96b7-c3d8-4ec1-a509-86522d20f751	1	GET	8
5bb058a2-7939-4e82-b99b-d98116eda0af	10327621-aaad-460d-b944-daa8c8a9a927	2020-10-20 03:20:40	App\\Entity\\SaleItem	aabb96ad-8721-4f0b-bdab-fb035540729e	1	GET	222
e38e2afb-b4cd-4fb1-846c-a693f08c3d11	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-10-20 03:20:40	App\\Entity\\SaleItem	63a4cc6f-c8fa-4c05-8ddc-8ba39aa2de14	1	GET	22
cf4a99e5-55dd-4734-a49a-7b470fddb8b1	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-10-20 05:17:53	App\\Entity\\SaleItem	ef9fbdbd-0b38-4d62-a8ab-348d83eeb5d7	4	GET	45
70efab19-0c65-4469-aec2-5bfc0827b3bc	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-10-20 07:33:53	App\\Entity\\SaleItem	5d185d76-deb7-4d39-b733-e9b48874edc0	1	GET	753
4c790596-3ef9-41d8-82be-20e5b8ffeb3a	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-20 07:33:53	App\\Entity\\SaleItem	de5e3949-21fd-410a-a91d-fe3c10bbe572	2	GET	18
cf0d91b4-d947-4621-a83f-1cacdfacfdc8	e662ecc3-8c27-44e7-92a1-ff1e7b277ac4	2020-10-20 07:33:53	App\\Entity\\SaleItem	c86cdfaf-ac98-4b7c-bb14-2a0cb4a8a588	1	GET	9
c948120d-edd0-49a4-9ae1-d66a6790290a	3d031565-23f8-431c-a867-bbf3a17a69da	2020-10-20 07:33:53	App\\Entity\\SaleItem	2967a83e-2e3e-4b51-8160-47306b04c0bc	50	GET	-49
5cf4e991-544e-4e2a-aa7f-32a628f63438	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-20 07:33:53	App\\Entity\\SaleItem	72a00f43-95da-4905-8875-f5688a75fc28	12	GET	69
aa5a7086-83c8-4ee5-a607-0239d9614de8	d234256e-925c-4d1b-8426-c843d9f395aa	2020-10-20 07:33:53	App\\Entity\\SaleItem	05ec29f2-a0e3-423a-84f3-e16ce617ecd5	1	GET	56
8fde67b7-b1a2-4f54-b878-4032ba10d88f	10327621-aaad-460d-b944-daa8c8a9a927	2020-10-20 07:33:53	App\\Entity\\SaleItem	9f0a4810-dfd1-4de7-a2c7-90f59de23de5	1	GET	221
787619a6-4b12-4ba6-a416-88a0268e4163	d566b1d8-efbd-4a86-9943-9514d141175d	2020-10-20 07:33:53	App\\Entity\\SaleItem	0a7b96b7-c3d8-4ec1-a509-86522d20f751	1	GET	7
62137c2e-866b-4cd8-ba85-41f906676467	10327621-aaad-460d-b944-daa8c8a9a927	2020-10-20 07:33:53	App\\Entity\\SaleItem	aabb96ad-8721-4f0b-bdab-fb035540729e	1	GET	220
f63bd8db-c319-4256-b88b-87d9d1033614	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-10-20 07:33:53	App\\Entity\\SaleItem	63a4cc6f-c8fa-4c05-8ddc-8ba39aa2de14	1	GET	21
cb37b93d-06d6-4135-afd9-ec1b252a7d27	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-10-20 07:33:53	App\\Entity\\SaleItem	ef9fbdbd-0b38-4d62-a8ab-348d83eeb5d7	4	GET	41
ec1e80a2-ab23-4d7a-828d-d96cdbd3c70b	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-22 03:04:22	App\\Entity\\SaleItem	9034dec3-738b-4e01-a0c1-c9be54589dad	3	GET	15
912b605d-f142-4ad1-b4d5-173c0eb35574	e662ecc3-8c27-44e7-92a1-ff1e7b277ac4	2020-10-22 03:05:22	App\\Entity\\SaleItem	c86cdfaf-ac98-4b7c-bb14-2a0cb4a8a588	1	GET	8
40d2d4df-ec8d-41dd-b825-1e30b4d22bc1	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-22 04:46:29	App\\Entity\\SaleItem	2ef09636-2e6b-448d-985d-d9d23d0ab9e4	1	GET	14
c6a20839-1381-47cc-a3a0-8d9788191507	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-22 07:13:31	App\\Entity\\SaleItem	9034dec3-738b-4e01-a0c1-c9be54589dad	3	GET	11
6a2e5d69-6049-4ea7-bfd6-c9908b30e61c	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-22 07:13:31	App\\Entity\\SaleItem	2ef09636-2e6b-448d-985d-d9d23d0ab9e4	1	GET	10
f055cdbb-edb3-408d-9630-ff4e2c6c1ae1	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-23 05:43:02	App\\Entity\\SaleItem	85c87643-335a-4be0-9016-863c06593e34	2	GET	67
73ac157e-e8f7-4301-a8d0-1e754ac50cdc	171fabf2-d96a-4423-8ee2-9626efbbd16e	2020-10-23 05:43:26	App\\Entity\\SaleItem	a80d55c4-eef0-4beb-b33f-a5ffa3d5b9c0	1	GET	9
283f1d9c-4068-4ccf-ad47-4f04390173d6	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-23 05:43:41	App\\Entity\\SaleItem	24858c42-5e27-4840-a925-673efde0173c	1	GET	9
c6d581d5-0094-4ffe-a908-47e79779ee8f	4a478acc-84aa-4340-9839-32ba0505a2d8	2020-10-23 05:46:17	App\\Entity\\SaleItem	4690cb23-6131-4360-a302-5512742c84ad	1	GET	-1
ec93aa14-896b-48a6-a020-fd470a11af7b	914edc82-ac6f-47e2-8a37-4693f562e586	2020-10-23 05:46:32	App\\Entity\\SaleItem	6e5cabb7-d19b-4727-981f-407096666138	1	GET	40
a50311a7-308f-4ca2-820c-d178c037ec5e	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-10-23 05:46:43	App\\Entity\\SaleItem	9b2f86ff-f98d-471e-a077-04fbf6378230	1	GET	20
8d6fed50-0b9f-4f04-a203-af87a654fe88	4a478acc-84aa-4340-9839-32ba0505a2d8	2020-10-23 05:47:01	App\\Entity\\SaleItem	5468c9e1-5544-4abf-9f8c-09b241957b26	1	GET	-2
e1babf1b-f7cd-4a7a-9df4-136faf69e4ed	dd6bf8b3-b77b-4b88-ad7b-bc71c69455d8	2020-10-23 05:47:12	App\\Entity\\SaleItem	bb14127f-0376-474f-aa4a-f741cd7ef469	3	GET	-4
5e42a9f0-946e-4d74-9421-d2a9832d6f15	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-23 05:47:25	App\\Entity\\SaleItem	2ef09636-2e6b-448d-985d-d9d23d0ab9e4	1	GET	8
ea32ddd0-c2cf-4cf7-b7b7-8e2ada31ca7e	7df4ed86-c958-41e0-96ef-c2059d9ebae4	2020-10-23 05:47:37	App\\Entity\\SaleItem	9e9620f0-b64f-4d31-aff0-25cd6bd6eae9	1	GET	10
40920b69-9cd4-4683-b1c6-0c600c421d21	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-10-23 05:47:52	App\\Entity\\SaleItem	9d31028d-be19-4ab7-9de3-e041ffa56b41	1	GET	7
8c04bba3-f552-42dd-916c-0fc961c794c0	2dbfef19-f0b5-4fa6-903c-67b818864e6a	2020-10-23 05:48:18	App\\Entity\\SaleItem	b2b14fda-0dcf-458c-9e6c-ab9e34ac717f	5	GET	14
c6331ccd-aa81-4ce8-944f-e5d69772d22c	3d031565-23f8-431c-a867-bbf3a17a69da	2020-10-23 05:48:34	App\\Entity\\SaleItem	2967a83e-2e3e-4b51-8160-47306b04c0bc	50	GET	-99
4e63302b-8298-4de2-8e79-51146a3426a5	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-23 05:48:34	App\\Entity\\SaleItem	72a00f43-95da-4905-8875-f5688a75fc28	12	GET	55
db85a038-e763-4b00-a552-9918c7c17452	d234256e-925c-4d1b-8426-c843d9f395aa	2020-10-23 05:48:34	App\\Entity\\SaleItem	05ec29f2-a0e3-423a-84f3-e16ce617ecd5	1	GET	55
885f27dd-cca1-4601-97f1-328fd456d152	10327621-aaad-460d-b944-daa8c8a9a927	2020-10-23 05:48:34	App\\Entity\\SaleItem	9f0a4810-dfd1-4de7-a2c7-90f59de23de5	1	GET	219
ef83487e-e2a0-4f97-9868-afc54928e1eb	d566b1d8-efbd-4a86-9943-9514d141175d	2020-10-23 05:48:34	App\\Entity\\SaleItem	0a7b96b7-c3d8-4ec1-a509-86522d20f751	1	GET	6
d28ab5b6-b915-4283-b6bb-84df847ae275	10327621-aaad-460d-b944-daa8c8a9a927	2020-10-23 05:48:34	App\\Entity\\SaleItem	aabb96ad-8721-4f0b-bdab-fb035540729e	1	GET	218
1ae4e8d7-8ac8-41ab-b999-5474c433065e	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-10-23 05:48:34	App\\Entity\\SaleItem	63a4cc6f-c8fa-4c05-8ddc-8ba39aa2de14	1	GET	19
04aeff66-f8d7-49b3-b426-78f011735acd	5770e029-af52-4630-840c-0703653586ce	2020-10-23 05:48:54	App\\Entity\\SaleItem	c08a6568-294e-49fe-975e-21c81f12cfc5	1	GET	5
c6368f9f-2b31-4396-a4bb-406021582566	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-10-23 05:51:09	App\\Entity\\SaleItem	85c87643-335a-4be0-9016-863c06593e34	2	GET	53
ae72a305-8b5d-48bd-ad54-39b4b5d8737c	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-03 02:35:10	App\\Entity\\SaleItem	ef9fbdbd-0b38-4d62-a8ab-348d83eeb5d7	4	GET	37
c0bdd322-f744-4d65-997f-bf1acf934b27	b58e0a50-2e90-4527-9aab-812dbdcc7f02	2020-11-03 02:35:56	App\\Entity\\SaleItem	d3bcafaf-b9a3-45b6-ba07-26f832fcea8d	1	GET	24
a24fb3af-6cc1-4122-89c6-0aea8f748180	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-11-03 02:36:55	App\\Entity\\SaleItem	de5e3949-21fd-410a-a91d-fe3c10bbe572	2	GET	5
7046cffe-5760-40d1-89da-800b268b40c2	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-06 01:34:07	App\\Entity\\SaleItem	67a6c988-fd6c-448c-94df-7515cbac12f8	4	GET	125
8fb19dea-a7cc-4e5d-acbc-98ba4e5ba07a	17a6b989-7718-4487-94a3-651d019a5a4a	2020-11-06 01:34:07	App\\Entity\\SaleItem	81028238-9a52-4b68-9527-818d1aca398a	1	GET	45
3fb6ed7f-bb94-415a-863d-435fff5d63de	d31957df-bf1f-45bf-a931-39635432e566	2020-11-06 01:34:07	App\\Entity\\SaleItem	a61fb3dd-5e81-47ec-936c-f6aa30ca1dac	1	GET	72
9fe83108-85ee-485e-88dd-c985d0119eb6	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-11-06 01:34:07	App\\Entity\\SaleItem	fc09d49e-7062-48c0-9786-6f79a8480a49	1	GET	52
ac44c6c3-d385-4b32-acc7-54a98f1a6734	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-06 01:34:07	App\\Entity\\SaleItem	33419aa8-0b0f-49ae-8f64-b0b250413390	1	GET	107
ed9b91a8-bb1d-49d5-ae53-73d45cebcc87	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-11-06 01:34:07	App\\Entity\\SaleItem	fb65f1bf-db4c-4e3c-9e4e-30e531794378	1	GET	257
2efe3218-4c23-423c-a34e-fbdb9db07cc6	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	2020-11-06 01:34:07	App\\Entity\\SaleItem	a10d230d-4729-47af-9e10-61d4839b19ab	1	GET	198
5d99e8e2-fa5a-4a38-9dae-43861fe59200	2aeb8827-643d-4b9d-885a-577ad3de919c	2020-11-06 01:36:30	App\\Entity\\SaleItem	1cba2401-ec16-43ce-b168-6c250c2f2330	1	GET	4
68295d84-0322-4fc0-9f8e-38d81cde461c	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-06 01:36:30	App\\Entity\\SaleItem	85e61d7c-a9e3-40e1-9837-0f0c43438297	1	GET	18
3e432633-28ec-448c-9107-314262d5b7c8	61a18df1-774f-49b6-8f49-7cb2682b3fd6	2020-11-06 01:36:30	App\\Entity\\SaleItem	0b7d1cce-43d3-4d86-88d3-323e57f2515b	1	GET	8
01580590-ece0-41fb-8a2f-116c117175ff	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-11-06 01:36:30	App\\Entity\\SaleItem	441c75f3-01fb-4905-9106-333e2c26a4b1	1	GET	4
1de5f250-4056-41af-9ff4-e6823e00cb81	b58e0a50-2e90-4527-9aab-812dbdcc7f02	2020-11-06 01:39:02	App\\Entity\\SaleItem	aee814a7-55f5-4189-ada3-4346a5e4aaec	2	GET	22
0ce362c1-2b8b-4426-93da-d8190827c3ff	b58e0a50-2e90-4527-9aab-812dbdcc7f02	2020-11-06 01:40:21	App\\Entity\\SaleItem	aee814a7-55f5-4189-ada3-4346a5e4aaec	2	GET	20
4412f7eb-9f4f-4a1a-b783-dcd3b66ac487	b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	2020-11-06 04:11:30	App\\Entity\\SaleItem	f7ee1f1a-f91e-4abe-8ee8-6157d2438a5c	1	GET	145
22ba5bfe-754a-437a-bb4f-4c97585ab8b3	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-06 04:11:58	App\\Entity\\SaleItem	97613694-dd88-4066-8430-e750f4b55206	1	GET	17
706d039b-824b-40e0-9c75-79a6f6a40b71	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-06 04:14:39	App\\Entity\\SaleItem	67a6c988-fd6c-448c-94df-7515cbac12f8	4	GET	121
48f0c563-4f36-4a48-adfa-e16027700d9a	17a6b989-7718-4487-94a3-651d019a5a4a	2020-11-06 04:14:39	App\\Entity\\SaleItem	81028238-9a52-4b68-9527-818d1aca398a	1	GET	44
1743c8a6-dc2c-4dfe-9498-6526df9ce4d2	d31957df-bf1f-45bf-a931-39635432e566	2020-11-06 04:14:39	App\\Entity\\SaleItem	a61fb3dd-5e81-47ec-936c-f6aa30ca1dac	1	GET	71
520431d4-d3cc-437c-b642-ef1c5b2b213b	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-11-06 04:14:39	App\\Entity\\SaleItem	fc09d49e-7062-48c0-9786-6f79a8480a49	1	GET	51
3e53d1fc-08f9-46ce-b9b2-7d184b0445f2	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-06 04:14:39	App\\Entity\\SaleItem	33419aa8-0b0f-49ae-8f64-b0b250413390	1	GET	106
95f03ae5-b5f6-4796-b9be-1240f228ce67	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-11-06 04:14:39	App\\Entity\\SaleItem	fb65f1bf-db4c-4e3c-9e4e-30e531794378	1	GET	256
77df3176-00dc-477c-9e37-d754cb5d3b3b	2ee2f91a-eefc-43ce-aa7a-26f54fe2ff66	2020-11-06 04:14:39	App\\Entity\\SaleItem	a10d230d-4729-47af-9e10-61d4839b19ab	1	GET	197
10a4e2cf-8b37-4786-acd0-a80decb61440	2aeb8827-643d-4b9d-885a-577ad3de919c	2020-11-06 04:14:39	App\\Entity\\SaleItem	1cba2401-ec16-43ce-b168-6c250c2f2330	1	GET	3
f90748de-2754-4b7d-abfe-be3b205adb02	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-06 04:14:39	App\\Entity\\SaleItem	85e61d7c-a9e3-40e1-9837-0f0c43438297	1	GET	16
84339a9f-65ab-4aa7-a4ac-3a902aeff58d	61a18df1-774f-49b6-8f49-7cb2682b3fd6	2020-11-06 04:14:39	App\\Entity\\SaleItem	0b7d1cce-43d3-4d86-88d3-323e57f2515b	1	GET	7
3f5c03ad-65bc-4689-ad4c-d8ce536fa8ae	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-11-06 04:14:39	App\\Entity\\SaleItem	441c75f3-01fb-4905-9106-333e2c26a4b1	1	GET	3
dcfa524c-9821-4d65-ad20-2a8eab019ec7	b58e0a50-2e90-4527-9aab-812dbdcc7f02	2020-11-06 04:14:39	App\\Entity\\SaleItem	aee814a7-55f5-4189-ada3-4346a5e4aaec	2	GET	18
a9d3070e-a656-491c-b723-c074cc432475	b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	2020-11-06 04:14:39	App\\Entity\\SaleItem	f7ee1f1a-f91e-4abe-8ee8-6157d2438a5c	1	GET	144
c04c90b8-783b-4911-bd9a-20bb371906b7	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-06 04:14:39	App\\Entity\\SaleItem	97613694-dd88-4066-8430-e750f4b55206	1	GET	15
f1ecb786-8b02-4575-8aeb-cd7eb3849c03	c280399a-dd0f-487a-93c2-93d631d82782	2020-11-07 02:28:08	App\\Entity\\PurchaseDetail	3f133c07-5b48-428c-82ff-c39f74bc4eb6	100	ADD	104
001ed80d-c748-4989-a687-6ebac19abf31	78561982-b3fa-45d6-94c6-3b99b397c756	2020-11-12 03:30:08	App\\Entity\\SaleItem	9ce272f5-70c8-472d-8c0b-f55530d1bf8b	2	GET	121
472a3c9e-53af-4a8d-ba3b-7622a38636eb	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-12 03:30:08	App\\Entity\\SaleItem	339e3c9e-ae49-44df-8b9a-37b1371d8909	3	GET	750
c6ee4f93-d867-4d40-b7eb-c3ec3dc386c3	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-12 03:30:08	App\\Entity\\SaleItem	eba60025-f469-441c-84d3-52ec5bb9c6ce	1	GET	105
2f03e103-78a2-4377-8dda-a80551183304	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 03:30:08	App\\Entity\\SaleItem	146fd5a1-d582-4b9f-abb1-5a618b7a93a0	6	GET	115
8a403195-2c98-44a6-b432-6dbf7297db7e	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 03:30:08	App\\Entity\\SaleItem	7f97f811-93bb-4100-b78c-5842317654b5	6	GET	109
4d7d952e-7d60-437a-9d1f-cce2382193c2	60f0ecfe-0c71-4b43-a002-de6e2994e076	2020-11-12 03:30:08	App\\Entity\\SaleItem	afd0cbab-693b-4f72-aa74-7bd7eaee3e3c	1	GET	25
16713e4c-ca17-4f13-b4ab-042d2be40d06	06a0cade-0e05-456a-b408-ba1842c95f18	2020-11-12 03:30:08	App\\Entity\\SaleItem	c1d57688-9580-438b-8ca7-dc7a38482653	1	GET	127
e590c159-fd01-4a97-9ebe-ac006c1651b1	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-12 03:30:08	App\\Entity\\SaleItem	d7cb73a1-8e3a-4516-a2bb-af4e94bc4d35	1	GET	155
6e633120-5ff9-4d06-a8a6-31320902334c	a710f31b-9840-46a3-a903-0bed2ad13807	2020-11-12 03:30:08	App\\Entity\\SaleItem	d39a2d22-7aa0-46de-9a24-003a6c50d686	1	GET	56
29ec21ed-aef5-4019-9845-125e2f8cd36d	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-12 03:30:08	App\\Entity\\SaleItem	1de280ca-41c3-4b1b-8a32-33f9d6b0a652	1	GET	749
bb3b3173-b3a0-4198-8000-ca46b781c935	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-11-12 03:32:55	App\\Entity\\SaleItem	581f57c2-78bf-4833-9a03-e879fb12cf8b	2	GET	1
ddcba1d5-604e-46af-9568-ca91124df3d4	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-12 03:32:55	App\\Entity\\SaleItem	db20f13e-1680-4930-a274-a4a72e871b9a	12	GET	143
ecc544be-2238-4501-ad61-915ce00230ef	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 03:32:55	App\\Entity\\SaleItem	0d968b2f-666f-45ad-a9d3-1e2d138598de	12	GET	97
68fce8f6-d002-4b44-9fc4-3a57ffa52748	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-12 03:32:55	App\\Entity\\SaleItem	499505a9-ad05-481f-bfbf-3baeceee0f09	2	GET	35
4cf5211c-1f01-4b47-b6f0-520f6a99c9bd	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-12 03:34:02	App\\Entity\\SaleItem	3c9c2463-b7a2-4e78-adfc-9a4340d4e7ad	1	GET	14
c134c5aa-ee3b-40d4-999a-b98310d947c9	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-12 03:36:07	App\\Entity\\SaleItem	08e8c964-5cae-4e11-8336-feb22c7b2479	1	GET	34
c41e7093-d105-42fa-b709-5edbb16c54cc	c2f5f050-3dd7-4862-9b67-e72cb4aa9c57	2020-11-12 03:36:07	App\\Entity\\SaleItem	06c0335c-564b-4bd3-a082-c94cf4183be2	1	GET	11
b15c01f2-ea55-4632-9627-216f9bc7b32f	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-12 03:36:07	App\\Entity\\SaleItem	d1ebcc7d-1cd7-4861-9af4-0eac8c0969ff	12	GET	22
1159179b-c306-41b8-93e1-e49b1404ea8d	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 03:42:00	App\\Entity\\SaleItem	71bd3d2f-4c2f-4bdd-a13a-0d305c67a072	12	GET	85
35a1e80f-7e78-47ec-8b3e-8d80738344d1	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-12 03:42:00	App\\Entity\\SaleItem	985a9eaf-1b5b-4029-aed9-8650a73190ca	2	GET	12
fd38891a-b47c-4231-81ec-e2f62dd4351b	78561982-b3fa-45d6-94c6-3b99b397c756	2020-11-12 03:42:00	App\\Entity\\SaleItem	45a4d99e-c022-4b9e-aced-6883796e6f5d	50	GET	71
7431ccee-0318-48f6-9151-2fb8f54cd9de	9e9e5f57-a956-470f-8b8c-a3d6f29a9894	2020-11-12 03:42:00	App\\Entity\\SaleItem	e2f7d07a-5e60-4627-98fb-7eb124127673	5	GET	10
f4eda1b0-d368-401b-b383-531c1e89bec8	5770e029-af52-4630-840c-0703653586ce	2020-11-12 03:42:00	App\\Entity\\SaleItem	51a0972f-4d4d-4f75-9888-1552e4b0ea6e	1	GET	4
baac1b7a-1f24-4fe9-97b1-0b2868665355	3a9a6286-20b7-42e8-903f-0a64cc5ec639	2020-11-12 03:42:00	App\\Entity\\SaleItem	bef5cd26-07ec-4f6b-afb5-f31edb4de4ad	1	GET	6
cb69bc81-2597-4ca0-933f-80d83c8fbb37	5556e2f3-a8c0-441d-b696-07ea76048743	2020-11-12 03:42:00	App\\Entity\\SaleItem	dbb5a4eb-675b-4f09-9f07-6b5265d6cea2	2	GET	8
f4cd33b8-1c1c-49e2-aac9-b632d15e56df	ce2cad02-0a94-4138-bd11-fae5f15376b1	2020-11-12 03:43:22	App\\Entity\\SaleItem	94a7c911-4968-461b-a0e3-a246885f1737	2	GET	7
5d1e6106-a435-4917-a2f9-a58cc55ebecd	78561982-b3fa-45d6-94c6-3b99b397c756	2020-11-12 06:26:17	App\\Entity\\SaleItem	9ce272f5-70c8-472d-8c0b-f55530d1bf8b	2	GET	69
3a94585f-ebcf-4798-b944-162bf876aa7d	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-12 06:26:17	App\\Entity\\SaleItem	339e3c9e-ae49-44df-8b9a-37b1371d8909	3	GET	746
d2af79fa-5594-4184-8236-ea13b796cc91	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-12 06:26:17	App\\Entity\\SaleItem	eba60025-f469-441c-84d3-52ec5bb9c6ce	1	GET	104
d5238ed5-8ffc-4e60-b962-b712375ca727	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 06:26:17	App\\Entity\\SaleItem	146fd5a1-d582-4b9f-abb1-5a618b7a93a0	6	GET	79
44908118-6c68-4897-9fb2-81e8db82557d	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 06:26:17	App\\Entity\\SaleItem	7f97f811-93bb-4100-b78c-5842317654b5	6	GET	73
641e2c43-5967-4266-8d6d-441a6360f09c	60f0ecfe-0c71-4b43-a002-de6e2994e076	2020-11-12 06:26:17	App\\Entity\\SaleItem	afd0cbab-693b-4f72-aa74-7bd7eaee3e3c	1	GET	24
14106edd-b488-4892-8275-d57d6b3b2787	06a0cade-0e05-456a-b408-ba1842c95f18	2020-11-12 06:26:17	App\\Entity\\SaleItem	c1d57688-9580-438b-8ca7-dc7a38482653	1	GET	126
6136e2a8-eb9e-48fb-8e80-e0b93ab009a3	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-12 06:26:17	App\\Entity\\SaleItem	d7cb73a1-8e3a-4516-a2bb-af4e94bc4d35	1	GET	142
a9f8d40a-5bb6-461b-8f7d-71a93d3cd775	a710f31b-9840-46a3-a903-0bed2ad13807	2020-11-12 06:26:17	App\\Entity\\SaleItem	d39a2d22-7aa0-46de-9a24-003a6c50d686	1	GET	55
1154ddbe-5e3c-4c4f-824f-45213fb0b8d4	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-12 06:26:17	App\\Entity\\SaleItem	1de280ca-41c3-4b1b-8a32-33f9d6b0a652	1	GET	745
509edcbd-4b30-49c1-898a-5a056f664f13	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-11-12 06:26:17	App\\Entity\\SaleItem	581f57c2-78bf-4833-9a03-e879fb12cf8b	2	GET	-1
04971209-1a5c-4802-8622-4f1dd13959cc	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-12 06:26:17	App\\Entity\\SaleItem	db20f13e-1680-4930-a274-a4a72e871b9a	12	GET	130
e32ea105-9ba4-4d03-977e-332e83ec221b	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 06:26:17	App\\Entity\\SaleItem	0d968b2f-666f-45ad-a9d3-1e2d138598de	12	GET	61
1bdab10f-36a1-4c96-8722-d36f432527ee	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-12 06:26:17	App\\Entity\\SaleItem	499505a9-ad05-481f-bfbf-3baeceee0f09	2	GET	20
2633279c-f6b8-4899-ab55-4e6aaad237bb	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-12 06:26:17	App\\Entity\\SaleItem	3c9c2463-b7a2-4e78-adfc-9a4340d4e7ad	1	GET	11
385b550c-3af3-4d04-ac5e-e7a0cb782d0c	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-12 06:26:17	App\\Entity\\SaleItem	08e8c964-5cae-4e11-8336-feb22c7b2479	1	GET	19
da911f4e-bbf5-493a-a9a6-644bb7e34e21	c2f5f050-3dd7-4862-9b67-e72cb4aa9c57	2020-11-12 06:26:17	App\\Entity\\SaleItem	06c0335c-564b-4bd3-a082-c94cf4183be2	1	GET	10
3fee355a-eee5-4b2e-b0b0-cb81cfac105c	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-12 06:26:17	App\\Entity\\SaleItem	d1ebcc7d-1cd7-4861-9af4-0eac8c0969ff	12	GET	7
ba05d9e9-e067-4f88-b495-06cd6011a9e3	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-12 06:26:17	App\\Entity\\SaleItem	71bd3d2f-4c2f-4bdd-a13a-0d305c67a072	12	GET	49
3d396aec-3226-45a2-8772-fd48ebb73a78	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-12 06:26:17	App\\Entity\\SaleItem	985a9eaf-1b5b-4029-aed9-8650a73190ca	2	GET	9
8b95868f-cb05-4e82-adc8-7428c11faff6	78561982-b3fa-45d6-94c6-3b99b397c756	2020-11-12 06:26:17	App\\Entity\\SaleItem	45a4d99e-c022-4b9e-aced-6883796e6f5d	50	GET	19
32fed974-5a59-4802-ab8f-e74094ab4102	9e9e5f57-a956-470f-8b8c-a3d6f29a9894	2020-11-12 06:26:17	App\\Entity\\SaleItem	e2f7d07a-5e60-4627-98fb-7eb124127673	5	GET	5
1fc23d84-5638-4cdc-a4fe-e4f1357bd0a0	5770e029-af52-4630-840c-0703653586ce	2020-11-12 06:26:17	App\\Entity\\SaleItem	51a0972f-4d4d-4f75-9888-1552e4b0ea6e	1	GET	3
0644d217-e1c3-49c7-9ee0-60dbaeb87710	3a9a6286-20b7-42e8-903f-0a64cc5ec639	2020-11-12 06:26:17	App\\Entity\\SaleItem	bef5cd26-07ec-4f6b-afb5-f31edb4de4ad	1	GET	5
853bd4b0-937e-45a0-91af-8b430dea7535	5556e2f3-a8c0-441d-b696-07ea76048743	2020-11-12 06:26:17	App\\Entity\\SaleItem	dbb5a4eb-675b-4f09-9f07-6b5265d6cea2	2	GET	6
4628c5a6-246f-469a-9611-f48bc6934707	ce2cad02-0a94-4138-bd11-fae5f15376b1	2020-11-12 06:26:17	App\\Entity\\SaleItem	94a7c911-4968-461b-a0e3-a246885f1737	2	GET	5
8a394003-2d05-4603-86a0-386cb156ce7a	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-17 02:28:26	App\\Entity\\SaleItem	a5da85cc-73b9-4912-b5b7-1acbb84a61df	7	GET	738
ed36156f-053d-4029-92be-8c587e745dd8	2a3db456-1c32-4bbe-bcaa-e6f303c40401	2020-11-17 02:28:26	App\\Entity\\SaleItem	a1b98fe7-af62-4641-8853-5f3736275db1	2	GET	197
982ee993-926f-45e8-97eb-66cc73814d43	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-17 02:28:26	App\\Entity\\SaleItem	2920aedb-edbd-4479-a499-1a21af3214c4	1	GET	48
0ae32f0f-ec85-45d9-8fce-8ea4733a6191	3178a471-6e02-4db9-a506-602818e6447f	2020-11-17 02:28:26	App\\Entity\\SaleItem	51f9302f-2921-47ff-88e0-e9b6de6d337a	2	GET	24
92b800a5-ce8b-4e61-a3ee-f620bd8946f0	8a554019-4e83-458a-83c0-20fb0cd842d3	2020-11-17 02:28:26	App\\Entity\\SaleItem	becf9589-ab3b-474c-845c-c2cddc99c549	3	GET	2
0cbd3a47-5d2a-4ac3-a78b-f6488c90a6f8	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-11-17 02:28:26	App\\Entity\\SaleItem	f6bb0b8a-5afc-4762-8dc5-2e46db84fec1	1	GET	255
03a3198e-eb76-409b-aa43-079c26e85fc8	9f9c516a-b6f1-449c-8663-9cbedc6ca0a5	2020-11-17 02:28:26	App\\Entity\\SaleItem	5fcb1364-490a-40bc-b629-b0c687271074	1	GET	36
a8d4de3a-b402-450b-afcb-60072f47612a	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-17 02:28:26	App\\Entity\\SaleItem	bb645e19-307f-4dfd-be58-f3346eb7144c	1	GET	103
6345aa9d-d135-485f-9764-f4798e042abb	0c5cc742-e4c9-4e54-8c49-a9fcda2cf45a	2020-11-17 02:29:09	App\\Entity\\SaleItem	8f7d8b64-e83e-4015-8f88-fd941c2efb5e	1	GET	19
052d657c-2dfe-4e61-85e6-4d1aa822b50a	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	2020-11-17 02:29:52	App\\Entity\\SaleItem	1d20b57a-2eed-451a-8f1f-3d68f2890a75	1	GET	2
4dc97221-1831-4c6f-9c08-e05a609b929a	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	2020-11-17 02:30:58	App\\Entity\\SaleItem	5e45a40c-d188-4596-8c61-73e88301d565	1	GET	1
0150545f-ef37-4211-871d-ad6d5f09d599	914edc82-ac6f-47e2-8a37-4693f562e586	2020-11-17 02:30:58	App\\Entity\\SaleItem	1ff5c221-c8f2-40c3-abc7-ebd0c1a19ddc	12	GET	28
891af487-fdf0-4398-9ebc-04d386a77066	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-17 05:28:31	App\\Entity\\SaleItem	a5da85cc-73b9-4912-b5b7-1acbb84a61df	7	GET	731
70945766-0c1d-4a4e-90f4-859b43b2ee9f	2a3db456-1c32-4bbe-bcaa-e6f303c40401	2020-11-17 05:28:31	App\\Entity\\SaleItem	a1b98fe7-af62-4641-8853-5f3736275db1	2	GET	195
bebe9c1f-22bc-4541-8b20-2aab9f141578	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-17 05:28:31	App\\Entity\\SaleItem	2920aedb-edbd-4479-a499-1a21af3214c4	1	GET	47
4d5a8cb3-4451-45d8-820e-623b55d17478	3178a471-6e02-4db9-a506-602818e6447f	2020-11-17 05:28:31	App\\Entity\\SaleItem	51f9302f-2921-47ff-88e0-e9b6de6d337a	2	GET	22
cf613cad-18b7-41c5-bd11-eac3cf07b411	8a554019-4e83-458a-83c0-20fb0cd842d3	2020-11-17 05:28:31	App\\Entity\\SaleItem	becf9589-ab3b-474c-845c-c2cddc99c549	3	GET	-1
1c381a9a-7f20-4564-9c0b-0a898c8be247	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-11-17 05:28:31	App\\Entity\\SaleItem	f6bb0b8a-5afc-4762-8dc5-2e46db84fec1	1	GET	254
eed32d1a-7523-47ac-a0c3-169317e000ff	9f9c516a-b6f1-449c-8663-9cbedc6ca0a5	2020-11-17 05:28:31	App\\Entity\\SaleItem	5fcb1364-490a-40bc-b629-b0c687271074	1	GET	35
a172a3ba-97d2-41a0-8717-c3f113a45516	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-17 05:28:31	App\\Entity\\SaleItem	bb645e19-307f-4dfd-be58-f3346eb7144c	1	GET	102
1db2d842-27a6-48e9-adee-b796fffa7011	0c5cc742-e4c9-4e54-8c49-a9fcda2cf45a	2020-11-17 05:28:31	App\\Entity\\SaleItem	8f7d8b64-e83e-4015-8f88-fd941c2efb5e	1	GET	18
bfe3bdd9-d604-4c73-be76-352991d45e8a	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	2020-11-17 05:28:31	App\\Entity\\SaleItem	1d20b57a-2eed-451a-8f1f-3d68f2890a75	1	GET	0
1024bacc-14b4-4754-aed9-498ead13fed3	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	2020-11-17 05:28:31	App\\Entity\\SaleItem	5e45a40c-d188-4596-8c61-73e88301d565	1	GET	-1
a161353b-c310-4f97-9293-c41a8ad42c4e	914edc82-ac6f-47e2-8a37-4693f562e586	2020-11-17 05:28:31	App\\Entity\\SaleItem	1ff5c221-c8f2-40c3-abc7-ebd0c1a19ddc	12	GET	16
b0faba23-8727-462f-b758-93920c6ffbf8	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	2020-11-21 02:36:23	App\\Entity\\SaleItem	1d20b57a-2eed-451a-8f1f-3d68f2890a75	1	GET	-2
43d96d40-6d96-45a7-872b-5a1fbf81e958	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-21 02:38:39	App\\Entity\\SaleItem	08e8c964-5cae-4e11-8336-feb22c7b2479	1	GET	6
cbfcd230-a2d4-4a09-b2e7-cd3e0633dae2	c2f5f050-3dd7-4862-9b67-e72cb4aa9c57	2020-11-21 02:38:39	App\\Entity\\SaleItem	06c0335c-564b-4bd3-a082-c94cf4183be2	1	GET	9
87cc525f-7736-43d0-bfc8-073cda7646dc	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-21 02:38:39	App\\Entity\\SaleItem	d1ebcc7d-1cd7-4861-9af4-0eac8c0969ff	12	GET	-6
686eb3a0-0c21-4890-966d-eb562e3fec93	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-21 02:59:20	App\\Entity\\SaleItem	5377bda0-dd2e-4c2b-b12f-174425115de3	3	GET	728
2d778196-d9f7-4c1a-b465-389848490734	0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	2020-11-21 02:59:20	App\\Entity\\SaleItem	bbd85f7d-2531-462a-97fd-be47ea7b636a	2	GET	24
54b74409-ada3-488a-a2cb-78dce2b99e77	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-11-21 02:59:20	App\\Entity\\SaleItem	c4fc47fd-7466-4e75-beef-5e48810c5b38	3	GET	251
0b2c5436-284c-4586-80cd-9c0e8f3d3ec4	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-21 02:59:20	App\\Entity\\SaleItem	14aef385-36d7-44f6-b912-d2e4db635e82	2	GET	100
a0b21ff1-948e-4ab9-837a-8c8a83f48be6	b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	2020-11-21 02:59:20	App\\Entity\\SaleItem	f5f1379d-f0e3-47b0-8b2b-297ca616caab	2	GET	142
8e9a1f60-7ee8-4188-8f84-b35c72d9a05f	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-11-21 02:59:20	App\\Entity\\SaleItem	6eba46fa-e133-45b4-9420-c24caca601e5	4	GET	47
e21809a5-c4ef-4a1c-b60b-77104c972e56	b67980a9-de04-4973-b7dc-bc61242c8f38	2020-11-21 02:59:20	App\\Entity\\SaleItem	b9671579-9ade-47d1-b247-6c230c641018	2	GET	168
0290c973-07fa-4e19-b957-3721bacb93cf	06a0cade-0e05-456a-b408-ba1842c95f18	2020-11-21 02:59:20	App\\Entity\\SaleItem	e37c9cc0-6b44-44da-8c04-83a5e02257ab	2	GET	124
05f39373-58cf-4c4b-8fd7-31659f63369e	e20071bd-1488-4e16-8b65-f9ba74f5c0e5	2020-11-21 02:59:20	App\\Entity\\SaleItem	a5647cbe-1bf3-48ce-bb2b-982960156940	1	GET	15
ca292ea0-14d4-4fa3-ad64-78d61772f8b0	805f19d0-7aff-4694-818a-3e5165d146ef	2020-11-21 02:59:20	App\\Entity\\SaleItem	d105601f-c595-4724-9173-bc66fe48e193	1	GET	55
c1901fe1-4367-445c-ae10-7b92ccca453e	d234256e-925c-4d1b-8426-c843d9f395aa	2020-11-21 02:59:20	App\\Entity\\SaleItem	f12d2fe7-abb0-4547-89eb-9410067ee43d	1	GET	54
863607da-d22f-4dd5-8b8d-0aad085caa67	3a9a6286-20b7-42e8-903f-0a64cc5ec639	2020-11-21 02:59:20	App\\Entity\\SaleItem	b65129ef-b0ca-467b-9ee9-57fa4088b3ec	1	GET	4
6fdeb286-469b-44ef-9d12-cd4a715c27fe	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-21 02:59:20	App\\Entity\\SaleItem	bf8ccc31-64f5-4fdb-9acd-050f49229612	3	GET	44
30679a19-af95-414c-aee0-678fbde6e140	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-21 02:59:20	App\\Entity\\SaleItem	f35424fb-e1e6-4de8-b1cd-828dd1c378fa	5	GET	125
e6238b5c-5c3d-464b-8a67-aae3aa7ba131	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-21 02:59:20	App\\Entity\\SaleItem	c65517d0-734a-43d5-802b-6415e0c4e7c2	1	GET	99
032765fa-d0f6-4915-a1eb-7b7057975024	9737c64e-cda0-4e10-9c0d-de9e94bc4a51	2020-11-21 02:59:20	App\\Entity\\SaleItem	7343803b-cda9-455c-9682-180fd4c80458	2	GET	9
4fd3a61e-e4ee-4249-bdbe-180bff41565b	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-21 02:59:20	App\\Entity\\SaleItem	e2a3a539-9b5e-49a1-94e2-7fb2de34af26	1	GET	124
49314c8d-50c6-418a-afd4-e8e3088cf51d	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-11-21 02:59:20	App\\Entity\\SaleItem	1cfb58c3-16e2-4b7c-8664-3235de827fbd	1	GET	46
d8d8e320-65f4-4a2c-8eae-626c93eb44dd	c280399a-dd0f-487a-93c2-93d631d82782	2020-11-21 03:00:36	App\\Entity\\SaleItem	34e8abab-4a9f-4728-add5-1a6051afc306	10	GET	94
937bff70-024c-41d6-9aba-7263c76b9172	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-21 03:12:23	App\\Entity\\SaleItem	9c24e0ed-f4d0-47e6-a593-63ff77f65443	1	GET	8
ba780b7d-da98-4f78-b429-3f281373bda3	230e0a48-0047-45e7-a253-601ba845011e	2020-11-21 03:12:23	App\\Entity\\SaleItem	2e48ce49-4482-40dd-b613-f2a92adbd2ab	1	GET	1
4725aa8a-72aa-4a48-8ebd-4048e41a0579	8a554019-4e83-458a-83c0-20fb0cd842d3	2020-11-21 03:50:22	App\\Entity\\ImportStockItem	4c69a6a0-2ba4-11eb-937f-18d6c71396fe	1	ADD	1
d0768379-1c20-41b4-93d5-719daa49aa20	5c9e60e8-2825-4e46-945b-74e8b1a73fc9	2020-11-21 04:43:53	App\\Entity\\SaleItem	5377bda0-dd2e-4c2b-b12f-174425115de3	3	GET	725
76ed4656-8c95-45d5-844c-c8f1514099c4	0c7fe2c2-9c6b-4ebc-a71f-b01a2041e5a3	2020-11-21 04:43:53	App\\Entity\\SaleItem	bbd85f7d-2531-462a-97fd-be47ea7b636a	2	GET	22
7741a228-ce6c-4bbd-a6ac-41e4928512cc	b7dbe40f-0e23-4480-88ab-f78c4e76fe3d	2020-11-21 04:43:53	App\\Entity\\SaleItem	c4fc47fd-7466-4e75-beef-5e48810c5b38	3	GET	248
c3d8ff57-9fda-41d3-8900-feeabd3aaac9	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-21 04:43:53	App\\Entity\\SaleItem	14aef385-36d7-44f6-b912-d2e4db635e82	2	GET	97
91afaa4d-9315-4ced-910c-4f91ad70aaa8	b66ce5a3-4f0a-4d17-bc69-b2ce7a6040f4	2020-11-21 04:43:53	App\\Entity\\SaleItem	f5f1379d-f0e3-47b0-8b2b-297ca616caab	2	GET	140
cdb43e23-3628-4399-8114-5c01fa26268d	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-11-21 04:43:53	App\\Entity\\SaleItem	6eba46fa-e133-45b4-9420-c24caca601e5	4	GET	42
1b5865ef-11ee-4269-ae7b-fa94c307b947	b67980a9-de04-4973-b7dc-bc61242c8f38	2020-11-21 04:43:53	App\\Entity\\SaleItem	b9671579-9ade-47d1-b247-6c230c641018	2	GET	166
8280c386-1c60-41e7-9b8b-df420af91369	06a0cade-0e05-456a-b408-ba1842c95f18	2020-11-21 04:43:53	App\\Entity\\SaleItem	e37c9cc0-6b44-44da-8c04-83a5e02257ab	2	GET	122
3392c00d-3a48-4f75-b0a9-5b8df6e8a21f	e20071bd-1488-4e16-8b65-f9ba74f5c0e5	2020-11-21 04:43:53	App\\Entity\\SaleItem	a5647cbe-1bf3-48ce-bb2b-982960156940	1	GET	14
dc2de601-0a89-4805-8fef-6fd7ace1dc84	805f19d0-7aff-4694-818a-3e5165d146ef	2020-11-21 04:43:53	App\\Entity\\SaleItem	d105601f-c595-4724-9173-bc66fe48e193	1	GET	54
30d21620-8cc6-478f-bc0e-5ecc57732eb8	d234256e-925c-4d1b-8426-c843d9f395aa	2020-11-21 04:43:53	App\\Entity\\SaleItem	f12d2fe7-abb0-4547-89eb-9410067ee43d	1	GET	53
c855744f-0786-4232-bef6-4a763dedba19	3a9a6286-20b7-42e8-903f-0a64cc5ec639	2020-11-21 04:43:53	App\\Entity\\SaleItem	b65129ef-b0ca-467b-9ee9-57fa4088b3ec	1	GET	3
6f6f1520-0c8f-478e-aea8-8f571b095958	788a1deb-c1d9-449d-b07e-d800e4feaab3	2020-11-21 04:43:53	App\\Entity\\SaleItem	bf8ccc31-64f5-4fdb-9acd-050f49229612	3	GET	41
a1a35d69-6bb9-45e6-9986-6d20c1ce4a9c	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-21 04:43:53	App\\Entity\\SaleItem	f35424fb-e1e6-4de8-b1cd-828dd1c378fa	5	GET	119
19f635d5-f41c-48a2-b916-36845c9536e4	6e780ad3-48b9-4cae-9faf-e39cfb3c4176	2020-11-21 04:43:53	App\\Entity\\SaleItem	c65517d0-734a-43d5-802b-6415e0c4e7c2	1	GET	96
38c92785-be80-4439-9388-c402eef51190	9737c64e-cda0-4e10-9c0d-de9e94bc4a51	2020-11-21 04:43:53	App\\Entity\\SaleItem	7343803b-cda9-455c-9682-180fd4c80458	2	GET	7
6fcd25d9-7474-4953-8383-b77b815b7453	ed2a6d7e-fb8a-45dd-8281-4cf0cc2b292a	2020-11-21 04:43:53	App\\Entity\\SaleItem	e2a3a539-9b5e-49a1-94e2-7fb2de34af26	1	GET	118
807fc8a4-0a0b-4b0a-baa8-9398f7ab54ed	aa8051af-fcf7-43ea-9449-c61ab4597017	2020-11-21 04:43:53	App\\Entity\\SaleItem	1cfb58c3-16e2-4b7c-8664-3235de827fbd	1	GET	41
c5832a79-0ae7-4e5c-be2e-16210c534b07	c280399a-dd0f-487a-93c2-93d631d82782	2020-11-21 04:43:53	App\\Entity\\SaleItem	34e8abab-4a9f-4728-add5-1a6051afc306	10	GET	84
de44a53a-841a-46be-8061-e498a89c02dc	9668ff45-83d7-466d-a266-9c4d54eb2993	2020-11-21 04:43:53	App\\Entity\\SaleItem	9c24e0ed-f4d0-47e6-a593-63ff77f65443	1	GET	7
e80fb09b-dff2-4747-a837-d08cdb7cecc8	230e0a48-0047-45e7-a253-601ba845011e	2020-11-21 04:43:53	App\\Entity\\SaleItem	2e48ce49-4482-40dd-b613-f2a92adbd2ab	1	GET	0
811ae6bc-d93c-4975-91a0-463ef97e82c9	c4ece0d9-bb42-4240-bff0-7adf92f4b93c	2020-11-21 05:06:47	App\\Entity\\ImportStockItem	f99f5432-2bae-11eb-9e7a-18d6c71396fe	1	ADD	1
50101b9f-af66-46b0-9e2b-06480deb7431	43dadff4-fb9f-4d4c-aca8-bc59386fbf8b	2020-11-21 05:06:48	App\\Entity\\ImportStockItem	f9e49b96-2bae-11eb-84d0-18d6c71396fe	23	ADD	23
6c14efdc-67e3-49e4-aeb8-1c1120db0725	f9d4e754-8efe-43ff-bb7a-cad985bed182	2020-11-21 05:06:48	App\\Entity\\ImportStockItem	f9e510d0-2bae-11eb-95a0-18d6c71396fe	13	ADD	13
f0ffbde3-296b-4090-9e24-3c6f24ecc76f	3d031565-23f8-431c-a867-bbf3a17a69da	2020-11-21 05:06:48	App\\Entity\\ImportStockItem	f9e58600-2bae-11eb-9100-18d6c71396fe	1	ADD	1
\.


--
-- Data for Name: store; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.store (id, code, name, address, is_default) FROM stdin;
bf17e7ff-a64f-4b0b-9cdb-90afb4033cea	00002	MI Bahrul Ulum	Jl. Raya Pelem Watu No.9 - Menganti, Gresik	f
567be6e8-f932-4cd7-b70f-3f5689781c64	00001	SMPNU-SMKNU Bahrul Ulum	Jl. Raya Pelem Watu No.9 - Menganti, Gresik	t
\.


--
-- Data for Name: supplier; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.supplier (id, name, address, phone) FROM stdin;
6838c151-22bd-4c2b-8e87-8ce4d084ae96	SPG Materai	-	-
\.


--
-- Name: serah_terima_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.serah_terima_id_seq', 21, true);


--
-- Name: backup_log backup_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.backup_log
    ADD CONSTRAINT backup_log_pkey PRIMARY KEY (id);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: documentation documentation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documentation
    ADD CONSTRAINT documentation_pkey PRIMARY KEY (id);


--
-- Name: item_category item_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_category
    ADD CONSTRAINT item_category_pkey PRIMARY KEY (id);


--
-- Name: item_package item_package_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_package
    ADD CONSTRAINT item_package_pkey PRIMARY KEY (id);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (id);


--
-- Name: item_price_log item_price_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_price_log
    ADD CONSTRAINT item_price_log_pkey PRIMARY KEY (id);


--
-- Name: kmj_printer_client kmj_printer_client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kmj_printer_client
    ADD CONSTRAINT kmj_printer_client_pkey PRIMARY KEY (id);


--
-- Name: kmj_routing kmj_routing_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kmj_routing
    ADD CONSTRAINT kmj_routing_pkey PRIMARY KEY (id);


--
-- Name: kmj_routing_role kmj_routing_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kmj_routing_role
    ADD CONSTRAINT kmj_routing_role_pkey PRIMARY KEY (id);


--
-- Name: kmj_user kmj_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kmj_user
    ADD CONSTRAINT kmj_user_pkey PRIMARY KEY (id);


--
-- Name: packaging packaging_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.packaging
    ADD CONSTRAINT packaging_pkey PRIMARY KEY (id);


--
-- Name: purchase_detail purchase_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_detail
    ADD CONSTRAINT purchase_detail_pkey PRIMARY KEY (id);


--
-- Name: purchase purchase_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase
    ADD CONSTRAINT purchase_pkey PRIMARY KEY (id);


--
-- Name: sale_item sale_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale_item
    ADD CONSTRAINT sale_item_pkey PRIMARY KEY (id);


--
-- Name: sale sale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale
    ADD CONSTRAINT sale_pkey PRIMARY KEY (id);


--
-- Name: serah_terima serah_terima_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.serah_terima
    ADD CONSTRAINT serah_terima_pkey PRIMARY KEY (id);


--
-- Name: stock_card stock_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stock_card
    ADD CONSTRAINT stock_card_pkey PRIMARY KEY (id);


--
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.store
    ADD CONSTRAINT store_pkey PRIMARY KEY (id);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (id);


--
-- Name: idx_1f1b251e12469de2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_1f1b251e12469de2 ON public.item USING btree (category_id);


--
-- Name: idx_28194a0a4322c9eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_28194a0a4322c9eb ON public.kmj_printer_client USING btree (kmj_user_id);


--
-- Name: idx_3ac1724058735c4c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_3ac1724058735c4c ON public.kmj_routing_role USING btree (routing_id);


--
-- Name: idx_6117d13b2add6d8c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_6117d13b2add6d8c ON public.purchase USING btree (supplier_id);


--
-- Name: idx_a35551fb126f525e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_a35551fb126f525e ON public.sale_item USING btree (item_id);


--
-- Name: idx_a35551fb4a7e4868; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_a35551fb4a7e4868 ON public.sale_item USING btree (sale_id);


--
-- Name: idx_bd3933d2b092a811; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_bd3933d2b092a811 ON public.kmj_user USING btree (store_id);


--
-- Name: idx_d53541c1126f525e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_d53541c1126f525e ON public.item_package USING btree (item_id);


--
-- Name: idx_d53541c14e7b3801; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_d53541c14e7b3801 ON public.item_package USING btree (packaging_id);


--
-- Name: idx_d68975ba126f525e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_d68975ba126f525e ON public.item_price_log USING btree (item_id);


--
-- Name: idx_e54bc0059395c3f3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_e54bc0059395c3f3 ON public.sale USING btree (customer_id);


--
-- Name: idx_e54bc005b092a811; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_e54bc005b092a811 ON public.sale USING btree (store_id);


--
-- Name: idx_f0ec017126f525e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f0ec017126f525e ON public.stock_card USING btree (item_id);


--
-- Name: idx_f5697dc6126f525e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f5697dc6126f525e ON public.purchase_detail USING btree (item_id);


--
-- Name: idx_f5697dc64e7b3801; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f5697dc64e7b3801 ON public.purchase_detail USING btree (packaging_id);


--
-- Name: idx_f5697dc6558fbeb9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f5697dc6558fbeb9 ON public.purchase_detail USING btree (purchase_id);


--
-- Name: uniq_bd3933d2f85e0677; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_bd3933d2f85e0677 ON public.kmj_user USING btree (username);


--
-- Name: item fk_1f1b251e12469de2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT fk_1f1b251e12469de2 FOREIGN KEY (category_id) REFERENCES public.item_category(id);


--
-- Name: kmj_printer_client fk_28194a0a4322c9eb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kmj_printer_client
    ADD CONSTRAINT fk_28194a0a4322c9eb FOREIGN KEY (kmj_user_id) REFERENCES public.kmj_user(id);


--
-- Name: kmj_routing_role fk_3ac1724058735c4c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kmj_routing_role
    ADD CONSTRAINT fk_3ac1724058735c4c FOREIGN KEY (routing_id) REFERENCES public.kmj_routing(id);


--
-- Name: purchase fk_6117d13b2add6d8c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase
    ADD CONSTRAINT fk_6117d13b2add6d8c FOREIGN KEY (supplier_id) REFERENCES public.supplier(id);


--
-- Name: sale_item fk_a35551fb126f525e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale_item
    ADD CONSTRAINT fk_a35551fb126f525e FOREIGN KEY (item_id) REFERENCES public.item(id);


--
-- Name: sale_item fk_a35551fb4a7e4868; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale_item
    ADD CONSTRAINT fk_a35551fb4a7e4868 FOREIGN KEY (sale_id) REFERENCES public.sale(id);


--
-- Name: kmj_user fk_bd3933d2b092a811; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kmj_user
    ADD CONSTRAINT fk_bd3933d2b092a811 FOREIGN KEY (store_id) REFERENCES public.store(id);


--
-- Name: item_package fk_d53541c1126f525e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_package
    ADD CONSTRAINT fk_d53541c1126f525e FOREIGN KEY (item_id) REFERENCES public.item(id);


--
-- Name: item_package fk_d53541c14e7b3801; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_package
    ADD CONSTRAINT fk_d53541c14e7b3801 FOREIGN KEY (packaging_id) REFERENCES public.packaging(id);


--
-- Name: item_price_log fk_d68975ba126f525e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_price_log
    ADD CONSTRAINT fk_d68975ba126f525e FOREIGN KEY (item_id) REFERENCES public.item(id);


--
-- Name: sale fk_e54bc0059395c3f3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale
    ADD CONSTRAINT fk_e54bc0059395c3f3 FOREIGN KEY (customer_id) REFERENCES public.customer(id);


--
-- Name: sale fk_e54bc005b092a811; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sale
    ADD CONSTRAINT fk_e54bc005b092a811 FOREIGN KEY (store_id) REFERENCES public.store(id);


--
-- Name: stock_card fk_f0ec017126f525e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stock_card
    ADD CONSTRAINT fk_f0ec017126f525e FOREIGN KEY (item_id) REFERENCES public.item(id);


--
-- Name: purchase_detail fk_f5697dc6126f525e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_detail
    ADD CONSTRAINT fk_f5697dc6126f525e FOREIGN KEY (item_id) REFERENCES public.item(id);


--
-- Name: purchase_detail fk_f5697dc64e7b3801; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_detail
    ADD CONSTRAINT fk_f5697dc64e7b3801 FOREIGN KEY (packaging_id) REFERENCES public.packaging(id);


--
-- Name: purchase_detail fk_f5697dc6558fbeb9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_detail
    ADD CONSTRAINT fk_f5697dc6558fbeb9 FOREIGN KEY (purchase_id) REFERENCES public.purchase(id);


--
-- PostgreSQL database dump complete
--

