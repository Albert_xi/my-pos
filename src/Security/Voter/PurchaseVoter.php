<?php

namespace App\Security\Voter;

use App\Entity\Purchase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Kematjaya\UserBundle\Security\Voter\BaseVoter;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseVoter extends BaseVoter 
{
    protected function supports(string $attribute, $subject)
    {
        if (!parent::supports($attribute, $subject)) {
            return false;
        }
        
        if (!$subject instanceof Purchase) {
            return false;
        }
        
        return true;
    }
    
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if ($this->isAdministrator()) {
            //return true;
        }
        
        switch ($attribute)
        {
            case self::ACTION_CREATE:
            case self::ACTION_VIEW:
                return parent::voteOnAttribute($attribute, $subject, $token);
                break;
            case self::ACTION_EDIT:
            case self::ACTION_DELETE:
                return (parent::voteOnAttribute($attribute, $subject, $token) and !$subject->getIsLocked());
                break;
        }
        
        return false;
    }
}
