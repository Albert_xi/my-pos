<?php

namespace App\Security\Voter;

use App\Entity\SerahTerima;
use App\Repository\SerahTerimaRepository;
use Kematjaya\UserBundle\Security\Voter\BaseVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SerahTerimaVoter extends BaseVoter 
{
    private $repo;
    
    public function __construct(\Symfony\Component\Security\Core\Security $security, SerahTerimaRepository $repo) 
    {
        $this->repo = $repo;
        parent::__construct($security);
    }
    
    protected function supports(string $attribute, $subject)
    {
        if (!parent::supports($attribute, $subject)) {
            return false;
        }
        
        if (!$subject instanceof SerahTerima) {
            return false;
        }
        
        return true;
    }
    
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        switch ($attribute) {
            case self::ACTION_CREATE:
                return true;
                break;
            case self::ACTION_VIEW:
                return parent::voteOnAttribute($attribute, $subject, $token);
                break;
            default:
                return false;
        }
        
        return false;
    }
}
