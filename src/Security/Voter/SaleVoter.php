<?php

namespace App\Security\Voter;

use App\Entity\Sale;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Kematjaya\UserBundle\Security\Voter\BaseVoter;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleVoter extends BaseVoter 
{
    const ACTION_PRINT = 'print';
    const ACTION_PAY = 'pay';
    
    protected function listActions():array
    {
        return array_merge([self::ACTION_PRINT, self::ACTION_PAY], parent::listActions());
    }
    
    protected function supports(string $attribute, $subject)
    {
        if (!parent::supports($attribute, $subject)) {
            return false;
        }
        
        if (!$subject instanceof Sale) {
            return false;
        }
        
        return true;
    }
    
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if ($this->isAdministrator()) {
            //return true;
        }
        switch($attribute)
        {
            case self::ACTION_CREATE:
            case self::ACTION_VIEW:
                return parent::voteOnAttribute($attribute, $subject, $token);
                break;
            case self::ACTION_EDIT:
            case self::ACTION_DELETE:
                return (parent::voteOnAttribute($attribute, $subject, $token) and !$subject->getIsLocked());
                break;
            case self::ACTION_PRINT:
                return true;
            case self::ACTION_PAY:
                return !$subject->isPaid();
            default:
                return false;
        }
        
        return false;
    }
}
