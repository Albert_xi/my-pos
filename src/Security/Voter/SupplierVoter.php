<?php

namespace App\Security\Voter;

use App\Entity\Supplier;
use Kematjaya\UserBundle\Security\Voter\BaseVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SupplierVoter extends BaseVoter 
{
    
    protected function supports(string $attribute, $subject)
    {
        if (!parent::supports($attribute, $subject)) {
            return false;
        }
        
        if (!$subject instanceof Supplier) {
            return false;
        }
        
        return true;
    }
    
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        switch ($attribute) {
            case self::ACTION_CREATE:
            case self::ACTION_EDIT:
            case self::ACTION_VIEW:
                return parent::voteOnAttribute($attribute, $subject, $token);
                break;
            case self::ACTION_DELETE:
                return (parent::voteOnAttribute($attribute, $subject, $token) and $subject->getPurchases()->isEmpty());
                break;
        }
        
        return false;
    }
}
