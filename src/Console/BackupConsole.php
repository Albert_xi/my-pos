<?php

namespace App\Console;

use App\Library\Database\Backup\BackupInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class BackupConsole extends Command 
{
    protected static $defaultName = 'app:backup';
    
    /**
     *
     * @var BackupInterface
     */
    private $backup;
    
    public function __construct(mixed $name = null, BackupInterface $backup) 
    {
        $this->backup = $backup;
        
        parent::__construct($name);
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try{
            
            $this->backup->run();
            
            $output->writeln(sprintf("backup date %s : %s", date('d/m/Y H:i:s'), 'OK'));
            
        } catch (\Exception $ex) {
            
            $output->writeln(sprintf('backup date %s : %s', date('d/m/Y H:i:s'), $ex->getMessage()));
            
            return self::FAILURE;
        }
        
        return self::SUCCESS;
    }
}
