<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ApiViewEvent 
{
    public function onKernelView(ViewEvent $event)
    {
        $value = $event->getControllerResult();
        $format = [
            'status' => (isset($value['http_status'])) ? $value['http_status'] : Response::HTTP_OK,
            'data' => (isset($value['data'])) ? $value['data'] : null,
            'error' => (isset($value['error'])) ? $value['error'] : null
        ];
        
        $response = new JsonResponse($format, $format['status']);
        $event->setResponse($response);
    }
}
