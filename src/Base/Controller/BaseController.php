<?php

namespace App\Base\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Kematjaya\BaseControllerBundle\Controller\BaseLexikFilterController;
use Kematjaya\ImportBundle\Controller\ImportControllerTrait;
use Kematjaya\ImportBundle\Manager\ImportManagerInterface;
use Kematjaya\Export\Processor\Excel\SpreadsheetFromFileProcessor;
use Kematjaya\Export\Manager\ManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class BaseController extends BaseLexikFilterController 
{
    
    /**
     * 
     * @var ManagerInterface
     */
    protected $exportManager;
    
    /**
     * 
     * @var ImportManagerInterface
     */
    protected $importManager;
    
    use ImportControllerTrait;
    
    public function __construct(
            ImportManagerInterface $importManager, 
            ManagerInterface $exportManager,
            FilterBuilderUpdaterInterface $filterBuilderUpdater, 
            TranslatorInterface $translator,
            PaginatorInterface $paginator) 
    {
        $this->importManager = $importManager;
        $this->exportManager = $exportManager;
        
        parent::__construct($translator, $filterBuilderUpdater, $paginator);
    }
    
    protected function templateExcel(string $filePath, $data = []): BinaryFileResponse
    {
        $baseDir = $this->getParameter('excel_template_dir');
        
        $templateFile = $baseDir . DIRECTORY_SEPARATOR . $filePath;
        
        $processor = (new SpreadsheetFromFileProcessor($templateFile))->setStartCell('A4');
        
        return $this->exportManager->render($data, $processor);
    }
}
