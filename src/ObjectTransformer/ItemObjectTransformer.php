<?php

namespace App\ObjectTransformer;

use App\Entity\Item;
use App\Entity\ItemPackage;
use App\Entity\ImportStockItem;
use Kematjaya\ItemPack\Lib\ItemCategory\Entity\ItemCategoryInterface;
use Kematjaya\ItemPack\Service\StockCardServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Kematjaya\ImportBundle\DataTransformer\AbstractDataTransformer;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemObjectTransformer extends AbstractDataTransformer
{
    /**
     *
     * @var StockCardServiceInterface
     */
    private $stockCardService;
    
    public function __construct(EntityManagerInterface $entityManager, StockCardServiceInterface $stockCardService) 
    {
        $this->stockCardService = $stockCardService;
        
        parent::__construct($entityManager);
    }
    
    public function getColumns():array
    {
        return [
            [
                self::KEY_FIELD => 'code',
                self::KEY_INDEX => 0,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                    self::CONSTRAINT_UNIQUE => true,
                    self::CONSTRAINT_REFERENCE_CLASS => Item::class,
                    self::CONSTRAINT_REFERENCE_FIELD => 'code'
                ]   
            ],
            [
                self::KEY_FIELD => 'name',
                self::KEY_INDEX => 1,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                ]   
            ],
            [
                self::KEY_FIELD => 'code',
                self::KEY_INDEX => 2,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                    self::CONSTRAINT_REFERENCE_CLASS => \App\Entity\ItemCategory::class,
                    self::CONSTRAINT_REFERENCE_FIELD => 'code'
                ]   
            ],
            [
                self::KEY_FIELD => 'last_stock',
                self::KEY_INDEX => 3,
                self::KEY_TYPE => self::CONSTRAINT_TYPE_NUMBER,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                ]   
            ],
            [
                self::KEY_FIELD => 'principal_price',
                self::KEY_INDEX => 4,
                self::KEY_TYPE => self::CONSTRAINT_TYPE_NUMBER,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                ]   
            ],
            [
                self::KEY_FIELD => 'last_price',
                self::KEY_INDEX => 5,
                self::KEY_TYPE => self::CONSTRAINT_TYPE_NUMBER,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                ]   
            ],
            [
                self::KEY_FIELD => 'smallest_package_code',
                self::KEY_INDEX => 6,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                    self::CONSTRAINT_REFERENCE_CLASS => \App\Entity\Packaging::class,
                    self::CONSTRAINT_REFERENCE_FIELD => 'code'
                ]   
            ]
        ];
    }
    
    public function fromArray(array $data) 
    {
        $data = $this->checkConstraints($data);
        
        if (!$data[2] instanceof ItemCategoryInterface) {
            throw new \Exception(sprintf('invalid category'));
        }
        
        $itemPackage = (new ItemPackage())
                ->setQuantity(1)
                ->setSalePrice($data[5])
                ->setPrincipalPrice($data[4])
                ->setPackaging($data[6])
                ->setIsSmallestUnit(true);
                
        $item = (new Item())
                ->addItemPackage($itemPackage)
                ->setCode($data[0])
                ->setName($data[1])
                ->setCategory($data[2])
                ->setLastStock(($data[3]>0) ? $data[3]:0)
                ->setPrincipalPrice($data[4])
                ->setLastPrice($data[5])
                ->setUseBarcode(false)
                ;
        $import = (new ImportStockItem())->setQuantity($item->getLastStock());
        
        $this->entityManager->persist($itemPackage);
        
        $this->stockCardService->insertStockCard($item, $import);
        
        return $item;
    }

}
