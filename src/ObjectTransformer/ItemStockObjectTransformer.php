<?php

namespace App\ObjectTransformer;

use App\Entity\Packaging;
use App\Entity\ItemPackage;
use App\Entity\Item;
use App\Entity\ImportStockItem;
use Kematjaya\ItemPack\Service\StockCardServiceInterface;
use Kematjaya\ImportBundle\DataTransformer\AbstractDataTransformer;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemStockObjectTransformer extends AbstractDataTransformer
{
    private $stockCardService;
    
    public function __construct(EntityManagerInterface $entityManager, StockCardServiceInterface $stockCardService) 
    {
        $this->stockCardService = $stockCardService;
        
        parent::__construct($entityManager);
    }
    
    public function fromArray(array $data) 
    {
        $data = $this->checkConstraints($data);
        
        $item = $data[0];
        if(!$item instanceof Item) {
            throw new \Exception(sprintf('barang dengan kode %s tidak ditemukan', $item));
        }
        
        $packaging = $data[2];
        if(!$packaging instanceof Packaging) {
            throw new \Exception(sprintf('kemasan dengan kode %s tidak ditemukan', $packaging));
        }
        
        $itemPackage = $this->entityManager->getRepository(ItemPackage::class)->findOneBy(['item' => $item, 'packaging' => $packaging]);
        if(!$itemPackage) {
            throw new \Exception(sprintf('barang %s tidak mendukung kemasan %s', (string) $item, (string) $packaging ));
        }
        
        $item->setLastStock($data[1]);
        
        $import = (new ImportStockItem())->setQuantity($item->getLastStock());
        
        $this->entityManager->persist($item);
        
        $this->stockCardService->insertStockCard($item, $import);
        
        return $item;
    }

    public function getColumns(): array 
    {
        return [
            [
                self::KEY_FIELD => 'code',
                self::KEY_INDEX => 0,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                    self::CONSTRAINT_REFERENCE_CLASS => Item::class,
                    self::CONSTRAINT_REFERENCE_FIELD => 'code'
                ]   
            ],
            [
                self::KEY_FIELD => 'last_stock',
                self::KEY_INDEX => 1,
                self::KEY_TYPE => self::CONSTRAINT_TYPE_NUMBER,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                ]   
            ],
            [
                self::KEY_FIELD => 'smallest_package_code',
                self::KEY_INDEX => 2,
                self::KEY_CONSTRAINT => [
                    self::CONSTRAINT_REQUIRED => true,
                    self::CONSTRAINT_REFERENCE_CLASS => Packaging::class,
                    self::CONSTRAINT_REFERENCE_FIELD => 'code'
                ]   
            ]
        ];
    }
}
