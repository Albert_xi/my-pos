<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Gumlet\ImageResize;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class HomePageController extends AbstractController 
{
    /**
     * @Route("/", name="homepage")
     */
    public function index():Response
    {
        if($this->getUser())
        {
            $this->addFlash("info", 'wellcome back : '. $this->getUser()->getUsername());
            return $this->redirectToRoute('admin_index');
        }
        
        $this->addFlash("info", 'please login first');
        return $this->redirectToRoute('kmj_user_login');
    }
    
    
    /**
     * @Route("/access-denied", name="app_access_denied")
     */
    public function accessDenied():Response
    {
        return $this->render('access-denied.html.twig', [], (new Response('', Response::HTTP_UNAUTHORIZED)));
    }
    
    /**
     * @Route("/upload", name="app_upload_images", methods={"POST"})
     */
    public function uploadImage(Request $request, ParameterBagInterface $parameter)
    {
        $file = $request->files->get('upload');
        if($file instanceof UploadedFile)
        {
            try{
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileName   = $originalFilename.'-'.uniqid().'.'.$file->guessExtension();
                $uploadDir  = $parameter->get('kernel.upload_dir').DIRECTORY_SEPARATOR.'blog';
                if(!is_dir($uploadDir))
                {
                    mkdir($uploadDir, 0777, true);
                }
                
                $file = $file->move($uploadDir, $fileName);
                if($file) 
                {
                    $treshold = 100000; // 100Kb
                    while($file->getSize() > $treshold)
                    {
                        $resize = new ImageResize($file->getLinkTarget());
                        $resize->scale(50);
                        $resize->save($file->getLinkTarget());
                        if($file->getSize() <= $treshold)
                        {
                            break;
                        }
                    } 
        
                    $url = $this->generateUrl('app_view_images', ['fileName' => $fileName]);
                    $message = 'ok';
                    $ckeditor = $request->get('CKEditorFuncNum');
                    return new Response(sprintf('<script>window.parent.CKEDITOR.tools.callFunction(%s, "%s", "%s")</script>', $ckeditor, $url, $message));
                }
            } catch (\Exception $ex) 
            {
                return new Response(sprintf('<script>alert("%s")</script>', $ex->getMessage()));
            }
        }
        
        return new Response('<script>alert("Unable to upload the file")</script>');
    }
    
    /**
     * @Route("/view/{fileName}", name="app_view_images", methods={"GET"})
     */
    public function viewImages(string $fileName, ParameterBagInterface $parameter)
    {
        try
        {
            $file = new File($parameter->get('kernel.upload_dir').DIRECTORY_SEPARATOR.'blog'.DIRECTORY_SEPARATOR.$fileName);
        } catch (\Exception $ex) 
        {
            return $this->json(['error' => true, "message" => $ex->getMessage()]);
        }
        
        return $this->file($file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/documentation", name="app_documentation")
     */
    public function documentation(ParameterBagInterface $parameterBag)
    {
        try{
            $file = new File($parameterBag->get('kernel.project_dir').'/manual book.pdf');
        } catch (\Exception $ex) 
        {
            return $this->json(['error' => true, "message" => $ex->getMessage()]);
        }
        
        return $this->file($file, 'documentation.pdf', ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
