<?php

namespace App\Controller\Admin;

use App\Entity\Purchase;
use App\Entity\PurchaseDetail;
use App\Form\PurchaseDetailType;
use App\Repository\PurchaseDetailRepository;
use App\Base\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("purchase-detail")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseDetailController extends BaseController
{
    /**
     * @Route("/create/{id}", name="purchase_detail_create", methods={"GET", "POST"})
     */
    public function create(Request $request, Purchase $purchase, PurchaseDetailRepository $purchaseDetailRepo)
    {
        $purchaseDetail = $purchaseDetailRepo->createObject($purchase);
        $form = $this->createForm(PurchaseDetailType::class, $purchaseDetail, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("purchase_detail_create", ["id" => $purchase->getId()])]);
        if ($request->get('submit')) {
            $result = $this->processFormAjax($request, $form);
            if ($result["process"]) {
                return $this->json($result);
            }
        } else {
            $form->handleRequest($request);
            $purchaseDetail->setPurchase($purchase);
        }
            
        return $this->render('pages/purchase-detail/form.html.twig', [
            'form' => $form->createView(), 
            'title' => 'create',
            'object' => $purchaseDetail]);
    }
    
    /**
     * @Route("/edit/{id}", name="purchase_detail_edit", methods={"GET", "POST"})
     */
    public function edit(PurchaseDetail $purchaseDetail, Request $request)
    {
        $form = $this->createForm(PurchaseDetailType::class, $purchaseDetail, ["attr" =>["id" => "ajaxForm"],"action" => $this->generateUrl("purchase_detail_edit", ["id" => $purchaseDetail->getId()])]);
        if ($request->get('submit')) {
            $result = $this->processFormAjax($request, $form);
            if ($result["process"]) {
                return $this->json($result);
            }
        } else {
            $purchase = $purchaseDetail->getPurchase();
            $form->handleRequest($request);
            $purchaseDetail->setPurchase($purchase);
        }
        
        return $this->render('pages/purchase-detail/form.html.twig', [
            'form' => $form->createView(), 
            'title' => 'edit',
            'object' => $purchaseDetail]);
    }
    
    /**
     * @Route("/delete/{id}", name="purchase_detail_delete", methods={"DELETE"})
     */
    public function delete(PurchaseDetail $purchaseDetail, Request $request)
    {
        $id = $purchaseDetail->getPurchase()->getId();
        $tokenName = 'delete'.$purchaseDetail->getId();
        parent::doDelete($request, $purchaseDetail, $tokenName);
        
        return $this->redirectToRoute('purchase_detail', ['id' => $id]);
    }
    
}
