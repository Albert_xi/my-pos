<?php

namespace App\Controller\Admin;

use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use App\Report\Sale\SaleReport;
use App\Report\Item\ItemReport;
use App\Report\Sale\UnpaidTransaction;
use App\Report\Purchase\PurchaseReport;
use App\Repository\SerahTerimaRepository;
use App\Filter\Report\TransactionFilterType;
use Kematjaya\ReportBundle\Helper\ReportHelper;
use Kematjaya\ImportBundle\Manager\ImportManagerInterface;
use Kematjaya\Export\Manager\ManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/report", name="report_")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ReportController extends BaseController 
{
    protected $reportHelper;
    
    public function __construct(
            ImportManagerInterface $importManager,
            ManagerInterface $exportManager,
            FilterBuilderUpdaterInterface $filterBuilderUpdater, 
            ReportHelper $reportHelper,
            TranslatorInterface $translator,
            PaginatorInterface $paginator) 
    {
        $this->reportHelper = $reportHelper;
        
        parent::__construct($importManager, $exportManager, $filterBuilderUpdater, $translator, $paginator);
    }
    /**
     * 
     * @Route("/profit", name="profit_index", methods={"GET", "POST"})
     */
    public function profit(Request $request, BreadcrumbBuilder $builder):Response
    {
        $title = 'profit';
        $builder->getBuilder()->add('report');
        $builder->getBuilder()->add($title);
        
        $report = $this->reportHelper->buildReport(SaleReport::class);
        //dump($report->getTheme());exit;
        return $this->render('report/profit.html.twig', [
            'title' => $title,
            'myreport' => $this->reportHelper->renderHTML($report)
        ]);
    }
    
    /**
     * @Route("/profit/print", name="profit_print", methods={"GET", "POST"})
     */
    public function profitPrint()
    {
        $report = $this->reportHelper->buildReport(SaleReport::class);
        $fileName = 'profit.xls';
        $tempFile = $this->reportHelper->toExcel($report, $fileName);
        
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/profit-by-store", name="profit_store_index", methods={"GET", "POST"})
     */
    public function profitStore(Request $request, BreadcrumbBuilder $builder)
    {
        $title = 'profit_by_store';
        $builder->getBuilder()->add('report');
        $builder->getBuilder()->add($title);
        
        $report = $this->reportHelper->buildReport(SaleReport::class);
        
        return $this->render('report/profit-by-store.html.twig', [
            'title' => $title,
            'myreport' => $this->reportHelper->renderHTML($report, 'ProfitByStore')
        ]);
    }
    
    /**
     * @Route("/print-profit-by-store", name="profit_store_print", methods={"GET", "POST"})
     */
    public function profitByStorePrint()
    {
        $report = $this->reportHelper->buildReport(SaleReport::class);
        $fileName = 'profit-by-store.xls';
        $tempFile = $this->reportHelper->toExcel($report, $fileName, 'ProfitByStore');
        
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/item-stock", name="item_stock_index", methods={"GET", "POST"})
     */
    public function itemStock(Request $request, BreadcrumbBuilder $builder)
    {
        $title = 'item_stock';
        $builder->getBuilder()->add('report');
        $builder->getBuilder()->add($title);
        
        $report = $this->reportHelper->buildReport(ItemReport::class);
        
        return $this->render('report/item-stock.html.twig', [
            'title' => $title,
            'myreport' => $this->reportHelper->renderHTML($report, 'ItemReportGrouping')
        ]);
    }
    
    /**
     * @Route("/print-item-stock", name="stock_print", methods={"GET", "POST"})
     */
    public function itemStockPrint()
    {
        $report = $this->reportHelper->buildReport(ItemReport::class);
        $fileName = sprintf('item-stock-%s.xls', date('YmdHis'));
        $tempFile = $this->reportHelper->toExcel($report, $fileName, 'ItemReportGrouping');
        
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/unpaid-transaction", name="unpaid_transaction_index", methods={"GET", "POST"})
     */
    public function unpaidTransaction(BreadcrumbBuilder $builder)
    {
        $title = 'unpaid_transaction';
        $builder->getBuilder()->add('report');
        $builder->getBuilder()->add($title);
        
        $report = $this->reportHelper->buildReport(UnpaidTransaction::class);
        
        return $this->render('report/unpaid-transaction.html.twig', [
            'title' => $title,
            'myreport' => $this->reportHelper->renderHTML($report)
        ]);
    }
    
    /**
     * @Route("/unpaid-transaction-print", name="unpaid_transaction_print", methods={"GET", "POST"})
     */
    public function unpaidTransactionPrint()
    {
        $report = $this->reportHelper->buildReport(UnpaidTransaction::class);
        $fileName = sprintf('unpaid-transaction-%s.xls', date('YmdHis'));
        $tempFile = $this->reportHelper->toExcel($report, $fileName);
        
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/sale", name="sale_index", methods={"GET", "POST"})
     */
    public function sale(BreadcrumbBuilder $builder)
    {
        $title = 'sale';
        $builder->getBuilder()->add('report');
        $builder->getBuilder()->add($title);
        
        $report = $this->reportHelper->buildReport(SaleReport::class);
        
        $date = new \DateTime();
        return $this->render('report/sale.html.twig', [
            'title' => $title, 'now' => $date,
            'myreport' => $this->reportHelper->renderHTML($report, 'SaleInMonth')
        ]);
    }
    
    /**
     * @Route("/sale-print", name="sale_print", methods={"GET", "POST"})
     */
    public function salePrint()
    {
        $report = $this->reportHelper->buildReport(SaleReport::class);
        $fileName = sprintf('sale-transaction-%s.xls', date('Ym'));
        $tempFile = $this->reportHelper->toExcel($report, $fileName, 'SaleInMonth');
        
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/purchase", name="purchase_index", methods={"GET", "POST"})
     */
    public function purchase(BreadcrumbBuilder $builder)
    {
        $title = 'purchase';
        $builder->getBuilder()->add('report');
        $builder->getBuilder()->add($title);
        
        $report = $this->reportHelper->buildReport(PurchaseReport::class);
        
        $date = new \DateTime();
        return $this->render('report/purchase.html.twig', [
            'title' => $title, 'now' => $date,
            'myreport' => $this->reportHelper->renderHTML($report)
        ]);
    }
    
    /**
     * @Route("/purchase-print", name="purchase_print", methods={"GET", "POST"})
     */
    public function purchasePrint()
    {
        $report = $this->reportHelper->buildReport(PurchaseReport::class);
        $fileName = sprintf('purchase-transaction-%s.xls', date('Ym'));
        $tempFile = $this->reportHelper->toExcel($report, $fileName);
        
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/transaction", name="transaction_index", methods={"GET", "POST"})
     */
    public function transaction(Request $request, BreadcrumbBuilder $builder, SerahTerimaRepository $repo)
    {
        $title = 'transaction';
        $builder->getBuilder()->add('report');
        $builder->getBuilder()->add($title);
        
        $filterForm = $this->createFormFilter(TransactionFilterType::class);
        $filterForm->setData($this->getFilters($filterForm->getName()));
        $params = $this->setFilters($request, $filterForm);
        
        $date = new \DateTime();
        return $this->render('report/transaction.html.twig', [
            'title' => $title, 'now' => $date,
            'data' => $repo->findOneByMonth($date, $params),
            'form' => $filterForm->createView()
        ]);
    }
    
    /**
     * @Route("/transaction-print", name="transaction_print", methods={"GET", "POST"})
     */
    public function transactionPrint(
            Request $request,
            SerahTerimaRepository $repo,
            \Kematjaya\ReportBundle\Builder\ExcelBuilder $excelBuilder)
    {
        $title = 'transaction';
        
        $filterForm = $this->createFormFilter(TransactionFilterType::class);
        $filterForm->setData($this->getFilters($filterForm->getName()));
        $params = $this->setFilters($request, $filterForm);
        
        $date = new \DateTime();
        $html = $this->renderView('report/transaction.html.twig', [
            'title' => $title, 'now' => $date,
            'data' => $repo->findOneByMonth($date, $params),
            'form' => $filterForm->createView()
        ]);
        
        $crawler = new \Symfony\Component\DomCrawler\Crawler($html);
        $content = sprintf('<table>%s</table>', $crawler->filter('#trasaction')->html());
        
        $fileName = sprintf('purchase-transaction-%s.xls', date('Ym'));
        
        $tempFile = $excelBuilder->fromHtml($content, $fileName);
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
