<?php

namespace App\Controller\Admin;

use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use App\Library\RoutingManagement\RoutingService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
/**
 * @Route("/administrator/routing")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjRoutingController extends BaseController
{
    /**
     * @Route("/", name="kmj_routing_index", methods={"GET", "POST"})
     */
    public function index(RoleHierarchyInterface $roleHierarchy, TokenStorageInterface $token, BreadcrumbBuilder $builder):Response
    {
        $builder->getBuilder()->add('administrator');
        $builder->getBuilder()->add('kmj_routing_index');
        
        return $this->render('pages/routing/index.html.twig', [
            'roles' => $roleHierarchy->getReachableRoleNames($token->getToken()->getRoleNames()),
            'title' => 'kmj_routing_index'
        ]);
    }
    
    /**
     * @Route("/{role}/roles", name="kmj_routing_roles", methods={"GET", "POST"})
     */
    public function setRoutingRole(Request $request, string $role, RoutingService $routingService, \Symfony\Contracts\Translation\TranslatorInterface $translator):Response
    {
        if($request->getMethod() == Request::METHOD_POST) {
            $results = ["process" => false, "status" => null, "message" => null, "errors" => null];
            if ($this->isCsrfTokenValid('kmj_routing_roles_' . $role, $request->request->get('_token')))
            {
                try
                {
                    $result = $routingService->updateCredential($request->get('credential'), $role, $this->getDoctrine()->getManager());
                    if($result)
                    {
                        $results = ["process" => true, "status" => true, "message" => $translator->trans('messages.update.success'), "errors" => null];
                    }
                } catch (\Exception $ex) 
                {
                    $results = ["process" => true, "status" => false, "message" => $translator->trans('messages.update.error'), "errors" => $ex->getMessage()];
                }
            }else
            {
                $results = ["process" => true, "status" => false, "message" => $translator->trans('messages.csrf-token-invalid'), "errors" => $translator->trans('messages.csrf-token-invalid')];
            }
            
            return $this->json($results);
        }
        
        return $this->render('pages/routing/roles.html.twig', [
            'title' => 'edit', 'role' => $role,
            'routers' => $routingService->getIndexRoute(false, 'admin'),
            'routingService' => $routingService,
            'credentials' => $routingService->getCredentials($role)
        ]);
    }
}
