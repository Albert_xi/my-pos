<?php

namespace App\Controller\Admin;

use App\Entity\KmjPrinterClient;
use App\Filter\KmjPrinterClientFilterType;
use App\Repository\KmjPrinterClientRepository;
use App\Form\KmjPrinterClientType;
use App\Library\BreadcrumbBuilder;
use App\Library\UserService;
use App\Base\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/administrator/printer", name="kmj_printer_")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjPrinterClientController extends BaseController
{
    /**
     * @Route("/", name="index", methods={"GET", "POST"})
     */
    public function index(Request $request, KmjPrinterClientRepository $kmjPrinterClientRepo, BreadcrumbBuilder $builder)
    {
        $builder->getBuilder()->add('administrator');
        $builder->getBuilder()->add('printer');
        
        $form = $this->createFormFilter(KmjPrinterClientFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $kmjPrinterClientRepo->createQueryBuilder('this'));
        
        return $this->render('pages/printer/index.html.twig', [
            'title' => 'printer', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     */
    public function create(Request $request, UserService $userService)
    {
        $client = (new KmjPrinterClient())
                ->setKmjUser($userService->getUser());
        if ($request->server->get('USERDOMAIN')) {
            $client->setComputerName($request->server->get('USERDOMAIN'));
        }
        
        $form   = $this->createForm(KmjPrinterClientType::class, $client, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("kmj_printer_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/printer/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="edit", methods={"GET", "POST"})
     */
    public function edit(KmjPrinterClient $client, Request $request)
    {
        $form = $this->createForm(KmjPrinterClientType::class, $client, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("kmj_printer_edit", ['id' => $client->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/printer/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="detail", methods={"GET"})
     */
    public function detail(KmjPrinterClient $client)
    {
        return $this->render('pages/printer/detail.html.twig', ['data' => $client, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="delete", methods={"DELETE"})
     */
    public function delete(KmjPrinterClient $client, Request $request)
    {
        $tokenName = 'delete'.$client->getId();
        parent::doDelete($request, $client, $tokenName);
        
        return $this->redirectToRoute('kmj_printer_index');
    }
}
