<?php

namespace App\Controller\Admin;

use App\Entity\Customer;
use App\Form\CustomerType;
use App\Filter\CustomerFilterType;
use App\Repository\CustomerRepository;
use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/master/customer")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class CustomerController extends BaseController
{
    
    /**
     * @Route("/", name="customer_index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, CustomerRepository $repo)
    {
        
        $builder->getBuilder()->add('master');
        $builder->getBuilder()->add('customer');
        
        $form = $this->createFormFilter(CustomerFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
         
        return $this->render('pages/customer/index.html.twig', [
            'title' => 'customer', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="customer_create", methods={"GET", "POST"})
     */
    public function create(Request $request, CustomerRepository $customerRepo)
    {
        $form = $this->createForm(CustomerType::class, $customerRepo->createCustomer(), ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("customer_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        return $this->render('pages/customer/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="customer_edit", methods={"GET", "POST"})
     */
    public function edit(Customer $customer, Request $request)
    {
        $form = $this->createForm(CustomerType::class, $customer, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("customer_edit", ['id' => $customer->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        return $this->render('pages/customer/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="customer_detail", methods={"GET"})
     */
    public function detail(Customer $customer)
    {
        return $this->render('pages/customer/detail.html.twig', ['data' => $customer, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="customer_delete", methods={"DELETE"})
     */
    public function delete(Customer $customer, Request $request)
    {
        $tokenName = 'delete'.$customer->getId();
        parent::doDelete($request, $customer, $tokenName);
        
        return $this->redirectToRoute('customer_index');
    }
}
