<?php

namespace App\Controller\Admin;

use App\Entity\Purchase;
use App\Form\PurchaseType;
use App\Filter\PurchaseFilterType;
use App\Repository\PurchaseRepository;
use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/transaction/purchase")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseController extends BaseController
{
    
    /**
     * @Route("/", name="purchase_index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, PurchaseRepository $repo)
    {
        $builder->getBuilder()->add('transaction');
        $builder->getBuilder()->add('purchase');
        
        $form = $this->createFormFilter(PurchaseFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
        return $this->render('pages/purchase/index.html.twig', [
            'title' => 'purchase', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="purchase_create", methods={"GET", "POST"})
     */
    public function create(Request $request, BreadcrumbBuilder $builder, PurchaseRepository $purchaseRepo)
    {
        $purchase = $purchaseRepo->createObject();
        $form = $this->createForm(PurchaseType::class, $purchase, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("purchase_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            
            return $this->json($result);
        }
        
        return $this->render('pages/purchase/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    protected function buildSuccessResult(string $type, $object)
    {
        $result = parent::buildSuccessResult($type, $object);
        if ($object) {
            $result['redirectURL'][] = [
                'url' => $this->generateUrl('purchase_detail', ['id' => $object->getId()])
            ];
        }
        
        return $result;
    }
    
    /**
     * @Route("/{id}/detail", name="purchase_detail", methods={"GET", "POST"})
     */
    public function detail(Purchase $purchase, Request $request, BreadcrumbBuilder $builder)
    {
        $builder->getBuilder()->add('transaction');
        $builder->getBuilder()->add('purchase', 'purchase_index');
        $builder->getBuilder()->add('detail');
        $builder->getBuilder()->add($purchase->getCode());
        
        $form = $this->createForm(PurchaseType::class, $purchase);
        $result = $this->processForm($request, $form);
        if ($result) {
            
            return $this->redirectToRoute('purchase_detail', ['id' => $result->getId()]);
        }
        
        return $this->render('pages/purchase/detail.html.twig', [
            'data' => $purchase, 'title' => 'detail', 'form' => $form->createView()]);
    }
    
    /**
     * @Route("/{id}/delete", name="purchase_delete", methods={"DELETE"})
     */
    public function delete(Purchase $purchase, Request $request)
    {
        $tokenName = 'delete'.$purchase->getId();
        parent::doDelete($request, $purchase, $tokenName);
        
        return $this->redirectToRoute('purchase_index');
    }
    
}
