<?php

namespace App\Controller\Admin;

use App\Entity\Supplier;
use App\Repository\SupplierRepository;
use App\Form\SupplierType;
use App\Filter\SupplierFilterType;
use App\Library\BreadcrumbBuilder;
use App\Base\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/master/supplier")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SupplierController extends BaseController 
{
    /**
     * @Route("/", name="supplier_index", methods={"GET", "POST"})
     */ 
    public function index(Request $request, SupplierRepository $repo, BreadcrumbBuilder $builder)
    {
        $builder->getBuilder()->add('master');
        $builder->getBuilder()->add('supplier');
        
        $form = $this->createFormFilter(SupplierFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
          
        return $this->render('pages/supplier/index.html.twig', [
            'title' => 'supplier', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="supplier_create", methods={"GET", "POST"})
     */
    public function create(Request $request)
    {
        $form = $this->createForm(SupplierType::class, new Supplier(), ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("supplier_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/supplier/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="supplier_edit", methods={"GET", "POST"})
     */
    public function edit(Supplier $supplier, Request $request)
    {
        $form = $this->createForm(SupplierType::class, $supplier, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("supplier_edit", ['id' => $supplier->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/supplier/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="supplier_detail", methods={"GET"})
     */
    public function detail(Supplier $supplier)
    {
        return $this->render('pages/supplier/detail.html.twig', ['data' => $supplier, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="supplier_delete", methods={"DELETE"})
     */
    public function delete(Supplier $supplier, Request $request)
    {
        $tokenName = 'delete'.$supplier->getId();
        parent::doDelete($request, $supplier, $tokenName);
        
        return $this->redirectToRoute('supplier_index');
    }
}
