<?php

namespace App\Controller\Admin;

use App\Entity\Item;
use App\Form\ItemType;
use App\Filter\ItemFilterType;
use App\Form\Type\ImportType;
use App\Base\Controller\BaseController;
use App\Repository\ItemRepository;
use App\Repository\ItemPriceLogRepository;
use App\Library\BreadcrumbBuilder;
use App\ObjectTransformer\ItemObjectTransformer;
use App\ObjectTransformer\ItemStockObjectTransformer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("/master/item")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemController extends BaseController 
{
    /**
     * @Route("/", name="item_index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, ItemPriceLogRepository $itemPriceLog, ItemRepository $repo)
    {
        $builder->getBuilder()->add('master');
        $builder->getBuilder()->add('item');
        
        $form = $this->createFormFilter(ItemFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
        
        return $this->render('pages/item/index.html.twig', [
            'title' => 'item', 
            'filter' => $form->createView(),
            'approval_price_count' => $itemPriceLog->getListPriceLogNew()->count(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="item_create", methods={"GET", "POST"})
     */
    public function create(Request $request, ItemRepository $itemRepo)
    {
        $form = $this->createForm(ItemType::class, $itemRepo->createItem(), ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("item_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/item/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/data/", name="item_data", methods={"GET"})
     */
    public function getJsonData(ItemRepository $itemRepo, Request $request)
    {
        $item = $itemRepo->find($request->get('q'));
        
        if (!$item) {
            $data = [
                'code' => null, 'name' => null, 'last_price' => null, 'last_stock' => null
            ];
            return $this->json($data);
        }
        
        $data = [
            'code' => $item->getCode(),
            'name' => $item->getName(),
            'last_price' => $item->getLastPrice(),
            'last_stock' => $item->getLastStock(),
            'smallest_package' => ($item->getSmallestPackages()) ? $item->getSmallestPackages()->getPackaging()->getName():null
        ];
        
        return $this->json($data);
    }
    
    /**
     * @Route("/{id}/edit", name="item_edit", methods={"GET", "POST"})
     */
    public function edit(Item $item, Request $request)
    {
        $form = $this->createForm(ItemType::class, $item, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("item_edit", ['id' => $item->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/item/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="item_detail", methods={"GET"})
     */
    public function detail(Item $item, BreadcrumbBuilder $builder)
    {
        $builder->getBuilder()->add('master');
        $builder->getBuilder()->add('item', 'item_index');
        $builder->getBuilder()->add('detail');
        $builder->getBuilder()->add($item->getName());
        return $this->render('pages/item/detail.html.twig', ['data' => $item, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="item_delete", methods={"DELETE"})
     */
    public function delete(Item $item, Request $request)
    {
        $tokenName = 'delete'.$item->getId();
        parent::doDelete($request, $item, $tokenName);
        
        return $this->redirectToRoute('item_index');
    }
    
    /**
     * @Route("/export", name="item_export", methods={"GET"})
     */
    public function export(\Kematjaya\ReportBundle\Helper\ReportHelper $reportHelper)
    {
        $report     = $reportHelper->buildReport(\App\Report\Item\ItemReport::class);
        //dump($report->run()->dataStore('item')->meta());exit;
        $fileName   = 'item.xls';
        $tempFile   = $reportHelper->toExcel($report, $fileName);
        
        return $this->file($tempFile, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }
    
    /**
     * @Route("/import", name="item_import", methods={"GET", "POST"})
     */
    public function import(Request $request, ItemObjectTransformer $itemObjectTransformer)
    {
        $form = $this->createForm(ImportType::class, null, ["action" => $this->generateUrl("item_import"), "attr" => ["id" => "ajaxForm"]]);
        
        $result = $this->doSpreadsheetImport($form, $request, $itemObjectTransformer);
        if ($result['process']) {
            return $this->json($result);
        }
        
        return $this->render('pages/import.html.twig', [
            'title' => 'item',
            'template_url' => $this->generateUrl('item_template_import'),
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/import-stock", name="item_import_stock", methods={"GET", "POST"})
     */
    public function importStock(Request $request, ItemStockObjectTransformer $itemObjectTransformer)
    {
        $form = $this->createForm(ImportType::class, null, ["action" => $this->generateUrl("item_import_stock"), "attr" => ["id" => "ajaxForm"]]);
        
        $result = $this->doSpreadsheetImport($form, $request, $itemObjectTransformer);
        if ($result['process']) {
            return $this->json($result);
        }
        
        return $this->render('pages/import.html.twig', [
            'title' => 'item',
            'template_url' => $this->generateUrl('item_template_import_stock'),
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/template-import", name="item_template_import", methods={"GET"})
     */
    function templateImport()
    {
        $fileName = 'item.xlsx';
        return parent::templateExcel($fileName);
    }
    
    /**
     * @Route("/template-import-stock", name="item_template_import_stock", methods={"GET"})
     */
    function templateImportStock()
    {
        $fileName = 'item-stock.xlsx';
        return parent::templateExcel($fileName);
    }
    
    /**
     * @Route("/price-approval-list", name="item_price_approval_list")
     */
    public function priceApprovalList(ItemPriceLogRepository $itemPriceLog)
    {
        return $this->render('pages/item/price_change.html.twig', [
            'datas' => $itemPriceLog->getListPriceLogNew(), 
            'title' => 'price_approval_list']);
    }
}
