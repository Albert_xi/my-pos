<?php

namespace App\Controller\Admin;

use App\Base\Controller\BaseController;
use App\Repository\SaleItemRepository;
use App\Entity\Sale;
use App\Entity\SaleItem;
use App\Form\SaleItemType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/sale-item")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleItemController extends BaseController
{
    /**
     * @Route("/{id}/create", name="sale_item_create", methods={"GET", "POST"})
     */
    public function create(Request $request, Sale $sale, SaleItemRepository $saleItemRepo)
    {
        $saleItem = $saleItemRepo->createSaleItem($sale);
        $form = $this->createForm(SaleItemType::class, $saleItem, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("sale_item_create", ["id" => $sale->getId()])]);
        if ($request->get('submit')) {
            $result = $this->processFormAjax($request, $form);
            if ($result["process"]) {
                return $this->json($result);
            }
        } else {
            $form->handleRequest($request);
            $saleItem->setSale($sale);
        }
            
        return $this->render('pages/sale-item/form.html.twig', [
            'form' => $form->createView(), 
            'title' => 'create',
            'object' => $saleItem]);
    }
    
    /**
     * @Route("/{id}/edit", name="sale_item_edit", methods={"GET", "POST"})
     */
    public function edit(SaleItem $saleItem, Request $request)
    {
        $form = $this->createForm(SaleItemType::class, $saleItem, ["attr" =>["id" => "ajaxForm"],"action" => $this->generateUrl("sale_item_edit", ["id" => $saleItem->getId()])]);
        if ($request->get('submit')) {
            $result = $this->processFormAjax($request, $form);
            if ($result["process"]) {
                return $this->json($result);
            }
        } else {
            $form->handleRequest($request);
        }
        
        return $this->render('pages/sale-item/form.html.twig', [
            'form' => $form->createView(), 
            'title' => 'edit',
            'object' => $saleItem]);
    }
    
    /**
     * @Route("/{id}/delete", name="sale_item_delete", methods={"DELETE"})
     */
    public function delete(SaleItem $saleItem, Request $request)
    {
        $tokenName = 'delete'.$saleItem->getId();
        parent::doDelete($request, $saleItem, $tokenName);
        
        return $this->json(['status' => true, 'message' => null]);
    }
}
