<?php

namespace App\Controller\Admin;

use App\Entity\Sale;
use App\Form\SaleType;
use App\Filter\SaleFilterType;
use App\Repository\SaleRepository;
use App\Repository\KmjPrinterClientRepository;
use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use Kematjaya\EscposBundle\Helper\PrinterHelperInterface;
use Kematjaya\EscposBundle\Formatter\TextFormatter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/transaction/sale")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleController extends BaseController
{
    /**
     * @Route("/", name="sale_index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, SaleRepository $saleRepo)
    {
        $builder->getBuilder()->add('transaction');
        $builder->getBuilder()->add('sale');
        
        $form = $this->createFormFilter(SaleFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $saleRepo->createQueryBuilder('this'));
        return $this->render('pages/sale/index.html.twig', [
            'title' => 'sale', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="sale_create", methods={"GET", "POST"})
     */
    public function create(Request $request, SaleRepository $saleRepo)
    {
        $sale = $saleRepo->createSale();
        $form = $this->createForm(SaleType::class, $sale, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("sale_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            
            return $this->json($result);
        }
        
        return $this->render('pages/sale/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    protected function buildSuccessResult(string $type, $object)
    {
        $result = parent::buildSuccessResult($type, $object);
        if ($result["status"]) {
            $result['redirectURL'][] = [
                'url' => $this->generateUrl('sale_detail', ['id' => $object->getId()])
            ];
        }
        
        return $result;
    }
    
    /**
     * @Route("/print-test", name="sale_printout_test", methods={"GET", "POST"})
     */
    public function printTest(Request $request, PrinterHelperInterface $helper, KmjPrinterClientRepository $kmjPrinterClientRepo)
    {
        $printerSelected = null;
        $printers = $kmjPrinterClientRepo->findByUser();
        if (count($printers) > 0) {
            $form = $this->createForm(\App\Form\SelectPrinterType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $object = $form->getData();
                    $printerSelected = reset($object);
                }
            }
            
            if (!$printerSelected) {
                return $this->render('pages/select-printer.html.twig', [
                    'form' => $form->createView(), 'backPath' => $this->generateUrl('sale_index')
                ]);
            }
        }else {
            $this->addFlash('error', $this->translator->trans('add_printer'));
            return $this->redirectToRoute('kmj_printer_index');
        }
		
		
        try
        {   
            $formatter = new TextFormatter();
            
            $format = '%-10.40s : %17.40s';
            $formatter
                    ->addText(strtoupper($this->getParameter('app.company')))->addEnter()
                    ->addText("TES PRINT PAGE")->addEnter()->addEnter()
                    ->addText("==============================")->addEnter()
                    ;
                    
            $formatter->setHeader($formatter->getText());
            $formatter->clear();
			
            $helper->print($formatter, $printerSelected);
            dump($this->translator->trans('print_success'));exit;
        }catch (\Exception $ex) {
            dump($ex->getMessage());exit;
        }
		
        dump("OK");exit;
    }
	
        
    /**
     * @Route("/{id}/payment", name="sale_payment", methods={"GET", "POST"})
     */
    public function payment(Sale $sale, Request $request)
    {
        $form = $this->createForm(SaleType::class, $sale, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("sale_payment", ['id' => $sale->getId()])]);
        
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            
            return $this->json($result);
        }
        
        return $this->render('pages/sale/payment.html.twig', [
            'form' => $form->createView(), 'title' => 'payment',
            'data' => $sale
        ]);
    }
    
    /**
     * @Route("/{id}/printout", name="sale_printout", methods={"GET", "POST"})
     */
    public function printout(Sale $sale, Request $request, PrinterHelperInterface $helper, KmjPrinterClientRepository $kmjPrinterClientRepo)
    {
        $printerSelected = null;
        $printers = $kmjPrinterClientRepo->findByUser();
        if (count($printers) > 0) {
            $form = $this->createForm(\App\Form\SelectPrinterType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $object = $form->getData();
                    $printerSelected = reset($object);
                }
            }
            
            if (!$printerSelected) {
                return $this->render('pages/select-printer.html.twig', [
                    'form' => $form->createView(), 'backPath' => $this->generateUrl('sale_detail', ['id' => $sale->getId()])
                ]);
            }
        }else {
            $this->addFlash('error', $this->translator->trans('add_printer'));
            return $this->redirectToRoute('kmj_printer_index');
        }
            
        try
        {   
            $formatter = new TextFormatter();
            
            $format = '%-10.40s : %17.40s';
            $store = $sale->getStore();
            $formatter
                    ->addText(strtoupper($this->getParameter('app.company')))->addEnter()
                    ->addText($store->getAddress())->addEnter()->addEnter()
                    ->addText("==============================")->addEnter()
                    ;
                    
            $formatter->setHeader($formatter->getText());
            $formatter->clear();
            
            $formatter
                    ->addText(sprintf($format, $this->translator->trans('code'), $sale->getCode()))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('store'), strtoupper($store->getName())))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('date'), $sale->getCreatedAt()->format('d/M/Y H:i')))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('customer'), $sale->getCustomer()))->addEnter()
                    ->addText("==============================")->addEnter();
            
            $subTotal = 0;
            $subTotalDisc = 0;
            $subTotalTax = 0;
            foreach ($sale->getSaleItems() as $saleItem) {
                $line            = sprintf('%-12.40s %1.0f %-1.40s %1.40s : %6.40s', $saleItem->getItem() , $saleItem->getQuantity(), 'x', $saleItem->getSalePrice(), $saleItem->getSubTotal()); 
                $subTotal       += $saleItem->getSubTotal();
                $subTotalDisc   += $saleItem->getDiscount();
                $subTotalTax    += $saleItem->getTax();
                
                $formatter->addText($line)->addEnter();
            }
            
            $discount = $subTotalDisc + $sale->getDiscount();
            $tax = $subTotalTax + $sale->getTax();
            
            $formatter
                    ->addText("==============================")->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('sub_total'), $subTotal))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('discount'), $discount))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('tax'), $tax))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('total'), $sale->getTotal()))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('payment'), $sale->getPayment()))->addEnter()
                    ->addText(sprintf($format, $this->translator->trans('change'), $sale->getPaymentChange()))->addEnter()
                    ->addText("==============================")->addEnter()
                    ->addText("- Terima Kasih -")->addEnter();
            //dump($formatter->getHeader());
            //dump($formatter->getText());
            //exit;
            $helper->print($formatter, $printerSelected);
            $this->addFlash('info', $this->translator->trans('print_success'));
        }catch (\Exception $ex) 
        {
            $this->addFlash('error', $ex->getMessage());
        } 
        
        
        return $this->redirectToRoute('sale_detail', ['id' => $sale->getId()]);
    }
    
    /**
     * @Route("/{id}/detail", name="sale_detail", methods={"GET", "POST"})
     */
    public function detail(Sale $sale, Request $request, BreadcrumbBuilder $builder)
    {
        $builder->getBuilder()->add('transaction');
        $builder->getBuilder()->add('sale', 'sale_index');
        $builder->getBuilder()->add('detail');
        $builder->getBuilder()->add($sale->getCode());
        
        $form = $this->createForm(SaleType::class, $sale);
        $result = $this->processForm($request, $form);
        if ($result) {
            
            return $this->redirectToRoute('sale_detail', ['id' => $result->getId()]);
        }
        
        return $this->render('pages/sale/detail.html.twig', [
            'data' => $sale, 'title' => 'detail', 'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/{id}/delete", name="sale_delete", methods={"DELETE"})
     */
    public function delete(Sale $sale, Request $request)
    {
        $tokenName = 'delete'.$sale->getId();
        parent::doDelete($request, $sale, $tokenName);
        
        return $this->redirectToRoute('sale_index');
    }
}
