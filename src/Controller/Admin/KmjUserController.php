<?php

namespace App\Controller\Admin;

use App\Entity\KmjUser;
use App\Form\KmjUserType;
use App\Repository\KmjUserRepository;
use App\Filter\KmjUserFilterType;
use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/administrator/user")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjUserController extends BaseController 
{
    /**
     * @Route("/", name="kmj_user_index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, KmjUserRepository $repo)
    {
        $builder->getBuilder()->add('administrator');
        $builder->getBuilder()->add('user');
        
        $form = $this->createFormFilter(KmjUserFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
         
        return $this->render('pages/user/index.html.twig', [
            'title' => 'user', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="kmj_user_create", methods={"GET", "POST"})
     */
    public function create(Request $request)
    {
        $form = $this->createForm(KmjUserType::class, new KmjUser(), ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("kmj_user_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/user/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="kmj_user_edit", methods={"GET", "POST"})
     */
    public function edit(KmjUser $kmjUser, Request $request)
    {
        $form = $this->createForm(KmjUserType::class, $kmjUser, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("kmj_user_edit", ['id' => $kmjUser->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/user/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="kmj_user_detail", methods={"GET"})
     */
    public function detail(KmjUser $kmjUser)
    {
        return $this->render('pages/user/detail.html.twig', ['data' => $kmjUser, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="kmj_user_delete", methods={"DELETE"})
     */
    public function delete(KmjUser $kmjUser, Request $request)
    {
        $tokenName = 'delete'.$kmjUser->getId();
        parent::doDelete($request, $kmjUser, $tokenName);
        
        return $this->redirectToRoute('kmj_user_index');
    }
}
