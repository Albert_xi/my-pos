<?php

namespace App\Controller\Admin;

use App\Entity\Documentation;
use App\Repository\DocumentationRepository;
use App\Form\DocumentationType;
use App\Filter\DocumentationFilterType;
use App\Library\BreadcrumbBuilder;
use App\Base\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/documentation", name="kmj_documentation_")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class DocumentationController extends BaseController
{
    /**
     * @Route("/", name="index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, DocumentationRepository $repo)
    {
        $builder->getBuilder()->add('administrator');
        $builder->getBuilder()->add('documentation');
        
        $form = $this->createFormFilter(DocumentationFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
        
        return $this->render('pages/doc/index.html.twig', [
            'title' => 'documentation', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     */
    public function create(Request $request)
    {
        $doc = (new Documentation())
                ->setCreatedAt(new \DateTime());
        
        $form   = $this->createForm(DocumentationType::class, $doc, ["action" => $this->generateUrl("kmj_documentation_create")]);
        $result = $this->processForm($request, $form);
        if ($result) {
            return $this->redirectToRoute('kmj_documentation_index');
        }
        
        return $this->render('pages/doc/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="edit", methods={"GET", "POST"})
     */
    public function edit(Documentation $doc, Request $request)
    {
        $form = $this->createForm(DocumentationType::class, $doc, ["action" => $this->generateUrl("kmj_documentation_edit", ['id' => $doc->getId()])]);
        $result = $this->processForm($request, $form);
        if ($result) {
            return $this->redirectToRoute('kmj_documentation_index');
        }
        
        return $this->render('pages/doc/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="detail", methods={"GET"})
     */
    public function detail(Documentation $doc)
    {
        return $this->render('pages/doc/detail.html.twig', ['data' => $doc, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="delete", methods={"DELETE"})
     */
    public function delete(Documentation $doc, Request $request)
    {
        $tokenName = 'delete'.$doc->getId();
        parent::doDelete($request, $doc, $tokenName);
        
        return $this->redirectToRoute('kmj_documentation_index');
    }
}
