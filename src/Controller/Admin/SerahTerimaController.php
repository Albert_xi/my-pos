<?php

namespace App\Controller\Admin;

use App\Entity\SerahTerima;
use App\Form\SerahTerimaType;
use App\Filter\SerahTerimaFilterType;
use App\Base\Controller\BaseController;
use App\Report\Transaction\TransactionInterface;
use App\Report\Transaction\ReportBuilder;
use App\Library\BreadcrumbBuilder;
use App\Library\UserService;
use App\Repository\SerahTerimaRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Collection;
use Kematjaya\ImportBundle\Manager\ImportManagerInterface;
use Kematjaya\Export\Manager\ManagerInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/transaction/eod", name="eod_")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SerahTerimaController extends BaseController 
{
    /**
     * 
     * @var ReportBuilder
     */
    private $reportBuilder;
    
    /**
     * 
     * @var Collection
     */
    private $transactions;
    
    public function __construct(ReportBuilder $reportBuilder, ImportManagerInterface $importManager, ManagerInterface $exportManager, FilterBuilderUpdaterInterface $filterBuilderUpdater, TranslatorInterface $translator, PaginatorInterface $paginator) {
        
        $this->reportBuilder = $reportBuilder;
        parent::__construct($importManager, $exportManager, $filterBuilderUpdater, $translator, $paginator);
    }
    
    /**
     * @Route("/", name="index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, SerahTerimaRepository $repo)
    {
        $builder->getBuilder()->add('transaction');
        $builder->getBuilder()->add('eod');
        
        $form = $this->createFormFilter(SerahTerimaFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
        
        return $this->render('pages/eod/index.html.twig', [
            'title' => 'eod', 'object' => new SerahTerima(),
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     */
    public function create(Request $request, UserService $service)
    {
        $serahTerima = (new SerahTerima())
                ->setCreatedAt(new \DateTime())
                ->setApprovedBy($service->getUser()->getUsername());
        $this->transactions = $this->reportBuilder->getTransactionsUnChecked($serahTerima->getCreatedAt());
        $form = $this->createForm(SerahTerimaType::class, $serahTerima, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("eod_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            
            return $this->json($result);
        }
        
        return $this->render('pages/eod/form.html.twig', [
            'form' => $form->createView(), 
            'title' => 'create',
            'data' => $this->transactions ]);
    }
    
    
    protected function saveObject($object, EntityManagerInterface $manager)
    {
        $manager->transactional(function (EntityManagerInterface $em) use ($object) {
            
            foreach ($this->transactions as $transaction) {
                if (!$transaction instanceof TransactionInterface) {
                    continue;
                }

                $transaction->setCheckedAt($object->getCreatedAt());
                $em->persist($transaction);
            }
            
            $em->persist($object);
        });
        
        return $object;
    }
    
    /**
     * @Route("/{id}/detail", name="detail", methods={"GET"})
     */
    public function detail(SerahTerima $serahTerima, ReportBuilder $reportBuilder)
    {
        return $this->render('pages/eod/detail.html.twig', [
            'title' => 'detail',
            'datas' => $reportBuilder->getTransactionsChecked($serahTerima->getCreatedAt()),
            'data' => $serahTerima]);
    }
}
