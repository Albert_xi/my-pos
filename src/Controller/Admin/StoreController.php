<?php

namespace App\Controller\Admin;

use App\Entity\Store;
use App\Form\StoreType;
use App\Repository\StoreRepository;
use App\Filter\StoreFilterType;
use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/administrator/store")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StoreController extends BaseController
{
    /**
     * @Route("/", name="store_index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, StoreRepository $repo):Response
    {
        $builder->getBuilder()->add('administrator');
        $builder->getBuilder()->add('store');
        
        $form = $this->createFormFilter(StoreFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
        
        return $this->render('pages/store/index.html.twig', [
            'title' => 'store', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="store_create", methods={"GET", "POST"})
     */
    public function create(Request $request):Response
    {
        $form = $this->createForm(StoreType::class, new Store(), ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("store_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            
            return $this->json($result);
        }
        
        return $this->render('pages/store/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="store_edit", methods={"GET", "POST"})
     */
    public function edit(Store $store, Request $request)
    {
        $form = $this->createForm(StoreType::class, $store, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("store_edit", ['id' => $store->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            
            return $this->json($result);
        }
        
        return $this->render('pages/store/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="store_detail", methods={"GET"})
     */
    public function detail(Store $store)
    {
        return $this->render('pages/store/detail.html.twig', ['data' => $store, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="store_delete", methods={"DELETE"})
     */
    public function delete(Store $store, Request $request)
    {
        $tokenName = 'delete'.$store->getId();
        parent::doDelete($request, $store, $tokenName);
        
        return $this->redirectToRoute('store_index');
    }
}
