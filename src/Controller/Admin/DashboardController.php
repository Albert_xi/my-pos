<?php

namespace App\Controller\Admin;

use App\Library\BreadcrumbBuilder;
use App\Repository\SaleRepository;
use App\Repository\PurchaseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ItemPriceLogRepository;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 * @Route("/home")
 */
class DashboardController extends AbstractController 
{
    
    /**
     * @Route("/", name="admin_index")
     */
    public function index(
            BreadcrumbBuilder $builder,
            SaleRepository $saleRepo,
            PurchaseRepository $purchaseRepo,
            ItemPriceLogRepository $itemPriceLog)
    {
        $builder->getBuilder()->add('home');
        
        $date = (new \DateTime());//->modify('-1 month');
        return $this->render('pages/dashboard/index.html.twig', [
            'title' => 'home', 'date' => $date,
            'sale_day_to_day' => $saleRepo->getSaleChartByDateData($date->format('m'), $date->format('Y')),
            'sale_by_category' => $saleRepo->getSaleChartByCategoryData($date->format('m'), $date->format('Y')),
            'total_income' => $saleRepo->getTotalIncome($date, 'profit'),
            'total_sale' => $saleRepo->getTotalIncome($date, 'sale'),
            'total_purchase' => $purchaseRepo->getTotalPurchase($date),
            'approval_price_count' => $itemPriceLog->getListPriceLogNew()->count()
        ,]);
    }
}
