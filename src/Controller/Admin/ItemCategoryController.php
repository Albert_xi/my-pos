<?php

namespace App\Controller\Admin;

use App\Entity\ItemCategory;
use App\Form\ItemCategoryType;
use App\Filter\ItemCategoryFilterType;
use App\Repository\ItemCategoryRepository;
use App\Library\BreadcrumbBuilder;
use App\Base\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 * @Route("/master/item-category", name="item_category_")
 */
class ItemCategoryController extends BaseController
{
    
    /**
     * @Route("/", name="index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, ItemCategoryRepository $repo)
    {
        $builder->getBuilder()->add('master');
        $builder->getBuilder()->add('item_category');
        
        $form = $this->createFormFilter(ItemCategoryFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
        
        return $this->render('pages/item-category/index.html.twig', [
            'title' => 'item_category', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     */
    public function create(Request $request, ItemCategoryRepository $itemCategoryRepo)
    {
        $form = $this->createForm(ItemCategoryType::class, $itemCategoryRepo->createItemCategory(), ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("item_category_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        return $this->render('pages/item-category/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="edit", methods={"GET", "POST"})
     */
    public function edit(ItemCategory $itemCategory, Request $request)
    {
        $form = $this->createForm(ItemCategoryType::class, $itemCategory, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("item_category_edit", ['id' => $itemCategory->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        return $this->render('pages/item-category/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="detail", methods={"GET"})
     */
    public function detail(ItemCategory $itemCategory)
    {
        return $this->render('pages/item-category/detail.html.twig', ['data' => $itemCategory, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="delete", methods={"DELETE"})
     */
    public function delete(ItemCategory $itemCategory, Request $request)
    {
        $tokenName = 'delete'.$itemCategory->getId();
        parent::doDelete($request, $itemCategory, $tokenName);
        
        return $this->redirectToRoute('item_category_index');
    }
}
