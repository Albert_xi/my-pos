<?php

namespace App\Controller\Admin;

use App\Entity\Packaging;
use App\Form\PackagingType;
use App\Filter\PackagingFilterType;
use App\Repository\PackagingRepository;
use App\Base\Controller\BaseController;
use App\Library\BreadcrumbBuilder;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/master/packaging")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PackagingController extends BaseController
{
    /**
     * @Route("/", name="packaging_index", methods={"GET", "POST"})
     */
    public function index(Request $request, BreadcrumbBuilder $builder, PackagingRepository $repo)
    {
        $builder->getBuilder()->add('master');
        $builder->getBuilder()->add('packaging');
        
        $form = $this->createFormFilter(PackagingFilterType::class);
        $queryBuilder = $this->buildFilter($request, $form, $repo->createQueryBuilder('this'));
        
        return $this->render('pages/packaging/index.html.twig', [
            'title' => 'packaging', 
            'filter' => $form->createView(),
            'pagination' => parent::createPaginator($queryBuilder, $request)]);
    }
    
    /**
     * @Route("/create", name="packaging_create", methods={"GET", "POST"})
     */
    public function create(Request $request, PackagingRepository $packagingRepo)
    {
        $form = $this->createForm(PackagingType::class, $packagingRepo->createPackaging(), ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("packaging_create")]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/packaging/form.html.twig', ['form' => $form->createView(), 'title' => 'create']);
    }
    
    /**
     * @Route("/{id}/edit", name="packaging_edit", methods={"GET", "POST"})
     */
    public function edit(Packaging $packaging, Request $request)
    {
        $form = $this->createForm(PackagingType::class, $packaging, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("packaging_edit", ['id' => $packaging->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/packaging/form.html.twig', ['form' => $form->createView(), 'title' => 'edit']);
    }
    
    /**
     * @Route("/{id}/detail", name="packaging_detail", methods={"GET"})
     */
    public function detail(Packaging $packaging)
    {
        return $this->render('pages/packaging/detail.html.twig', ['data' => $packaging, 'title' => 'detail']);
    }
    
    /**
     * @Route("/{id}/delete", name="packaging_delete", methods={"DELETE"})
     */
    public function delete(Packaging $packaging, Request $request)
    {
        $tokenName = 'delete'.$packaging->getId();
        parent::doDelete($request, $packaging, $tokenName);
        
        return $this->redirectToRoute('packaging_index');
    }
}
