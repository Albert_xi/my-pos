<?php

namespace App\Controller\Admin;

use App\Entity\Item;
use App\Entity\ItemPackage;
use App\Form\ItemPackageType;
use App\Repository\ItemPackageRepository;
use App\Base\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/master/item-package")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPackageController extends BaseController
{
    
    /**
     * @Route("/{id}/create", name="item_package_create", methods={"GET", "POST"})
     */
    public function create(Request $request, Item $item, ItemPackageRepository $itemPackageRepo)
    {
        $itemPackage = $itemPackageRepo->createPackage($item);
        $form = $this->createForm(ItemPackageType::class, $itemPackage, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("item_package_create", ['id' => $item->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/item-package/form.html.twig', ['form' => $form->createView(), 'title' => 'create', 'object' => $itemPackage]);
    }
    
    /**
     * @Route("/{id}/edit", name="item_package_edit", methods={"GET", "POST"})
     */
    public function edit(ItemPackage $itemPackage, Request $request)
    {
        $form = $this->createForm(ItemPackageType::class, $itemPackage, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("item_package_edit", ['id' => $itemPackage->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/item-package/form.html.twig', ['form' => $form->createView(), 'title' => 'edit', 'object' => $itemPackage]);
    }
    
    /**
     * @Route("/{id}/delete", name="item_package_delete", methods={"DELETE"})
     */
    public function delete(ItemPackage $itemPackage, Request $request)
    {
        $id = $itemPackage->getItem()->getId();
        $tokenName = 'delete'.$itemPackage->getId();
        parent::doDelete($request, $itemPackage, $tokenName);
        
        return $this->redirectToRoute('item_detail', ['id' => $id]);
    }
}
