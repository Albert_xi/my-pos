<?php

namespace App\Controller\Admin;

use App\Entity\ItemPriceLog;
use App\Form\ItemPriceLogType;
use App\Base\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/item-price-log")
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPriceLogController extends BaseController 
{
    /**
     * @Route("/{id}/approval", name="item_price_log_approval", methods={"GET", "POST"})
     */
    public function approval(ItemPriceLog $itemPriceLog, Request $request)
    {
        $form = $this->createForm(ItemPriceLogType::class, $itemPriceLog, ["attr" =>["id" => "ajaxForm"], "action" => $this->generateUrl("item_price_log_approval", ['id' => $itemPriceLog->getId()])]);
        $result = $this->processFormAjax($request, $form);
        if ($result["process"]) {
            return $this->json($result);
        }
        
        return $this->render('pages/price-log/form.html.twig', ['form' => $form->createView(), 'title' => 'approval']);
    }
}
