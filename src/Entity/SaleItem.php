<?php

namespace App\Entity;

use Kematjaya\ItemPack\Lib\Stock\Entity\ClientStockCardInterface;
use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\SaleBundle\Entity\SaleInterface;
use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SaleItemRepository")
 */
class SaleItem implements SaleItemInterface, ClientStockCardInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="saleItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sale", inversedBy="saleItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sale;

    /**
     * @ORM\Column(type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $principal_price;

    /**
     * @ORM\Column(type="float")
     */
    private $sale_price;

    /**
     * @ORM\Column(type="float")
     */
    private $tax;

    /**
     * @ORM\Column(type="float")
     */
    private $sub_total;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $discount;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getClassId():?string
    {
        return (string) $this->getId();
    }
    
    public function getItem(): ?ItemInterface
    {
        return $this->item;
    }

    public function setItem(?ItemInterface $item): SaleItemInterface
    {
        $this->item = $item;

        return $this;
    }

    public function getSale(): ?SaleInterface
    {
        return $this->sale;
    }

    public function setSale(?SaleInterface $sale): SaleItemInterface
    {
        $this->sale = $sale;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): SaleItemInterface
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrincipalPrice(): ?float
    {
        return $this->principal_price;
    }

    public function setPrincipalPrice(float $principal_price): SaleItemInterface
    {
        $this->principal_price = $principal_price;

        return $this;
    }

    public function getSalePrice(): ?float
    {
        return $this->sale_price;
    }

    public function setSalePrice(float $sale_price): SaleItemInterface
    {
        $this->sale_price = $sale_price;

        return $this;
    }

    public function getTax(): ?float
    {
        return $this->tax;
    }

    public function setTax(float $tax): SaleItemInterface
    {
        $this->tax = $tax;

        return $this;
    }

    public function getSubTotal(): ?float
    {
        return $this->sub_total;
    }

    public function setSubTotal(float $sub_total): SaleItemInterface
    {
        $this->sub_total = $sub_total;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): SaleItemInterface
    {
        $this->total = $total;

        return $this;
    }

    public function getTypeTransaction(): string 
    {
        return \Kematjaya\ItemPack\Lib\Stock\Entity\StockCardInterface::TYPE_GET;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): SaleItemInterface
    {
        $this->discount = $discount;

        return $this;
    }

}
