<?php

namespace App\Entity;

use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\Packaging\Entity\PackagingInterface;
use Kematjaya\ItemPack\Lib\ItemPackaging\Entity\ItemPackageInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemPackageRepository")
 */
class ItemPackage implements ItemPackageInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_smallest_unit;

    /**
     * @ORM\Column(type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $principal_price = 0;

    /**
     * @ORM\Column(type="float")
     */
    private $sale_price = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="itemPackages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Packaging", inversedBy="itemPackages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $packaging;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    
    public function getIsSmallestUnit(): ?bool
    {
        return $this->is_smallest_unit;
    }

    public function setIsSmallestUnit(bool $is_smallest_unit): ItemPackageInterface
    {
        $this->is_smallest_unit = $is_smallest_unit;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): ItemPackageInterface
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrincipalPrice(): ?float
    {
        return $this->principal_price;
    }

    public function setPrincipalPrice(float $principal_price): ItemPackageInterface
    {
        $this->principal_price = $principal_price;

        return $this;
    }

    public function getSalePrice(): ?float
    {
        return $this->sale_price;
    }

    public function setSalePrice(float $sale_price): ItemPackageInterface
    {
        $this->sale_price = $sale_price;

        return $this;
    }

    public function getItem(): ItemInterface
    {
        return $this->item;
    }

    public function setItem(?ItemInterface $item): ItemPackageInterface
    {
        $this->item = $item;

        return $this;
    }

    public function isSmallestUnit(): bool 
    {
        return $this->getIsSmallestUnit();
    }

    public function getPackaging(): ?PackagingInterface
    {
        return $this->packaging;
    }

    public function setPackaging(?PackagingInterface $packaging): ItemPackageInterface
    {
        $this->packaging = $packaging;

        return $this;
    }

}
