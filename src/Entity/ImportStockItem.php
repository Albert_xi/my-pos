<?php

namespace App\Entity;

use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid;
use Kematjaya\ItemPack\Lib\Stock\Entity\ClientStockCardInterface;
use Kematjaya\ItemPack\Lib\Stock\Entity\StockCardInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ImportStockItem implements ClientStockCardInterface
{
    private $quantitaty;
    
    public function getClassId(): ?string 
    {
        return (string) Uuid::fromDateTime(new \DateTime());
    }

    public function setQuantity(float $quantity):self
    {
        $this->quantitaty = $quantity;
        
        return $this;
    }
    
    public function getQuantity(): ?float 
    {
        return $this->quantitaty;
    }

    public function getTypeTransaction(): string 
    {
        return StockCardInterface::TYPE_ADD;
    }

}
