<?php

namespace App\Entity;

use App\Repository\KmjPrinterClientRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use Kematjaya\EscposBundle\Client\AbstractWindowsClient;

/**
 * @ORM\Entity(repositoryClass=KmjPrinterClientRepository::class)
 */
class KmjPrinterClient extends AbstractWindowsClient
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $computer_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $printer_name;

    /**
     * @ORM\ManyToOne(targetEntity=KmjUser::class, inversedBy="kmjPrinterClients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kmj_user;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_default;

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function __toString() 
    {
        return sprintf('%s (%s)', $this->getPrinterName(), $this->getComputerName());
    }
    
    public function getComputerName(): ?string
    {
        return $this->computer_name;
    }

    public function setComputerName(string $computer_name): self
    {
        $this->computer_name = $computer_name;

        return $this;
    }

    public function getPrinterName(): ?string
    {
        return $this->printer_name;
    }

    public function setPrinterName(string $printer_name): self
    {
        $this->printer_name = $printer_name;

        return $this;
    }

    public function getKmjUser(): ?KmjUser
    {
        return $this->kmj_user;
    }

    public function setKmjUser(?KmjUser $kmj_user): self
    {
        $this->kmj_user = $kmj_user;

        return $this;
    }

    public function getIsDefault(): ?bool
    {
        return $this->is_default;
    }

    public function setIsDefault(?bool $is_default): self
    {
        $this->is_default = $is_default;

        return $this;
    }
}
