<?php

namespace App\Entity;

use App\Report\Transaction\TransactionInterface;
use Kematjaya\PurchashingBundle\Entity\SupplierInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PurchaseRepository")
 */
class Purchase implements PurchaseInterface, TransactionInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="date")
     */
    private $purchase_at;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_locked;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseDetail", mappedBy="purchase", orphanRemoval=true)
     */
    private $purchaseDetails;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="purchases")
     */
    private $supplier;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $checked_at;

    public function __construct()
    {
        $this->purchaseDetails = new ArrayCollection();
    }

    public function __toString() 
    {
        return $this->getCode();
    }
    
    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): PurchaseInterface
    {
        $this->code = $code;

        return $this;
    }

    public function getPurchaseAt(): ?\DateTimeInterface
    {
        return $this->purchase_at;
    }

    public function setPurchaseAt(\DateTimeInterface $purchase_at): PurchaseInterface
    {
        $this->purchase_at = $purchase_at;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): PurchaseInterface
    {
        $this->total = $total;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): PurchaseInterface
    {
        $this->description = $description;

        return $this;
    }

    public function getIsLocked(): ?bool
    {
        return $this->is_locked;
    }

    public function setIsLocked(?bool $is_locked): PurchaseInterface
    {
        $this->is_locked = $is_locked;

        return $this;
    }

    /**
     * @return Collection|PurchaseDetail[]
     */
    public function getPurchaseDetails(): Collection
    {
        return $this->purchaseDetails;
    }

    public function addPurchaseDetail(PurchaseDetail $purchaseDetail): self
    {
        if (!$this->purchaseDetails->contains($purchaseDetail)) {
            $this->purchaseDetails[] = $purchaseDetail;
            $purchaseDetail->setPurchase($this);
        }

        return $this;
    }

    public function removePurchaseDetail(PurchaseDetail $purchaseDetail): self
    {
        if ($this->purchaseDetails->contains($purchaseDetail)) {
            $this->purchaseDetails->removeElement($purchaseDetail);
            // set the owning side to null (unless already changed)
            if ($purchaseDetail->getPurchase() === $this) {
                $purchaseDetail->setPurchase(null);
            }
        }

        return $this;
    }

    public function getSupplier(): ?SupplierInterface
    {
        return $this->supplier;
    }

    public function setSupplier(?SupplierInterface $supplier): PurchaseInterface
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface 
    {
        return $this->getPurchaseAt();
    }

    public function getTypeTransaction(): string 
    {
        return self::TYPE_IN;
    }

    public function getCredit(): ?float 
    {
        return $this->getTotal();
    }

    public function getDebt(): ?float 
    {
        return null;
    }

    public function getCheckedAt(): ?\DateTimeInterface
    {
        return $this->checked_at;
    }

    public function setCheckedAt(?\DateTimeInterface $checked_at): TransactionInterface
    {
        $this->checked_at = $checked_at;

        return $this;
    }

}
