<?php

namespace App\Entity;

use App\Repository\KmjRoutingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use App\Library\RoutingManagement\Entity\KmjRoutingInterface;
/**
 * @ORM\Entity(repositoryClass=KmjRoutingRepository::class)
 */
class KmjRouting implements KmjRoutingInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="integer")
     */
    private $sequence;

    /**
     * @ORM\Column(type="uuid", length=255, nullable=true)
     */
    private $parent;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $routing_group;

    /**
     * @ORM\OneToMany(targetEntity=KmjRoutingRole::class, mappedBy="routing", orphanRemoval=true)
     */
    private $kmjRoutingRoles;

    public function __construct()
    {
        $this->kmjRoutingRoles = new ArrayCollection();
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(int $sequence): self
    {
        $this->sequence = $sequence;

        return $this;
    }

    public function getParent(): ?UuidInterface
    {
        return $this->parent;
    }

    public function setParent(?UuidInterface $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getRoutingGroup(): ?string
    {
        return $this->routing_group;
    }

    public function setRoutingGroup(?string $routing_group): self
    {
        $this->routing_group = $routing_group;

        return $this;
    }

    /**
     * @return Collection|KmjRoutingRole[]
     */
    public function getKmjRoutingRoles(): Collection
    {
        return $this->kmjRoutingRoles;
    }

    public function addKmjRoutingRole(KmjRoutingRole $kmjRoutingRole): self
    {
        if (!$this->kmjRoutingRoles->contains($kmjRoutingRole)) {
            $this->kmjRoutingRoles[] = $kmjRoutingRole;
            $kmjRoutingRole->setRouting($this);
        }

        return $this;
    }

    public function removeKmjRoutingRole(KmjRoutingRole $kmjRoutingRole): self
    {
        if ($this->kmjRoutingRoles->contains($kmjRoutingRole)) {
            $this->kmjRoutingRoles->removeElement($kmjRoutingRole);
            // set the owning side to null (unless already changed)
            if ($kmjRoutingRole->getRouting() === $this) {
                $kmjRoutingRole->setRouting(null);
            }
        }

        return $this;
    }
}
