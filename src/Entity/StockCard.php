<?php

namespace App\Entity;

use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\Stock\Entity\StockCardInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockCardRepository")
 */
class StockCard implements StockCardInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="stockCards")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $class_name;

    /**
     * @ORM\Column(type="string")
     */
    private $class_id;

    /**
     * @ORM\Column(type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getItem(): ItemInterface
    {
        return $this->item;
    }

    public function setItem(?ItemInterface $item): StockCardInterface
    {
        $this->item = $item;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): StockCardInterface
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getClassName(): string
    {
        return $this->class_name;
    }

    public function setClassName(string $class_name): StockCardInterface
    {
        $this->class_name = $class_name;

        return $this;
    }

    public function getClassId(): string
    {
        return $this->class_id;
    }

    public function setClassId(string $class_id): StockCardInterface
    {
        $this->class_id = $class_id;

        return $this;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): StockCardInterface
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): StockCardInterface
    {
        $this->type = $type;

        return $this;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function setTotal(float $total): StockCardInterface
    {
        $this->total = $total;

        return $this;
    }
}
