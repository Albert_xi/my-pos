<?php

namespace App\Entity;

use App\Repository\BackupLogRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Library\Database\Log\OperationLogInterface;

/**
 * @ORM\Entity(repositoryClass=BackupLogRepository::class)
 */
class BackupLog implements OperationLogInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): OperationLogInterface
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): OperationLogInterface
    {
        $this->filename = $filename;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): OperationLogInterface
    {
        $this->type = $type;

        return $this;
    }
}
