<?php

namespace App\Entity;

use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseDetailInterface;
use Kematjaya\ItemPack\Lib\Packaging\Entity\PackagingInterface;
use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\Stock\Entity\ClientStockCardInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PurchaseDetailRepository")
 */
class PurchaseDetail implements PurchaseDetailInterface, ClientStockCardInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="purchaseDetails")
     */
    private $item;

    /**
     * @ORM\Column(type="float")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="float")
     */
    private $tax;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Purchase", inversedBy="purchaseDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $purchase;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Packaging", inversedBy="purchaseDetails")
     */
    private $packaging;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getClassId():?string
    {
        return (string) $this->getId();
    }
    
    public function getItem(): ?ItemInterface
    {
        return $this->item;
    }

    public function setItem(?ItemInterface $item): PurchaseDetailInterface
    {
        $this->item = $item;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): PurchaseDetailInterface
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): PurchaseDetailInterface
    {
        $this->price = $price;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): PurchaseDetailInterface
    {
        $this->total = $total;

        return $this;
    }

    public function getTax(): ?float
    {
        return $this->tax;
    }

    public function setTax(float $tax): PurchaseDetailInterface
    {
        $this->tax = $tax;

        return $this;
    }

    public function getPurchase(): ?PurchaseInterface
    {
        return $this->purchase;
    }

    public function setPurchase(?PurchaseInterface $purchase): PurchaseDetailInterface
    {
        $this->purchase = $purchase;

        return $this;
    }

    public function getPackaging(): ?PackagingInterface
    {
        return $this->packaging;
    }

    public function setPackaging(?PackagingInterface $packaging): PurchaseDetailInterface
    {
        $this->packaging = $packaging;

        return $this;
    }

    public function getTypeTransaction(): string 
    {
        return \Kematjaya\ItemPack\Lib\Stock\Entity\StockCardInterface::TYPE_ADD;
    }

}
