<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\Price\Entity\PriceLogClientInterface;
use Kematjaya\ItemPack\Lib\ItemCategory\Entity\ItemCategoryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item implements ItemInterface, PriceLogClientInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ItemCategory", inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $use_barcode = false;

    /**
     * @ORM\Column(type="float")
     */
    private $last_stock = 0;

    /**
     * @ORM\Column(type="float")
     */
    private $last_price;

    /**
     * @ORM\Column(type="float")
     */
    private $harga_pokok;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseDetail", mappedBy="item")
     */
    private $purchaseDetails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItemPackage", mappedBy="item", orphanRemoval=true)
     */
    private $itemPackages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SaleItem", mappedBy="item")
     */
    private $saleItems;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockCard", mappedBy="item", orphanRemoval=true)
     * @ORM\OrderBy({"created_at": "DESC"})
     */
    private $stockCards;

    /**
     * @ORM\OneToMany(targetEntity=ItemPriceLog::class, mappedBy="item", orphanRemoval=true)
     * @ORM\OrderBy({"created_at": "DESC"})
     */
    private $itemPriceLogs;

    public function __construct()
    {
        $this->purchaseDetails = new ArrayCollection();
        $this->itemPackages = new ArrayCollection();
        $this->saleItems = new ArrayCollection();
        $this->stockCards = new ArrayCollection();
        $this->itemPriceLogs = new ArrayCollection();
        $this->use_barcode = false;
    }

    public function __toString()
    {
        return $this->getName();
    }
    
    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): ItemInterface
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): ItemInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?ItemCategoryInterface
    {
        return $this->category;
    }

    public function setCategory(?ItemCategoryInterface $category): ItemInterface
    {
        $this->category = $category;

        return $this;
    }

    public function getUseBarcode(): ?bool
    {
        return $this->use_barcode;
    }

    public function setUseBarcode(?bool $use_barcode): ItemInterface
    {
        $this->use_barcode = $use_barcode;

        return $this;
    }

    public function getLastStock(): ?float
    {
        return $this->last_stock;
    }

    public function setLastStock(float $last_stock): ItemInterface
    {
        $this->last_stock = $last_stock;

        return $this;
    }

    public function getLastPrice(): ?float
    {
        return $this->last_price;
    }

    public function setLastPrice(float $last_price): ItemInterface
    {
        $this->last_price = $last_price;

        return $this;
    }

    public function getHargaPokok(): ?float
    {
        return $this->harga_pokok;
    }

    public function setHargaPokok(float $harga_pokok): self
    {
        $this->harga_pokok = $harga_pokok;

        return $this;
    }

    public function getPrincipalPrice(): ?float 
    {
        return $this->getHargaPokok();
    }

    public function setPrincipalPrice(float $principal_price): ItemInterface 
    {
        return $this->setHargaPokok($principal_price);
    }
    
    /**
     * @return Collection|PurchaseDetail[]
     */
    public function getPurchaseDetails(): Collection
    {
        return $this->purchaseDetails;
    }

    public function addPurchaseDetail(PurchaseDetail $purchaseDetail): self
    {
        if (!$this->purchaseDetails->contains($purchaseDetail)) {
            $this->purchaseDetails[] = $purchaseDetail;
            $purchaseDetail->setItem($this);
        }

        return $this;
    }

    public function removePurchaseDetail(PurchaseDetail $purchaseDetail): self
    {
        if ($this->purchaseDetails->contains($purchaseDetail)) {
            $this->purchaseDetails->removeElement($purchaseDetail);
            // set the owning side to null (unless already changed)
            if ($purchaseDetail->getItem() === $this) {
                $purchaseDetail->setItem(null);
            }
        }

        return $this;
    }

    public function getSmallestPackages():?ItemPackage
    {
        $itemPackages = $this->getItemPackages()->filter(function(ItemPackage $package){
            return $package->isSmallestUnit();
        });
        
        return (!$itemPackages->isEmpty()) ? $itemPackages->first() : null;
    }
    
    /**
     * @return Collection|ItemPackage[]
     */
    public function getItemPackages(): Collection
    {
        return $this->itemPackages;
    }

    public function addItemPackage(ItemPackage $itemPackage): self
    {
        if (!$this->itemPackages->contains($itemPackage)) {
            $this->itemPackages[] = $itemPackage;
            $itemPackage->setItem($this);
        }

        return $this;
    }

    public function removeItemPackage(ItemPackage $itemPackage): self
    {
        if ($this->itemPackages->contains($itemPackage)) {
            $this->itemPackages->removeElement($itemPackage);
            // set the owning side to null (unless already changed)
            if ($itemPackage->getItem() === $this) {
                $itemPackage->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SaleItem[]
     */
    public function getSaleItems(): Collection
    {
        return $this->saleItems;
    }

    public function addSaleItem(SaleItem $saleItem): self
    {
        if (!$this->saleItems->contains($saleItem)) {
            $this->saleItems[] = $saleItem;
            $saleItem->setItem($this);
        }

        return $this;
    }

    public function removeSaleItem(SaleItem $saleItem): self
    {
        if ($this->saleItems->contains($saleItem)) {
            $this->saleItems->removeElement($saleItem);
            // set the owning side to null (unless already changed)
            if ($saleItem->getItem() === $this) {
                $saleItem->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StockCard[]
     */
    public function getStockCards(): Collection
    {
        return $this->stockCards;
    }

    public function addStockCard(StockCard $stockCard): self
    {
        if (!$this->stockCards->contains($stockCard)) {
            $this->stockCards[] = $stockCard;
            $stockCard->setItem($this);
        }

        return $this;
    }

    public function removeStockCard(StockCard $stockCard): self
    {
        if ($this->stockCards->contains($stockCard)) {
            $this->stockCards->removeElement($stockCard);
            // set the owning side to null (unless already changed)
            if ($stockCard->getItem() === $this) {
                $stockCard->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemPriceLog[]
     */
    public function getItemPriceLogs(): Collection
    {
        return $this->itemPriceLogs;
    }

    public function addItemPriceLog(ItemPriceLog $itemPriceLog): self
    {
        if (!$this->itemPriceLogs->contains($itemPriceLog)) {
            $this->itemPriceLogs[] = $itemPriceLog;
            $itemPriceLog->setItem($this);
        }

        return $this;
    }

    public function removeItemPriceLog(ItemPriceLog $itemPriceLog): self
    {
        if ($this->itemPriceLogs->contains($itemPriceLog)) {
            $this->itemPriceLogs->removeElement($itemPriceLog);
            // set the owning side to null (unless already changed)
            if ($itemPriceLog->getItem() === $this) {
                $itemPriceLog->setItem(null);
            }
        }

        return $this;
    }

    public function getActivePrincipalPrice(): ?float 
    {
        return $this->getPrincipalPrice();
    }

    public function getActiveSalePrice(): ?float 
    {
        return $this->getLastPrice();
    }

    public function setActivePrincipalPrice(float $price): PriceLogClientInterface 
    {
        return $this->setPrincipalPrice($price);
    }

    public function setActiveSalePrice(float $price): PriceLogClientInterface 
    {
        return $this->setLastPrice($price);
    }

}
