<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kematjaya\UserBundle\Entity\KmjUserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KmjUserRepository")
 */
class KmjUser implements KmjUserInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    private $password_old;
    
    private $password_new;
    
    private $password_re_new;

    /**
     * @ORM\ManyToOne(targetEntity=Store::class, inversedBy="persons")
     */
    private $store;

    /**
     * @ORM\OneToMany(targetEntity=KmjPrinterClient::class, mappedBy="kmj_user", orphanRemoval=true)
     */
    private $kmjPrinterClients;

    public function __construct()
    {
        $this->kmjPrinterClients = new ArrayCollection();
    }
    
    const ROLE_OPERATOR = "ROLE_OPERATOR";
    const ROLE_KEPALA = "ROLE_KEPALA";
    
    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function __toString() 
    {
        return $this->getName();
    }
    
    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): KmjUserInterface
    {
        $this->username = $username;

        return $this;
    }

    public function isOperator()
    {
        return in_array(self::ROLE_OPERATOR, $this->getRoles());
    }
    
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        if(empty($roles))
        {
            $roles[] = self::ROLE_USER;
        }
        

        return array_unique($roles);
    }

    public function setRoles(array $roles): KmjUserInterface
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): KmjUserInterface
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): KmjUserInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): KmjUserInterface
    {
        $this->is_active = $is_active;

        return $this;
    }
    
    public function setPasswordOld(string $password): KmjUserInterface
    {
        $this->password_old = $password;
        
        return $this;
    }
    
    public function getPasswordOld():?string
    {
        return $this->password_old;
    }
    
    public function setPasswordNew(string $password): KmjUserInterface
    {
        $this->password_new = $password;
        
        return $this;
    }
    
    public function getPasswordNew():?string
    {
        return $this->password_new;
    }
    
    public function setPasswordReNew(string $password): KmjUserInterface
    {
        $this->password_re_new = $password;
        
        return $this;
    }
    
    public function getPasswordReNew():?string
    {
        return $this->password_re_new;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    /**
     * @return Collection|KmjPrinterClient[]
     */
    public function getKmjPrinterClients(): Collection
    {
        return $this->kmjPrinterClients;
    }

    public function addKmjPrinterClient(KmjPrinterClient $kmjPrinterClient): self
    {
        if (!$this->kmjPrinterClients->contains($kmjPrinterClient)) {
            $this->kmjPrinterClients[] = $kmjPrinterClient;
            $kmjPrinterClient->setKmjUser($this);
        }

        return $this;
    }

    public function removeKmjPrinterClient(KmjPrinterClient $kmjPrinterClient): self
    {
        if ($this->kmjPrinterClients->contains($kmjPrinterClient)) {
            $this->kmjPrinterClients->removeElement($kmjPrinterClient);
            // set the owning side to null (unless already changed)
            if ($kmjPrinterClient->getKmjUser() === $this) {
                $kmjPrinterClient->setKmjUser(null);
            }
        }

        return $this;
    }
}
