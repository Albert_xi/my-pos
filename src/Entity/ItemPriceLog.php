<?php

namespace App\Entity;

use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\Price\Entity\PriceLogInterface;
use App\Repository\ItemPriceLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemPriceLogRepository::class)
 */
class ItemPriceLog implements PriceLogInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Item::class, inversedBy="itemPriceLogs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $principal_price_old;

    /**
     * @ORM\Column(type="float")
     */
    private $principal_price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $sale_price_old;

    /**
     * @ORM\Column(type="float")
     */
    private $sale_price;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $inserted_by;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $approved_by;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }
    
    public function isNew():bool
    {
        return $this->getStatus() === self::STATUS_NEW;
    }

    public function isApproved():bool
    {
        return $this->getStatus() === self::STATUS_APPROVED;
    }
    
    public function isRejected():bool
    {
        return $this->getStatus() === self::STATUS_REJECTED;
    }
    
    public function getStatusString()
    {
        $arr = self::getArrayStatus();
        return (isset($arr[$this->getStatus()])) ? $arr[$this->getStatus()] : '';
    }
    
    public static function getArrayStatus()
    {
        return [
            self::STATUS_APPROVED => 'approved',
            self::STATUS_REJECTED => 'rejected',
            self::STATUS_NEW => 'new'
        ];
    }
    
    public function getItem(): ?ItemInterface
    {
        return $this->item;
    }

    public function setItem(?ItemInterface $item): PriceLogInterface
    {
        $this->item = $item;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): PriceLogInterface
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getPrincipalPriceOld(): ?float
    {
        return $this->principal_price_old;
    }

    public function setPrincipalPriceOld(?float $principal_price_old): PriceLogInterface
    {
        $this->principal_price_old = $principal_price_old;

        return $this;
    }

    public function getPrincipalPrice(): ?float
    {
        return $this->principal_price;
    }

    public function setPrincipalPrice(float $principal_price): PriceLogInterface
    {
        $this->principal_price = $principal_price;

        return $this;
    }

    public function getSalePriceOld(): ?float
    {
        return $this->sale_price_old;
    }

    public function setSalePriceOld(?float $sale_price_old): PriceLogInterface
    {
        $this->sale_price_old = $sale_price_old;

        return $this;
    }

    public function getSalePrice(): ?float
    {
        return $this->sale_price;
    }

    public function setSalePrice(float $sale_price): PriceLogInterface
    {
        $this->sale_price = $sale_price;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): PriceLogInterface
    {
        $this->status = $status;

        return $this;
    }

    public function getInsertedBy(): ?string
    {
        return $this->inserted_by;
    }

    public function setInsertedBy(string $inserted_by): self
    {
        $this->inserted_by = $inserted_by;

        return $this;
    }

    public function getApprovedBy(): ?string
    {
        return $this->approved_by;
    }

    public function setApprovedBy(?string $approved_by): self
    {
        $this->approved_by = $approved_by;

        return $this;
    }
}
