<?php

namespace App\Entity;

use App\Report\Transaction\TransactionInterface;
use Kematjaya\SaleBundle\Entity\CustomerInterface;
use Kematjaya\SaleBundle\Entity\SaleInterface;
use Kematjaya\CodeManager\Entity\CodeLibraryClientInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SaleRepository")
 */
class Sale implements SaleInterface, CodeLibraryClientInterface, TransactionInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $created_by;

    /**
     * @ORM\Column(type="float")
     */
    private $sub_total;

    /**
     * @ORM\Column(type="float")
     */
    private $tax;

    /**
     * @ORM\Column(type="float")
     */
    private $total;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_locked;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="sales")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SaleItem", mappedBy="sale", orphanRemoval=true)
     */
    private $saleItems;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $discount;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $payment;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $payment_change;

    /**
     * @ORM\ManyToOne(targetEntity=Store::class, inversedBy="sales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $store;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $checked_at;

    public function __construct()
    {
        $this->saleItems = new ArrayCollection();
    }

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): SaleInterface
    {
        $this->code = $code;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): SaleInterface
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->created_by;
    }

    public function setCreatedBy(string $created_by): SaleInterface
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getTotal(): ?float
    {
        if(!$this->total and !$this->getSaleItems()->isEmpty())
        {
            foreach($this->getSaleItems() as $saleItem)
            {
                $this->total += $saleItem->getTotal();
            }
            
            $this->total += $this->getTax();
            $this->total -= $this->getDiscount();
        }
        
        return $this->total;
    }

    public function setTotal(float $total): SaleInterface
    {
        $this->total = $total;

        return $this;
    }

    public function getSubTotal(): ?float
    {
        return $this->sub_total;
    }

    public function setSubTotal(float $sub_total): SaleInterface
    {
        $this->sub_total = $sub_total;

        return $this;
    }

    public function getTax(): ?float
    {
        return $this->tax;
    }

    public function setTax(float $tax): SaleInterface
    {
        $this->tax = $tax;

        return $this;
    }

    public function getIsLocked(): ?bool
    {
        return $this->is_locked;
    }

    public function setIsLocked(?bool $is_locked): SaleInterface
    {
        $this->is_locked = $is_locked;

        return $this;
    }

    public function getCustomer(): ?CustomerInterface
    {
        return $this->customer;
    }

    public function setCustomer(?CustomerInterface $customer): SaleInterface
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|SaleItem[]
     */
    public function getSaleItems(): Collection
    {
        return $this->saleItems;
    }

    public function addSaleItem(SaleItem $saleItem): SaleInterface
    {
        if (!$this->saleItems->contains($saleItem)) {
            $this->saleItems[] = $saleItem;
            $saleItem->setSale($this);
        }

        return $this;
    }

    public function removeSaleItem(SaleItem $saleItem): SaleInterface
    {
        if ($this->saleItems->contains($saleItem)) {
            $this->saleItems->removeElement($saleItem);
            // set the owning side to null (unless already changed)
            if ($saleItem->getSale() === $this) {
                $saleItem->setSale(null);
            }
        }

        return $this;
    }

    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    public function setDiscount(float $discount): SaleInterface
    {
        $this->discount = $discount;

        return $this;
    }

    public function getPayment(): ?float
    {
        return $this->payment;
    }

    public function setPayment(float $payment): SaleInterface
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPaymentChange(): ?float
    {
        if(!$this->payment_change)
        {
            $this->payment_change = $this->getPayment() - $this->getTotal();
        }
        return $this->payment_change;
    }

    public function setPaymentChange(float $payment_change): SaleInterface
    {
        $this->payment_change = $payment_change;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }
    
    // =================== CodeManager ===============
    public function getClassId():?string
    {
        return (string) $this->getId();
    }
    
    public function getLibrary():array
    {
        return [
            "type" => 'SL'
        ];
    }
    
    public function getGeneratedCode():?string
    {
        return $this->getCode();
    }
    
    public function setGeneratedCode(string $code): CodeLibraryClientInterface
    {
        return $this->setCode($code);
    }
    
    // =================== end CodeManager ===============

    public function getDescription(): ?string 
    {
        return 'penjualan';
    }

    public function getTypeTransaction(): string 
    {
        return self::TYPE_OUT;
    }

    public function getCredit(): ?float 
    {
        return null;
    }

    public function getDebt(): ?float 
    {
        return $this->getTotal();
    }

    public function isPaid()
    {
        return $this->getPayment() >= $this->getTotal();
    }
    // end implement App\Library\CodeGenerator\Entity\ClientInterface

    public function getCheckedAt(): ?\DateTimeInterface
    {
        return $this->checked_at;
    }

    public function setCheckedAt(?\DateTimeInterface $checked_at): TransactionInterface
    {
        $this->checked_at = $checked_at;

        return $this;
    }
}
