<?php

namespace App\Entity;

use App\Repository\StoreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StoreRepository::class)
 */
class Store
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $address;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_default = false;

    /**
     * @ORM\OneToMany(targetEntity=KmjUser::class, mappedBy="store")
     */
    private $persons;

    /**
     * @ORM\OneToMany(targetEntity=Sale::class, mappedBy="store")
     */
    private $sales;

    public function __construct()
    {
        $this->persons = new ArrayCollection();
        $this->sales = new ArrayCollection();
    }

    public function __toString() 
    {
        return sprintf('%s - %s', $this->getCode(), $this->getName());
    }
    
    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getIsDefault(): ?bool
    {
        return $this->is_default;
    }

    public function setIsDefault(bool $is_default = false): self
    {
        $this->is_default = $is_default;

        return $this;
    }

    /**
     * @return Collection|KmjUser[]
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function addPerson(KmjUser $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
            $person->setStore($this);
        }

        return $this;
    }

    public function removePerson(KmjUser $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
            // set the owning side to null (unless already changed)
            if ($person->getStore() === $this) {
                $person->setStore(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sale[]
     */
    public function getSales(): Collection
    {
        return $this->sales;
    }

    public function addSale(Sale $sale): self
    {
        if (!$this->sales->contains($sale)) {
            $this->sales[] = $sale;
            $sale->setStore($this);
        }

        return $this;
    }

    public function removeSale(Sale $sale): self
    {
        if ($this->sales->contains($sale)) {
            $this->sales->removeElement($sale);
            // set the owning side to null (unless already changed)
            if ($sale->getStore() === $this) {
                $sale->setStore(null);
            }
        }

        return $this;
    }
}
