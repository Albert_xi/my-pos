<?php

namespace App\Entity;

use App\Repository\SerahTerimaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SerahTerimaRepository::class)
 */
class SerahTerima
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_debt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_credit;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $delegated_by;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $approved_by;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTotalDebt(): ?float
    {
        return $this->total_debt;
    }

    public function setTotalDebt(?float $total_debt): self
    {
        $this->total_debt = $total_debt;

        return $this;
    }

    public function getTotalCredit(): ?float
    {
        return $this->total_credit;
    }

    public function setTotalCredit(?float $total_credit): self
    {
        $this->total_credit = $total_credit;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDelegatedBy(): ?string
    {
        return $this->delegated_by;
    }

    public function setDelegatedBy(KmjUser $delegated_by): self
    {
        $this->delegated_by = $delegated_by->getUsername();

        return $this;
    }

    public function getApprovedBy(): ?string
    {
        return $this->approved_by;
    }

    public function setApprovedBy(?string $approved_by): self
    {
        $this->approved_by = $approved_by;

        return $this;
    }
}
