<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kematjaya\ItemPack\Lib\Packaging\Entity\PackagingInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PackagingRepository")
 */
class Packaging implements PackagingInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItemPackage", mappedBy="packaging", orphanRemoval=true)
     */
    private $itemPackages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseDetail", mappedBy="packaging")
     */
    private $purchaseDetails;

    public function __construct()
    {
        $this->itemPackages = new ArrayCollection();
        $this->purchaseDetails = new ArrayCollection();
    }

    public function __toString() 
    {
        return $this->getName();
    }
    
    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): PackagingInterface
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): PackagingInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ItemPackage[]
     */
    public function getItemPackages(): Collection
    {
        return $this->itemPackages;
    }

    public function addItemPackage(ItemPackage $itemPackage): self
    {
        if (!$this->itemPackages->contains($itemPackage)) {
            $this->itemPackages[] = $itemPackage;
            $itemPackage->setPackaging($this);
        }

        return $this;
    }

    public function removeItemPackage(ItemPackage $itemPackage): self
    {
        if ($this->itemPackages->contains($itemPackage)) {
            $this->itemPackages->removeElement($itemPackage);
            // set the owning side to null (unless already changed)
            if ($itemPackage->getPackaging() === $this) {
                $itemPackage->setPackaging(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PurchaseDetail[]
     */
    public function getPurchaseDetails(): Collection
    {
        return $this->purchaseDetails;
    }

    public function addPurchaseDetail(PurchaseDetail $purchaseDetail): self
    {
        if (!$this->purchaseDetails->contains($purchaseDetail)) {
            $this->purchaseDetails[] = $purchaseDetail;
            $purchaseDetail->setPackaging($this);
        }

        return $this;
    }

    public function removePurchaseDetail(PurchaseDetail $purchaseDetail): self
    {
        if ($this->purchaseDetails->contains($purchaseDetail)) {
            $this->purchaseDetails->removeElement($purchaseDetail);
            // set the owning side to null (unless already changed)
            if ($purchaseDetail->getPackaging() === $this) {
                $purchaseDetail->setPackaging(null);
            }
        }

        return $this;
    }
}
