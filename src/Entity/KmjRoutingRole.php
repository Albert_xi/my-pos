<?php

namespace App\Entity;

use App\Repository\KmjRoutingRoleRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use App\Library\RoutingManagement\Entity\KmjRoutingRoleInterface;
use App\Library\RoutingManagement\Entity\KmjRoutingInterface;

/**
 * @ORM\Entity(repositoryClass=KmjRoutingRoleRepository::class)
 */
class KmjRoutingRole implements KmjRoutingRoleInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=KmjRouting::class, inversedBy="kmjRoutingRoles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $routing;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_allowed = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getRouting(): ?KmjRoutingInterface
    {
        return $this->routing;
    }

    public function setRouting(?KmjRouting $routing): self
    {
        $this->routing = $routing;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }
    
    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getIsAllowed(): ?bool
    {
        return $this->is_allowed;
    }

    public function setIsAllowed(bool $is_allowed): self
    {
        $this->is_allowed = $is_allowed;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
    
    public function isAllowed():bool
    {
        return $this->getIsAllowed();
    }
}
