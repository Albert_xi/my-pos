<?php

namespace App\Entity;

use App\Repository\CodeLibraryRepository;
use Doctrine\ORM\Mapping as ORM;
use Kematjaya\CodeManager\Entity\CodeLibraryInterface;

/**
 * @ORM\Entity(repositoryClass=CodeLibraryRepository::class)
 */
class CodeLibrary implements CodeLibraryInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=Ramsey\Uuid\Doctrine\UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $format;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $class_name;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $separator;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUsed;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $last_sequence;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_code;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getClassName(): ?string
    {
        return $this->class_name;
    }

    public function setClassName(string $class_name): self
    {
        $this->class_name = $class_name;

        return $this;
    }

    public function getSeparator(): ?string
    {
        return $this->separator;
    }

    public function setSeparator(string $separator): self
    {
        $this->separator = $separator;

        return $this;
    }

    public function getLastUsed(): \DateTimeInterface
    {
        return $this->lastUsed;
    }

    public function setLastUsed(?\DateTimeInterface $lastUsed): CodeLibraryInterface
    {
        $this->lastUsed = $lastUsed;

        return $this;
    }

    public function getLastSequence(): ?int
    {
        return $this->last_sequence;
    }

    public function setLastSequence(?int $last_sequence): CodeLibraryInterface
    {
        $this->last_sequence = $last_sequence;

        return $this;
    }

    public function getLastCode(): ?string
    {
        return $this->last_code;
    }

    public function setLastCode(?string $last_code): CodeLibraryInterface
    {
        $this->last_code = $last_code;

        return $this;
    }
}
