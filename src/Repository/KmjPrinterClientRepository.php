<?php

namespace App\Repository;

use App\Entity\KmjPrinterClient;
use App\Library\UserService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @method KmjPrinterClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method KmjPrinterClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method KmjPrinterClient[]    findAll()
 * @method KmjPrinterClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KmjPrinterClientRepository extends ServiceEntityRepository
{
    protected $userService, $translator;
    
    public function __construct(ManagerRegistry $registry, UserService $userService, TranslatorInterface $translator)
    {
        $this->userService = $userService;
        $this->translator = $translator;
        parent::__construct($registry, KmjPrinterClient::class);
    }

    public function createQueryBuilder($alias, $indexBy = null) 
    {
        $qb = parent::createQueryBuilder($alias, $indexBy)->where(sprintf('%s.kmj_user = :kmj_user', $alias))->setParameter('kmj_user', $this->userService->getUser());
        
        return $qb;
    }
    
    public function findByUser()
    {
        return $this->createQueryBuilder('t')->getQuery()->getResult();
    }
    
    public function countPrinterByUser():float
    {
        return $this->createQueryBuilder('t')
                ->select('count(t.id)')->getQuery()->getSingleScalarResult();
    }
    
    public function findIsDefaultByUser():KmjPrinterClient
    {
        $printerClient = $this->findOneBy(['kmj_user' => $this->userService->getUser(), 'is_default' => true]);
        if(!$printerClient)
        {
            throw new \Exception(sprintf('%s %s: %s', $this->translator->trans('printer_not_found'), $this->translator->trans('for_user'), $this->userService->getUser()->getName()));
        }
        
        return $printerClient;
    }
}
