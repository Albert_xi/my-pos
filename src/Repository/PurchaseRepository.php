<?php

namespace App\Repository;

use App\Entity\Purchase;
use App\Library\UserService;
use Kematjaya\PurchashingBundle\Repo\PurchaseRepoInterface;
use Kematjaya\PurchashingBundle\Entity\PurchaseInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Purchase|null find($id, $lockMode = null, $lockVersion = null)
 * @method Purchase|null findOneBy(array $criteria, array $orderBy = null)
 * @method Purchase[]    findAll()
 * @method Purchase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchaseRepository extends ServiceEntityRepository implements PurchaseRepoInterface
{
    private $userService;
    
    use DoctrineOperation;
    
    public function __construct(ManagerRegistry $registry, UserService $userService)
    {
        $this->userService = $userService;
        parent::__construct($registry, Purchase::class);
    }

    public function createObject(): PurchaseInterface
    {
        return new Purchase();
    }

    public function save(PurchaseInterface $package): void 
    {
        $this->doPersist($package);
    }

    public function getTotalPurchase(\DateTimeInterface $date):float
    {
        $qb = $this->createQueryBuilder('t')
                ->select('extract(month from t.purchase_at) as months')
                ->addSelect('extract(year from t.purchase_at) as years')
                ->addSelect('sum(t.total) as total')
                ->where('extract(year from t.purchase_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.purchase_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('t.is_locked = true')
                ->groupBy('months')->addGroupBy('years');
        $query = $qb->getQuery()->useQueryCache(true)->enableResultCache(30, $this->userService->getGeneratedTokenName());
        $resultSet = $query->getOneOrNullResult();
        if($resultSet)
        {
            return (float)$resultSet['total'];
        }
        return 0;
    }
}
