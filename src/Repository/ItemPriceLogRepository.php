<?php

namespace App\Repository;

use App\Entity\ItemPriceLog;
use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\Price\Entity\PriceLogInterface;
use Kematjaya\ItemPack\Lib\Price\Repo\PriceLogRepoInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method ItemPriceLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemPriceLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemPriceLog[]    findAll()
 * @method ItemPriceLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemPriceLogRepository extends ServiceEntityRepository implements PriceLogRepoInterface
{
    use DoctrineOperation;
    
    private $tokenStorage;
    
    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, ItemPriceLog::class);
    }

    public function createPriceLog(ItemInterface $item): PriceLogInterface 
    {
        $insertedBy = ($this->tokenStorage->getToken()) ? $this->tokenStorage->getToken()->getUser()->getUsername() : 'guest';
        return (new ItemPriceLog())->setItem($item)->setInsertedBy($insertedBy)->setCreatedAt(new \DateTime());
    }

    public function getNewPriceLogByItem(ItemInterface $item): ?PriceLogInterface 
    {
        $qb = $this->createQueryBuilder('t')
                ->where('t.status = :status')->setParameter('status', PriceLogInterface::STATUS_NEW)
                ->andWhere('t.item = :item')->setParameter('item', $item)
                ->setMaxResults(1)->orderBy('t.created_at', 'DESC');
        
        //return new ArrayCollection($qb->getQuery()->getResult());
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getNewPriceLogs(): Collection 
    {
        return new ArrayCollection();
    }

    public function save(PriceLogInterface $priceLog): void 
    {
        if($priceLog->getId())
        {
            $approvedBy = ($this->tokenStorage->getToken()) ? $this->tokenStorage->getToken()->getUser()->getUsername() : 'guest';
            $priceLog->setApprovedBy($approvedBy);
            $this->doPersist($priceLog);
        } else
        {
            $this->create($priceLog);
        }
    }

    public function getListPriceLogNew():Collection
    {
        $qb = $this->createQueryBuilder('t')
                ->select('i.id')->addSelect('i.code')->addSelect('i.name')->addSelect('i.last_price')->addSelect('i.harga_pokok')
                ->addSelect('max(t.principal_price) as new_principal_price')
                ->join('t.item', 'i')
                ->where('t.status = :status')->setParameter('status', PriceLogInterface::STATUS_NEW)
                ->groupBy('i.id');
        
        return new ArrayCollection($qb->getQuery()->getResult());
    }
}
