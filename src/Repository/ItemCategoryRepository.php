<?php

namespace App\Repository;

use App\Entity\ItemCategory;
use Kematjaya\ItemPack\Lib\ItemCategory\Entity\ItemCategoryInterface;
use Kematjaya\ItemPack\Lib\ItemCategory\Repo\ItemCategoryRepoInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemCategory[]    findAll()
 * @method ItemCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemCategoryRepository extends ServiceEntityRepository implements ItemCategoryRepoInterface
{
    use DoctrineOperation;
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemCategory::class);
    }
    
    public function createItemCategory():ItemCategoryInterface
    {
        return new ItemCategory();
    }

    public function save(ItemCategoryInterface $itemPackage): void 
    {
        $this->doPersist($itemPackage);
    }

}
