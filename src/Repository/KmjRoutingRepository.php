<?php

namespace App\Repository;

use App\Entity\KmjRouting;
use App\Library\UserService;
use App\Repository\KmjRoutingRoleRepository;
use App\Library\RoutingManagement\Entity\KmjRoutingInterface;
use App\Library\RoutingManagement\Repository\KmjRoutingRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @method KmjRouting|null find($id, $lockMode = null, $lockVersion = null)
 * @method KmjRouting|null findOneBy(array $criteria, array $orderBy = null)
 * @method KmjRouting[]    findAll()
 * @method KmjRouting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KmjRoutingRepository extends ServiceEntityRepository implements KmjRoutingRepositoryInterface
{
    use DoctrineOperation;
    
    private $kmjRoutingRoleRepo, $userService;
    
    public function __construct(
            ManagerRegistry $registry, 
            KmjRoutingRoleRepository $kmjRoutingRoleRepo,
            UserService $userService)
    {
        parent::__construct($registry, KmjRouting::class);
        $this->kmjRoutingRoleRepo = $kmjRoutingRoleRepo;
        $this->userService = $userService;
    }

    public function saveCredential(KmjRoutingInterface $kmjRouting): KmjRoutingInterface
    {
        $this->doPersist($kmjRouting);
        
        return $kmjRouting;
    }

    public function createKmjRouting():KmjRoutingInterface 
    {
        return new KmjRouting();
    }
    
    public function findAllowedByRole(string $role): Collection 
    {
        $qb = $this->createQueryBuilder('t')
                ->join('t.kmjRoutingRoles', 'routingRole')
                ->where('routingRole.is_allowed = :is_allowed')->setParameter('is_allowed', true)
                ->andWhere('routingRole.role = :role')->setParameter('role', $role)
                ->addOrderBy('t.routing_group', 'ASC')->addOrderBy('t.sequence', 'ASC');
        $query = $qb->getQuery()->useQueryCache(true)->enableResultCache(60, $this->userService->getGeneratedTokenName());
        
        return new ArrayCollection($query->getResult());
    }

}
