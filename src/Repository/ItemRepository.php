<?php

namespace App\Repository;

use App\Entity\Item;
use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\Item\Repo\ItemRepoInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository implements ItemRepoInterface
{
    use DoctrineOperation;
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function createItem():ItemInterface
    {
        return new Item();
    }

    public function save(ItemInterface $item): void 
    {
        $this->doPersist($item);
    }

    public function findIdenticItem(ItemInterface $item):?ItemInterface
    {
        if($item->getId()) {
            $qb = $this->createQueryBuilder('t')
                    ->where('t.code = :code')->setParameter('code', $item->getCode())
                    ->andWhere('t.id != :id')->setParameter('id', $item->getId());
            return $qb->getQuery()->getOneOrNullResult();
        }
        
        return $this->findOneBy(['code' => $item->getCode()]);
    }
}
