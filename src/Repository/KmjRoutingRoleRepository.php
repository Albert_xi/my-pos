<?php

namespace App\Repository;

use App\Entity\KmjRoutingRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Library\RoutingManagement\Entity\KmjRoutingInterface;
use App\Library\RoutingManagement\Entity\KmjRoutingRoleInterface;
use App\Library\RoutingManagement\Repository\KmjRoutingRoleRepositoryInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @method KmjRoutingRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method KmjRoutingRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method KmjRoutingRole[]    findAll()
 * @method KmjRoutingRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KmjRoutingRoleRepository extends ServiceEntityRepository implements KmjRoutingRoleRepositoryInterface
{
    use DoctrineOperation;
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KmjRoutingRole::class);
    }

    public function updateRole(string $path, string $role, KmjRoutingInterface $kmjRouting, bool $isAllowed):KmjRoutingRoleInterface
    {
        $kmjRoutingRole = $this->findOneBy(['role' => $role, 'path' => $path, 'routing' => $kmjRouting ]);
        if(!$kmjRoutingRole)
        {
            $kmjRoutingRole = (new KmjRoutingRole())
                    ->setRole($role)->setPath($path)
                    ->setRouting($kmjRouting);
        }
        
        return $this->updateRoutingRole($kmjRoutingRole, $isAllowed);
    }
    
    public function updateRoutingRole(KmjRoutingRoleInterface $kmjRoutingRole, bool $isAllowed = false):KmjRoutingRoleInterface
    {
        $kmjRoutingRole->setIsAllowed((bool) $isAllowed);
        $kmjRoutingRole->setUpdatedAt(new \DateTime());
        
        $this->doPersist($kmjRoutingRole);
        
        return $kmjRoutingRole;
    }
    
    public function getCredentialByRole(string $role): Collection 
    {
        return new ArrayCollection($this->findBy(['role' => $role]));
    }

    public function findOneByPath(string $path): ?KmjRoutingRoleInterface 
    {
        return $this->createQueryBuilder('this')
                ->where('this.path = :path')->setParameter('path', $path)
                ->setMaxResults(1)
                ->getQuery()
                ->useQueryCache(true)->enableResultCache(60, $path)
                ->getOneOrNullResult();
    }

    public function findOneByPathAndRole(string $path, string $role): ?KmjRoutingRoleInterface 
    {
        return $this->createQueryBuilder('this')
                ->where('this.path = :path')->setParameter('path', $path)
                ->andWhere('this.role = :role')->setParameter('role', $role)
                ->setMaxResults(1)
                ->getQuery()
                ->useQueryCache(true)->enableResultCache(60, sprintf('%s_%s', $path, $role))
                ->getOneOrNullResult();
    }

}
