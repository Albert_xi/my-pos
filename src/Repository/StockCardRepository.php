<?php

namespace App\Repository;

use App\Entity\StockCard;
use Kematjaya\ItemPack\Lib\Stock\Entity\StockCardInterface;
use Kematjaya\ItemPack\Lib\Stock\Repo\StockCardRepoInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockCard[]    findAll()
 * @method StockCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockCardRepository extends ServiceEntityRepository implements StockCardRepoInterface
{
    use DoctrineOperation;
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockCard::class);
    }

    public function createStockCard(): StockCardInterface 
    {
        return new StockCard();
    }

    public function save(StockCardInterface $stockCard): void 
    {
        if($stockCard->getId())
        {
            $this->doPersist($stockCard);
        } else
        {
            $this->create($stockCard);
        }
    }
}
