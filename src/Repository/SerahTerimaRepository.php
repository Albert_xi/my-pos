<?php

namespace App\Repository;

use App\Entity\SerahTerima;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SerahTerima|null find($id, $lockMode = null, $lockVersion = null)
 * @method SerahTerima|null findOneBy(array $criteria, array $orderBy = null)
 * @method SerahTerima[]    findAll()
 * @method SerahTerima[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SerahTerimaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SerahTerima::class);
    }

    
    public function findOneByDate(\DateTimeInterface $date)
    {
        return $this->createQueryBuilder('t')
                ->andWhere('extract(year from t.created_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.created_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('extract(day from t.created_at) = :date')->setParameter('date', $date->format('d'))
                ->getQuery()->getOneOrNullResult();
    }
    
    public function findOneByMonth(\DateTimeInterface $date, $filters = array())
    {
        $qb = $this->createQueryBuilder('t');
        if(!empty($filters))
        {
            if($filters['from'])
            {
                $from = (!$filters['from'] instanceof \DateTimeInterface) ? new \DateTime($filters['from']) : $filters['from'];
                $qb->andWhere('t.created_at >= :from')->setParameter('from', $from);
            }
            if($filters['to'])
            {
                $to = (!$filters['to'] instanceof \DateTimeInterface) ? (new \DateTime($filters['to']))->setTime(23, 59) : $filters['to'];
                $qb->andWhere('t.created_at <= :to')->setParameter('to', $to);
            }
            
            return $qb->getQuery()->getResult();
        }
        
        return $qb
                ->andWhere('extract(year from t.created_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.created_at) = :month')->setParameter('month', $date->format('m'))
                ->getQuery()->getResult();
    }
}
