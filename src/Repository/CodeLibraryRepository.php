<?php

namespace App\Repository;

use App\Entity\CodeLibrary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Kematjaya\CodeManager\Repository\CodeLibraryRepositoryInterface;
use Kematjaya\CodeManager\Entity\CodeLibraryClientInterface;
use Kematjaya\CodeManager\Entity\CodeLibraryInterface;

/**
 * @method CodeLibrary|null find($id, $lockMode = null, $lockVersion = null)
 * @method CodeLibrary|null findOneBy(array $criteria, array $orderBy = null)
 * @method CodeLibrary[]    findAll()
 * @method CodeLibrary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CodeLibraryRepository extends ServiceEntityRepository implements CodeLibraryRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CodeLibrary::class);
    }

    public function findOneByClient(CodeLibraryClientInterface $client): ?CodeLibraryInterface 
    {
        return $this->findOneBy(['class_name' => get_class($client)]);
    }

    public function save(CodeLibraryInterface $object): void 
    {
        $this->_em->persist($object);
    }

}
