<?php

namespace App\Repository;

use App\Entity\Sale;
use App\Library\UserService;
use App\Report\Transaction\TransactionRepoInterface;
use Kematjaya\SaleBundle\Entity\SaleInterface;
use Kematjaya\SaleBundle\Repo\SaleRepoInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @method Sale|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sale|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sale[]    findAll()
 * @method Sale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaleRepository extends ServiceEntityRepository implements SaleRepoInterface, TransactionRepoInterface
{
    private $translator, $userService;
    
    use DoctrineOperation;
    
    public function __construct(ManagerRegistry $registry, TranslatorInterface $translator, UserService $userService)
    {
        $this->translator = $translator;
        $this->userService = $userService;
        parent::__construct($registry, Sale::class);
    }

    public function createSale(): SaleInterface 
    {
        return new Sale();
    }

    public function save(SaleInterface $sale): void 
    {
        $this->doPersist($sale);
    }

    public function createQueryBuilder($alias, $indexBy = null) 
    {
        $qb = parent::createQueryBuilder($alias, $indexBy);
        switch($this->userService->getRoleNames()->first())
        {
            case \App\Entity\KmjUser::ROLE_OPERATOR:
                $store = $this->userService->getUser()->getStore();
                if($store)
                {
                    $sql = sprintf('%s.%s = :%s', $alias, 'store', 'store');
                    $qb->where($sql)->setParameter('store', $store);
                }
                    
                break;
            default:
                break;
        }
        
        return $qb;
    }
    
    public function countTotalRow():int
    {
        $qb = $this->createQueryBuilder('t')
                ->select('count(t.id)')
                ->getQuery();
        
        return $qb->getSingleScalarResult();
    }
    
    public function getSaleChartByCategoryData($month = null, $year = null)
    {
        if(is_null($month))
        {
            $month = date('m');
        }
        
        if(is_null($year))
        {
            $year = date('Y');
        }
        
        $date = new \DateTime(sprintf('%s-%s-%s', $year, $month, 1));
        
        $qb = $this->createQueryBuilder('t')
                ->select('c.code')->addSelect('c.name')
                ->addSelect('sum(si.principal_price * si.quantity) as principal_price')
                ->addSelect('sum(si.sale_price * si.quantity) as sale_price')
                ->addSelect('sum(si.discount) + sum(t.discount) as discount')
                ->addSelect('sum(si.tax) + sum(t.tax) as tax')
                ->join('t.saleItems', 'si')->join('si.item', 'i')->join('i.category', 'c')
                ->andWhere('extract(year from t.created_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.created_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('t.is_locked = true')
                ->groupBy('c.code')->addGroupBy('c.name')
                ->orderBy('c.name');
        
        $category = [];
        $data = [
            'principal_price' => [
                'name' => $this->translator->trans('harga_pokok'),
                'colorByPoint' => true,
                'data' => []
            ],
            'sale_price' => [
                'name' => $this->translator->trans('sale_price'),
                'colorByPoint' => true,
                'data' => []
            ],
            'discount' => [
                'name' => $this->translator->trans('discount'),
                'colorByPoint' => true,
                'data' => []
            ],
            'tax' => [
                'name' => $this->translator->trans('tax'),
                'colorByPoint' => true,
                'data' => []
            ],
            'profit' => [
                'name' => $this->translator->trans('profit'),
                'colorByPoint' => true,
                'data' => []
            ]
        ];
        $query = $qb->getQuery()->useQueryCache(true)->enableResultCache(30, $this->userService->getGeneratedTokenName());
        $resultSet = $query->getResult();
        foreach($resultSet as $k => $value)
        {
            $category[] = sprintf('%s - %s', $value['code'], $value['name']);
            $data['principal_price']['data'][] = (float)$value['principal_price'];
            $data['sale_price']['data'][] = (float)$value['sale_price'];
            $data['discount']['data'][] = (float)$value['discount'];
            $data['tax']['data'][] = (float)$value['tax'];
            $data['profit']['data'][] = ((float)$value['sale_price'] - (float)$value['principal_price']) - (float)$value['discount'];
        }
        
        return [
            'title' => sprintf('%s %s %s: %s', $this->translator->trans('sale_recapitulation'), $this->translator->trans('by'), $this->translator->trans('category'), $date->format('M Y')),
            'params' => [
                'month' => $date->format('M'), 'year' => $date->format('Y')
            ],
            'category' => $category,
            'series' => array_values($data)
        ];
    }
    
    public function getSaleChartByDateData($month = null, $year = null)
    {
        if(is_null($month))
        {
            $month = date('m');
        }
        
        if(is_null($year))
        {
            $year = date('Y');
        }
        
        $date = new \DateTime(sprintf('%s-%s-%s', $year, $month, 1));
        
        $qb = $this->createQueryBuilder('t')
                ->select('extract(day from t.created_at) as days')
                ->addSelect('extract(month from t.created_at) as months')
                ->addSelect('extract(year from t.created_at) as years')
                ->addSelect('sum(t.total) as total')
                ->andWhere('extract(year from t.created_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.created_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('t.is_locked = true')
                ->groupBy('days')->addGroupBy('months')->addGroupBy('years')
                ->orderBy('days');
        
        $category = [];
        $data = [
            'total' => [
                'name' => $this->translator->trans('total'),
                'colorByPoint' => true,
                'data' => []
            ]
        ];
        
        $query = $qb->getQuery()->useQueryCache(true)->enableResultCache(30, $this->userService->getGeneratedTokenName());
        $resultSet = $query->getResult();
        foreach($resultSet as $k => $value)
        {
            $category[] = sprintf('%s/%s/%s', $value['days'], $date->format('M'), $date->format('Y'));
            $data['total']['data'][] = (float)$value['total'];
        }
        
        return [
            'title' => sprintf('%s %s %s: %s', $this->translator->trans('sale_recapitulation'), $this->translator->trans('by'), $this->translator->trans('date'), $date->format('M Y')),
            'params' => [
                'month' => $date->format('M'), 'year' => $date->format('Y')
            ],
            'category' => $category,
            'series' => array_values($data)
        ];
    }
    
    public function getTotalIncome(\DateTimeInterface $date, $column =  null):float
    {
        $qb = $this->createQueryBuilder('t')
                ->select('extract(month from t.created_at) as months')
                ->addSelect('extract(year from t.created_at) as years')
                ->addSelect('sum(si.principal_price * si.quantity) as principal_price')
                ->addSelect('sum(si.sale_price * si.quantity) as sale_price')
                ->addSelect('sum(si.discount) + sum(t.discount) as discount')
                ->addSelect('sum(si.tax) + sum(t.tax) as tax')
                ->join('t.saleItems', 'si')
                ->andWhere('extract(year from t.created_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.created_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('t.is_locked = true')
                ->groupBy('months')->addGroupBy('years');
        $query = $qb->getQuery()->useQueryCache(true)->enableResultCache(30, $this->userService->getGeneratedTokenName());
        $resultSet = $query->getOneOrNullResult();
        
        switch($column)
        {
            case 'profit':
                return ($resultSet) ? ((float)$resultSet['sale_price'] - (float)$resultSet['principal_price'] - (float)$resultSet['discount']) + (float)$resultSet['tax'] : 0;
                break;
            case 'sale':
                return ($resultSet) ? (float)$resultSet['sale_price']:0;
                break;
            default:
                return $resultSet;
                break;
        }
        
    }

    public function findTransactionUncheckedByDate(\DateTimeInterface $date):array
    {
        $qb = $this->createQueryBuilder('t')
                ->andWhere('extract(year from t.created_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.created_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('extract(day from t.created_at) = :date')->setParameter('date', $date->format('d'))
                ->andWhere('t.is_locked = true')->andWhere('t.checked_at is null');
        
        $resultSet = $qb->getQuery()->getResult();
        
        $qb = $this->getEntityManager()->getRepository(\App\Entity\Purchase::class)->createQueryBuilder('t')
                ->andWhere('extract(year from t.purchase_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.purchase_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('extract(day from t.purchase_at) = :date')->setParameter('date', $date->format('d'))
                ->andWhere('t.is_locked = true')->andWhere('t.checked_at is null');
        
        return array_merge($resultSet, $qb->getQuery()->getResult());
    }
    
    public function findTransactionByDate(\DateTimeInterface $date): array 
    {
        $qb = $this->createQueryBuilder('t')
                ->andWhere('extract(year from t.created_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.created_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('extract(day from t.created_at) = :date')->setParameter('date', $date->format('d'))
                ->andWhere('t.is_locked = true')->andWhere('t.checked_at is null');
        
        $resultSet = $qb->getQuery()->getResult();
        
        $qb = $this->getEntityManager()->getRepository(\App\Entity\Purchase::class)->createQueryBuilder('t')
                ->andWhere('extract(year from t.purchase_at) = :year')->setParameter('year', $date->format('Y'))
                ->andWhere('extract(month from t.purchase_at) = :month')->setParameter('month', $date->format('m'))
                ->andWhere('extract(day from t.purchase_at) = :date')->setParameter('date', $date->format('d'))
                ->andWhere('t.is_locked = true')->andWhere('t.checked_at is null');
        
        return array_merge($resultSet, $qb->getQuery()->getResult());
    }

    public function findTransactionsCheckedByDate(\DateTimeInterface $date):array
    {
        $qb = $this->createQueryBuilder('t')
                ->andWhere('t.is_locked = true')
                ->andWhere('t.checked_at = :checked_at')->setParameter('checked_at', $date);
        $query = $qb->getQuery()->useQueryCache(true)->enableResultCache(30, $this->userService->getGeneratedTokenName());
        $resultSet = $query->getResult();
        
        $qb = $this->getEntityManager()->getRepository(\App\Entity\Purchase::class)->createQueryBuilder('t')
                ->andWhere('t.is_locked = true')
                ->andWhere('t.checked_at = :checked_at')->setParameter('checked_at', $date);
        
        $query = $qb->getQuery()->useQueryCache(true)->enableResultCache(30, $this->userService->getGeneratedTokenName());
        return array_merge($resultSet, $query->getResult());
    }
}
