<?php

namespace App\Repository;

use App\Entity\KmjUser;
use Kematjaya\UserBundle\Entity\KmjUserInterface;
use Kematjaya\UserBundle\Repo\KmjUserRepoInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method KmjUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method KmjUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method KmjUser[]    findAll()
 * @method KmjUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KmjUserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface, KmjUserRepoInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KmjUser::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof KmjUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findOneByUsernameAndActive(string $username): ?KmjUserInterface 
    {
        return $this->findOneBy(['username' => $username, 'is_active' => true]);
    }

    public function createUser(): KmjUserInterface 
    {
        return new KmjUser();
    }

    public function findOneByIdentityNumber(string $identityNumber): ?KmjUserInterface 
    {
        return $this->find($identityNumber);
    }

}
