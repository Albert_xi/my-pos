<?php

namespace App\Repository;

use App\Entity\BackupLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Library\Database\Log\OperationLogInterface;
use App\Library\Database\Log\OperationLogRepositoryInterface;

/**
 * @method BackupLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method BackupLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method BackupLog[]    findAll()
 * @method BackupLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BackupLogRepository extends ServiceEntityRepository implements OperationLogRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BackupLog::class);
    }

    public function createObject(): OperationLogInterface 
    {
        return new BackupLog();
    }

    public function save(OperationLogInterface $entity): void 
    {
        $this->_em->beginTransaction();
        
        try{
            $this->_em->persist($entity);

            $this->_em->flush();
            
            $this->_em->commit();
            
        } catch (\Exception $ex) {
            $this->_em->rollback();
            
            throw $ex;
        }
    }
    
    public function findLastBackup(): ?OperationLogInterface 
    {
        return $this->findOneBy(['type' => OperationLogInterface::TYPE_BACKUP], ['created_at' => 'desc']);
    }

}
