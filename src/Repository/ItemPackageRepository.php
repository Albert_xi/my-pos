<?php

namespace App\Repository;

use App\Entity\ItemPackage;
use Kematjaya\ItemPack\Lib\Item\Entity\ItemInterface;
use Kematjaya\ItemPack\Lib\ItemPackaging\Entity\ItemPackageInterface;
use Kematjaya\ItemPack\Lib\ItemPackaging\Repo\ItemPackageRepoInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ItemPackage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemPackage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemPackage[]    findAll()
 * @method ItemPackage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemPackageRepository extends ServiceEntityRepository implements ItemPackageRepoInterface
{
    
    use DoctrineOperation;
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemPackage::class);
    }

    public function createPackage(ItemInterface $item): ItemPackageInterface 
    {
        return (new ItemPackage())->setItem($item);
    }

    public function save(ItemPackageInterface $itemPackage): void 
    {
        $this->doPersist($itemPackage);
    }

    public function findSmallestUnitByItem(ItemInterface $item): ?ItemPackageInterface 
    {
        return $this->findOneBy(['is_smallest_unit' => true, 'item' => $item]);
    }

}
