<?php

namespace App\Form;

use App\Entity\Item;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Kematjaya\Currency\Type\PriceType;
use Kematjaya\ItemPack\FormSubscriber\ItemEventSubscriber;

class ItemType extends AbstractType
{
    
    /**
     * 
     * @var ItemEventSubscriber
     */
    private $itemEventSubscriber;
    
    public function __construct(ItemEventSubscriber $itemEventSubscriber) {
        $this->itemEventSubscriber = $itemEventSubscriber;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, [
                'label' => 'code',
                'attr' => ['class' => 'form-control']
            ])
            ->add('name', null, [
                'label' => 'name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('use_barcode', HiddenType::class, [
                'label' => 'use_barcode'
            ])
            ->add('last_stock', null, [
                'label' => 'last_stock',
                'attr' => ['class' => 'form-control']
            ])
            ->add('harga_pokok', PriceType::class, [
                'label' => 'harga_pokok',
                'attr' => ['class' => 'form-control priceformat']
            ])
            ->add('last_price', PriceType::class, [
                'label' => 'last_price',
                'attr' => ['class' => 'form-control priceformat']
            ])
            ->add('category', null, [
                'label' => 'category',
                'attr' => ['class' => 'form-control']
            ])
        ;
        
        $builder->addEventSubscriber($this->itemEventSubscriber);
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
        ]);
    }
}
