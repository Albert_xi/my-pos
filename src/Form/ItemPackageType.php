<?php

namespace App\Form;

use App\Entity\ItemPackage;
use App\Repository\ItemPackageRepository;
use Kematjaya\HiddenTypeBundle\Type\HiddenEntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Kematjaya\ItemPack\FormSubscriber\ItemPackageFormSubscriber;

class ItemPackageType extends AbstractType
{
    private $itemPackageRepo, $itemPackageFormSubscriber;
    
    public function __construct(ItemPackageRepository $itemPackageRepo, ItemPackageFormSubscriber $itemPackageFormSubscriber) 
    {
        $this->itemPackageRepo = $itemPackageRepo;
        $this->itemPackageFormSubscriber = $itemPackageFormSubscriber;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('packaging', null, [
                'label' => 'packaging',
                'attr' => ['class' => 'form-control']
            ])
            ->add('is_smallest_unit')
            ->add('quantity', NumberType::class, [
                'label' => 'quantity', 'html5' => true,
                'attr' => ['class' => 'form-control', 'min' => 1]
            ])
            ->add('principal_price', HiddenType::class)
            ->add('sale_price', HiddenType::class)
            ->add('item', HiddenEntityType::class, [
                'class' => \App\Entity\Item::class
            ])
        ;
        
        $builder->addEventSubscriber($this->itemPackageFormSubscriber);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ItemPackage::class,
        ]);
    }
}
