<?php

namespace App\Form;

use App\Entity\KmjPrinterClient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Kematjaya\HiddenTypeBundle\Type\HiddenEntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class KmjPrinterClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kmj_user', HiddenEntityType::class, [
                'class' => \App\Entity\KmjUser::class
            ])
            ->add('computer_name', null, [
                'label' => 'computer_name'
            ])
            ->add('printer_name', null, [
                'label' => 'printer_name'
            ])
            ->add('is_default', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KmjPrinterClient::class,
        ]);
    }
}
