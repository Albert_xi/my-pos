<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Repository\KmjPrinterClientRepository;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SelectPrinterType extends AbstractType
{
    protected $kmjPrinterClientRepo;
    
    public function __construct(KmjPrinterClientRepository $kmjPrinterClientRepo) 
    {
        $this->kmjPrinterClientRepo = $kmjPrinterClientRepo;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('computer_name', EntityType::class, [
            'class' => \App\Entity\KmjPrinterClient::class, 'required' => true,
            'choices' => $this->kmjPrinterClientRepo->findByUser(), 'label' => 'select_printer'
        ]);
    }
}
