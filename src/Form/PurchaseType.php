<?php

namespace App\Form;

use App\Entity\Purchase;
use Kematjaya\Currency\Type\PriceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Kematjaya\PurchashingBundle\EventSubscriber\PurchaseFormEventSubscriber;

class PurchaseType extends AbstractType
{
    private $purchaseFormEventSubscriber;
    
    public function __construct(PurchaseFormEventSubscriber $purchaseFormEventSubscriber) 
    {
        $this->purchaseFormEventSubscriber = $purchaseFormEventSubscriber;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, [
                'label' => 'code',
                'attr' => ['class' => 'form-control']
            ])
            ->add('supplier')
            ->add('purchase_at', null, [
                'label' => 'purchase_at',
                'widget' => 'single_text',
                'attr' => ['class' => 'form-control']
            ])
            ->add('total', PriceType::class, [
                'label' => 'total',
                'attr' => ['class' => 'form-control priceformat', "readonly" => true]
            ])
            ->add('description', null, [
                'label' => 'description',
                'attr' => ['class' => 'form-control']
            ])
            ->add('is_locked', null, [
                'label' => 'is_locked'
            ])
        ;
        $builder->addEventSubscriber($this->purchaseFormEventSubscriber);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Purchase::class,
        ]);
    }
}
