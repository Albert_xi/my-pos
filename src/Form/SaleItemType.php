<?php

namespace App\Form;

use App\Entity\SaleItem;
use Kematjaya\SaleBundle\EventSubscriber\SaleItemFormEventSubscriber;
use Kematjaya\Currency\Type\PriceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Kematjaya\HiddenTypeBundle\Type\HiddenEntityType;

class SaleItemType extends AbstractType
{
    private $saleItemFormEventSubscriber;
    
    public function __construct(SaleItemFormEventSubscriber $saleItemFormEventSubscriber) 
    {
        $this->saleItemFormEventSubscriber = $saleItemFormEventSubscriber;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', null, [
                'label' => 'item',
                'required' => false])
            ->add('quantity', NumberType::class, [
                'label' => 'quantity', 'html5' => true
            ])
            ->add('sale_price', PriceType::class, [
                'label' => 'sale_price',
                'attr' => ['class' => 'priceformat', 'readonly' => true]
            ])
            ->add('sub_total', PriceType::class, [
                'label' => 'sub_total',
                'attr' => ['class' => 'priceformat', 'readonly' => true]
            ])
            ->add('tax', PriceType::class, [
                'label' => 'tax',
                'attr' => ['class' => 'priceformat']
            ])
            ->add('total', PriceType::class, [
                'label' => 'total',
                'attr' => ['class' => 'priceformat', 'readonly' => true]
            ])
            ->add('sale', HiddenEntityType::class, ['class' => \App\Entity\Sale::class])
        ;
        
        $builder->addEventSubscriber($this->saleItemFormEventSubscriber);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SaleItem::class,
        ]);
    }
}
