<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null,[
                'label' => 'code'
            ])
            ->add('name', null, [
                'label' => 'name'
            ])
            ->add('address', null, [
                'label' => 'address'
            ])
            ->add('phone', null, [
                'label' => 'phone'
            ])
            ->add('mail', EmailType::class, [
                'label' => 'mail'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
