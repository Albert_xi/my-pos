<?php

namespace App\Form;

use App\Entity\ItemPriceLog;
use Kematjaya\Currency\Type\PriceType;
use Kematjaya\HiddenTypeBundle\Type\HiddenDateTimeType;
use Kematjaya\HiddenTypeBundle\Type\HiddenEntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemPriceLogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created_at', HiddenDateTimeType::class)
            ->add('principal_price_old', PriceType::class,[
                'label' => 'principal_price_old',
                'attr' => ['readonly' => true, 'class' => 'priceformat']
            ])
            ->add('principal_price', PriceType::class, [
                'label' => 'principal_price',
                'attr' => ['readonly' => true, 'class' => 'priceformat']
            ])
            ->add('sale_price_old', PriceType::class, [
                'label' => 'sale_price_old',
                'attr' => ['readonly' => true, 'class' => 'priceformat']
            ])
            ->add('sale_price', PriceType::class, [
                'label' => 'sale_price',
                'attr' => ['class' => 'priceformat']
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'status',
                'choices' => array_flip(ItemPriceLog::getArrayStatus())
            ])
            ->add('inserted_by', HiddenType::class)
            ->add('approved_by', HiddenType::class)
            ->add('item', HiddenEntityType::class, [
                'class' => \App\Entity\Item::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ItemPriceLog::class,
        ]);
    }
}
