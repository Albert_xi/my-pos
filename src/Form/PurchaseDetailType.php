<?php

namespace App\Form;

use App\Entity\Purchase;
use App\Entity\PurchaseDetail;
use Kematjaya\PurchashingBundle\EventSubscriber\ItemPostEventSubscriber;
use Kematjaya\PurchashingBundle\EventSubscriber\PurchaseDetailEventSubscriber;
use Kematjaya\HiddenTypeBundle\Type\HiddenEntityType;
use Kematjaya\Currency\Type\PriceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PurchaseDetailType extends AbstractType
{
    private $itemPostEventSubscriber, $purchaseDetailEventSubscriber;
    
    public function __construct(ItemPostEventSubscriber $itemPostEventSubscriber, PurchaseDetailEventSubscriber $purchaseDetailEventSubscriber) 
    {
        $this->itemPostEventSubscriber = $itemPostEventSubscriber;
        $this->purchaseDetailEventSubscriber = $purchaseDetailEventSubscriber;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('purchase', HiddenEntityType::class, [
                'class' => Purchase::class
            ])
            ->add('item', null, [
                'label' => 'item'
            ])
            ->add('packaging', null, [
                'label' => 'packaging'
            ])
            ->add('quantity', null, [
                'label' => 'quantity'
            ])
            ->add('price', null, [
                'label' => 'price'
            ])
            ->add('tax', PriceType::class, [
                'label' => 'tax'
            ])
            ->add('total')
        ;
        
        $builder->get('item')->addEventSubscriber($this->itemPostEventSubscriber);
        $builder->addEventSubscriber($this->purchaseDetailEventSubscriber);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PurchaseDetail::class,
        ]);
    }
}
