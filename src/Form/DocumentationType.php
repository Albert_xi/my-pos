<?php

namespace App\Form;

use App\Entity\Documentation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Kematjaya\SaleBundle\Form\Type\HiddenDateTimeType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Routing\RouterInterface;

class DocumentationType extends AbstractType
{
    private $router;
    
    public function __construct(RouterInterface $router) {
        $this->router = $router;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('topic', null, [
                'label' => 'topic'
            ])
            ->add('title', null, [
                'label' => 'title'
            ])
            ->add('created_at', HiddenDateTimeType::class)
            ->add('content', CKEditorType::class, [
                'label' => 'content',
                'config' => [
                    'filebrowserImageUploadUrl' => $this->router->generate('app_upload_images'),
                    'filebrowserUploadMethod' => 'form',
                    'extraPlugins' => 'codesnippet'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Documentation::class,
        ]);
    }
}
