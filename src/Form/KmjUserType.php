<?php

namespace App\Form;

use App\Entity\KmjUser;
use Kematjaya\UserBundle\Entity\KmjUserInterface;
use Kematjaya\UserBundle\Repo\KmjUserRepoInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class KmjUserType extends AbstractType
{
    
    private $encoderFactory;
    
    /**
     * 
     * @var array
     */
    private $roleList = [];
    
    /**
     * 
     * @var KmjUserRepoInterface
     */
    private $repo;
    
    public function __construct(
            TokenStorageInterface $token, 
            EncoderFactoryInterface $encoderFactory, 
            RoleHierarchyInterface $roleHierarchy, 
            KmjUserRepoInterface $repo) 
    {
        $this->encoderFactory = $encoderFactory;
        $this->repo = $repo;
        foreach($roleHierarchy->getReachableRoleNames($token->getToken()->getRoleNames()) as $role) {
            $this->roleList[$role] = $role;
        }
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('roles', ChoiceType::class, [
                'choices' => $this->roleList,
                'multiple'  => true
            ])
            ->add('name')
            ->add('store')
            ->add('is_active')
        ;
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){
            $data = $event->getData();
            $form = $event->getForm();
            if (!$data || null === $data->getId()) 
            {
                $form->add('password', PasswordType::class, array(
                    'label' => 'password'
                ));
            }
        });
        
        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event){
            $data = $event->getData();
            if(!$data instanceof KmjUserInterface) {
                return;
            }
            
            if($data->getId()) {
                return;
            }
            
            $userExist = $this->repo->findOneByUsernameAndActive($data->getUsername());
            if($userExist) {
                $event->getForm()->addError(new FormError(sprintf('username "%s" already exist', $data->getUsername())));
            }

            $encoder = $this->encoderFactory->getEncoder($data);
            $password = $encoder->encodePassword( $data->getPassword(), $data->getUsername());
            $data->setPassword($password);
            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KmjUser::class,
        ]);
    }
}
