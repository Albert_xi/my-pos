<?php

namespace App\Form;

use App\Entity\ItemCategory;
use Kematjaya\ItemPack\FormSubscriber\ItemCategoryEventSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, [
                'label' => 'code',
                'attr' => ['class' => 'form-control']
            ])
            ->add('name', null, [
                'label' => 'name',
                'attr' => ['class' => 'form-control']
            ])
            ->add('is_active', null, [
                'label' => 'is_active'
            ])
        ;
        
        $builder->addEventSubscriber(new ItemCategoryEventSubscriber());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ItemCategory::class,
        ]);
    }
}
