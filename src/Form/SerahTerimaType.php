<?php

namespace App\Form;

use App\Entity\SerahTerima;
use App\Entity\KmjUser;
use Kematjaya\Currency\Type\PriceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Kematjaya\HiddenTypeBundle\Type\HiddenDateTimeType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SerahTerimaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created_at',HiddenDateTimeType::class)
            ->add('total_debt', PriceType::class)
            ->add('total_credit', PriceType::class)
            ->add('description')
            ->add('delegated_by', ChoiceType::class, [
                'choices' => []
            ])
            ->add('approved_by',HiddenType::class)
        ;
        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event){
            $form = $event->getForm();
            $form->add('delegated_by', EntityType::class, [
                'class' => KmjUser::class,
                'query_builder' => function (\Doctrine\ORM\EntityRepository $repo) {
                    $qb = $repo->createQueryBuilder('q');
                    
                    $qb->where(
                        $qb->expr()->like('TEXT(q.roles)', $qb->expr()->literal("%".KmjUser::ROLE_OPERATOR."%"))
                    );
                    
                    return $qb;
                },
                'choice_value' => function (?KmjUser $kmjUser){
                    return ($kmjUser) ? $kmjUser->getUsername() : null;
                }
            ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SerahTerima::class,
        ]);
    }
}
