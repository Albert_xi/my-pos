<?php

namespace App\Form;

use App\Entity\Store;
use App\Repository\StoreRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class StoreType extends AbstractType
{
    private $storeRepo, $trans;
    
    public function __construct(StoreRepository $storeRepo, TranslatorInterface $trans) 
    {
        $this->storeRepo = $storeRepo;
        $this->trans = $trans;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', null, [
                'label' => 'code'
            ])
            ->add('name', null, [
                'label' => 'name'
            ])
            ->add('address', null, [
                'label' => 'address'
            ])
            ->add('is_default', CheckboxType::class, [
                'label' => 'is_default', 'required' => false
            ])
        ;
        
        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event){
            $data = $event->getData();
            if (!$data->getIsDefault()) {
                
                return;
            }
             
            $defaultStore = $this->storeRepo->findDefaultStore();
            if (!$defaultStore) {
                
                return;
            }
            
            if (!$data->getId()) {
                
                $event->getForm()->addError(new FormError($this->trans->trans('is_default_exist')));
                return;
            }
            
            if ($defaultStore->getId() !== $event->getData()->getId()) {
                $event->getForm()->addError(new FormError($this->trans->trans('is_default_exist')));
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Store::class,
        ]);
    }
}
