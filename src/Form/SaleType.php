<?php

namespace App\Form;

use App\Entity\Sale;
use App\Entity\Store;
use App\Entity\Customer;
use App\Library\UserService;
use Kematjaya\SaleBundle\EventSubscriber\SaleFormEventSubscriber;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Kematjaya\HiddenTypeBundle\Type\HiddenEntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class SaleType extends AbstractType
{
    private $saleFormEventSubscriber, $userService;
    
    public function __construct(SaleFormEventSubscriber $saleFormEventSubscriber, UserService $userService) 
    {
        $this->saleFormEventSubscriber = $saleFormEventSubscriber;
        $this->userService = $userService;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code', null, [
                    'label' => 'code', 'required' => false,
                    'attr' => ['readonly' => true]
                ])
                ->add('customer', null, [
                    'label' => 'customer'
                ])
                ->add('created_by')
                ->add('created_at')
                ->add('sub_total', null, [
                    'label' => 'sub_total',
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('tax', null, [
                    'label' => 'tax',
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('discount', null, [
                    'label' => 'discount', 
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('payment', null, [
                    'label' => 'payment',
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('payment_change', null, [
                    'label' => 'payment_change',
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('total', null, [
                    'label' => 'total',
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('is_locked', null, [
                    'label' => 'is_locked'
                ]);
        
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event){
            $kmjUser = $this->userService->getUser();
            $form = $event->getForm();
            $data = $event->getData();
            if(!$data->isPaid() and $data->getIsLocked())
            {
                $form->add('store', HiddenEntityType::class, [
                    'class' => Store::class
                ])->add('customer', HiddenEntityType::class, [
                    'class' => Customer::class
                ]);
            }else {
                $form->add('store', EntityType::class, [
                    'class' => \App\Entity\Store::class,
                    'query_builder' => function (EntityRepository $repo) use ($kmjUser) {
                        $queryBuilder = $repo->createQueryBuilder('t');

                        if($kmjUser->isOperator()) {
                            $queryBuilder->where('t.id = :id')->setParameter('id', $kmjUser->getStore()->getId());
                        }

                        return $queryBuilder;
                    },
                    'attr' => ['readonly' => (bool)$event->getData()->getIsLocked()]
                ]);
            }
                
        });
        
        $builder->addEventSubscriber($this->saleFormEventSubscriber);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sale::class,
        ]);
    }
}
