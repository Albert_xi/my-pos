<?php

namespace App\Library;

use App\Entity\KmjUser;
use App\Repository\KmjUserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Ramsey\Uuid\Uuid;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class UserService 
{
    private $tokenStorage, $kmjUserRepo;
    
    public function __construct(TokenStorageInterface $tokenStorage, KmjUserRepository $kmjUserRepo) 
    {
        $this->tokenStorage = $tokenStorage;
        $this->kmjUserRepo = $kmjUserRepo;
    }
    
    public function getUser():KmjUser
    {
        $kmjUser = ($this->getToken()) ? $this->getToken()->getUser() : null;
        if(!$kmjUser)
        {
            $kmjUser = $this->kmjUserRepo->findOneBy(['username' => 'root']);
            if(!$kmjUser)
            {
                throw new \Exception('user not found');
            }
        }
        
        return $kmjUser;
    }
    
    public function getToken():?TokenInterface
    {
        return $this->tokenStorage->getToken();
    }
    
    public function getRoleNames():Collection
    {
        return $this->getToken() ? new ArrayCollection($this->getToken()->getRoleNames()) : new ArrayCollection();
    }
    
    public function getGeneratedTokenName():string
    {
        return (string) Uuid::fromString(md5($this->getRoleNames()->first()));
    }
    
    public function generateUserToken()
    {
        $token = sprintf('%s_%s', $this->getGeneratedTokenName(), md5(rand()));
        $this->session->set($this->getGeneratedTokenName(), $token);
        return $token;
    }
    
    public function getGeneratedToken():string
    {
        return $this->session->get($this->getGeneratedTokenName());
    }
    
    public function tokenExist(string $token)
    {
        return $this->session->get($this->getGeneratedTokenName()) === $token;
    }
}
