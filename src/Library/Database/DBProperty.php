<?php

namespace App\Library\Database;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class DBProperty 
{
    public function all():array
    {
        $databaseURL                    = $_ENV['DATABASE_URL'];
        list($driver, $URL)             = explode('://', $databaseURL);
        list($connection, $property)    = explode('/', $URL);
        list($userProperty, $networkProperty)   = explode('@', $connection);
        list($username, $password)  = explode(':', $userProperty);
        list($host, $port)          = explode(':', $networkProperty);
        list($dbname, $property)    = explode('?', $property);
        $properties                 = explode('&', $property);
        
        return [
            'host' => $host,
            'username' => $username,
            'password' => $password,
            'dbname' => $dbname,
            'charset' => 'utf8',
            'port' => $port,
        ];
    }
}
