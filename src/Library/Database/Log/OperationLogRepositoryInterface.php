<?php

namespace App\Library\Database\Log;

use App\Library\Database\Log\OperationLogInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface OperationLogRepositoryInterface 
{
    public function createObject():OperationLogInterface;
    
    public function save(OperationLogInterface $entity):void;
    
    public function findLastBackup():?OperationLogInterface;
}
