<?php

namespace App\Library\Database\Log;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface OperationLogInterface 
{
    const TYPE_BACKUP = 'backup';
    const TYPE_RESTORE = 'restore';
    
    public function getCreatedAt(): ?\DateTimeInterface;

    public function setCreatedAt(\DateTimeInterface $created_at): self;

    public function getFilename(): ?string;

    public function setFilename(string $filename): self;

    public function getType(): ?string;

    public function setType(string $type): self;
}
