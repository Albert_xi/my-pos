<?php

namespace App\Library\Database\Backup;

use App\Library\Database\Log\OperationLogInterface;
use App\Library\Database\Log\OperationLogRepositoryInterface;
use App\Library\Database\DBProperty;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Spatie\DbDumper\Databases\PostgreSql;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class Backup implements BackupInterface
{
    private $dbProperty;
    
    private $backupPath;
    
    private $operationLogRepo;
    
    public function __construct(
            ContainerBagInterface $containerBag, OperationLogRepositoryInterface $operationLogRepo) 
    {
        $this->dbProperty = (new DBProperty())->all();
        $this->backupPath = $containerBag->get('kernel.project_dir') . DIRECTORY_SEPARATOR . 'backup';
        $this->operationLogRepo = $operationLogRepo;
    }
    
    public function run(): bool 
    {
        try{
            $fileSystem = new Filesystem();

            if(!is_dir($this->backupPath))
            {
                $fileSystem->mkdir($this->backupPath);
            }

            $dumper = PostgreSql::create()
                    ->setHost($this->dbProperty['host'])
                    ->setDbName($this->dbProperty['dbname'])
                    ->setPort($this->dbProperty['port'])
                    ->setUserName($this->dbProperty['username'])
                    ->setPassword($this->dbProperty['password']);
            $dumpPath = $this->backupPath . DIRECTORY_SEPARATOR . date('Y-m-d');
            if(!is_dir($dumpPath))
            {
                $fileSystem->mkdir($dumpPath);
            }
            
            $fileName = sprintf($dumpPath . DIRECTORY_SEPARATOR . 'dump_%s.sql', date('Ymd_H'));
            
            $dumper->dumpToFile($fileName);
            
            $this->saveLog($fileName);
        
        } catch (\Exception $ex) {
            throw $ex;
        }
        
        return true;
    }
    
    protected function saveLog(string $fileName)
    {
        $log = $this->operationLogRepo->createObject();
        $log->setCreatedAt(new \DateTime())
                ->setFilename($fileName)
                ->setType(\App\Library\Database\Log\OperationLogInterface::TYPE_BACKUP);
        
        $this->operationLogRepo->save($log);
    }
    
    public function getLastBackup():?OperationLogInterface
    {
        return $this->operationLogRepo->findLastBackup();
    }
}
