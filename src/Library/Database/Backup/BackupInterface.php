<?php

namespace App\Library\Database\Backup;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface BackupInterface 
{
    public function run():bool;
}
