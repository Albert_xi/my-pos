<?php

namespace App\Library\CodeGenerator\Generator;

use App\Repository\SaleRepository;
use Kematjaya\CodeGenerator\Generator\Generator;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleGenerator extends Generator
{
    private $saleRepo;
    
    public function __construct(SaleRepository $saleRepo) 
    {
        $this->saleRepo = $saleRepo;
    }
    
    public function getLengthNumber(): int 
    {
        return 4;
    }
    
    public function getLastNumber(): int
    {
        return $this->saleRepo->countTotalRow();
    }
}
