<?php

namespace App\Library\CodeGenerator\Event;

use Kematjaya\CodeGenerator\Entity\ClientInterface;
use Kematjaya\CodeGenerator\Service\GeneratorService;
use App\Library\CodeGenerator\GeneratorFactory;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class CodeGeneratorEvent implements EventSubscriber
{
    private $generatorService, $generatorFactory;
    
    public function __construct(GeneratorService $generatorService, GeneratorFactory $generatorFactory) 
    {
        $this->generatorService = $generatorService;
        $this->generatorFactory = $generatorFactory;
    }
    
    public function getSubscribedEvents() 
    {
        return [
            Events::onFlush
        ];
    }
    
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        
        foreach ($uow->getScheduledEntityInsertions() as $entity) 
        {
            if($entity instanceof ClientInterface) 
            {
                $this->setCode($entity, $em);
            }
        }
        
        foreach ($uow->getScheduledEntityUpdates() as $entity) 
        {
            if($entity instanceof ClientInterface) 
            {
                $this->setCode($entity, $em);
            }
        }
    }
    
    private function setCode(ClientInterface $entity, EntityManagerInterface $entityManager)
    {
        $uow = $entityManager->getUnitOfWork();
        if(!$entity->getGeneratedCode())
        {
            $generator = $this->generatorFactory->getCodeGenerator($entity);
            if($generator)
            {
                $this->generatorService = $this->generatorService->setGenerator($generator);
            }
            
            $code = $this->generatorService->setClient($entity)->generate();
            $entity->setGeneratedCode($code);
            
            $entityChangeSet    = $uow->getEntityChangeSet($entity);
            $uow->computeChangeSet(
                $entityManager->getClassMetadata(get_class($entity)),
                $entity
            );
            $entityChangeSetNew  = $uow->getEntityChangeSet($entity);
            if(!empty($entityChangeSetNew))
            {
                foreach($entityChangeSetNew as $key => $value)
                {
                    $entityChangeSet[$key] = $value;
                }

                $uow->clearEntityChangeSet(spl_object_hash($entity));
            }
            
            foreach($entityChangeSet as $attribute => $value)
            {
                $uow->propertyChanged($entity, $attribute, $value[0], $value[1]);
            }
            
            $entityManager->persist($entity);
        }
    }
}
