<?php

namespace App\Library\CodeGenerator;

use App\Entity\Sale;
use App\Library\CodeGenerator\Generator\SaleGenerator;
use Kematjaya\CodeGenerator\Entity\ClientInterface;
use Kematjaya\CodeGenerator\Generator\GeneratorInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class GeneratorFactory 
{
    private $saleGenerator;
    
    public function __construct(SaleGenerator $saleGenerator) 
    {
        $this->saleGenerator = $saleGenerator;
    }
    
    public function getCodeGenerator(ClientInterface $client):?GeneratorInterface
    {
        switch(true)
        {
            case $client instanceof Sale:
                return $this->saleGenerator;
                break;
            default:
                break;
        }
        
        return null;
    }
}
