<?php

namespace App\Library\RoutingManagement\Twig;

use App\Library\UserService;
use App\Library\RoutingManagement\RoutingService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class UrlExtension extends AbstractExtension 
{
    private $routingService, $generator, $userService, $security;
    
    private $access = [];
    
    public function __construct(
            RoutingService $routingService,
            UrlGeneratorInterface $generator,
            UserService $userService,
            Security $security
        ) 
    {
        $this->routingService = $routingService;
        $this->generator = $generator;
        $this->userService = $userService;
        $this->security = $security;
    }
    
    public function getFunctions()
    {
        return [
            new TwigFunction('link_to', [$this, 'linkTo'], ['is_safe' => ['html']]),
            new TwigFunction('submit_tag', [$this, 'submitTag'], ['is_safe' => ['html']])
        ];
    }
    
    private function checkAccess($name)
    {
        if(!isset($this->access[$name]))
        {
            $this->access[$name] = true;
            foreach($this->userService->getToken()->getRoleNames() as $role)
            {
                $this->access[$name] = $this->routingService->pathIsAllowedByRole($name, $role);
            }
        }
        
        return $this->access;
    }
    
    public function linkTo(string $name, $parameters = [], $htmlParameters = [], $label = null, bool $relative = false)
    {
        if(!$this->isGranted($parameters))
        {
            return '';
        }
        
        $this->checkAccess($name);
        
        if($this->access[$name] and $this->access[$name]=== true)
        {
            $params = [];
            foreach($htmlParameters as $key => $value)
            {
                $params[] = sprintf('%s="%s"', $key, $value);
            }
            
            $optionalParams = [];
            if(isset($parameters['params']))
            {
                $optionalParams = $parameters['params'];
                unset($parameters['params']);
            }
            
            $url = $this->generator->generate($name, $parameters, $relative ? UrlGeneratorInterface::RELATIVE_PATH : UrlGeneratorInterface::ABSOLUTE_PATH);
            $label = ($label) ? $label:$name;
            
            $additionalParams = [];
            foreach($optionalParams as $k => $v)
            {
                $additionalParams[] = $k .'='.$v;
            }
            
            $optParams = (!empty($additionalParams)) ? '?'.implode("&", $additionalParams) : null;
            
            $html = sprintf('<a href="%s" %s>%s</a>', $url . $optParams, implode(" ", $params), $label);
            
            return $html;
        }   
        
        return '';
    }
    
    private function isGranted(&$parameters = [])
    {
        $isGranted = true;
        if(isset($parameters['object']))
        {
            $isGranted = $this->security->isGranted($parameters['action'], $parameters['object']);
            unset($parameters['object']);
            unset($parameters['action']);
        }
        
        return $isGranted;
    }
    
    public function submitTag(string $name, $parameters = [], $htmlParameters = [], $label = null)
    {
        if(!$this->isGranted($parameters))
        {
            return '';
        }
        
        $this->checkAccess($name);
        
        if($this->access[$name] and $this->access[$name]=== true)
        {
            $params = [];
            foreach($htmlParameters as $key => $value)
            {
                $params[] = sprintf('%s="%s"', $key, $value);
            }

            $html = sprintf('<button type="submit" %s>%s</button>', implode(" ", $params), $label);
            return $html;
        }
          
        return '';
    }
}
