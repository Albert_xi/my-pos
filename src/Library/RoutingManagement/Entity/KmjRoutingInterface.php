<?php

namespace App\Library\RoutingManagement\Entity;

use Ramsey\Uuid\UuidInterface;
use Doctrine\Common\Collections\Collection;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface KmjRoutingInterface 
{
    public function getName(): ?string;

    public function getPath(): ?string;

    public function getSequence(): ?int;

    public function getParent(): ?UuidInterface;
    
    public function getRoutingGroup(): ?string;
    
    public function getKmjRoutingRoles(): Collection;
}
