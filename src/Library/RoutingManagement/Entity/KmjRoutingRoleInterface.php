<?php

namespace App\Library\RoutingManagement\Entity;

use App\Library\RoutingManagement\Entity\KmjRoutingInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface KmjRoutingRoleInterface 
{
    public function getRouting(): ?KmjRoutingInterface;
    
    public function getRole(): ?string;
    
    public function isAllowed():bool;
    
    public function getPath(): ?string;
}
