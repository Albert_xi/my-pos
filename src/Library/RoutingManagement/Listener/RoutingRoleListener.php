<?php

namespace App\Library\RoutingManagement\Listener;

use App\Library\UserService;
use App\Library\RoutingManagement\Repository\KmjRoutingRoleRepositoryInterface;
use App\Library\RoutingManagement\RoutingService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class RoutingRoleListener 
{
    private $session, $routingService, $kmjRoutingRoleRepo, $userService, $urlGenerator;
    
    public function __construct(
            SessionInterface $session, 
            RoutingService $routingService,
            KmjRoutingRoleRepositoryInterface $kmjRoutingRoleRepo, 
            UrlGeneratorInterface $urlGenerator,
            UserService $userService) 
    {
        $this->session = $session;
        $this->routingService = $routingService;
        $this->kmjRoutingRoleRepo = $kmjRoutingRoleRepo;
        $this->userService = $userService;
        $this->urlGenerator = $urlGenerator;
    }
    
    public function onKernelRequest(RequestEvent $event)
    {
        if($event->isMasterRequest())
        {
            $request    = $event->getRequest();
            $path       = $request->attributes->get('_route');
            $kmjRoutingRole = $this->routingService->pathIsExist($path);
            if($kmjRoutingRole)
            {
                foreach($this->userService->getToken()->getRoleNames() as $role)
                {
                    $response = new RedirectResponse($this->urlGenerator->generate('app_access_denied'));
                    $kmjRoutingRole = $this->kmjRoutingRoleRepo->findOneByPathAndRole($path, $role);
                    if($kmjRoutingRole)
                    {
                        if(!$kmjRoutingRole->isAllowed())
                        {
                            $event->setResponse($response);
                        }
                    }else
                    {
                        $event->setResponse($response);
                    }
                }
            }
            
            if($this->routingService->isIndexPath($path))
            {
                $this->session->set('active_route', $path);
                $groupRoute = $this->routingService->getGroupPath($path);
                if($groupRoute)
                {
                    $this->session->set('active_route_group', $groupRoute);
                }
            }
        }
    }
}
