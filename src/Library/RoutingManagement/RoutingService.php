<?php

namespace App\Library\RoutingManagement;

use App\Library\RoutingManagement\Entity\KmjRoutingRoleInterface;
use App\Library\RoutingManagement\Repository\KmjRoutingRoleRepositoryInterface;
use App\Library\RoutingManagement\Repository\KmjRoutingRepositoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use App\Library\UserService;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class RoutingService 
{
    private $router, $kmjRoutingRepo, $kmjRoutingRoleRepo,$userService;
    
    public function __construct(
            RouterInterface $router, 
            KmjRoutingRepositoryInterface $kmjRoutingRepo,
            KmjRoutingRoleRepositoryInterface $kmjRoutingRoleRepo, 
            UserService $userService) 
    {
        $this->router = $router;
        $this->kmjRoutingRepo = $kmjRoutingRepo;
        $this->kmjRoutingRoleRepo = $kmjRoutingRoleRepo;
        $this->userService = $userService;
    }
    
    public function getRouter():RouterInterface
    {
        return $this->router;
    }
    
    public function isIndexPath($path):bool
    {
        return preg_match('/_index/', $path);
    }
    
    public function getIndexRoute($useCredential = false, $eliminated = null)
    {
        if($useCredential)
        {
            return $this->getIndexRouteByCredential();
        }
        $groups = [];
        foreach($this->getRouter()->getRouteCollection()->all() as $key => $route)
        {
            if($this->isIndexPath($key)) 
            {
                $dirs   = array_unique(explode("/", $route->getPath()));
                $i      = 0;
                $path   = null;
                foreach($dirs as $dir)
                {
                    if(!$dir)
                    {
                        continue;
                    }
                    
                    if($eliminated)
                    {
                        if($dir == $eliminated)
                        {
                            $i++;
                            continue;
                        }
                    }
                    
                    if($i != count($dirs) - 2)
                    {
                        $path .= (is_null($path)) ? $dir: DIRECTORY_SEPARATOR . $dir;
                    }
                    $i++;
                }
                if(strlen($path)==0)
                {
                    $groups[$key] = $route;
                }else
                {
                    $groups[$path][$key] = $route;
                }
                
            }
        }
        
        ksort($groups);
        return new ArrayCollection($groups);
    }
    
    function getOtherPath($indexPath) 
    {
        $data = [];
        $path = str_replace('_index', '', $indexPath);
        $routing = (new ArrayCollection($this->router->getRouteCollection()->all()))->partition(function($key, $item) use ($path){
            return preg_match('/^'.$path.'/', $key);
        });
        
        if(isset($routing[0])) {
            // cek jika ada routing yang mirip
            $indexKey = $routing[0]->partition(function($key, $item) use ($path) {
                return preg_match('/_index\z/i', $key);
            });
            if($indexKey[0]->count() > 1) {
                foreach($indexKey[0] as $k => $val) {
                    if($k == $indexPath) {
                        continue;
                    }
                    $path = str_replace('_index', '', $k);
                    $routing = $routing[0]->partition(function($key, $item) use ($path) {
                        return !preg_match('/^'.$path.'/', $key);
                    });
                }
            }
            $data = $routing[0]->toArray();
        }
        
        return $data;
    }
    
    public function updateCredential(array $credentials, string $role, EntityManagerInterface $entityManager):bool
    {
        $entityManager->beginTransaction();
        try{
            
            $kmjRoutingRolesExisting = $this->kmjRoutingRoleRepo->getCredentialByRole($role);
            $kmjRoutingRoles = clone $kmjRoutingRolesExisting;
            foreach($credentials as $group => $indexRoute)
            {
                foreach($indexRoute as $path => $route)
                {
                    $kmjRouting = $this->kmjRoutingRepo->findOneBy(['path' => $path]);
                    $name = str_replace('_index', '', $path);
                    if(!$kmjRouting)
                    {
                        $kmjRouting = ($this->kmjRoutingRepo->createKmjRouting())
                            ->setPath($path)->setSequence(1)
                            ->setRoutingGroup($group)
                            ->setName($name);
                    }
                    
                    foreach($route as $childPath => $status)
                    {
                        $kmjRoutingRole = $this->kmjRoutingRoleRepo->updateRole($childPath, $role, $kmjRouting, true);
                        $kmjRouting->addKmjRoutingRole($kmjRoutingRole);
                        if($kmjRoutingRoles->contains($kmjRoutingRole))
                        {
                            $kmjRoutingRoles->removeElement($kmjRoutingRole);
                        }
                    }
                    
                    $kmjRouting = $this->kmjRoutingRepo->saveCredential($kmjRouting);
                }
            }

            if(!$kmjRoutingRoles->isEmpty())
            {
                foreach($kmjRoutingRoles as $kmjRoutingRole)
                {
                    $kmjRoutingRoleOld = $kmjRoutingRolesExisting->filter(function (KmjRoutingRoleInterface $kmjRoutingRoleExist) use ($kmjRoutingRole) {
                        return $kmjRoutingRoleExist->getPath() == $kmjRoutingRole->getPath();
                    })->first();
                    
                    if($kmjRoutingRoleOld)
                    {
                        $kmjRoutingRole = $this->kmjRoutingRoleRepo->updateRoutingRole($kmjRoutingRoleOld);
                    }

                }
            }
            $entityManager->flush();
            $entityManager->commit();
            return true;
        } catch (\Exception $ex) {
            $entityManager->rollback();
            throw $ex;
        }
        
        return false;
    }
    
    public function getCredentials(string $role):Collection
    {
        $credentials = new ArrayCollection();
        foreach($this->kmjRoutingRoleRepo->getCredentialByRole($role) as $kmjRoutingRole)
        {
            $credentials[$kmjRoutingRole->getPath()] = $kmjRoutingRole;
        }
        
        return $credentials;
    }
        
    private function getIndexRouteByCredential()
    {
        $routes = [];
        if($this->userService->getToken() and !empty($this->userService->getToken()->getRoleNames()))
        {
            $roles = $this->userService->getToken()->getRoleNames();
            foreach($roles as $role)
            {
                $kmjRoutings = $this->kmjRoutingRepo->findAllowedByRole($role);
                foreach($kmjRoutings as $kmjRouting)
                {
                    if(!isset($routes[$kmjRouting->getRoutingGroup()]))
                    {
                        $routes[$kmjRouting->getRoutingGroup()] = [];
                    }
                    
                    $routes[$kmjRouting->getRoutingGroup()][$kmjRouting->getPath()] = $kmjRouting;
                }
            }
        }
        
        return new ArrayCollection($routes);
    }
    
    public function getGroupPath($path):?string
    {
        $kmjRouting = $this->kmjRoutingRepo->findOneBy(['path' => $path]);
        if($kmjRouting)
        {
            return $kmjRouting->getRoutingGroup();
        }
        
        return null;
    }
    
    public function pathIsExist($path):?KmjRoutingRoleInterface
    {
        return $this->kmjRoutingRoleRepo->findOneByPath($path);
    }
    
    public function pathIsAllowedByRole($path, $role):bool
    {
        $kmjRoutingRole = $this->pathIsExist($path);
        if($kmjRoutingRole)
        {
            $kmjRoutingRole = $this->kmjRoutingRoleRepo->findOneByPathAndRole($path, $role);
            if($kmjRoutingRole)
            {
                return $kmjRoutingRole->isAllowed();
            }
            
            return false;
        }   
        
        return true;
    }
}
