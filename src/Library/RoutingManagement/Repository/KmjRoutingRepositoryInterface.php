<?php

namespace App\Library\RoutingManagement\Repository;

use Doctrine\Common\Collections\Collection;
use App\Library\RoutingManagement\Entity\KmjRoutingInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface KmjRoutingRepositoryInterface 
{
    public function saveCredential(KmjRoutingInterface $kmjRouting): KmjRoutingInterface;
    
    public function createKmjRouting():KmjRoutingInterface;
    
    public function findOneBy(array $criteria, array $orderBy = null);
    
    public function findAllowedByRole(string $role): Collection;
}
