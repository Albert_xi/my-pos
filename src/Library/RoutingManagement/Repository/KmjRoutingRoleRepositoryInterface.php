<?php

namespace App\Library\RoutingManagement\Repository;

use App\Library\RoutingManagement\Entity\KmjRoutingInterface;
use App\Library\RoutingManagement\Entity\KmjRoutingRoleInterface;
use Doctrine\Common\Collections\Collection;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface KmjRoutingRoleRepositoryInterface 
{
    public function updateRole(string $path, string $role, KmjRoutingInterface $kmjRouting, bool $isAllowed):KmjRoutingRoleInterface;
    
    public function updateRoutingRole(KmjRoutingRoleInterface $kmjRoutingRole, bool $isAllowed):KmjRoutingRoleInterface;
    
    public function getCredentialByRole(string $role):Collection;
    
    public function findOneByPath(string $path):?KmjRoutingRoleInterface;
    
    public function findOneByPathAndRole(string $path, string $role):?KmjRoutingRoleInterface;
}
