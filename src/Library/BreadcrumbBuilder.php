<?php

namespace App\Library;

use Kematjaya\Breadcrumb\Lib\Builder;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class BreadcrumbBuilder 
{
    private $builder;
    
    function __construct(Builder $builder) 
    {
        $builder->setDefault('home', 'admin_index');
        $this->builder = $builder;
    }
    
    function getBuilder():Builder
    {
        return $this->builder;
    }
}
