<?php

namespace App\Filter;

use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjUserFilterType extends AbstractFilterType
{
    private $roleList = [];
    
    public function __construct(TokenStorageInterface $token, RoleHierarchyInterface $roleHierarchy) 
    {
        foreach ($roleHierarchy->getReachableRoleNames($token->getToken()->getRoleNames()) as $role) {
            $this->roleList[$role] = $role;
        }
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('roles', Filters\ChoiceFilterType::class, [
                'choices' => $this->roleList,
                'apply_filter' => $this->JSONQuery()
            ])
            ->add('name', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('is_active', Filters\BooleanFilterType::class)
            ->add('store', Filters\EntityFilterType::class, [
                'class' => \App\Entity\Store::class
            ]);
    }
}
