<?php

namespace App\Filter;

use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjPrinterClientFilterType extends AbstractFilterType 
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kmj_user', Filters\EntityFilterType::class, [
                'class' => \App\Entity\KmjUser::class
            ])
            ->add('computer_name', Filters\TextFilterType::class, [
                'label' => 'computer_name', 'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('printer_name', Filters\TextFilterType::class, [
                'label' => 'printer_name', 'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('is_default', Filters\BooleanFilterType::class);
    }
}
