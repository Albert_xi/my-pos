<?php

namespace App\Filter;

use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class DocumentationFilterType extends AbstractFilterType 
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('topic', Filters\TextFilterType::class, [
                'label' => 'topic', 'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('title', Filters\TextFilterType::class, [
                'label' => 'title', 'condition_pattern' => FilterOperands::STRING_CONTAINS
            ]);
    }
}
