<?php

namespace App\Filter;

use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Kematjaya\BaseControllerBundle\Type\FloatRangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemFilterType extends AbstractFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('name', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('category', Filters\EntityFilterType::class, [
                'class' => \App\Entity\ItemCategory::class
            ])
            ->add('use_barcode', Filters\BooleanFilterType::class)
            ->add('last_stock', FloatRangeType::class, [
                'from_options' => [
                    "attr" => ["class" => "form-control", "title" => "from"]
                ],
                'to_options' => [
                    "attr" => ["class" => "form-control", "title" => "to"]
                ],
                'apply_filter' => $this->floatRangeQuery()
            ])
            ->add('harga_pokok', FloatRangeType::class, [
                'from_options' => [
                    "attr" => ["class" => "form-control priceformat", "title" => "from"]
                ],
                'to_options' => [
                    "attr" => ["class" => "form-control priceformat", "title" => "to"]
                ],
                'apply_filter' => $this->floatRangeQuery()
            ])
            ->add('last_price', FloatRangeType::class, [
                'from_options' => [
                    "attr" => ["class" => "form-control priceformat", "title" => "from"]
                ],
                'to_options' => [
                    "attr" => ["class" => "form-control priceformat", "title" => "to"]
                ],
                'apply_filter' => $this->floatRangeQuery()
            ])
        ;
        
    }
}
