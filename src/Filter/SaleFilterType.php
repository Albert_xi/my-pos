<?php

namespace App\Filter;

use Kematjaya\BaseControllerBundle\Type\DateRangeType;
use Kematjaya\BaseControllerBundle\Type\FloatRangeType;
use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleFilterType extends AbstractFilterType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('store', Filters\EntityFilterType::class, [
                'class' => \App\Entity\Store::class
            ])
            ->add('code', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('customer', Filters\EntityFilterType::class, [
                'class' => \App\Entity\Customer::class
            ])
            ->add('created_by', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('created_at', DateRangeType::class, [
                'from_options' => [
                    'widget' => 'single_text',
                    "attr" => ["class" => "form-control", "title" => "from"]
                ],
                'to_options' => [
                    'widget' => 'single_text',
                    "attr" => ["class" => "form-control", "title" => "to"]
                ],
                'apply_filter' => $this->dateRangeQuery()
            ])
            ->add('total', FloatRangeType::class, [
                'from_options' => [
                    "attr" => ["class" => "form-control priceformat", "title" => "from"]
                ],
                'to_options' => [
                    "attr" => ["class" => "form-control priceformat", "title" => "to"]
                ],
                'apply_filter' => $this->floatRangeQuery()
            ]);
    }
}
