<?php

namespace App\Filter;

use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StoreFilterType extends AbstractFilterType 
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('name', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('address', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('is_default', Filters\BooleanFilterType::class)
        ;
    }
}
