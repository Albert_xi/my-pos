<?php

namespace App\Filter;

use Kematjaya\BaseControllerBundle\Type\DateRangeType;
use Kematjaya\BaseControllerBundle\Type\FloatRangeType;
use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SerahTerimaFilterType extends AbstractFilterType 
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created_at', DateRangeType::class, [
                'from_options' => [
                    'widget' => 'single_text',
                    "attr" => ["title" => "from"]
                ],
                'to_options' => [
                    'widget' => 'single_text',
                    "attr" => ["title" => "to"]
                ],
                'apply_filter' => $this->dateRangeQuery()
            ])
            ->add('total_debt', FloatRangeType::class, [
                'from_options' => [
                    "attr" => ["class" => "priceformat", "title" => "from"]
                ],
                'to_options' => [
                    "attr" => ["class" => "priceformat", "title" => "to"]
                ],
                'apply_filter' => $this->floatRangeQuery()
            ])
            ->add('total_credit', FloatRangeType::class, [
                'from_options' => [
                    "attr" => ["class" => "priceformat", "title" => "from"]
                ],
                'to_options' => [
                    "attr" => ["class" => "priceformat", "title" => "to"]
                ],
                'apply_filter' => $this->floatRangeQuery()
            ])
            ->add('description', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('delegated_by', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
            ->add('approved_by', Filters\TextFilterType::class, [
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ])
        ;
    }
}
