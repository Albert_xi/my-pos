<?php

namespace App\Filter\Report;

use Kematjaya\BaseControllerBundle\Filter\AbstractFilterType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class TransactionFilterType extends AbstractFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('from', DateType::class, [
                "label" => 'from', 'required' => false,
                'widget' => 'single_text'
            ])
            ->add('to', DateType::class, [
                "label" => 'to', 'required' => false,
                'widget' => 'single_text'
            ]);
    }
}
