<?php

namespace App\DataFixtures;

use App\Entity\KmjUser;
use App\Repository\StoreRepository;
use App\Repository\KmjUserRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Kematjaya\UserBundle\DataFixtures\UserFixtures as BaseUserFixtures;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class UserFixtures extends BaseUserFixtures implements DependentFixtureInterface, FixtureGroupInterface
{
    private $storeRepo, $encoderFactory;
    
    public function __construct(
            KmjUserRepository $kmjUserRepo, 
            StoreRepository $storeRepo, 
            EncoderFactoryInterface $encoderFactory) 
    {
        parent::__construct($kmjUserRepo, $encoderFactory);
        $this->storeRepo = $storeRepo;
        $this->encoderFactory = $encoderFactory;
    }
    
    public function load(ObjectManager $manager)
    {
        $data = [
            KmjUser::ROLE_KEPALA => [
                [
                    'username' => 'kepala_01',
                    'name' => 'Kepala 01',
                    'store' => '00001',
                ]
            ],
            KmjUser::ROLE_OPERATOR => [
                [
                    'username' => 'operator_01',
                    'name' => 'Operator 01',
                    'store' => '00001',
                ],
                [
                    'username' => 'operator_02',
                    'name' => 'Operator 02',
                    'store' => '00002',
                ]
            ]
        ];
        
        foreach($data as $role => $users)
        {
            foreach($users as $user)
            {
                $store = $this->storeRepo->findOneBy(['code' => $user['store']]);
                $kmjUser = (new KmjUser())
                    ->setRoles([$role])
                    ->setStore($store)
                    ->setUsername($user['username'])
                    ->setName($user['name'])
                    ->setIsActive(true);
                
                $encoder = $this->encoderFactory->getEncoder($kmjUser);
                $password = $encoder->encodePassword( 'admin123', $kmjUser->getUsername());
                $kmjUser->setPassword($password);
                
                $manager->persist($kmjUser);
            }
            
            $manager->flush();
        }
    }
    
    public function getDependencies()
    {
        return [
            StoreFixtures::class
        ];
    }
    
    public static function getGroups(): array
    {
        return ['admin', 'master', 'transaction'];
    }
}
