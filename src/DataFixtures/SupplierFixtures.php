<?php

namespace App\DataFixtures;

use App\Repository\SupplierRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SupplierFixtures extends Fixture implements FixtureGroupInterface
{
    private $supplierRepo;
    
    const SUPPLIER_REFERENCE = 'supplier';
    
    public function __construct(SupplierRepository $supplierRepo) 
    {
        $this->supplierRepo = $supplierRepo;
    }
    
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            if ($i == 0) {
                $name = 'test';
            }else {
                $name = sprintf("Supplier - %s, PT", $i);
            }
            
            $suppplier = $this->supplierRepo->createSupplier();
            $suppplier->setName($name)
                    ->setAddress('test alamat')
                    ->setPhone('085647564' . $i);
            $manager->persist($suppplier);
            $this->setReference(self::SUPPLIER_REFERENCE, $suppplier);
        }
        
        $manager->flush();
    }
    
    public static function getGroups(): array
    {
        return ['master', 'transaction'];
    }
}
