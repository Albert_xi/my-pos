<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Entity\ItemPriceLog;
use Kematjaya\ItemPack\Lib\ItemPackaging\Entity\ItemPackageInterface;
use Kematjaya\ItemPack\Lib\Item\Repo\ItemRepoInterface;
use Kematjaya\PurchashingBundle\Repo\PurchaseDetailRepoInterface;
use Kematjaya\PurchashingBundle\Repo\PurchaseRepoInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    private $purchaseRepo, $purchaseDetailRepo, $itemRepo;
    
    public function __construct(
            PurchaseRepoInterface $purchaseRepo, 
            PurchaseDetailRepoInterface $purchaseDetailRepo, 
            ItemRepoInterface $itemRepo) 
    {
        $this->purchaseRepo = $purchaseRepo;
        $this->purchaseDetailRepo = $purchaseDetailRepo;
        $this->itemRepo = $itemRepo;
    }
    
    public function load(ObjectManager $manager)
    {
        $items = $manager->getRepository(Item::class)->findAll();
        $purchase = null;
        
        $maxItem = 5;
        $totals = 0;
        $createdAt = new \DateTime();
        foreach($items as $k => $item)
        {
            $count = $k % $maxItem;
            if($count == 0)
            {
                $totals = 0;
                $purchase = $this->purchaseRepo->createObject();
                $supplier = $this->getReference(SupplierFixtures::SUPPLIER_REFERENCE);
                $purchase->setCode(rand())
                        ->setPurchaseAt((clone $createdAt)->modify(sprintf('-%s month', $k+1)))
                        ->setSupplier($supplier);
            }
            
            $qty = 100 * random_int(5, 10);
            $price = 1000;
            $total = $qty * $price;
            $totals += $total;
            $packages = $item->getItemPackages()->filter(function (ItemPackageInterface $itemPackage){
                return $itemPackage->isSmallestUnit();
            });

            $package = ($packages) ? $packages->first():null;
            $purchaseDetail = $this->purchaseDetailRepo->createObject($purchase);
            $purchaseDetail->setQuantity($qty)
                    ->setPrice($price)
                    ->setTotal($total)
                    ->setItem($item)
                    ->setTax(0);
            if($package)
            {
                $purchaseDetail->setPackaging($package->getPackaging());
            }
            $manager->persist($purchaseDetail);
            
            $purchase->addPurchaseDetail($purchaseDetail);
            
            if($count == ($maxItem-1))
            {
                $purchase->setTotal($totals)->setIsLocked(true);
                $manager->persist($purchase);
            }
        }
        
        $manager->flush();
        
        $queryByilder = $manager->getRepository(ItemPriceLog::class)->createQueryBuilder('t');
        if($queryByilder instanceof \Doctrine\ORM\QueryBuilder)
        {
            $queryByilder->where('t.status = :status')->setParameter('status', ItemPriceLog::STATUS_NEW)
                    ->orderBy('t.created_at', 'ASC');
            foreach($queryByilder->getQuery()->getResult() as $itemPriceLog)
            {
                $salePrice = ($itemPriceLog->getPrincipalPrice() * 1.5);
                $itemPriceLog->setSalePrice($salePrice);
                $itemPriceLog->setStatus(ItemPriceLog::STATUS_APPROVED);
                $manager->persist($itemPriceLog);
            }
        }
        
        $manager->flush();
    }
    
    public function getDependencies()
    {
        return [
            ItemFixtures::class, SupplierFixtures::class
        ];
    }
    
    public static function getGroups(): array
    {
        return ['transaction'];
    }
}
