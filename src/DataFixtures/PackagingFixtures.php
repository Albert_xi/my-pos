<?php

namespace App\DataFixtures;

use App\Repository\PackagingRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PackagingFixtures extends Fixture implements FixtureGroupInterface
{
    private $packagingRepo;
    
    public const PACKAGING_REFERENCE = 'packaging';
    
    public function __construct(PackagingRepository $packagingRepo) 
    {
        $this->packagingRepo = $packagingRepo;
    }
    
    public function load(ObjectManager $manager)
    {
        $data = ['Kg' => "Kilogram", 'gram' =>"Gram", "Lt" => "liter", "Dus" => 'Dus', 'Pack' => 'Pack', 'Pcs' => 'Pcs'];
        foreach ($data as $k => $v)
        {
            $obj = $this->packagingRepo->createPackaging();
            $obj->setCode($k)->setName($v);
            $manager->persist($obj);
            $this->setReference(self::PACKAGING_REFERENCE, $obj);
        }
        
        $manager->flush();
    }
    
    public static function getGroups(): array
    {
        return ['admin', 'master', 'transaction'];
    }
}
