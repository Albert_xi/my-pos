<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class CustomerFixtures extends Fixture implements FixtureGroupInterface
{
    const CUSTOMER_REFERENCE = 'customer';
    
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $customer = (new Customer())
                    ->setCode(($i+1))
                    ->setName("Customer " . $i)
                    ->setAddress("Jl xxx no 17")
                    ->setPhone("08563673763" . $i)
                    ->setMail(sprintf("kematjaya%s@gmail.com", $i));
            
            $manager->persist($customer);
            $this->setReference(self::CUSTOMER_REFERENCE, $customer);
        }
        
        $manager->flush();
    }
    
    public static function getGroups(): array
    {
        return ['master', 'transaction'];
    }
}
