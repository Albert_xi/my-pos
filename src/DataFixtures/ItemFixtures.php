<?php

namespace App\DataFixtures;

use App\Repository\ItemRepository;
use App\Repository\ItemPackageRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    
    private $itemRepo;
    
    private $itemPackageRepo;
    
    public function __construct(ItemRepository $itemRepo, ItemPackageRepository $itemPackageRepo) 
    {
        $this->itemRepo = $itemRepo;
        $this->itemPackageRepo = $itemPackageRepo;
    }
    
    public function load(ObjectManager $manager)
    {
        $packaging = $this->getReference(PackagingFixtures::PACKAGING_REFERENCE);
        $category = $this->getReference(ItemCategoryFixtures::ITEM_CATEGORY_REFERENCE);
        for ($i = 1; $i <=30; $i++)
        {
            $obj = $this->itemRepo->createItem();
            $obj->setCode($i)
                ->setName('product '. $i)
                ->setUseBarcode(false)
                ->setCategory($category)
                ->setLastPrice(0)
                ->setLastStock(0)->setHargaPokok(0);
            $itemPackage = $this->itemPackageRepo->createPackage($obj);
            $itemPackage->setPackaging($packaging)->setPrincipalPrice(0)->setSalePrice(0)->setIsSmallestUnit(true);
            if($itemPackage->isSmallestUnit())
            {
                $obj->setLastPrice($itemPackage->getSalePrice());
                $itemPackage->setQuantity(1);
            }
            
            $manager->persist($obj);
            $manager->persist($itemPackage);
        }
        
        $manager->flush();
    }
    
    public function getDependencies()
    {
        return [
            PackagingFixtures::class, ItemCategoryFixtures::class
        ];
    }
    
    public static function getGroups(): array
    {
        return ['master', 'transaction'];
    }
}
