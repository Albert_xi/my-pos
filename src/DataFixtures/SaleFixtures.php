<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Entity\Sale;
use App\Entity\SaleItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface 
{
    public function load(ObjectManager $manager)
    {
        $items = $manager->getRepository(Item::class)->findAll();
        $purchase = null;
        
        $maxItem = 5;
        $subTotals = 0;
        $sale = null;
        $code = 1;
        $createdAt = new \DateTime();
        foreach($items as $k => $item)
        {
            $count = $k % $maxItem;
            if($count == 0)
            {
                $totals = 0;
                $customer = $this->getReference(CustomerFixtures::CUSTOMER_REFERENCE);
                $store = $this->getReference(StoreFixtures::STORE_REFERENCE);
                $sale = (new Sale())
                        ->setCode($code .'/SL/'.date('m').'/'.date('Y'))
                        ->setCreatedBy('test')
                        ->setStore($store)
                        ->setCreatedAt((clone $createdAt)->modify(sprintf('-%s month', $k+1)))
                        ->setCustomer($customer);
                $code++;
            }
            
            $qty = 10;
            $price = $item->getLastPrice();
            $subTotal = $qty * $price;
            $disc = 0;
            $tax = 0;
            $total = $subTotal + $tax - $disc;
            $subTotals += $total;
            $saleItem = (new SaleItem())
                    ->setDiscount($disc)
                    ->setItem($item)
                    ->setQuantity($qty)
                    ->setPrincipalPrice($item->getPrincipalPrice())
                    ->setSalePrice($price)
                    ->setTax($tax)
                    ->setSubTotal($subTotal)
                    ->setTotal($total);
            $manager->persist($saleItem);
            
            $sale->addSaleItem($saleItem);
            if($count == ($maxItem-1))
            {
                $tax = 0;
                $discount = 0;
                $totals = $subTotals + $tax - $discount;
                $paymentChange = 1000;
                $payment = $totals + $paymentChange;
                
                $sale->setSubTotal($subTotals)
                        ->setTax($tax)
                        ->setDiscount($discount)
                        ->setTotal($totals)
                        ->setPayment($payment)
                        ->setPaymentChange($paymentChange)
                        ->setIsLocked(true);
               
                $manager->persist($sale);
            }
        }
        
        $manager->flush();
    }
    
    public function getDependencies()
    {
        return [
            ItemFixtures::class, CustomerFixtures::class, StoreFixtures::class, PurchaseFixtures::class
        ];
    }
    
    public static function getGroups(): array
    {
        return ['transaction'];
    }
}
