<?php

namespace App\DataFixtures;

use Kematjaya\ItemPack\Lib\Item\Repo\ItemRepoInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPriceFixtures extends Fixture implements DependentFixtureInterface
{
    private $itemRepo;
    
    public function __construct(ItemRepoInterface $itemRepo) 
    {
        $this->itemRepo = $itemRepo;
    }
    
    public function load(ObjectManager $manager)
    {
        /*$benefitPercent = 5;
        foreach($this->itemRepo->findAll() as $item)
        {
            $price = (($benefitPercent / 100) * $item->getPrincipalPrice()) + $item->getPrincipalPrice();
            $item->setLastPrice($price);
            
            $manager->persist($item);
        }
        
        $manager->flush();*/
    }
    
    public function getDependencies()
    {
        return [
            ItemFixtures::class, PurchaseFixtures::class
        ];
    }
}
