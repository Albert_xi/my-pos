<?php

namespace App\DataFixtures;

use App\Entity\Sale;
use App\Entity\CodeLibrary;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class CodeLibraryFixtures extends Fixture
{
    public function load(ObjectManager $manager) 
    {
        $arr = [
            Sale::class => [
                'format' => ['{number}', '{type}', '{mm}', '{yy}'],
                'separator' => CodeLibrary::SEPARATOR_SLASH
            ]
        ];
        
        foreach($arr as $className => $value) {
            $object = (new CodeLibrary())
                    ->setClassName($className)
                    ->setFormat(implode($value['separator'], $value['format']))
                    ->setSeparator($value['separator']);
            
            $manager->persist($object);
        }
        
        $manager->flush();
    }

}
