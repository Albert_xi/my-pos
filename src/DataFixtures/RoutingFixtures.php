<?php

namespace App\DataFixtures;

use App\Entity\KmjUser;
use App\Library\RoutingManagement\RoutingService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class RoutingFixtures extends Fixture implements FixtureGroupInterface
{
    private $routingService;
    
    public function __construct(RoutingService $routingService) 
    {
        $this->routingService = $routingService;
    }
    
    public function load(ObjectManager $manager)
    {
        $credentials = [];
        foreach($this->routingService->getIndexRoute(false, 'admin') as $key => $routes)
        {
            foreach($routes as $path => $route)
            {
                foreach($this->routingService->getOtherPath($path) as $k => $router)
                {
                    $credentials[$key][$path][$k] = true;
                }
            }
        }
        
        $this->routingService->updateCredential($credentials, KmjUser::ROLE_SUPER_USER, $manager);
        $this->routingService->updateCredential($credentials, KmjUser::ROLE_ADMINISTRATOR, $manager);
    }
    
    public static function getGroups(): array
    {
        return ['admin', 'master', 'transaction'];
    }
}
