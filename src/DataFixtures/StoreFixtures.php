<?php

namespace App\DataFixtures;

use App\Entity\Store;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StoreFixtures extends Fixture implements FixtureGroupInterface
{
    const STORE_REFERENCE = 'store';
    
    public function load(ObjectManager $manager)
    {
        $data = [
            [
                'code' => '00001',
                'name' => 'SMK Bahrul Ulum',
                'address' => 'Jl. Raya Pelem Watu No.9, Bandut, Pelemwatu, Kec. Menganti, Kabupaten Gresik, Jawa Timur 61174',
                'is_default' => true
            ],
            [
                'code' => '00002',
                'name' => 'MI Bahrul Ulum',
                'address' => 'Jl. Raya Pelem Watu No.9, Palemwatu, Gempolkurung, Kec. Menganti, Kabupaten Gresik, Jawa Timur 61174',
                'is_default' => false
            ]  
        ];
        
        foreach($data as $store)
        {
            $object = (new Store())
                    ->setCode($store['code'])->setName($store['name'])
                    ->setAddress($store['address'])->setIsDefault($store['is_default']);
            
            $manager->persist($object);
            
            $this->setReference(self::STORE_REFERENCE, $object);
        }
        
        $manager->flush();
    }
    
    public static function getGroups(): array
    {
        return ['admin', 'master', 'transaction'];
    }
}
