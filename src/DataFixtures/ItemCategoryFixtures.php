<?php

namespace App\DataFixtures;

use App\Repository\ItemCategoryRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemCategoryFixtures extends Fixture implements FixtureGroupInterface
{
    private $temCategoryRepo;
    
    public const ITEM_CATEGORY_REFERENCE = 'item-category';
    
    public function __construct(ItemCategoryRepository $temCategoryRepo) 
    {
        $this->temCategoryRepo = $temCategoryRepo;
    }
    
    public function load(ObjectManager $manager)
    {
        $data = [];
        for($i = 1; $i <=10; $i++)
        {
            $obj = $this->temCategoryRepo->createItemCategory();
            $obj->setCode($i)->setName('Category ' . $i)->setIsActive(true);
            $manager->persist($obj);
            $this->setReference(self::ITEM_CATEGORY_REFERENCE, $obj);
        }
        
        
        $manager->flush();
    }
    
    public static function getGroups(): array
    {
        return ['master', 'transaction'];
    }
}
