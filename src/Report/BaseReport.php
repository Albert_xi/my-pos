<?php

namespace App\Report;

use koolreport\KoolReport;
use koolreport\datasources\PostgreSQLDataSource;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class BaseReport extends KoolReport 
{
    protected $connectionDriver;
    
    const DRIVER_MYSQL = 'mysql';
    const DRIVER_PGSQL = 'postgresql';
    
    public function settings()
    {
        $databaseURL                = $_ENV['DATABASE_URL'];
        list($driver, $URL)         = explode('://', $databaseURL);
        list($connection, $property)            = explode('/', $URL);
        list($userProperty, $networkProperty)   = explode('@', $connection);
        list($username, $password)  = explode(':', $userProperty);
        list($host, $port)          = explode(':', $networkProperty);
        list($dbname, $property)    = explode('?', $property);
        $properties                 = explode('&', $property);
        
        $class = null;
        switch($driver)
        {
            case self::DRIVER_MYSQL:
                $dataSource = [
                    "postgresql" => array(
                        "connectionString" => sprintf("mysql:host=%s;dbname=%s", $host, $dbname),
                        'username' => $username,
                        'password' => $password,
                        'charset' => 'utf8'
                    )
                ];
                $this->connectionDriver = $driver;
                break;
            case self::DRIVER_PGSQL:
                $dataSource = [
                    "postgresql" => array(
                        'host' => $host,
                        'username' => $username,
                        'password' => $password,
                        'dbname' => $dbname,
                        'charset' => 'utf8',
                        'port' => $port,
                        'class' => PostgreSQLDataSource::class  
                    )
                ];
                $this->connectionDriver = $driver;
                break;
            default:
                throw new \Exception(sprintf('no supported %s', $driver));
                break;
        }
        return [
            "dataSources" => $dataSource
        ];
    }
    
    public function getConnectionDriver()
    {
        return $this->connectionDriver;
    }
}
