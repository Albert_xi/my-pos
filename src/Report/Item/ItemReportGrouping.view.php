<?php

use Kematjaya\ReportBundle\Helper\TranslatorHelper;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */

$translator = $this->params['translator'];
\koolreport\widgets\koolphp\Table::create([
    "dataStore" =>  $this->dataStore('stock_item'),
    "grouping" => [
        "item_category" => [
            "top"    => "<b>{item_category}</b>"
        ]
    ],
    "columns" => [
        'code' => [
            "label" => TranslatorHelper::trans('code', $translator), "type"=>"text"
        ],
        'name' => [
            "label" => TranslatorHelper::trans('name', $translator), "type"=>"text",
        ],
        'last_stock' => [
            "label" => TranslatorHelper::trans('last_stock', $translator), "type" => "number", "cssStyle"  =>"text-align:right"
        ],
        'smallest_unit' => [
            "label" => TranslatorHelper::trans('smallest_unit', $translator), "type"=>"text",
        ]
    ]
]);

