<?php

namespace App\Report\Item;

use App\Report\BaseReport;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemReport extends BaseReport
{
    public function setup()
    {
        $sql = '
            SELECT 
                item.code, item.name, item.last_stock, item.last_price, item.harga_pokok,
                category.name as item_category, (
                    select packaging.name from item_package package 
                    JOIN packaging on package.packaging_id = packaging.id
                    WHERE package.item_id = item.id and package.is_smallest_unit = true
                ) as smallest_unit
            FROM item 
            JOIN item_category category on item.category_id = category.id';
        
        $this->src('postgresql')
                ->query($sql)
                ->pipe($this->dataStore('list_item'));
        
        $sql = '
            SELECT 
                item.code, item.name, item.last_stock,
                category.name as item_category, (
                    select packaging.name from item_package package 
                    JOIN packaging on package.packaging_id = packaging.id
                    WHERE package.item_id = item.id and package.is_smallest_unit = true
                ) as smallest_unit
            FROM item 
            JOIN item_category category on item.category_id = category.id
            ORDER BY item_category, item.code';
        
        $this->src('postgresql')
                ->query($sql)
                ->pipe($this->dataStore('stock_item'));
    }
}
