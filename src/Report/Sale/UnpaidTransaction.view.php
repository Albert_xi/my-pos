<?php

use Kematjaya\ReportBundle\Helper\TranslatorHelper;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */

$translator = $this->params['translator'];
\koolreport\widgets\koolphp\Table::create([
    "dataStore" =>  $this->dataStore('unpaid_transaction'),
    "grouping" => [
        "name" => [
            "calculate" => array(
                "{sumTotal}" => array("sum", "total"),
                "{sumPayment}" => array("sum", "payment"),
                "{sumPaymentChange}" => array("sum", "payment_change")
            ),
            "top"    => "<b>".TranslatorHelper::trans('customer', $translator).": {name}</b>",
            "bottom" => ""
            . "<td><b>".TranslatorHelper::trans('total', $translator)." {name}</b></td>"
            . "<td style='text-align:right'><b>{sumTotal}</b></td>"
            . "<td style='text-align:right'><b>{sumPayment}</b></td>"
            . "<td style='text-align:right'><b>{sumPaymentChange}</b></td>"
        ]
    ],
    "showFooter" => true,
    "columns" => [
        'code' => [
            "label" => TranslatorHelper::trans('code', $translator), "type" => "text"
        ],
        'total' => [
            "label"      => TranslatorHelper::trans('total', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  =>"text-align:right"
        ],
        'payment' => [
            "label"      => TranslatorHelper::trans('payment', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  =>"text-align:right"
        ],
        'payment_change' => [
            "label"      => TranslatorHelper::trans('payment_change', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ]
    ]
]);

