<?php

namespace App\Report\Sale;

use App\Report\BaseReport;
use App\Report\DQL\Date;
use koolreport\processes\CalculatedColumn;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleReport extends BaseReport
{
    public function setup()
    {
        $profitSQL = sprintf("
            SELECT
                %s,
                extract(year from s.created_at) as years,
                sum(si.principal_price * si.quantity) as total_principal_price,
                sum(si.sale_price * si.quantity) as total_sale_price,
                (sum(si.discount)+sum(s.discount)) as total_discount,
                (sum(si.tax) + sum(s.tax)) as total_tax
            FROM sale_item si join sale s on si.sale_id = s.id
            group by mon, years", Date::toChar('s.created_at', 'Mon', 'mon', $this));
        
        $this->src('postgresql')
                ->query($profitSQL)
                ->pipe(new CalculatedColumn([
                    "total" => function($row){
                        return (float)$row["total_sale_price"] - (float)$row["total_principal_price"] - (float)$row["total_discount"];
                    }
                ]))
                ->pipe($this->dataStore('profit'));
        
        $profitSQLByStore = "
            SELECT
                s.store_id as store_id, max(st.name) as store_name,
                extract(year from s.created_at) as years,
                sum(si.principal_price * si.quantity) as total_principal_price,
                sum(si.sale_price * si.quantity) as total_sale_price,
                (sum(si.discount)+sum(s.discount)) as total_discount,
                (sum(si.tax) + sum(s.tax)) as total_tax
            FROM sale_item si join sale s on si.sale_id = s.id
            JOIN store st on s.store_id = st.id
            group by s.store_id, years";
        
        $this->src('postgresql')
                ->query($profitSQLByStore)
                ->pipe(new CalculatedColumn([
                    "total" => function($row){
                        return (float)$row["total_sale_price"] - (float)$row["total_principal_price"] - (float)$row["total_discount"];
                    }
                ]))
                ->pipe($this->dataStore('profit_by_store'));
                
        $saleInMonth = sprintf("
            select s.created_at, s.code, s.sub_total, s.tax, s.total, s.discount, s.payment,
            (
              case when  s.total <= s.payment then true 
                else false
              end
            ) as payment_status from sale s
            where extract(month from created_at) = %s
            and extract(year from created_at) = %s
            and is_locked = true", date('m'), date('Y'));
        
        $this->src('postgresql')
                ->query($saleInMonth)
                ->pipe($this->dataStore('sale_in_month'));
    }
}
