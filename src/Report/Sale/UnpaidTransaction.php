<?php

namespace App\Report\Sale;

use App\Report\BaseReport;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class UnpaidTransaction extends BaseReport
{
    public function setup()
    {
        $sql = "
            SELECT
                c.code as cust_code, c.name, s.created_at, s.code, s.total, s.payment, s.payment_change
            FROM sale s
            JOIN customer c on s.customer_id = c.id 
            where s.payment_change < 0 and s.is_locked = true";
        
        $this->src('postgresql')
                ->query($sql)
                ->pipe($this->dataStore('unpaid_transaction'));
    }
}
