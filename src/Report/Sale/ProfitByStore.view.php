<?php

use Kematjaya\ReportBundle\Helper\TranslatorHelper;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */

$translator = $this->params['translator'];
\koolreport\widgets\koolphp\Table::create([
    "dataStore" =>  $this->dataStore('profit_by_store'),
    "grouping" => [
        "store_name" => [
            "calculate" => array(
                "{sumTotal}" => array("sum", "total"),
                "{sumTax}" => array("sum", "total_tax")
            ),
            "top"    => "<b>{store_name}</b>",
            "bottom" => ""
            . "<td colspan='5'><b>Total {store_name}</b></td>"
            . "<td style='text-align:right'><b>{sumTotal}</b></td>"
            . "<td style='text-align:right'><b>{sumTax}</b></td>"
        ]
    ],
    "showFooter" => true,
    "columns" => [
        'store_name' => [
            "label" => TranslatorHelper::trans('store_name', $translator), "type" => "text"
        ],
        'years' => [
            "label" => TranslatorHelper::trans('years', $translator), "type" => "text",
        ],
        'total_principal_price' => [
            "label"      => TranslatorHelper::trans('total', $translator). ' '. TranslatorHelper::trans('principal_price', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  =>"text-align:right"
        ],
        'total_sale_price' => [
            "label"      => TranslatorHelper::trans('total', $translator).' '.TranslatorHelper::trans('sale_price', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  =>"text-align:right"
        ],
        'total_discount' => [
            "label"      => TranslatorHelper::trans('total', $translator).' '.TranslatorHelper::trans('discount', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ],
        'total' => [
            "label"      => TranslatorHelper::trans('total', $translator) . TranslatorHelper::trans('profit', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ],
        'total_tax' => [
            "label"      => TranslatorHelper::trans('total', $translator).' '.TranslatorHelper::trans('tax', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ]
    ]
]);

