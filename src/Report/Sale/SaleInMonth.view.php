<?php

use Kematjaya\ReportBundle\Helper\TranslatorHelper;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */

$translator = $this->params['translator'];
\koolreport\widgets\koolphp\Table::create([
    "dataStore" =>  $this->dataStore('sale_in_month'),
    "showFooter" => true,
    "columns" => [
        'created_at' => [
            "label" => TranslatorHelper::trans('created_at', $translator), "type" => "text",
            "formatValue" => function($value){
                return (new \DateTime($value))->format('d M Y');
            }
        ],
        'code' => [
            "label" => TranslatorHelper::trans('code', $translator), "type" => "text",
        ],
        'sub_total' => [
            "label"      => TranslatorHelper::trans('sub_total', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ],
        'tax' => [
            "label"      => TranslatorHelper::trans('tax', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ],
        'total' => [
            "label"      => TranslatorHelper::trans('total', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ],
        'discount' => [
            "label"      => TranslatorHelper::trans('discount', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ],
        'payment' => [
            "label"      => TranslatorHelper::trans('payment', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ],
        'payment_status' => [
            "label" => TranslatorHelper::trans('payment_status', $translator), "type" => "text",
            "formatValue" => function($value) use ($translator) {
            
                return ($value == 't') ? sprintf('<span class="label label-success">%s</span>', TranslatorHelper::trans('yes', $translator)) : sprintf('<span class="label label-default">%s</span>', TranslatorHelper::trans('no', $translator));
            }
        ]
    ]
]);

