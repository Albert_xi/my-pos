<?php

namespace App\Report\Transaction;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface TransactionInterface 
{
    const TYPE_IN = 'in';
    const TYPE_OUT = 'out';
    
    public function getCreatedAt(): ?\DateTimeInterface;
        
    public function getCode(): ?string;
    
    public function getDescription():?string;
    
    public function getDebt(): ?float;
    
    public function getCredit(): ?float;
    
    public function getTypeTransaction():string;
    
    public function getCheckedAt(): ?\DateTimeInterface;

    public function setCheckedAt(?\DateTimeInterface $checked_at): self;
    
}
