<?php

namespace App\Report\Transaction;

use App\Report\Transaction\TransactionInterface;
use App\Report\Transaction\TransactionRepoInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ReportBuilder 
{
    private $transactionRepo;
    
    public function __construct(TransactionRepoInterface $transactionRepo) 
    {
        $this->transactionRepo = $transactionRepo;
    }
    
    public function build(\DateTimeInterface $date): Collection
    {
        $results = new ArrayCollection($this->transactionRepo->findTransactionByDate($date));
        
        $iterator = $results->getIterator();
        $iterator->uasort(function (TransactionInterface $first, TransactionInterface $second) 
        {
            return $first->getCreatedAt() > $second->getCreatedAt() ? 1 : -1;
        });
        
        return new ArrayCollection(iterator_to_array($iterator));
    }
    
    public function getTransactionsUnChecked(\DateTimeInterface $date): Collection
    {
        $results = new ArrayCollection($this->transactionRepo->findTransactionUncheckedByDate($date));
        
        $iterator = $results->getIterator();
        $iterator->uasort(function (TransactionInterface $first, TransactionInterface $second) 
        {
            return $first->getCreatedAt() > $second->getCreatedAt() ? 1 : -1;
        });
        
        return new ArrayCollection(iterator_to_array($iterator));
    }
    
    public function getTransactionsChecked(\DateTimeInterface $checkedAt):Collection
    {
        $results = new ArrayCollection($this->transactionRepo->findTransactionsCheckedByDate($checkedAt));
        
        $iterator = $results->getIterator();
        $iterator->uasort(function (TransactionInterface $first, TransactionInterface $second) 
        {
            return $first->getCreatedAt() > $second->getCreatedAt() ? 1 : -1;
        });
        
        return new ArrayCollection(iterator_to_array($iterator));
    }
}
