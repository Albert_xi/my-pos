<?php

namespace App\Report\Transaction;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface TransactionRepoInterface 
{
    public function findTransactionByDate(\DateTimeInterface $date):array;
    
    public function findTransactionsCheckedByDate(\DateTimeInterface $date):array;
    
    public function findTransactionUncheckedByDate(\DateTimeInterface $date):array;
}
