<?php

namespace App\Report\Purchase;

use App\Report\BaseReport;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseReport extends BaseReport
{
    public function setup()
    {
        $saleInMonth = sprintf("
            select s.purchase_at, s.code, sp.name, s.total from purchase s
            join supplier sp on s.supplier_id = sp.id
            where extract(month from s.purchase_at) = %s
            and extract(year from s.purchase_at) = %s
            and s.is_locked = true", date('m'), date('Y'));
        
        $this->src('postgresql')
                ->query($saleInMonth)
                ->pipe($this->dataStore('purchase_in_month'));
    }
}
