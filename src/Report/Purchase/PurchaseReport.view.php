<?php

use Kematjaya\ReportBundle\Helper\TranslatorHelper;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */

$translator = $this->params['translator'];
\koolreport\widgets\koolphp\Table::create([
    "dataStore" =>  $this->dataStore('purchase_in_month'),
    "showFooter" => true,
    "columns" => [
        'purchase_at' => [
            "label" => TranslatorHelper::trans('purchase_at', $translator), "type" => "text",
            "formatValue" => function($value){
                return (new \DateTime($value))->format('d M Y');
            }
        ],
        'code' => [
            "label" => TranslatorHelper::trans('code', $translator), "type" => "text",
        ],
        'name' => [
            "label" => TranslatorHelper::trans('name', $translator), "type" => "text",
        ],
        'total' => [
            "label"      => TranslatorHelper::trans('total', $translator), 
            "type"       =>"number", 
            "prefix"     =>"Rp. ",
            "footerText" =>"<b>@value</b>", "cssStyle"  => "text-align:right"
        ]
    ]
]);

