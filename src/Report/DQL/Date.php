<?php

namespace App\Report\DQL;

use App\Report\BaseReport;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class Date 
{
    public static function toChar(string $field, string $key, string $alias, BaseReport $report)
    {
        switch($report->getConnectionDriver())
        {
            case BaseReport::DRIVER_MYSQL:
                return sprintf("DATE_FORMAT(%s,'%s') as %s", $field, $key, $alias);
                break;
            case BaseReport::DRIVER_PGSQL:
                return sprintf("to_char(%s,'%s') as %s", $field, $key, $alias);
                break;
            default:
                throw new \Exception(sprintf('no supported %s', $report->getConnectionDriver()));
                break;
        }
    }
}
