<?php

namespace App\Tests\Controller;

use App\Tests\Controller\Base\BaseControllerTest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class HomePageControllerTest extends BaseControllerTest
{
    public function testIndex()
    {
        $this->client->request(Request::METHOD_GET, parent::BASE_URL);
        $this->assertTrue($this->client->getResponse()->isRedirect('/kmj-user/login'));
        parent::login();
        $this->client->request(Request::METHOD_GET, parent::BASE_URL);
        $isLogin = !empty($this->client->getCookieJar()->all());
        $this->assertTrue($this->client->getResponse()->isRedirect('/admin/home/'));
    }
    
    public function testDokumentasi()
    {
        $url = $this->router->generate('app_documentation');
        
        $fileDoc = static::$container->getParameter('kernel.project_dir') . '/manual book.pdf';
        $fileTarget = static::$container->getParameter('kernel.project_dir') . '/manual_book.pdf';
        $fileSystem = new Filesystem();
        if($fileSystem->exists($fileDoc))
        {
            $fileSystem->rename($fileDoc, $fileTarget);
            $this->client->request(Request::METHOD_GET, $url);
            $content = json_decode($this->client->getResponse()->getContent(), true);
            $this->assertTrue($content['error']);
        }   
        
        if($fileSystem->exists($fileTarget))
        {
            $fileSystem->rename($fileTarget, $fileDoc);
            $this->isPdfResponse($url);
        }
    }
    
    public function testViewImages()
    {
        $fileName = 'pp.png';
        $url = $this->router->generate('app_view_images', ['fileName' => $fileName]);
        
        $file = static::$container->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR . 'docs' . DIRECTORY_SEPARATOR . $fileName;
        $fileDoc = static::$container->getParameter('kernel.upload_dir') . DIRECTORY_SEPARATOR . 'blog' .DIRECTORY_SEPARATOR . $fileName;
        $fileSystem = new Filesystem();
        
        if($fileSystem->exists($fileDoc))
        {
            $fileSystem->remove($fileDoc);
        }
        
        if(!$fileSystem->exists($fileDoc))
        {
            $this->client->request(Request::METHOD_GET, $url);
            $content = json_decode($this->client->getResponse()->getContent(), true);
            $this->assertTrue($content['error']);
            $fileSystem->copy($file, $fileDoc, true);
        }   
        
        if($fileSystem->exists($fileDoc))
        {
            $this->isImagesResponse($url);
        }
    }
    
    public function testAccessDenied()
    {
        $this->login();
        $crawler = $this->client->request(Request::METHOD_GET, $this->router->generate('app_access_denied'));
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $this->client->getResponse()->getStatusCode());
    }
}
