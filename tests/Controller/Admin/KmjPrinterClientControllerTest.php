<?php

namespace App\Tests\Controller\Admin;

use App\Tests\Controller\Base\BaseCRUDController;
use \App\Entity\KmjPrinterClient;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjPrinterClientControllerTest extends BaseCRUDController
{
    const COMPUTER_NAME = 'TEST';
    
    protected function buildObject() :KmjPrinterClient
    {
        return (new KmjPrinterClient())
                ->setComputerName(self::COMPUTER_NAME)
                ->setIsDefault(true)
                ->setKmjUser($this->user)
                ->setPrinterName("test");
    }
    
    public function testIndex()
    {
        $url = $this->router->generate('kmj_printer_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $this->login();
        $data = $this->buildObject();
        $post = [
            'computer_name' => $data->getComputerName(),
            'is_default' => $data->getIsDefault(),
            'printer_name' => $data->getPrinterName(),
            'kmj_user' => (string) $data->getKmjUser()->getId()
        ];
        
        $url    = $this->router->generate('kmj_printer_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->doctrine->getRepository(KmjPrinterClient::class)->findOneBy(['computer_name' => self::COMPUTER_NAME]);
        $post = [
            'computer_name' => $data->getComputerName(),
            'is_default' => $data->getIsDefault(),
            'printer_name' => $data->getPrinterName(),
            'kmj_user' => (string) $data->getKmjUser()->getId()
        ];
        
        $url    = $this->router->generate('kmj_printer_edit', ['id' => $data->getId()]);
        parent::ajaxForm($url, $post);
    }

    public function testShow()
    {
        $data = $this->doctrine->getRepository(KmjPrinterClient::class)->findOneBy(['computer_name' => self::COMPUTER_NAME]);
        
        $url    = $this->router->generate('kmj_printer_detail', ['id' => $data->getId()]);
        parent::doShow($url);
    }
    
    public function testDelete()
    {
        $data = $this->doctrine->getRepository(KmjPrinterClient::class)->findOneBy(['computer_name' => self::COMPUTER_NAME]);
        
        $url        = $this->router->generate('kmj_printer_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('kmj_printer_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
