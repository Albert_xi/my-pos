<?php

namespace App\Tests\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ReportControllerTest extends \App\Tests\Controller\Base\BaseControllerTest
{
    public function testProfit()
    {
        $this->login();
        $url = $this->router->generate('report_profit_index');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testPrintProfit()
    {
        $this->login();
        $url = $this->router->generate('report_profit_print');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        
        $this->isExcelResponse();
    }
    
    public function testProfitByStore()
    {
        $this->login();
        $url = $this->router->generate('report_profit_store_index');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testPrintProfitByStore()
    {
        $this->login();
        $url = $this->router->generate('report_profit_store_print');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        
        $this->isExcelResponse();
    }
    
    public function testItemStock()
    {
        $this->login();
        $url = $this->router->generate('report_item_stock_index');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testItemStockPrint()
    {
        $this->login();
        $url = $this->router->generate('report_stock_print');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        
        $this->isExcelResponse();
    }
    
    public function testUnpaidTransaction()
    {
        $this->login();
        $url = $this->router->generate('report_unpaid_transaction_index');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testUnpaidTransactionPrint()
    {
        $this->login();
        $url = $this->router->generate('report_unpaid_transaction_print');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        
        $this->isExcelResponse();
    }
    
    public function testSale()
    {
        $this->login();
        $url = $this->router->generate('report_sale_index');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testSalePrint()
    {
        $this->login();
        $url = $this->router->generate('report_sale_print');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        
        $this->isExcelResponse();
    }
    
    public function testPurchase()
    {
        $this->login();
        $url = $this->router->generate('report_purchase_index');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testPurchasePrint()
    {
        $this->login();
        $url = $this->router->generate('report_purchase_print');
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        
        $this->isExcelResponse();
    }
}
