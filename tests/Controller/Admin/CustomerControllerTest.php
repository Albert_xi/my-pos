<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Customer;
use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class CustomerControllerTest extends BaseCRUDController 
{
    //put your code here
    protected function buildObject() 
    {
        $customer = (new Customer())
                ->setCode(rand())
                ->setName("test")
                ->setAddress("alamat")
                ->setMail("test@gmail.com")
                ->setPhone("875765878");
        
        return $customer;
    }

    public function testIndex()
    {
        $url = $this->router->generate('customer_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'address' => $data->getAddress(),
            'phone' => $data->getPhone(),
            'mail' => $data->getMail()
        ];
        
        $url    = $this->router->generate('customer_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'address' => $data->getAddress(),
            'phone' => $data->getPhone(),
            'mail' => $data->getMail()
        ];

        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('customer_edit', ['id' => $data->getId()]);
        
        parent::ajaxForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->buildObject();
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('customer_detail', ['id' => $data->getId()]);
        parent::doShow($url);
    }
    
    public function testDelete()
    {
        $data       = $this->buildObject();
        $manager    = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url        = $this->router->generate('customer_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('customer_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
