<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Item;
use App\Entity\ItemCategory;
use App\Tests\Controller\Base\BaseCRUDController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemControllerTest extends BaseCRUDController 
{
    protected function buildObject() 
    {
        $category = $this->doctrine->getRepository(ItemCategory::class)->findOneBy(['is_active' => true]);
        $item = (new Item())
            ->setCode(rand())
            ->setName("test")
            ->setCategory($category)
            ->setLastStock(0)
            ->setPrincipalPrice(1000)
            ->setLastPrice(1200)
            ->setUseBarcode(false)
            ;
        return $item;
    }
    
    public function testIndex()
    {
        $url = $this->router->generate('item_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode().rand(),
            'name' => $data->getName(),
            'use_barcode' => $data->getUseBarcode(),
            'last_stock' => $data->getLastStock(),
            'harga_pokok' => $data->getPrincipalPrice(),
            'last_price' => $data->getLastPrice(),
            'category' => (string)$data->getCategory()->getId(),
        ];
        
        $url    = $this->router->generate('item_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'use_barcode' => $data->getUseBarcode(),
            'last_stock' => $data->getLastStock(),
            'harga_pokok' => $data->getPrincipalPrice(),
            'last_price' => $data->getLastPrice(),
            'category' => (string)$data->getCategory()->getId(),
        ];
        
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('item_edit', ['id' => $data->getId()]);
        parent::ajaxForm($url, $post);
    }

    public function testGetData()
    {
        $data = $this->buildObject();
        
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('item_data');
        $this->login();
        parent::request(\Symfony\Component\HttpFoundation\Request::METHOD_GET, $url .'?q='.$data->getId());
    }
    
    public function testShow()
    {
        $data = $this->buildObject();
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('item_detail', ['id' => $data->getId()]);
        parent::doShow($url);
    }
    
    public function testDelete()
    {
        $data       = $this->buildObject();
        $manager    = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url        = $this->router->generate('item_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('item_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
    
    public function testExport()
    {
        $this->login();
        
        $url = $this->router->generate('item_export');
        $this->client->request(\Symfony\Component\HttpFoundation\Request::METHOD_GET, $url);
        $this->isExcelResponse();
    }
    
    public function testTemplateImport()
    {
        $this->login();
        
        $url = $this->router->generate('item_template_import');
        $this->client->request(\Symfony\Component\HttpFoundation\Request::METHOD_GET, $url);
        $this->isExcelResponse();
    }
    
    public function testImport()
    {
        $post = [];
        $fileName = 'import-item.xlsx';
        $projectDir = static::$kernel->getProjectDir().DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'docs'.DIRECTORY_SEPARATOR;
        $fileNameDest = md5(rand()).'.xlsx';
        if(copy($projectDir . $fileName, $projectDir . $fileNameDest))
        {
            $file = new UploadedFile($projectDir . $fileNameDest, $fileNameDest);

            $url    = $this->router->generate('item_import');
            parent::ajaxForm($url, [], ['attachment' => $file]);
        }   
    }
}
