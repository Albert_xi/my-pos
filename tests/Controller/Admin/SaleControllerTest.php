<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Sale;
use App\Entity\SaleItem;
use App\Entity\Customer;
use App\Entity\KmjPrinterClient;
use Symfony\Component\HttpFoundation\Request;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleControllerTest extends \App\Tests\Controller\Base\BaseCRUDController 
{
    protected function buildObject() :Sale
    {
        $customer = $this->doctrine->getRepository(Customer::class)->findOneBy(['code' => random_int(1, 20)]);
        $store = $this->doctrine->getRepository(\App\Entity\Store::class)->findOneBy(['is_default' => true]);
        return (new Sale())
                ->setStore($store)
                ->setCode('test')
                ->setCreatedAt(new \DateTime())
                ->setCreatedBy('test')
                ->setCustomer($customer)
                ->setDiscount(0)
                ->setIsLocked(false)
                ->setPayment(0)
                ->setPaymentChange(0)
                ->setSubTotal(0)
                ->setTax(0)
                ->setTotal(0);
    }

    public function buildSaleItem()
    {
        $sale = $this->saveObject($this->buildObject());
        $item = $this->doctrine->getRepository(\App\Entity\Item::class)->findOneBy(['code' => random_int(1, 10)]);
        
        $saleItem = (new SaleItem())
                ->setDiscount(0)
                ->setItem($item)
                ->setPrincipalPrice($item->getHargaPokok())
                ->setQuantity(5)
                ->setSale($sale)
                ->setSalePrice($item->getLastPrice())
                ->setSubTotal(5 * $item->getLastPrice())
                ->setTax(0)
                ->setTotal(5 * $item->getLastPrice());
        
        return $saleItem;
    }
    
    public function saveObject(Sale $data):Sale
    {
        $manager = $this->doctrine->getManager();
        
        foreach($data->getSaleItems() as $detail)
        {
            $manager->persist($detail);
        }
        $manager->persist($data);
        
        $manager->flush();
        
        return $data;
    }
    
    public function testIndex()
    {
        $url = $this->router->generate('sale_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'store' => (string)$data->getStore()->getId(),
            'code' => $data->getCode(),
            'customer' => (string)$data->getCustomer()->getId(),
            'created_by' => $data->getCreatedBy(),
            'created_at' => $data->getCreatedAt()->format('Y-m-d'),
            'sub_total' => $data->getSubTotal(),
            'tax' => $data->getTax(),
            'discount' => $data->getDiscount(),
            'payment' => $data->getPayment(),
            'payment_change' => $data->getPaymentChange(),
            'total' => $data->getTotal(),
            'is_locked' => false
        ];
        
        $url    = $this->router->generate('sale_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->saveObject($this->buildObject());
        
        $post = [
            'store' => (string)$data->getStore()->getId(),
            'code' => $data->getCode(),
            'customer' => (string)$data->getCustomer()->getId(),
            'created_by' => $data->getCreatedBy(),
            'created_at' => $data->getCreatedAt()->format('Y-m-d'),
            'sub_total' => $data->getSubTotal(),
            'tax' => $data->getTax(),
            'discount' => $data->getDiscount(),
            'payment' => $data->getPayment(),
            'payment_change' => $data->getPaymentChange(),
            'total' => $data->getTotal(),
            'is_locked' => !$data->getSaleItems()->isEmpty()
        ];
        
        $url    = $this->router->generate('sale_detail', ['id' => $data->getId()]);
        parent::doEdit($url, $post, null, true);
    }
    
    public function testPrintout()
    {
        $data = $this->saveObject($this->buildObject());
        
        $url    = $this->router->generate('sale_printout', ['id' => $data->getId()]);
        
        $this->login();
        
        $this->client->request(Request::METHOD_GET, $url);
        
        $kmjPrinter = $this->doctrine->getManager()->getRepository(KmjPrinterClient::class)->findOneBy(['kmj_user' => $this->user]);
        if(!$kmjPrinter)
        {
            $this->assertTrue($this->client->getResponse()->isRedirect($this->router->generate('kmj_printer_index')));
        
            $kmjPrinter = (new KmjPrinterClient())
                    ->setComputerName('TEST')
                    ->setIsDefault(true)
                    ->setKmjUser($this->user)
                    ->setPrinterName('POS-58');

            $manager = $this->doctrine->getManager();
            $manager->persist($kmjPrinter);
            $manager->flush();
        }
        
            
        $post = [
            'computer_name' => (string) $kmjPrinter->getId()
        ];
        
        parent::doEdit($url, $post, null, true);
    }
    
    public function testDelete()
    {
        $data = $this->saveObject($this->buildObject());
        
        $url        = $this->router->generate('sale_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('sale_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
    
    public function testAddItem()
    {
        $data = $this->buildSaleItem();
        
        $post = [
            'item' => (string) $data->getItem()->getId(),
            'quantity' => $data->getQuantity(),
            'sale_price' => $data->getSalePrice(),
            'sub_total' => $data->getSubTotal(), 
            'tax' => $data->getTax(),
            'total' => $data->getTotal(),
            'sale' => (string) $data->getSale()->getId()
        ];
        
        $url    = $this->router->generate('sale_item_create', ['id' => $data->getSale()->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testEditItem()
    {
        $data = $this->buildSaleItem();
        
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $post = [
            'item' => (string) $data->getItem()->getId(),
            'quantity' => $data->getQuantity(),
            'sale_price' => $data->getSalePrice(),
            'sub_total' => $data->getSubTotal(), 
            'tax' => $data->getTax(),
            'total' => $data->getTotal(),
            'sale' => (string) $data->getSale()->getId()
        ];
        
        $url    = $this->router->generate('sale_item_edit', ['id' => $data->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testDeleteItem()
    {
        $data = $this->buildSaleItem();
        
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $sale   = $data->getSale();
        
        $url        = $this->router->generate('sale_item_delete', ['id' => $data->getId()]);
        parent::doDelete($url, $data, null, true);
    }
}
