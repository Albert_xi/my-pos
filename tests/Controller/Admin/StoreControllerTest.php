<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Store;
use App\Tests\Controller\Base\BaseCRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DomCrawler\Form;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StoreControllerTest extends BaseCRUDController 
{
    protected function buildObject()
    {
        $store = $this->doctrine->getRepository(Store::class)->findOneBy(['code' => '00000234']);
        if (!$store) {
            $store = (new Store())
                ->setCode('00000234')
                ->setAddress('jl xxx no 17')
                ->setName('test')
                ->setIsDefault(false);
        }
        
        return $store;
    }
    
    protected function buildObjectDefault()
    {
        $store = $this->doctrine->getRepository(Store::class)->findOneBy(['is_default' => true]);
        if (!$store) {
            $store = (new Store())
                ->setCode('00000234')
                ->setAddress('jl xxx no 17')
                ->setName('test default')
                ->setIsDefault(true);
        }
        
        return $store;
    }
    
    public function testIndex()
    {
        $url = $this->router->generate('store_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'address' => $data->getAddress()
        ];
        
        if ($data->getId()) {
            $this->assertTrue(true);
            return;
        }
        
        $url    = $this->router->generate('store_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'address' => $data->getAddress()
        ];
        
        $url    = $this->router->generate('store_edit', ['id' => $data->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testCreateDefault()
    {
        $data = $this->buildObjectDefault();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'address' => $data->getAddress(),
            'is_default' => true
        ];
        
        if ($data->getId()) {
            $this->assertTrue(true);
            return;
        }
        
        $url    = $this->router->generate('store_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEditDefault()
    {
        $data = $this->buildObjectDefault();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'address' => $data->getAddress(),
            'is_default' => true
        ];
        
        $url    = $this->router->generate('store_edit', ['id' => $data->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->buildObject();
        $url    = $this->router->generate('store_detail', ['id' => $data->getId()]);
        parent::doShow($url);
    }
    
    public function testDelete()
    {
        $data       = $this->buildObject();
        $url        = $this->router->generate('store_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('store_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
