<?php

namespace App\Tests\Controller\Admin;

use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class DashboardControllerTest extends BaseCRUDController 
{
    //put your code here
    protected function buildObject() 
    {
        
    }

    public function testIndex()
    {
        $url = $this->router->generate('admin_index');
        parent::doIndex($url);
    }
}
