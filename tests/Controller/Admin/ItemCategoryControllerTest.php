<?php

namespace App\Tests\Controller\Admin;

use App\Entity\ItemCategory;
use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemCategoryControllerTest extends BaseCRUDController 
{
    //put your code here
    protected function buildObject() 
    {
        return (new ItemCategory())
                ->setCode(rand())
                ->setName("test")->setIsActive(true);
        
    }

    public function testIndex()
    {
        $url = $this->router->generate('item_category_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'is_active' => $data->getIsActive()
        ];
        
        $url    = $this->router->generate('item_category_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName(),
            'is_active' => $data->getIsActive()
        ];

        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('item_category_edit', ['id' => $data->getId()]);
        
        parent::ajaxForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->buildObject();
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('item_category_detail', ['id' => $data->getId()]);
        parent::doShow($url);
    }
    
    public function testDelete()
    {
        $data       = $this->buildObject();
        $manager    = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url        = $this->router->generate('item_category_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('item_category_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
