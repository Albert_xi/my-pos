<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Packaging;
use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PackagingControllerTest extends BaseCRUDController 
{
    protected function buildObject():Packaging
    {
        $store = (new Packaging())
                ->setCode(rand())
                ->setName("test");
        
        return $store;
    }
    
    public function testIndex()
    {
        $url = $this->router->generate('packaging_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName()
        ];
        
        $url    = $this->router->generate('packaging_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'name' => $data->getName()
        ];

        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('packaging_edit', ['id' => $data->getId()]);
        
        parent::ajaxForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->buildObject();
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('packaging_detail', ['id' => $data->getId()]);
        parent::doShow($url);
    }
    
    public function testDelete()
    {
        $data       = $this->buildObject();
        $manager    = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url        = $this->router->generate('packaging_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('packaging_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
