<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Purchase;
use App\Entity\Supplier;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PurchaseControllerTest extends \App\Tests\Controller\Base\BaseCRUDController 
{
    protected function buildObject() :Purchase
    {
        $supplier = $this->doctrine->getRepository(Supplier::class)->findOneBy(['name' => 'test']);
        $data = (new Purchase())
                ->setCode('test')
                ->setPurchaseAt(new \DateTime())
                ->setSupplier($supplier)
                ->setTotal(0)
                ->setIsLocked(false);
        
        return $data;
    }

    
    public function buildPurchaseItem()
    {
        $purchase = $this->saveObject($this->buildObject());
        $item = $this->doctrine->getRepository(\App\Entity\Item::class)->findOneBy(['code' => random_int(1, 10)]);
        $packaging = $item->getItemPackages()->filter(function(\App\Entity\ItemPackage $itemPackage){
            return $itemPackage->isSmallestUnit();
        })->first();
        
        $purchaseItem = (new \App\Entity\PurchaseDetail())
                ->setItem($item)
                ->setPackaging($packaging->getPackaging())
                ->setPrice($item->getHargaPokok())
                ->setPurchase($purchase)
                ->setQuantity(100)
                ->setTax(0)
                ->setTotal((100 * $item->getHargaPokok()));
        return $purchaseItem;
    }
    
    public function saveObject(Purchase $data):Purchase
    {
        $manager = $this->doctrine->getManager();
        
        foreach($data->getPurchaseDetails() as $detail)
        {
            $manager->persist($detail);
        }
        $manager->persist($data);
        
        $manager->flush();
        
        return $data;
    }
    
    public function testIndex()
    {
        $url = $this->router->generate('purchase_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'code' => $data->getCode(),
            'supplier' => (string)$data->getSupplier()->getId(),
            'purchase_at' => $data->getPurchaseAt()->format('Y-m-d'),
            'total' => $data->getTotal(),
            'description' => $data->getDescription(),
            'is_locked' => false
        ];
        
        $url    = $this->router->generate('purchase_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->saveObject($this->buildObject());
        
        $post = [
            'code' => $data->getCode(),
            'supplier' => (string)$data->getSupplier()->getId(),
            'purchase_at' => $data->getPurchaseAt()->format('Y-m-d'),
            'total' => $data->getTotal(),
            'description' => $data->getDescription(),
            'is_locked' => !$data->getPurchaseDetails()->isEmpty()
        ];
        
        $url    = $this->router->generate('purchase_detail', ['id' => $data->getId()]);
        parent::doEdit($url, $post, null, true);
    }
    
    public function testDelete()
    {
        $data = $this->saveObject($this->buildObject());
        
        $url        = $this->router->generate('purchase_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('purchase_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
    
    public function testAddItem()
    {
        $data = $this->buildPurchaseItem();
        
        $post = [
            'purchase' => (string) $data->getPurchase()->getId(),
            'item' => (string) $data->getItem()->getId(),
            'packaging' => (string) $data->getPackaging()->getId(),
            'quantity' => $data->getQuantity(),
            'price' => $data->getPrice(),
            'tax' => $data->getTax(), 
            'total' => $data->getTotal()
        ];
        
        $url    = $this->router->generate('purchase_detail_create', ['id' => $data->getPurchase()->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testEditItem()
    {
        $data = $this->buildPurchaseItem();
        
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $post = [
            'purchase' => (string) $data->getPurchase()->getId(),
            'item' => (string) $data->getItem()->getId(),
            'packaging' => (string) $data->getPackaging()->getId(),
            'quantity' => $data->getQuantity(),
            'price' => $data->getPrice(),
            'tax' => $data->getTax(), 
            'total' => $data->getTotal()
        ];
        
        $url    = $this->router->generate('purchase_detail_edit', ['id' => $data->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testDeleteItem()
    {
        $data = $this->buildPurchaseItem();
        
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $purchase   = $data->getPurchase();
        
        $url        = $this->router->generate('purchase_detail_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('purchase_detail', ['id' => $purchase->getId()]);
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
