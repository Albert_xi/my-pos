<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Item;
use App\Entity\Packaging;
use App\Entity\ItemPackage;
use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPackageControllerTest extends BaseCRUDController
{
    //put your code here
    protected function buildObject() :ItemPackage
    {
        $packaging = $this->doctrine->getRepository(Packaging::class)->findOneBy(['code' => 'Pack']);
        $item = $this->doctrine->getRepository(Item::class)->findOneBy(['code' => 1]);
        $data = $this->doctrine->getRepository(ItemPackage::class)->findOneBy(['packaging' => $packaging, 'item' => $item]);
        if (!$data) {
            $data = (new ItemPackage())
                ->setQuantity(10)
                ->setIsSmallestUnit(false)
                ->setPackaging($packaging)
                ->setItem($item);
        }
        
        return $data;
    }

    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'packaging' => (string)$data->getPackaging()->getId(),
            'quantity' => $data->getQuantity(),
            'principal_price' => 1000,
            'sale_price' => 1000,
            'item' => (string)$data->getItem()->getId()
        ];
        
        $url    = $this->router->generate('item_package_create', ['id' => $data->getItem()->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'packaging' => (string)$data->getPackaging()->getId(),
            'quantity' => $data->getQuantity(),
            'principal_price' => 1000,
            'sale_price' => 1000,
            'item' => (string)$data->getItem()->getId()
        ];

        $url    = $this->router->generate('item_package_edit', ['id' => $data->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testDelete()
    {
        $data       = $this->buildObject();
        if (!$data->getId()) {
            $manager    = $this->doctrine->getManager();
            $manager->persist($data);
            $manager->flush();
        }
        
        $url        = $this->router->generate('item_package_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('store_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
