<?php

namespace App\Tests\Controller\Admin;

use App\Entity\KmjUser;
use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjUserControllerTest extends BaseCRUDController
{
    public function testIndex()
    {
        $url = $this->router->generate('kmj_user_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'username' => $data->getUsername().rand(),
            'name' => $data->getName(),
            'roles' => $data->getRoles(),
            'is_active' => $data->getIsActive(),
            'password' => 'admin123'
        ];
        
        $url = $this->router->generate('kmj_user_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'username' => $data->getUsername(),
            'name' => $data->getName() . rand(),
            'roles' => $data->getRoles(),
            'is_active' => $data->getIsActive()
        ];
        
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url = $this->router->generate('kmj_user_edit', ["id" => $data->getId()]);
        parent::ajaxForm($url, $post);
    }
    
    public function testDelete()
    {
        $data = $this->buildObject();
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url = $this->router->generate('kmj_user_delete', ["id" => $data->getId()]);
        parent::doDelete($url, $data);
    }
    
    public function testDetail()
    {
        $data = $this->buildObject();
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url = $this->router->generate('kmj_user_detail', ["id" => $data->getId()]);
        parent::doShow($url, $data);
    }

    protected function buildObject() 
    {
        $rand = rand();
        $ptiUser = (new KmjUser())
                ->setName('test-user-'.$rand)
                ->setUsername('test-username' . $rand)
                ->setIsActive(true)
                ->setRoles([KmjUser::ROLE_OPERATOR]);

        $encoderFactory = self::$container->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($ptiUser);
        $password = $encoder->encodePassword( 'admin123', $ptiUser->getUsername());

        $ptiUser->setPassword($password);
        
        return $ptiUser;
    }

}
