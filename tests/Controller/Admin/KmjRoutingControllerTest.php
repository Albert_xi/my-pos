<?php

namespace App\Tests\Controller\Admin;

use App\Entity\KmjUser;
use App\Tests\Controller\Base\BaseCRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class KmjRoutingControllerTest extends BaseCRUDController
{
    //put your code here
    protected function buildObject() 
    {
        
    }

    public function testIndex()
    {
        $url = $this->router->generate('kmj_routing_index');
        parent::doIndex($url);
    }
    
    public function testSetRoutingRole()
    {
        $role = KmjUser::ROLE_ADMINISTRATOR;
        $url = $this->router->generate('kmj_routing_roles', ['role'=> $role]);
        $post = [];
        
        $this->login();
        
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $token = self::$container->get('security.csrf.token_manager')->getToken('kmj_routing_roles_' .$role);
        $routingService = self::$container->get(\App\Library\RoutingManagement\RoutingService::class);
        
        $credentials = [];
        foreach($routingService->getIndexRoute(false, 'admin') as $key => $routes)
        {
            foreach($routes as $path => $route)
            {
                foreach($routingService->getOtherPath($path) as $k => $router)
                {
                    $credentials[$key][$path][$k] = true;
                }
                
            }
        }
        
        $post = [
            'credential' => $credentials
        ];
        
        $this->client->request(Request::METHOD_POST, $url, $post);
        $output = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(json_last_error() == JSON_ERROR_NONE);
        $this->assertTrue($output['process']);
        $this->assertTrue(strlen($output['errors'])>0);
           
        $post['_token'] = (string) $token;
        $this->client->request(Request::METHOD_POST, $url, $post);
        $output = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertTrue(json_last_error() == JSON_ERROR_NONE);
        $this->assertTrue($output['process'] and $output['status']);
        
    }
    
}
