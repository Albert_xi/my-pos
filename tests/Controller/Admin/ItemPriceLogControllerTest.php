<?php

namespace App\Tests\Controller\Admin;

use App\Entity\ItemPriceLog;
use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPriceLogControllerTest extends BaseCRUDController
{
    //put your code here
    protected function buildObject() :?ItemPriceLog
    {
        $itemPriceLog = $this->doctrine->getRepository(ItemPriceLog::class)->findOneBy(['status' => ItemPriceLog::STATUS_NEW]);
        if(!$itemPriceLog)
        {
            $itemPriceLog = $this->doctrine->getRepository(ItemPriceLog::class)->findOneBy(['status' => ItemPriceLog::STATUS_APPROVED]);
        }
        return $itemPriceLog;
    }
    
    public function testApproval()
    {
        $data = $this->buildObject();
        if($data)
        {
            $salePriceNew = (0.5 * $data->getPrincipalPrice()) + $data->getPrincipalPrice();
            $post = [
                'created_at' => $data->getCreatedAt()->format('Y-m-d H:i:s'),
                'principal_price_old' => ($data->getPrincipalPriceOld()===null) ? 0 : $data->getPrincipalPriceOld(),
                'principal_price' => $data->getPrincipalPrice(),
                'sale_price_old' => ($data->getSalePriceOld() === null) ? 0 : $data->getSalePriceOld(),
                'sale_price' => $salePriceNew,
                'status' => ItemPriceLog::STATUS_APPROVED,
                'item' => (string)$data->getItem()->getId(),
                'inserted_by' => $data->getInsertedBy()
            ];
            
            $url    = $this->router->generate('item_price_log_approval', ['id' => $data->getId()]);
            parent::ajaxForm($url, $post);
        }
    }

}
