<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Supplier;
use App\Tests\Controller\Base\BaseCRUDController;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SupplierControllerTest extends BaseCRUDController 
{
    protected function buildObject()
    {
        $supplier = (new Supplier())
                ->setName("test")
                ->setAddress("address")
                ->setPhone('986000000000');
        
        return $supplier;
    }
    
    public function testIndex()
    {
        $url = $this->router->generate('supplier_index');
        parent::doIndex($url);
    }
    
    public function testCreate()
    {
        $data = $this->buildObject();
        $post = [
            'name' => $data->getName(),
            'address' => $data->getAddress(),
            'phone' => $data->getPhone(),
        ];
        
        $url    = $this->router->generate('supplier_create');
        parent::ajaxForm($url, $post);
    }
    
    public function testEdit()
    {
        $data = $this->buildObject();
        $post = [
            'name' => $data->getName(),
            'address' => $data->getAddress(),
            'phone' => $data->getPhone(),
        ];

        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('supplier_edit', ['id' => $data->getId()]);
        
        parent::ajaxForm($url, $post);
    }
    
    public function testShow()
    {
        $data = $this->buildObject();
        $manager = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url    = $this->router->generate('supplier_detail', ['id' => $data->getId()]);
        parent::doShow($url);
    }
    
    public function testDelete()
    {
        $data       = $this->buildObject();
        $manager    = $this->doctrine->getManager();
        $manager->persist($data);
        $manager->flush();
        
        $url        = $this->router->generate('supplier_delete', ['id' => $data->getId()]);
        $urlReferer = $this->router->generate('supplier_index');
        
        parent::doDelete($url, $data, $urlReferer);
    }
}
