<?php

namespace App\Tests\Controller\Base;

use App\Tests\Controller\Base\BaseControllerTest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\DomCrawler\Field\InputFormField;
use Symfony\Component\DomCrawler\Field\ChoiceFormField;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class BaseCRUDController extends BaseControllerTest 
{
    abstract protected function buildObject();
    
    public function doIndex(string $url)
    {
        $this->login();
        $this->request(Request::METHOD_GET, $url);
        
        // filter
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $element = $crawler->filter('button[type=submit]');
        if($element->count() > 0)
        {
            $form = $element->form();
            if($form->getName())
            {
                $post = [];
                foreach($form->get($form->getName()) as $field => $value)
                {
                    switch(true)
                    {
                        case $value instanceof InputFormField:
                            $post[$field] = ($value->getValue()) ? $value->getValue(): 'test';
                            break;
                        case is_array($value):
                            if(strpos($field, '_at'))
                            {
                                $now = (new \DateTime())->modify('-1day');
                                foreach($value as $type => $fields)
                                {
                                    $post[$field][$type] = $now->modify(sprintf('+1day'))->format("Y-m-d H:i:s");
                                }
                            }else
                            {
                                $i = 1;
                                foreach($value as $type => $fields)
                                {
                                    $post[$field][$type] = $i;
                                    $i += 10;
                                }
                            }
                            
                                
                            break;
                        case $value instanceof ChoiceFormField:
                            foreach($value->availableOptionValues() as $optVal)
                            {
                                if(strlen($optVal)>0)
                                {
                                    $post[$field] = $optVal;
                                    break;
                                }
                            }
                            break;
                        default:
                            
                            break;
                    }
                }
                
                $postData = [
                    'submit' => true,
                    $form->getName() => $post
                ];
                
                $this->request(Request::METHOD_POST, $url, $postData);
                $this->assertTrue($this->client->getResponse()->isSuccessful());
                $this->request(Request::METHOD_GET, $url.'?_reset=1');
                $this->assertTrue($this->client->getResponse()->isSuccessful());
            }   
        }
    }
    
    public function doCreate(string $url, $post = array(), $urlReferer = null, $isRedirectPage = false)
    {
        $this->login();
        
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $form = $crawler->filter('button[type=submit]')->form();
        $this->assertInstanceOf(Form::class, $form);
        
        if(!empty($post))
        {
            $server = [];
            if($urlReferer)
            {
                $server['HTTP_REFERER'] = $urlReferer;
            }
            $post['_token'] =  $form->get($form->getName())['_token']->getValue();
            $post = [
                'submit' => true,
                $form->getName() => $post
            ];
            
            $this->client->request(Request::METHOD_POST, $url, $post, [], $server);
            if(isset($server['HTTP_REFERER']))
            {
                $this->assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
            }elseif($isRedirectPage)
            {
                $this->assertStringContainsString('>Redirecting to', $this->client->getResponse()->getContent());
            } else
            {
                $this->assertTrue($this->client->getResponse()->isSuccessful());
            }
            
        }
    }
    
    public function doEdit(string $url, $post = array(), $urlReferer = null, $isRedirectPage = false)
    {
        $this->login();
        
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $form = $crawler->filter('button[type=submit]')->form();
        $this->assertInstanceOf(Form::class, $form);
        
        if(!empty($post))
        {
            $server = [];
            if($urlReferer)
            {
                $server['HTTP_REFERER'] = $urlReferer;
            }
            $post['_token'] =  'test';
            $post = [
                'submit' => true,
                $form->getName() => $post
            ];
            
            $this->client->request(Request::METHOD_POST, $url, $post, [], $server);
            
            $this->assertStringContainsString('CSRF', $this->client->getResponse()->getContent());
            
            $post[$form->getName()]['_token'] = $form->get($form->getName())['_token']->getValue();
            $this->client->request(Request::METHOD_POST, $url, $post, [], $server);
            
            if(isset($server['HTTP_REFERER']))
            {
                $this->assertSame(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());
            }elseif($isRedirectPage)
            {
                $this->assertTrue($this->client->getResponse()->isRedirection());
            } else
            {
                $this->assertTrue($this->client->getResponse()->isSuccessful());
            }
        }
    }
    
    protected function ajaxForm($url, $post = array(), $files = [])
    {
        $this->login();
        
        $crawler = $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $form = $crawler->filter('button[type=submit]')->form();
        $this->assertInstanceOf(Form::class, $form);
        
        $postData = [];
        if($form->getName())
        {
            // test csrf
            $post['_token'] =  'test';
            $postData = [
                'submit' => true,
                $form->getName() => $post
            ];
            
            $duplicatedFile = [];
            if(!empty($files))
            {
                foreach($files as $k => $file)
                {
                    $fileNameDest = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
                    
                    if(copy($file->getPath() . DIRECTORY_SEPARATOR . $file->getClientOriginalName(), $file->getPath() . DIRECTORY_SEPARATOR . $fileNameDest))
                    {
                        $duplicatedFile[$k] = new UploadedFile($file->getPath() . DIRECTORY_SEPARATOR . $fileNameDest, $fileNameDest);
                    }
                }
                
                $files = [
                    $form->getName() => $files
                ];
            }
        }
        
        if(!empty($postData) or !empty($files))
        {
            $this->client->request(Request::METHOD_POST, $url, $postData, $files);
            $this->assertTrue($this->client->getResponse()->isSuccessful());
            $output = json_decode($this->client->getResponse()->getContent(), true);
            $this->assertTrue(json_last_error() == JSON_ERROR_NONE);
            $this->assertTrue($output['process']);
            $this->assertTrue(strlen($output['errors'])>0);
            
            // test true
            $postData[$form->getName()]['_token'] =  $form->get($form->getName())['_token']->getValue();
            
            if(!empty($files) and !empty($duplicatedFile))
            {
                $files = [
                    $form->getName() => $duplicatedFile
                ];
            }
            
            $postData[$form->getName()]['_token'] =  $form->get($form->getName())['_token']->getValue();
            $this->client->request(Request::METHOD_POST, $url, $postData, $files);
            $this->assertTrue($this->client->getResponse()->isSuccessful());
            $output = json_decode($this->client->getResponse()->getContent(), true);
            $this->assertTrue(json_last_error() == JSON_ERROR_NONE);
            $this->assertTrue($output['process'] and $output['status']);
        }
    }
    
    public function doDelete(string $url, $object, string $urlReferer = null, $isAjax = false)
    {
        $this->login();
        
        $token = self::$container->get('security.csrf.token_manager')->getToken('delete' . $object->getId());
        $server = [];
        if($urlReferer)
        {
            $server['HTTP_REFERER'] = $urlReferer;
        }
        
        // test csrf
        $crawler = $this->client->request(Request::METHOD_DELETE, $url, [], [], $server);
        
        $crawler = $this->client->request(Request::METHOD_DELETE, $url, [
            '_token' => (string) $token,
        ], [], $server);
        
        if($urlReferer)
        {
            $this->assertTrue($this->client->getResponse()->isRedirection());
        }
        if($isAjax)
        {
            $result = json_decode($this->client->getResponse()->getContent(), true);
            $this->assertTrue($result['status']);
        }
        
        $object = $this->doctrine->getRepository(get_class($object))->find($object->getId());
        $this->assertNull($object);
    }
    
    public function doShow(string $url)
    {
        $this->login();
        $this->request(Request::METHOD_GET, $url);
    }
}
