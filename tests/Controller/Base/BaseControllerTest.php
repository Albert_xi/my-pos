<?php

namespace App\Tests\Controller\Base;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class BaseControllerTest extends WebTestCase 
{
    /**
     * @var Registry
     */
    protected $doctrine;
    /**
     * @var KernelBrowser
     */
    protected $client;
    
    /**
     *
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var \App\Entity\KmjUser
     */
    protected $user;
    
    const BASE_URL = '';
    
    protected function login()
    {
        $session = self::$container->get('session');
        
        $user = $this->doctrine->getRepository(\App\Entity\KmjUser::class)->findOneBy(['username' => "root"]);
        $firewallName = 'main';
        $firewallContext = 'main';
        if($user)
        {
            $token = new \Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken($user, $firewallName, $user->getRoles());
            $session->set('_security_'.$firewallContext, serialize($token));
            $session->save();

            $cookie = new \Symfony\Component\BrowserKit\Cookie($session->getName(), $session->getId());
            $this->client->getCookieJar()->set($cookie);
            $this->user = $user;
        }
            
    }
    
    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();

        $this->doctrine = static::$container->get('doctrine');
        
        $this->router = static::$container->get('router');
    }
    
    protected function request(string $method, string $url, $params = [])
    {
        $crawler = $this->client->request($method, $url, $params);
        switch($method)
        {
            case Request::METHOD_POST:
                break;
            default:
                $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
                $this->assertTrue($this->client->getResponse()->isSuccessful());
                break;
        }       
    }
    
    protected function getExcelFormats():array
    {
        return [
            'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ];
    }
    
    protected function isExcelResponse()
    {
        $excels = $this->getExcelFormats();//dump($this->client->getResponse()->getContent());//dump($this->client->getResponse()->headers->get('content-type'));exit;
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertTrue(in_array($this->client->getResponse()->headers->get('content-type'), $excels));
    }
    
    protected function isPdfResponse(string $url)
    {
        $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertEquals('application/pdf', $this->client->getResponse()->headers->get('content-type'));
    }
    
    protected function isImagesResponse(string $url)
    {
        $images = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
        $this->client->request(Request::METHOD_GET, $url);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertTrue(in_array($this->client->getResponse()->headers->get('content-type'), $images));
    }
}
