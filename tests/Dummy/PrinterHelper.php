<?php

namespace App\Tests\Dummy;

use Kematjaya\EscposBundle\Formatter\FormatterInterface;
use Kematjaya\EscposBundle\Client\ClientInterface;
use Kematjaya\EscposBundle\Helper\PrinterHelperInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PrinterHelper implements PrinterHelperInterface
{
    
    public function print(FormatterInterface $formatter, ClientInterface $client):void
    {
        echo "printer OK";
    }
}
