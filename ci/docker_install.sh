#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install apt-utils -yqq
apt-get install git curl wget -yqq
apt-get install zlib1g-dev -yqq
apt-get install libzip-dev -yqq
apt-get install unzip -yqq
apt-get install libpq-dev -yqq
apt-get install libfreetype6-dev -yqq
apt-get install libjpeg62-turbo-dev -yqq
apt-get install libpng-dev -yqq
apt-get install libicu-dev g++ -yqq

# Here you can install any other extension that you need
docker-php-ext-configure gd --with-freetype --with-jpeg
docker-php-ext-configure intl
docker-php-ext-install -j$(nproc) gd

docker-php-ext-install zip
docker-php-ext-install pdo_pgsql
docker-php-ext-install pgsql
docker-php-ext-install intl

pecl install xdebug-$XDEBUG_VERSION
docker-php-ext-enable xdebug
