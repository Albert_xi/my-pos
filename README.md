instalation
1. clone project using git
   - git clone https://gitlab.com/kematjaya/my-pos.git
2. update composer
   - composer update 
3. set database connection
   - create file .env.local in root directory
   - change or add line :
     - postgresql
       DATABASE_URL=postgresql://postgres:password@host:port/db-name?serverVersion=11&charset=utf8
     - mysql
       DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
4. create database 
   - running script on console:
     php bin/console doctrine:database:create
   - update database table
     php bin/console doctrine:schema:update --force
5. insert data dummy
   - php bin/console doctrine:fixtures:load --no-interaction
6. running on localhost
   - using web server like xampp, wampp or etc,
     - open http://host/folder_app/public/index.php
   - using symfony console:
     - running script on console:
       symfony serve -d --port=8000
     - following instruction given
7. user and password access:
   - username: root
   - password: admin123
8. coverage test
    - php bin/phpunit --prepend build/xdebug-filter.php --coverage-html var/coverage-report


# trobelshoot
- printer access denied
    1. klik kanan folder C:\Windows\System32\spool\PRINTERS
    2. tab permission, everyone set full control